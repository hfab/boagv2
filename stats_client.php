<?php
require_once("fonctions_db.php");
require_once("fonctions.php");
$link = connection(MYDATABASE);
$requete = "
SELECT 
DISTINCT dossiers.id AS id_dossier,
clients.civilite,
dossiers.adultes, 
dossiers.enfants,
dossiers.bebes
FROM
dossiers,
clients
WHERE
dossiers.id_client = clients.id
ORDER BY
dossiers.id
";
$result = mysql_query($requete) or die(mysql_error());
//echo mysql_num_rows($result)."<br/><br/>";

$nb_h = 0;
$nb_f = 0;
$nb_s = 0;
$nb_total = 0;

$total_passagers = 0;

unset($min);
unset($max);
unset($moyen);

while($row = mysql_fetch_assoc($result)){
	//Pourcentage h/f/ste
	$nb_total++;
	if($row['civilite'] == 'Mr')
	{
		$nb_h++;
	}
	else if ($row['civilite'] == 'Mme' || $row['civilite'] == 'Melle')
	{
		$nb_f++;
	}
	else
	{
		$nb_s++;
	}
	
	//Calcul panier moyen min et max
	$total = Calcul_total_dossier($row['id_dossier']);
	if ($min > $total || empty($min))
	{
		$min = $total;
	}
	if ($max < $total || empty($max))
	{
		$max = $total;
	}
	$panier_total += $total;

	//Nb de passager max/min/moyen
	$total_passagers += $row['adultes'];
	$total_passagers += $row['enfants'];
	$total_passagers += $row['bebes'];
	
	
//	echo $total.";".$row['id_dossier'].";".$row['civilite'].";".$row['date_naiss'].";".$row['ville'].";".$row['dest_ville'].";".ucfirst(strtolower($row['lpays_fr'])).";".$row['to'].";".$row['adultes'].";".$row['enfants'].";".$row['bebes'].";".($row['adultes']+$row['enfants']+$row['bebes'])."<br/>";
}
echo "<h1>R�partition h/f/ste et %</h1>";
echo "Hommes : ".$nb_h." (".(100*$nb_h)/$nb_total." %)<br/>";
echo "Femmes : ".$nb_f." (".(100*$nb_f)/$nb_total." %)<br/>";
echo "Soci�t� : ".$nb_s." (".(100*$nb_s)/$nb_total." %)<br/>";
echo "Total : ".$nb_total."<br/>";

echo "<h1>Nb moyen de passagers par dossiers</h1>";
echo $total_passagers/$nb_total."<br/>";


echo "<h1>Panier min/max/moyen</h1>";
echo "Panier mini : ".$min." �<br/>";
echo "Panier maxi : ".$max." �<br/>";
echo "Panier moyen : ".($panier_total/$nb_total)." �<br/>";
//echo $panier_total."<br/>";

$requete = "
SELECT 
DISTINCT dossiers.id AS id_dossier,
COUNT(ville_depart) AS nb_ville_depart,
ville_depart
FROM
dossiers,
clients
WHERE
dossiers.id_client = clients.id
GROUP BY ville_depart
ORDER BY
nb_ville_depart DESC
";
$result = mysql_query($requete) or die(mysql_error());
echo "<h1>Villes de d�part</h1>";
while($row = mysql_fetch_assoc($result)){
	echo $row['ville_depart']." - " .$row['nb_ville_depart']."<br/>";
}

$requete = "
SELECT 
DISTINCT dossiers.id AS id_dossier,
COUNT(ville) AS nb_ville,
ville,
code_pays.lpays_fr
FROM
dossiers,
clients,
code_pays
WHERE
dossiers.id_client = clients.id
AND
dossiers.dest_pays = code_pays.cpays
GROUP BY ville
ORDER BY
nb_ville DESC
"; 
$result = mysql_query($requete) or die(mysql_error());
echo "<h1>Ville du client</h1>";
while($row = mysql_fetch_assoc($result)){
	echo $row['ville']." - " .$row['nb_ville']."<br/>";
}

$requete = "
SELECT 
passagers.date_naiss
FROM
passagers
WHERE
passagers.date_naiss != '0000-00-00'
AND
passagers.date_naiss != '1930-01-01'
"; 
$result = mysql_query($requete) or die(mysql_error());
echo "<h1>Age moyen</h1>";
while($row = mysql_fetch_assoc($result)){
	$ages[] = age($row['date_naiss']);
}
//print_r($ages);
//echo count($ages)."<br/>";
//echo array_sum($ages)."<br/>";
echo "Age moyen : ".(array_sum($ages) / count($ages))." ans<br/>";
echo  "Age minimum : ".min($ages)." ans<br/>";
echo  "Age maximum : ".max($ages)." ans<br/>";




 function age($naiss)  {
  list($annee, $mois, $jour) = split('[-.]', $naiss);
  $today['mois'] = date('n');
  $today['jour'] = date('j');
  $today['annee'] = date('Y');
  $annees = $today['annee'] - $annee;
  if ($today['mois'] <= $mois) {
    if ($mois == $today['mois']) {
      if ($jour > $today['jour'])
        $annees--;
      }
    else
      $annees--;
    }
  return $annees;
 }
  

?>
