<?php
session_start();

if (!isset($_SESSION["id_vendeur"])) {
		   header("Location:index.php");
		   exit();
		}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Recherche / Modification des clients de AgenceDeVoyage&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<link href="client.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">
<?php
require_once("fonctions_db.php");
require_once("fonctions.php");

$id = $_POST["id"];
$id_client = $_POST["id_client"];

$flag = $_POST["flag"];
$nom = $_POST["nom"];
$prenom = $_POST["prenom"];
$mail = $_POST["mail"];
$adresse1 = $_POST["adresse1"];
$adresse2 = $_POST["adresse2"];
$tel1 = $_POST["tel1"];
$tel2 = $_POST["tel2"];
$tel3 = $_POST["tel3"];
$fax = $_POST["fax"];
$ville = $_POST["ville"];
$cp = $_POST["cp"];
$pays = $_POST["pays"];
$sexe = $_POST["sexe"];
$jour_naissance = $_POST["jour_naissance"];
$mois_naissance = $_POST["mois_naissance"];
$annee_naissance = $_POST["annee_naissance"];
$connu_par = $_POST["connu_par"];
$avis_cli = $_POST["avis_cli"];
$commentaire = $_POST["commentaire"];

$recherche = $_POST["recherche"];
$champ = $_POST["champ"];
$partie = $_POST["partie"];
$civilite = $_POST["civilite"];



if ($flag == "modification") {
	Modifier_client($id, $nom, $prenom, $mail, $adresse1, $adresse2, $tel1, $tel2, $tel3, $fax, $ville, $cp, $pays, $sexe, $jour_naissance, $mois_naissance, $annee_naissance, $connu_par, $avis_cli, $commentaire, $civilite);
}
else if ($flag == "modifier") {
	$link = connection(MYDATABASE);
  $requete = "SELECT * FROM clients WHERE id = '$id_client';";
  //echo $requete;
  $result = mysql_query($requete,$link) or die(mysql_error() ." : ". $requete);
  while($row = mysql_fetch_row($result))
	{
	  //Affectation des données pour le traitement
	  $id=$row[0];
	  $nom = stripslashes($row[2]);
		$prenom = stripslashes($row[3]);
		$mail = stripslashes($row[4]);
		$sexe = stripslashes($row[5]);
		$date_naissance = stripslashes($row[6]);
		$jour_naissance = substr($date_naissance,0,2);
		$mois_naissance = substr($date_naissance,2,2);
		$annee_naissance = substr($date_naissance,4,4);
		$adresse1 = stripslashes($row[7]);
		$adresse2 = stripslashes($row[8]);
		$ville = stripslashes($row[9]);
		$cp = stripslashes($row[10]);
		$pays = $row[11];
		$tel1 = $row[12];
		$tel2 = $row[13];
		$tel3 = $row[14];
		$fax = $row[15];
		$connu_par = $row[16];
		$avis_cli = stripslashes($row[17]);
		$commentaire = stripslashes($row[18]);
		$civilite = $row[1];
	}
  deconnection($link);
  
	Form_new_client($nom, $prenom, $mail, $adresse1, $adresse2, $tel1, $tel2, $tel3, $fax, $ville, $cp, $pays, $sexe, $jour_naissance, $mois_naissance, $annee_naissance, $connu_par, $avis_cli, $commentaire, $civilite,"modification",$id);
	
	
	
	
}
else if ($flag == "modification") {
	Update_client($id,$nom, $prenom, $mail, $adresse1, $adresse2, $tel1, $tel2, $tel3, $fax, $ville, $cp, $pays, $sexe, $jour_naissance, $mois_naissance, $annee_naissance, $connu_par, $avis_cli, $commentaire, $civilite);
}
else if ($flag == "chercher") {
	Recherche_client($recherche, $champ, $partie);
}
else {
	Afficher_form_recherche();
}


?>
</td>
</tr>
</table>

</body></html>

