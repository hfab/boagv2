<?php
session_start();
if (!isset($_SESSION["id_vendeur"])) {
		   header("Location:index.php");
		   exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Statistiques Monagence&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="client.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
require_once("fonctions_db.php");
require_once("fonctions.php");
require_once("comptabilite/mailto.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">
<?php

$debut_sem = $_POST["debut_sem"];

$envoi_mail = $_POST["envoi_mail"];
$html = $_POST["html"];
$num_sem = $_POST["num_sem"];
$id_dossiers_envoi[] = $_POST["id_dossiers_envoi"];


if($envoi_mail=="ok")
{
	foreach($id_dossiers_envoi as $id)
	{
		if(!(is_array($id)))
		{
			$link = connection(MYDATABASE);
			$requete = "UPDATE passagers SET envoi_assur='1' WHERE id_dossier='".$id."'";
			$result=mysql_query($requete) or die(mysql_error());
			echo "Dossier N°".$id." marqué comme envoyé<br>";
		}
	}
}


//Préparation du mail et envoi
/*
if($envoi_mail=="ok")
{

	$boundary = "-----=".md5(uniqid(rand()));
	
	$header = "MIME-Version: 1.0\r\n";
	
	$header .= "Content-Type: multipart/mixed; boundary=\"$boundary\"\r\n";
	$header .= "\r\n";
	
	
	
	$msg = "Votre lecteur de mail ne semble pas compatible. Merci de contacter AgenceDeVoyage.com au 01 77 69 02 52\r\n";
	
	
	$msg .= "--$boundary\r\n";
	$msg .= "Content-Type: text/html; charset=\"iso-8859-1\"\r\n";
	
	$msg .= "Content-Transfer-Encoding:8bit\r\n";
	$msg .= "\r\n";

	$msg .= "<style>*{font-family:Verdana}</style><center><img src=\"http://www.agencedevoyage.com/img/header/logo.jpg\"></center><br/><br/>";
	$msg .= "\r\n";
	$msg .= "Bonjour <br/><br/>\r\n";
	$msg .= "\r\n";
	$msg .= "Veuillez trouver ci-joint les assurances pour la semaine N° $num_sem<br/>\r\n";
	$msg .= "<br/>\r\n";
	$msg .= "Cordialement, <br/>\r\n";
	$msg .= "L'équipe de AgenceDeVoyage.com <br/>\r\n";
	$msg .= "Tél : 01 77 69 02 52<br/>\r\n";
	$msg .= "Fax : 01.46.21.02.88 <br/>\r\n";

	$msg .= "\r\n\r\n";
	//Texte
	$msg .= "--$boundary\r\n";

	$msg .= "Content-Type: text/html; name=\"Liste_assurances_semaine".$num_sem.".html\"\r\n";
	$msg .= "Content-Transfer-Encoding: 8bit\r\n";
	
	$msg .= "Content-Disposition: attachment; filename=\"Liste_assurances_semaine".$num_sem.".html\"\r\n";
	
	$msg .= "\r\n";
	
	$msg .= stripslashes($html). "\r\n";
	$msg .= "\r\n\r\n";
	
	
	$expediteur="info@agencedevoyage.com";
	$reponse = $expediteur;
	$destinataire="info@agencedevoyage.com";
	//Hack de test a commenter pour envoyer au client
	//$destinataire="webmaster@medisite.fr";
	//&& mail($expediteur, "COPIE - Assurances pour la semaine N° $num_sem - $destinataire", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header)
	if (mail($destinataire, "Assurances pour la semaine N° $num_sem", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header) && mail($expediteur, "COPIE - Assurances pour la semaine N° $num_sem - $destinataire", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header))
	{
		echo "Le mail a bien été envoyé à <b>$destinataire</b> de la part de <b>$expediteur</b> <br/>";	
	}
	
}
*/


echo ("<form action='".$_SERVER['PHP_SELF']."' name='semaine' id='semaine' method='post'>\n\r");
echo("<fieldset><legend>Choix de la semaine</legend>");

echo ("<label for='semaine'>Semaine</label>\n\r");

echo("<select name='debut_sem'>");

$oneweek = 60*60*24*7;
$premier_jour = mktime(0,0,0,date("m"),date("d")-date("w")+1-420,date("Y"));
$d = $premier_jour;

$jour_encours = mktime(0,0,0,date("m"),date("d")-date("w")+1,date("Y"));


for ($i = 0; $i < 70; $i++)
{
    echo "<option value='".date("Y-m-d",$d)."'";
    if(date("Y-m-d",$d) == date("Y-m-d",$jour_encours))
    {
    	echo(" selected='selected' ");
    }
    echo ">";
    echo date("d-m-Y",$d)." au ".date("d-m-Y", $d + $oneweek)."</option>\n\r";
    
    $d += $oneweek;
}

echo("</select>");
echo ("<div class='center'><input type='submit' class='submit' name='Choisir' id='Choisir' value='Choisir'><br/></div>\n\r");
echo("</fieldset></form>");


//echo("Semaine du $debut_sem<br/>");

if(!(isset($debut_sem))) {$debut_sem = date("Y-m-d",$jour_encours);}

$debut_sem_jour = substr($debut_sem, 8,2);
$debut_sem_mois = substr($debut_sem, 5,2);
$debut_sem_annee = substr($debut_sem, 0,4);
$num_semaine = date("W",mktime(0,0,0,$debut_sem_mois,$debut_sem_jour+1,$debut_sem_annee));
$prime=0;
$html="";
$html .= "N° de contrat : 78 373 773 / 78 454 318 25  Remplacé à partir du 28/02/2013 par le contrat n°78 679 122/M<br/>";
$html .= "Semaine ".$num_semaine." du : ";
$html .= $debut_sem_jour."-";
$html .= $debut_sem_mois."-";
$html .= $debut_sem_annee."<br/><br/>";

if(empty($num_sem) || $num_sem == null)
	$num_sem = $num_semaine ;

$file_csv_ce   = 'comptabilite/journalAssurance'.$num_sem.'.csv';

$output_ce_csv .= "";


//$destinataires =array('julien@comptoirdesdeals.com');
$destinataires =array('yveschaponic@gmail.com', 'info@agencedevoyage.com');

if($envoi_mail=="ok")
{
	if ( mailToCompta($destinataires, "assurance ",$file_csv_ce,$file_csv_hce) )
	{
		echo "Le mail a bien été envoyé à <b>$destinataire</b> de la part de <b>$expediteur</b> <br/>";
	}
}




$html .= "<center><table border='1'><tr style='font-weight:bold;'>";
$html .= "<td>ID Dossier</td>";
$html .= "<td>Nom</td>";
$html .= "<td>Prénom</td>";
$html .= "<td>Formule</td>";
$html .= "<td>Date d'achat</td>";
$html .= "<td>Prime</td>";
$html .= "<td>Destination</td>";
$html .= "<td>Date départ</td>";
$html .= "<td>Date retour</td>";
$html .= "<td>Total dossier</td>";
$html .= "</tr>";

$output_ce_csv .= "ID Dossier;";
$output_ce_csv .= "Nom;";
$output_ce_csv .= "Prénom;";
$output_ce_csv .= "Formule;";
$output_ce_csv .= "Date d'achat;";
$output_ce_csv .= "Prime;";
$output_ce_csv .= "Destination;";
$output_ce_csv .= "Date départ;";
$output_ce_csv .= "Date retour;";
$output_ce_csv .= "Total dossier;\n";

for ($i=0;$i<7;$i++)
{
	$jour_suivant = date("Y-m-d", mktime(0,0,0,$debut_sem_mois,$debut_sem_jour,$debut_sem_annee) + (60*60*24*$i));

	$jour = substr($jour_suivant, 8,2);
	$mois = substr($jour_suivant, 5,2);
	$annee = substr($jour_suivant, 2,2);
	
	/*
	echo $jour." - ";
	echo $mois." - ";
	echo $annee."<br/>";
	*/

	//unset($id_dossiers_envoi);
	$link = connection(MYDATABASE);
	$requete = "SELECT dossiers.id, code_pays.lpays_fr, dossiers.date_deb, dossiers.date_fin FROM dossiers, code_pays WHERE dossiers.dest_pays=code_pays.cpays  AND (dossiers.etat='Confirmé' OR dossiers.etat='Annulé') AND SUBSTRING(dossiers.date_confirmation,9,2)='".$jour."' AND SUBSTRING(dossiers.date_confirmation,6,2)='".$mois."' AND SUBSTRING(dossiers.date_confirmation,3,2)='".$annee."'"; 
	//echo $requete."<br/>";
	$result=mysql_query($requete) or die(mysql_error());
	while($row = mysql_fetch_row($result))
	{
		$requete2 = "SELECT passagers.nom, passagers.prenom, passagers.assur, assurances_new.prime, passagers.envoi_assur, passagers.id FROM passagers, assurances_new WHERE id_dossier='".$row[0]."' AND passagers.assur=assurances_new.nom AND passagers.assur != 'Aucune'";
		$result2=mysql_query($requete2) or die(mysql_error());
		while($row2 = mysql_fetch_row($result2))
		{
			$id_dossiers_envoi[]=$row[0];
			if ($row2[4]=="1"){$bgcolor="#CCCCCC";} else {$bgcolor="#FFFFFF";};
			$html .= "<tr bgcolor='".$bgcolor."'>";
			$html .= "<td>$row[0]</td>";
			$html .= "<td>$row2[0]</td>";
			$html .= "<td>$row2[1]</td>";
			$html .= "<td>$row2[2]</td>";
			$html .= "<td>$jour / $mois / $annee</td>";
			$html .= "<td nowrap='nowrap'>$row2[3] </td>";
			$html .= "<td>$row[1]</td>";
			$html .= "<td>$row[2]</td>";
			$html .= "<td>$row[3]</td>";
			$html .= "<td>".Calcul_total_dossier($row[0])." </td>";
			$html .= "</tr>";
			$prime += $row2[3];
			
			$output_ce_csv .= "$row[0];";
			$output_ce_csv .= "$row2[0];";
			$output_ce_csv .= "$row2[1];";
			$output_ce_csv .= "$row2[2];";
			$output_ce_csv .= "$jour / $mois / $annee;";
			$output_ce_csv .= "$row2[3];";
			$output_ce_csv .= "$row[1];";
			$output_ce_csv .= "$row[2];";
			$output_ce_csv .= "$row[3];";
			$output_ce_csv .= Calcul_total_dossier($row[0]).";\n";
		}
	}

}
$html .= "</table><br/></center>";
$html .= "Total des primes : $prime ";

echo $html;

	file_put_contents($file_csv_ce, $output_ce_csv);

	$id_dossiers_envoi = array_unique($id_dossiers_envoi);
	echo ("<form action='".$_SERVER['PHP_SELF']."' name='envoi' id='envoi' method='post'>\n\r");
	echo("<fieldset><legend>Envoi mail à yveschaponic@gmail.com</legend>");
	echo("<textarea name='html' onFocus='this.blur()' cols='1' rows='1' style='visibility:hidden;'>$html</textarea>");
	echo("<input type='hidden' name='envoi_mail' value='ok'>\n\r");
	echo("<input type='hidden' name='num_sem' value='".$num_semaine."'>\n\r");
	foreach($id_dossiers_envoi as $id)
	{
		if($id != "")
		{
			echo("<input type='hidden' name='id_dossiers_envoi[]' value='".$id."'>\n\r");
		}
	}
	echo ("<div class='center'><input type='submit' class='submit' name='Envoi du mail' id='Envoi du mail' value='Envoi du mail'><br/></div>\n\r");
	echo("</fieldset></form>");


?>
</td>
</tr>
</table>

</body></html>

