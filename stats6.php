<?php
session_start();
if (!session_is_registered("id_vendeur")) {
		   header("Location:index.php");
		   exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Statistiques Monagence&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<link href="client.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
require_once("fonctions_db.php");
require_once("fonctions.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px" valign='top'>
<?php

$jour = $_POST["jour"];
$mois = $_POST["mois"];
$annee = $_POST["annee"];

$jour_courant=date("d");
$mois_courant=date("m");
$annee_courante=date("y");

if (!empty($jour)) {$jour_courant = $jour;};
if (!empty($mois)) {$mois_courant = $mois;};
if (!empty($annee)) {$annee_courante = $annee;};

if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};

echo "<h1>Visualisation des stats pour le jour de : ".$jour_courant." / ".$mois_courant." / ".$annee_courante."</h1>";
echo "<p><form method='post' action='stats6.php?PHPSESSID=".session_id()."'><fieldset><legend>Choix de la p�riode</legend>";
	
	echo ("<label for='jour'>Voir une autre p�riode : </label>\n\r");
	echo ("<select name='jour' id='mois'>\n\r");
	for ($i=1;$i<=31;$i++) {
		if (strlen($i) < 2){$jour="0".$i;} else {$jour=$i;};
		echo ("<option value='".$jour."'");
		if ($jour == $jour_courant) {echo("selected='selected'");};
		echo (">".$jour."</option>\n\r");
	}
	echo ("</select>\n\r");
	
	echo ("<select name='mois' id='mois'>\n\r");
	for ($i=1;$i<=12;$i++) {
		if (strlen($i) < 2){$mois="0".$i;} else {$mois=$i;};
		echo ("<option value='".$mois."'");
		if ($mois == $mois_courant) {echo("selected='selected'");};
		echo (">".$mois."</option>\n\r");
	}
	echo ("</select>\n\r");
	
	echo ("<select name='annee' id='annee'>\n\r");
	for ($i=2005;$i<=$annee_courante;$i++) {
		echo ("<option value='".$i."'");
		if ($i == $annee_courante) {echo("selected='selected'");};
		echo (">".$i."</option>\n\r");
	}
	echo ("</select><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");
	echo "<input type='submit' name='Voir' id='Voir' value='Voir'></fieldset></form></p>";

	$annee_courante = substr($annee_courante,2,4);


	$total_dossiers = 0;
	$compteur = 0;
	$link = connection(MYDATABASE);
	$requete = "SELECT id FROM `dossiers` WHERE SUBSTRING(`date_confirm`,1,2)=".$jour_courant." AND SUBSTRING(`date_confirm`,4,2)=".$mois_courant." AND SUBSTRING(`date_confirm`,7,2)=".$annee_courante."  GROUP BY dossiers.id;";
	//echo $requete;
	$result=mysql_query($requete) or die(mysql_error());
  while($row = mysql_fetch_row($result))
	{
		$total_courant = Calcul_total_dossier($row[0]);
		$total_dossiers += $total_courant;
		$compteur++;
	}
	
	echo "<hr><h1>Chiffre d'affaire total du jour :</h1> ".number_format($total_dossiers,2,',',' ')." �";
	if($compteur > 0)
	{
		echo "<hr><h1>Panier moyen ($compteur dossiers confirm�s) :</h1> ".number_format($total_dossiers / $compteur,2,',',' ')." �";
	}
	
?>
</td>
</tr>
</table>

</body></html>

