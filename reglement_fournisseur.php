<?php
session_start();
if (!isset($_SESSION["id_vendeur"])) {
		   header("Location:index.php");
		   exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Réglement des fournisseurs de Monagence&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="client.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
require_once("fonctions_db.php");
require_once("fonctions.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">
<?php

$temps_debut = microtime(true);

$jour = $_POST["jour"];
$mois = $_POST["mois"];
$annee = $_POST["annee"];

$jour_facturation = $_POST["jour_facturation"];
$mois_facturation = $_POST["mois_facturation"];
$annee_facturation = $_POST["annee_facturation"];

$jour_payement = $_POST["jour_payement"];
$mois_payement = $_POST["mois_payement"];
$annee_payement = $_POST["annee_payement"];

$date_facturation = $annee_facturation."-".$mois_facturation."-".$jour_facturation;

$date_payement = $annee_payement."-".$mois_payement."-".$jour_payement;

$banque = $_POST['banque'];

$montant_achat = $_POST["montant_achat"];
$facture_achat = $_POST["facture_achat"];
$id_dossier = $_POST["id_dossier"];
$mode_achat = $_POST["mode_achat"];
$num_cheque = $_POST["num_cheque"];

$jour_courant=date("d");
$mois_courant=date("m");
$annee_courante=date("y");

if (!empty($jour)) {$jour_courant = $jour;};
if (!empty($mois)) {$mois_courant = $mois;};
if (!empty($annee)) {$annee_courante = $annee;};

if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};

$envoi_mail = $_POST["envoi_mail"];
$html = $_POST["html"];


if($envoi_mail=="ok")
{

	$boundary = "-----=".md5(uniqid(rand()));
	
	$header = "MIME-Version: 1.0\r\n";
	
	$header .= "Content-Type: multipart/mixed; boundary=\"$boundary\"\r\n";
	$header .= "\r\n";
	
	
	
	$msg = "Votre lecteur de mail ne semble pas compatible. Merci de contacter AgenceDeVoyage.com au 01 77 69 02 52\r\n";
	
	
	$msg .= "--$boundary\r\n";
	$msg .= "Content-Type: text/html; charset=\"iso-8859-1\"\r\n";
	
	$msg .= "Content-Transfer-Encoding:8bit\r\n";
	$msg .= "\r\n";

	$msg .= "<style>*{font-family:Verdana}</style><center><img src='http://www.agencedevoyage.com/img/header/logo.jpg'></center><br/><br/>";
	$msg .= "\r\n";
	$msg .= "Bonjour <br/><br/>\r\n";
	$msg .= "\r\n";
	$msg .= "Veuillez trouver ci-joint les réglements fournisseurs de AgenceDeVoyage.com<br/>\r\n";
	$msg .= "<br/>\r\n";
	$msg .= "Cordialement, <br/>\r\n";
	$msg .= "L'équipe de monagence.com <br/>\r\n";
	$msg .= "Tél : 01 77 69 02 52<br/>\r\n";
	$msg .= "Fax : 01 77 69 02 14 <br/>\r\n";

	$msg .= "\r\n\r\n";
	//Texte
	$msg .= "--$boundary\r\n";

	$msg .= "Content-Type: text/html; name=\"reglement_fournisseurs.html\"\r\n";
	$msg .= "Content-Transfer-Encoding: 8bit\r\n";
	
	$msg .= "Content-Disposition: attachment; filename=\"reglement_fournisseurs.html\"\r\n";
	
	$msg .= "\r\n";
	
	$msg .= stripslashes($html). "\r\n";
	$msg .= "\r\n\r\n";
	
	
	$expediteur="info@agencedevoyage.com";
	$reponse = $expediteur;
	$destinataire="yves@agencedevoyage.com";
	//Hack de test a commenter pour envoyer au client
	//$destinataire="webmaster@medisite.fr";
	// && mail($expediteur, "COPIE - Réglement fournisseur - Monagence.com - $destinataire", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header)
	if (mail($destinataire, "Réglements fournisseurs - AgenceDeVoyage.com", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header) 
	&& mail($expediteur, "COPIE - Réglement fournisseur - AgenceDeVoyage.com - $destinataire", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header)
	)
	{
		echo "Le mail a bien été envoyé à <b>$destinataire</b> de la part de <b>$expediteur</b> <br/>";	
	}
	
}



$html_output="";
$html_output_mail="";
$html_output_mail .= "<h1>Réglements fournisseurs</h1>";



	echo "<h1>Réglements fournisseurs pour le : ".$jour_courant." / ".$mois_courant." / ".$annee_courante."</h1>";
	echo "<p><form method='post' action='".$_SERVER['PHP_SELF']."?PHPSESSID=".session_id()."'><fieldset><legend>Choix de la période</legend>";
	echo ("<label for='jour'>Voir une autre période : </label>\n\r");
	echo ("<select name='jour' id='jour'>\n\r");
	for ($i=1;$i<=31;$i++) {
		if (strlen($i) < 2){$jour="0".$i;} else {$jour=$i;};
		echo ("<option value='".$jour."'");
		if ($jour == $jour_courant) {echo("selected='selected'");};
		echo (">".$jour."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='mois' id='mois'>\n\r");
	for ($i=1;$i<=12;$i++) {
		if (strlen($i) < 2){$mois="0".$i;} else {$mois=$i;};
		echo ("<option value='".$mois."'");
		if ($mois == $mois_courant) {echo("selected='selected'");};
		echo (">".$mois."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='annee' id='annee'>\n\r");
	for ($i=2005;$i<=$annee_courante+1;$i++) {
		echo ("<option value='".$i."'");
		if ($i == $annee_courante) {echo("selected='selected'");};
		echo (">".$i."</option>\n\r");
	}
	echo ("</select><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");
	echo "<input type='submit' name='Voir' id='Voir' value='Voir'></fieldset></form></p>";

	echo "<p><a href='reglement_fournisseur_mois.php'>Voir tout le mois</a></p>";
	$annee_courante = substr($annee_courante,2,4);

if(isset($id_dossier) && ($montant_achat != "0.00" || $facture_achat != "0")) {
	$link = connection(MYDATABASE);
	$requete = "SELECT * FROM achats WHERE id_dossier='".$id_dossier."';";
	$result=mysql_query($requete) or die(mysql_error());
	$num_rows = mysql_num_rows($result);
	$date_courante = date("Y-m-d");
	if ($num_rows > 0) {
		$requete2 = "
		UPDATE 
		achats 
		SET 
		montant_achat='".$montant_achat."', 
		facture_achat='".$facture_achat."', 
		date='".$date_facturation."', 
		mode_achat='".$mode_achat."',
		num_cheque='".$num_cheque."',
		date_payement='".$date_payement."',
		banque='".$banque."'
		WHERE 
		id_dossier='".$id_dossier."';";
		
	}
	else
	{
		$requete2 = "
		INSERT INTO 
		achats 
		(id_dossier, montant_achat, facture_achat, date, mode_achat,num_cheque, date_payement, banque) 
		VALUES 
		(
		'".$id_dossier."',
		'".$montant_achat."',
		'".$facture_achat."', 
		'".$date_facturation."', 
		'".$mode_achat."',
		'".$num_cheque."',
		'".$date_payement."',
		'".$banque."'
		);";
	}
	//echo $requete2;
	$link = connection(MYDATABASE);
	$result2=mysql_query($requete2) or die(mysql_error());
	if ($result2) {
		echo("Facture / Montant mis à jour.");
	}
}

$montant_total_dossier=0;
$montant_total_achat=0;


if (strlen($jour_courant) < 2){$jour_courant="0".$jour_courant;};

	$date_courante="20".$annee_courante."-".$mois_courant."-".$jour_courant;
	
	$link = connection(MYDATABASE);
	$requete = "SELECT 
	DISTINCT 
	dossiers.id, 
	dossiers.to, 
	dossiers.dest_ville, 
	dossiers.ville_depart, 
	dossiers.date_deb, 
	dossiers.date_fin, 
	vendeurs.prenom, 
	clients.nom, 
	dossiers.ref_to
	FROM 
	`dossiers`, 
	`clients`, 
	`vendeurs`, 
	`lien_dossier_vendeur` 
	WHERE 
	dossiers.id_client=clients.id 
	AND 
	dossiers.id=lien_dossier_vendeur.id_dossier 
	AND 
	lien_dossier_vendeur.id_vendeur=vendeurs.id 
	AND 
	dossiers.etat='Confirmé' 
	AND 
	dossiers.date_confirm='".$date_courante."' 
	ORDER BY 
	dossiers.id;";
//	echo $requete;
	$result=mysql_query($requete) or die(mysql_error());
	$num_rows = mysql_num_rows($result);
	if ($num_rows > 0) {
		$html_output .= "<h2>$jour_courant / $mois_courant / $annee_courante</h2>\n\r";
		$html_output .= "<table border='1' cellpadding='2' cellspacing='0'>\n\r";
		$html_output .= "<tr>\n\r";
		$html_output .= "<td><b>ID dossier</b></td>\n\r";
		//$html_output .= "<td><b>Nom client</b></td>\n\r";
		$html_output .= "<td><b>TO</b></td>\n\r";
		$html_output .= "<td><b>Ref TO</b></td>\n\r";
		$html_output .= "<td><b>Montant</b></td>\n\r";
		//$html_output .= "<td><b>Date de facturation</b></td>\n\r";
		//$html_output .= "<td><b>N° de facture achat</b></td>";
		$html_output .= "<td><b>Montant achat</b></td>";
		//$html_output .= "<td><b>Date payement</b></td>";
		$html_output .= "<td><b>Assur</b></td>";
		//$html_output .= "<td><b>Banque</b></td>";
		//$html_output .= "<td><b>Numéro de cheque</b></td>";
		//$html_output .= "<td>&nbsp;</td>";
		$html_output .= "<td>Marge (%)</td>";
		$html_output .= "</tr>\n\r";

		//Html Output mail
		$html_output_mail .= "<h2>$jour_courant / $mois_courant / $annee_courante</h2>\n\r";
		$html_output_mail .= "<table border='1' cellpadding='2' cellspacing='0'>\n\r";
		$html_output_mail .= "<tr>\n\r";
		$html_output_mail .= "<td><b>ID dossier</b></td>\n\r";
		//$html_output_mail .= "<td><b>Nom client</b></td>\n\r";
		$html_output_mail .= "<td><b>TO</b></td>\n\r";
		$html_output_mail .= "<td><b>Ref TO</b></td>\n\r";
		$html_output_mail .= "<td><b>Montant</b></td>\n\r";
		//$html_output_mail .= "<td><b>Date de facturation</b></td>\n\r";
		//$html_output_mail .= "<td><b>N° de facture achat</b></td>";
		$html_output_mail .= "<td><b>Montant achat</b></td>";
		//$html_output_mail .= "<td><b>Date payement</b></td>";
		$html_output_mail .= "<td><b>Assur</b></td>";
		//$html_output_mail .= "<td><b>Banque</b></td>";
		//$html_output_mail .= "<td><b>Numéro de cheque</b></td>";
		$html_output_mail .= "<td>Marge (%)</td>";
		$html_output_mail .= "</tr>\n\r";
	}
	while($row = mysql_fetch_row($result))
	{
		$link = connection(MYDATABASE);
		$requete3 = "SELECT facture_achat, montant_achat, date, mode_achat, num_cheque, date_payement, banque FROM achats WHERE id_dossier='".$row[0]."' AND montant_achat >= 0;";
		$result3=mysql_query($requete3) or die(mysql_error());
		$num_rows3 = mysql_num_rows($result3);
		//echo $num_rows3;
		
		$facture_achat_temp = 0;
		$montant_achat_temp = 0;
		$montant_achat_assur = 0;
		$mode_achat_temp = "";
		$date="0000-00-00";
		$total_assur = Total_assur($row[0]);
		
		unset($row3);
		unset($jour_courant2);
		unset($mois_courant2);
		unset($annee_courante2);
		
		unset($date_payement);
		
		unset($jour_courant3);
		unset($mois_courant3);
		unset($annee_courante3);
				
		$num_cheque=0;
				
		while($row3 = mysql_fetch_row($result3))
		{
			$facture_achat_temp = $row3[0];
			$montant_achat_temp = $row3[1];
			$mode_achat_temp = $row3[3];
			$date = $row3[2];
		
			//echo "ICI : ".$facture_achat.$montant_achat;
			//echo "ICI : ".$date;
			
			$montant_total_dossier = $montant_total_dossier + Calcul_total_dossier($row[0]);
			$montant_total_achat+=$montant_achat_temp;
			$montant_total_assur+=$total_assur[1];
			$num_cheque = $row3[4];
			$date_payement = $row3[5];
			$banque = $row3[6];
		}
			if ($date != "0000-00-00") 
			{
				$jour_courant2 = substr($date,8,2);
				$mois_courant2 = substr($date,5,2);
				$annee_courante2 = substr($date,0,4);
			}
			else
			{
				$date = date("Y-m-d");
				$jour_courant2 = substr($date,8,2);
				$mois_courant2 = substr($date,5,2);
				$annee_courante2 = substr($date,0,4);
			}
			
			if ($date_payement != "0000-00-00" && isset($date_payement)) 
			{
				//echo $date_payement." ";
				$jour_courant3 = substr($date_payement,8,2);
				$mois_courant3 = substr($date_payement,5,2);
				$annee_courante3 = substr($date_payement,0,4);
			}
			else
			{
				//$date_payement = date("Y-m-d");
				
				$jour_courant3 = '00';
				$mois_courant3 = '00';
				$annee_courante3 = '0000';
			}
			
		$marge=100-(100*($montant_achat_temp+$total_assur[1])/Calcul_total_dossier($row[0]));

		if($montant_achat_temp > 0){$bgcolor="#CCCCCC";} else {$bgcolor="#FFFFFF";}
		
		if($marge < 0  && $row[1] != 'Jancarthier' && $row[1] != 'Go Voyages' && $row[1] != 'Océania'){$bgcolor="red";} else if($marge < 9  && $row[1] != 'Jancarthier' && $row[1] != 'Go Voyages' && $row[1] != 'Océania') {$bgcolor="orange";}

			

		$html_output .= "<tr bgcolor='".$bgcolor."'>\n\r";
		$html_output .= "<td>".$row[0]."</td>\n\r";
		//$html_output .= "<td>".$row[7]."</td>\n\r";
		$html_output .= "<td>".$row[1]."</td>\n\r";
		$html_output .= "<td>".$row[8]."</td>\n\r";
		$html_output .= "<td>".Calcul_total_dossier($row[0])."&euro;</td>\n\r";
		/**$html_output .= "<form method='post' action='".$_SERVER["PHP_SELF"]."?PHPSESSID=".session_id()."'>";
		$html_output .= "<td nowrap='nowrap'>\n\r";
		
		
		

		$html_output .= "<select name='jour_facturation' id='jour_facturation'>\n\r";
		for ($j=1;$j<=31;$j++) {
			if (strlen($j) < 2){$jour2="0".$j;} else {$jour2=$j;};
			$html_output .= "<option value='".$jour2."'";
			if ($jour2 == $jour_courant2) {$html_output .= "selected='selected'";};
			$html_output .= ">".$jour2."</option>\n\r";
		}
		$html_output .= "</select>\n\r";
		$html_output .= "<select name='mois_facturation' id='mois_facturation'>\n\r";
		for ($j=1;$j<=12;$j++) {
			if (strlen($j) < 2){$mois2="0".$j;} else {$mois2=$j;};
			$html_output .= "<option value='".$mois2."'";
			if ($mois2 == $mois_courant2) {$html_output .= "selected='selected'";};
			$html_output .= ">".$mois2."</option>\n\r";
		}
		$html_output .= "</select>\n\r";
		$html_output .= "<select name='annee_facturation' id='annee_facturation'>\n\r";
		for ($j=2005;$j<=$annee_courante2;$j++) {
			$html_output .= "<option value='".$j."'";
			if ($j == $annee_courante2) {$html_output .= "selected='selected'";};
			$html_output .= ">".$j."</option>\n\r";
		}
		$html_output .= "</select><br/>\n\r";
		$html_output .= "<br style='clear:both'/>\n\r";
		
		
		$html_output .= "</td>\n\r";**/
		
		//$html_output .= "<td><input type='hidden' value='".$row[0]."' name='id_dossier'><input type='text' value='".$facture_achat_temp."' name='facture_achat' style='width:50px;'></td>";
		//$html_output .= "<td><input type='text' value='".$montant_achat_temp."' name='montant_achat' style='width:50px;'></td>";
		$html_output .= "<td>".$montant_achat_temp." &euro;</td>";
		$html_output .= "<input type='hidden' name='mois' value='".$mois_courant."'>";
		$html_output .= "<input type='hidden' name='annee' value='".$annee_courante."'>";
		//$html_output .= "<td nowrap='nowrap'>\n\r";

		/**$fieldOptions = mysql_enum_values('achats','mode_achat');
		$html_output .= "<select name='mode_achat' size='1'>";
		foreach($fieldOptions as $tmp)
		{
		  $html_output .= "<option value='$tmp'";
		  if ($tmp == $mode_achat_temp) {$html_output .=" selected='selected' ";};
		  $html_output .= ">$tmp</option>";
		}
		$html_output .= "</select>";**/
		
		
		
		
		
/**
		$html_output .= "<select name='jour_payement' id='jour_payement'>\n\r";
		for ($j=0;$j<=31;$j++) {
			if (strlen($j) < 2){$jour3="0".$j;} else {$jour3=$j;};
			$html_output .= "<option value='".$jour3."'";
			if ($jour3 == $jour_courant3) {$html_output .= "selected='selected'";};
			$html_output .= ">".$jour3."</option>\n\r";
		}
		$html_output .= "</select>\n\r";
		$html_output .= "<select name='mois_payement' id='mois_payement'>\n\r";
		for ($j=0;$j<=12;$j++) {
			if (strlen($j) < 2){$mois3="0".$j;} else {$mois3=$j;};
			$html_output .= "<option value='".$mois3."'";
			if ($mois3 == $mois_courant3) {$html_output .= "selected='selected'";};
			$html_output .= ">".$mois3."</option>\n\r";
		}
		$html_output .= "</select>\n\r";
		
		
		//echo $annee_courante3."-";
		$html_output .= "<select name='annee_payement' id='annee_payement'>\n\r";
		
		$html_output .= "<option value='0000'";
			if ($j == $annee_courante3) {$html_output .= "selected='selected'";};
			$html_output .= ">0000</option>\n\r";
		
		$j_max = date("Y");
		for ($j=2005;$j<=$j_max;$j++) {
			$html_output .= "<option value='".$j."'";
			if ($j == $annee_courante3) {$html_output .= "selected='selected'";};
			$html_output .= ">".$j."</option>\n\r";
		}
		$html_output .= "</select><br/>\n\r";
		$html_output .= "<br style='clear:both'/>\n\r";

		$html_output .= "</td>";**/
		
		
		
		$html_output .= "<td>".$total_assur[1]."&euro;</td>";

/**
		$html_output .= "<td nowrap='nowrap'>\n\r";

		$fieldOptions = mysql_enum_values('achats','banque');
		$html_output .= "<select name='banque' size='1'>";
		foreach($fieldOptions as $tmp)
		{
		  $html_output .= "<option value='$tmp'";
		  if ($tmp == $banque) {$html_output .=" selected='selected' ";};
		  $html_output .= ">$tmp</option>";
		}
		$html_output .= "</select>";
		
		$html_output .= "</td>";
**/
		//$html_output .= "<td><input type='text' value='".$num_cheque."' name='num_cheque' style='width:50px;'></td>";
		//$html_output .= "<td><input type='submit' value='Valider' style='width:70px;'></td>";
		//$html_output .= "</form>";
		if($montant_achat_temp > 0)
		{
			
			$html_output .= "<td>".(Calcul_total_dossier($row[0]) - ($montant_achat_temp+$total_assur[1]))."&euro; (".number_format($marge,2,',',' ')."%)</td>";
		}
		else
		{
			$html_output .= "<td>N/A</td>";
		}
		$html_output .= "</tr>";
		
		$html_output_mail .= "<tr bgcolor='".$bgcolor."'>\n\r";
		$html_output_mail .= "<td>".$row[0]."</td>\n\r";
		$html_output_mail .= "<td>".$row[1]."</td>\n\r";
		$html_output_mail .= "<td>".$row[8]."</td>\n\r";
		$html_output_mail .= "<td>".Calcul_total_dossier($row[0])."&euro;</td>\n\r";
		//$html_output_mail .= "<td>".$date."</td>\n\r";
		//$html_output_mail .= "<td>".$facture_achat_temp."</td>";
		$html_output_mail .= "<td>".$montant_achat_temp."</td>";
		//$html_output_mail .= "<td>".$date_payement."</td>\n\r";
		$html_output_mail .= "<td>".$total_assur[1]."&euro;</td>";
		//$html_output_mail .= "<td>".$banque."</td>";
		//$html_output_mail .= "<td>".$num_cheque."</td>";
		
		if($montant_achat_temp > 0)
		{
			$marge=100-(100*($montant_achat_temp+$total_assur[1])/Calcul_total_dossier($row[0]));
			$html_output_mail .= "<td>".(Calcul_total_dossier($row[0]) - ($montant_achat_temp+$total_assur[1]))."&euro; (".number_format($marge,2,',',' ')."%)</td>";
		}
		else
		{
			$html_output_mail .= "<td>N/A</td>";
		}
	}

	if ($num_rows > 0) {
		$html_output .= "</table>\n\r";
		$html_output_mail .= "</table>\n\r";
	}


$html_output .= "<hr>\n\r";
$html_output_mail .= "<hr>\n\r";

if($montant_total_dossier > 0)
{
$marge=100-(100*($montant_total_achat+$montant_total_assur)/$montant_total_dossier);
}
$html_output .= "Total des ventes : ".$montant_total_dossier."&euro; / Total des achats : ".($montant_total_achat+$montant_total_assur)."&euro; / Marge : ".($montant_total_dossier-($montant_total_achat+$montant_total_assur))."&euro; (".number_format($marge,2,',',' ')."%)";
$html_output_mail .= "Total des ventes : ".$montant_total_dossier."&euro; / Total des achats : ".($montant_total_achat+$montant_total_assur)."&euro; / Marge : ".($montant_total_dossier-($montant_total_achat+$montant_total_assur))."&euro; (".number_format($marge,2,',',' ')."%)";

echo $html_output;

//echo $html_output_mail;

	echo ("<form action='".$_SERVER['PHP_SELF']."' name='envoi' id='envoi' method='post'>\n\r");
	echo("<fieldset><legend>Envoi mail à yves@agencedevoyage.com</legend>");
	echo("<textarea name='html' onFocus='this.blur()' cols='1' rows='1' style='visibility:hidden;'>$html_output_mail</textarea>");
	echo("<input type='hidden' name='envoi_mail' value='ok'>\n\r");
	echo ("<div class='center'><input type='submit' class='submit' name='Envoi du mail' id='Envoi du mail' value='Envoi du mail'><br/></div>\n\r");
	echo("</fieldset></form>");	
	
	
$temps_fin = microtime(true);	
//echo 'Temps d\'execution : '.round($temps_fin - $temps_debut, 4)."<br/>";

include("marge.php");
?>
</td>
</tr>
</table>

</body></html>

