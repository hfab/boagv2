<!doctype html public "-//W3C//DTD HTML 4.0 //EN">
<html>
<head>
       <title>Title here!</title>
</head>
<body>
<?php
require('class_fpdf/fpdf.php');

class Autorisation extends FPDF{
	
	function ajoutEntete($vendeur){
		$this->Open();
		$this->SetFillColor(240,52,58);
		$this->SetMargins(15, 15, 15);
		$this->AddPage();
		$this->Image('logo.jpg',15,10,50,18,'','http://www.agencedevoyage.com/');
		$this->Ln(25);//30
		
		$this->SetFont('Arial','B',12);
	    $this->Cell(180,3,"AUTORISATION DE PRELEVEMENT",0,1,'C',0);
	    $this->Ln(3);
	    $this->Cell(180,3,"CARTE BANCAIRE",0,1,'C',0);
	    $this->Ln(3);
		$this->SetFont('Arial','B',10);
		$this->Ln(3);
		$this->Cell(180,3,"A nous faxer au : 01 77 69 02 58",0,1,'C',0);
		$this->SetFont('Arial','B',10);
		$this->Ln(3);
		$this->Cell(180,3,"A l'attention de	$vendeur",0,1,'C',0);
		
	}

	function ajoutInfosClientCarte($type_produit,$montant,$numero_dossier,$date_depart,$passagers,$destination,$date_trans,$acompte,$montant_partiel,$hotel){
		$nb_pax=count($passagers);

		
		//$solde=$montant-($montant_partiel+$acompte);
             //$solde=$montant-$montant_partiel;

              //pour le bug de 30 pour cent
		$solde=$montant-$acompte;

		
            //cas general
	      if($montant_partiel !=""){
			$solde=$montant-$montant_partiel;
		}
		else{
			$solde=$montant-($montant_partiel+$acompte);
		}
	    
			
		$prestation="";
		
		switch($type_produit){
			case "Vol sec" :
			$prestation="VOL SEC";  
		    break;
		    case "Hotel seul" :
		    $prestation="HOTEL ";	 
		    break;
		    case "Croisiere" :
		    $prestation="CROISIERE";
		    break;
		    default:
		    $prestation="SEJOUR";	  
		}

		
		$declaration[0]="Suite � mon accord du $date_trans, je confirme Mme, Mlle, Mr .................................";
		$declaration[1]="avoir autoris�(e) par la pr�sente la soci�t� AGENCEDEVOYAGE.COM, 90 rue Baudin";
		$declaration[2]="92300 Levallois-Perret, � d�biter ma carte bancaire pour r�glement de ma cr�ance.";
		
		$coordonnes[0]="N� CARTE :";
		$coordonnes[1]="DATE D' EXPIRATION :";
		$coordonnes[2]="NOM & PRENOM DU PORTEUR :";
		$coordonnes[3]="NOM DE LA BANQUE DU PORTEUR :";
		$coordonnes[4]="CRYPTOGRAMME :";
		$coordonnes[5]="TYPE DE CARTE :  Visa , Mastercard :";
		$coordonnes[6]="N� DE DOSSIER : $numero_dossier";
		$coordonnes[7]="TOTAL DU DOSSIER : *** $montant  � ***";
		$coordonnes[8]="PRESTATION : * $prestation *";
		$coordonnes[9]="DESTINATION : $destination";
		$coordonnes[14]="HOTEL : $hotel";
		
		
		if( $montant_partiel > 0 ){	 

                            //pour les 30 pour cent
			        //$coordonnes[10]="SOMME A DEBITER : *** $acompte  � ***";

				//cas general
				$coordonnes[10]="SOMME A DEBITER : *** $montant_partiel  � ***";

				//$coordonnes[10]="SOMME A DEBITER : *** 601.60  � ***";
				//$coordonnes[11]="SOLDE A REGLER 1 MOIS AVANT LE DEPART : *** 1170.40 � ***";

                            $coordonnes[11]="SOLDE A REGLER 1 MOIS AVANT LE DEPART : *** $solde � ***";

				$coordonnes[12]="DATE DE DEPART : $date_depart";
				$coordonnes[13]="NOM DU OU DES PASSAGERS + DATE DE NAISSANCE : (� �crire en majuscules) :";
		}
		else{
			    $coordonnes[10]="SOMME A DEBITER : *** $solde � ***";
			   // $coordonnes[10]="SOLDE A REGLER 1 MOIS AVANT LE DEPART : *** $solde � ***";
			    $coordonnes[11]="DATE DE DEPART : $date_depart";
			    $coordonnes[12]="NOM DU OU DES PASSAGERS + DATE DE NAISSANCE : (� �crire en majuscules) :";
		}
		
	/*
		if( $solde > 0 ){	 
				$coordonnes[10]="SOMME A DEBITER : *** $solde  � ***";
				$coordonnes[11]="SOLDE A REGLER 1 MOIS AVANT LE DEPART : *** $solde � ***";
				$coordonnes[12]="DATE DE DEPART : $date_depart";
				$coordonnes[13]="NOM DU OU DES PASSAGERS + DATE DE NAISSANCE : (� �crire en majuscules) :";
		}
		else{
			    $coordonnes[10]="SOMME A DEBITER : *** $solde � ***";
			   // $coordonnes[10]="SOLDE A REGLER 1 MOIS AVANT LE DEPART : *** $solde � ***";
			    $coordonnes[11]="DATE DE DEPART : $date_depart";
				$coordonnes[12]="NOM DU OU DES PASSAGERS + DATE DE NAISSANCE : (� �crire en majuscules) :";
		}
		
		*/
	
	    $test="NOM DU OU DES PAX : (2 personnes) :";
		$this->Ln(15);//20
		$this->SetFont('Arial','I',12);
		
		foreach($declaration as $dec){
			$this->Cell(180,3,$dec,0,1,'L',0);
			$this->Ln(2);
		}
		$this->Ln(5);//15
		$this->SetFont('Arial','',13);
		$this->SetFillColor(215,207,207); 
	
		foreach($coordonnes as $cor){
			$this->SetFont('Arial','',13);
			if($cor!=="CRYPTOGRAMME :"){
				$this->Cell(180,3,$cor,0,1,'L',0);
			}
			else{
				$this->SetFont('Arial','',13);
				$this->Cell(180,3,$cor,0,1,'L',0);
				$this->SetFont('Arial','',10);
				$this->Ln(1);
				$this->Cell(180,3,"(3 derniers chiffres du num�ro au dos de la carte de cr�dit)",0,1,'L',0);	
			}
			if($cor!=="$test"){
				$this->Ln(4);//5
			}
			else{
				$this->Ln(2);
			}
			 
				
		}
		$this->SetFont('Arial','',9);
		$this->Cell(180,3,"(Attention, l'orthographe des noms et pr�noms doit correspondre � celle inscrite sur vos pi�ces d'identit�.)",0,1,'L',0);
		$this->Ln(3);

				
		foreach($passagers as $p){
			//$passager=$passager." ".$p;
			$this->SetFont('Arial','I',10);
			$this->Cell(180,5,"- $p									- - / - - /- - - -",0,1,'L',0);
		}
		//$this->SetFont('Arial','I',12);
		//$this->Cell(180,5,"$passager",0,1,'L',0);
		$this->Ln(6);
		$this->SetFont('Arial','I',10);
		$this->Cell(180,3,"J'atteste avoir pris connaissance des conditions g�n�rales de ventes consultable sur le site.",0,1,'L',0);
		$this->Ln(1);
		$this->Cell(180,3,"http://www.agencedevoyage.com/cgv.php",0,1,'L',0);
		$this->Ln(1);
		$this->Cell(180,3,"Je m'engage :",0,1,'L',0);
		$this->Ln(1);
		$this->SetFont('Arial','I',10);
		$this->Cell(180,3," - A v�rifier la validit� des passeports ou cartes d'indentit�s et de la totalit� des passagers y compris les b�b�s",0,1,'L',0);
		$this->Ln(1);
		$this->Cell(180,3," - A signaler, que les enfants voyageant avec un seul parent ou autre membre de leur famille doivent �tre",0,1,'L',0);
		$this->Ln(1);
		$this->Cell(180,3,"   en possession d'une autorisation de sortie du territoire d�livr� par la mairie.",0,1,'L',0);
		
		
		$this->Ln(5);
		$this->Cell(180,3,"Fait le :",0,1,'L',0);
		$this->Ln(1);
		$this->Cell(180,3,"Signature :",0,1,'L',0);

		
	}

	function Footer(){
    //Positionnement � 1,5 cm du bas
    	$this->SetY(-25);
    //Police Arial italique 8
    //Num�ro de page centr�
    		$coordonnes[0]="Si�ge Social : 12 rue Camille Desmoulins 92300 Levallois Perret ";
		$coordonnes[1]="Fax : 01 77 69 02 58";
		$coordonnes[2]="AgenceDeVoyage.com est une marque de Ethnosal SARL au capital de 7500 � - RCS Nanterre 751 870 627 ";
		$coordonnes[3]="IM09210053 - RC Gan eurocourtage N� 086833734 / Garantie APST Association ";

		$coordonnes[4]="N� TVA Intracommunautaire FR 8375187062700013";
		$coordonnes[5]="Internet : www.agencedevoyage.com";
		$this->SetFont('Arial','',6);
		$this->SetFont('Arial','',7);
		$this->SetFillColor(215,207,207); 
		foreach($coordonnes as $cor){
			$this->Cell(180,3,$cor,0,1,'C',1);
		}
	}	
}

?> 
</body>
</html>
