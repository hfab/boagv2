<?php
session_start();
if (!(Isset($_SESSION["id_vendeur"]))) {
		   header("Location:index.php");
		   exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Cr�ation de facture Agencedevoyage&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<link href="client.css" rel="stylesheet" type="text/css"/>

<style type="text/css" media="all">
	@import "css/info.css";
	@import "css/main.css";
	@import "css/widgEditor.css";
</style>

<script type="text/javascript" src="scripts/widgEditor.js"></script>

</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu_facture.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">
<?php
require_once("fonctions_db.php");
require_once('class_fpdf/fpdf.php');
require_once('facture_class.php');
require_once('confirmation_class.php');
require_once('autorisation_class.php');

$id_dos=$_POST['id_dossier'];

if(empty($id_dos)) $id_dos=$_GET['id_dossier'];
//echo "ID DOSSIER : $id_dos <br> ";

$montant_partiel=$_POST['montant_partiel'];

$link = connection(MYDATABASE);

$sql="SELECT distinct 
d.dest_pays,
d.dest_ville,
d.dest_hotel,
d.duree,
d.adultes,
d.enfants,
d.bebes,
d.pension,
d.assur,
d.date_deb,
d.date_fin,
d.prix_base_adulte,
d.prix_base_enfant,
d.prix_base_bebe,
d.suppl_adulte,
d.suppl_enfant,
d.suppl_bebe,
d.taxes_adulte,
d.taxes_enfant,
d.taxes_bebe,
d.frais_dossiers,
d.id_pkg,
d.id,
d.id_client,
d.etat,
d.acompte,
d.mode_payement,
d.nb_supp_adulte,
d.nb_supp_enfant,
d.nb_supp_bebe,
d.nb_taxe_adulte,
d.nb_taxe_enfant,
d.nb_taxe_bebe,
d.nb_reduc_adulte,
d.nb_reduc_enfant,
d.nb_reduc_bebe,
d.supp_divers,
d.nb_supp_divers,
d.reduc_adulte,
d.reduc_enfant,
d.reduc_bebe,
d.ville_depart,
d.compris,
d.hausse_carb, 
d.nb_hausse_carb, 
c.id,
c.nom,
c.prenom,
c.mail,
c.adresse1,
c.adresse2,
c.ville,
c.cp,
c.tel1,
c.civilite,
a.prix,
a.nom,
y.cpays,
y.lpays_fr,
v.id,
v.nom,
v.prenom,
l.id_dossier,
l.id_vendeur
FROM dossiers d, clients c, assurances_new a, code_pays y, vendeurs v,lien_dossier_vendeur l
					  
					  WHERE d.id=$id_dos
					  AND d.id_client=c.id

					  AND y.cpays=d.dest_pays
					  AND d.id=l.id_dossier
					  AND v.id=l.id_vendeur";
//echo $sql.'<br><br>';	  
$result = mysql_query($sql) or die(mysql_error().$sql);    

//echo $sql;

if(!$result){ 
    echo"Aucune selection : problemes<br/>";
		echo mysql_errno()." : ".mysql_error()."<br/>";
} 
else {
	while($row = mysql_fetch_row($result)){
			
		   //print_r($row);
		   //infos dossier
		    $pays=$row[0];
		    $ville=$row[1];
		    $hotel=$row[2];
		    $duree=$row[3];
		    
		    $nb_adultes=$row[4];
		    $nb_enfants=$row[5];
		    $nb_bebes=$row[6];
		    
		    $pension=$row[7];
		    $assurance=$row[8];
		    $date_deb=$row[9];
		    $date_fin=$row[10];
		    
		    $prix_base_adulte=$row[11];
		    $prix_base_enfant=$row[12];
		    $prix_base_bebe=$row[13];
		    
		    $suppl_adulte=$row[14];
		    $suppl_enfant=$row[15];
		    $suppl_bebe=$row[16];

		    $taxes_adulte=$row[17];
		    $taxes_enfant=$row[18];
		    $taxes_bebe=$row[19];
		    
		    $frais_dossiers=$row[20];
		    
		    $idpkg=$row[21];
		    
		    $id_dossier=$row[22];
		    $id_client=$row[23];
		    $etat_dossier=$row[24];
		
		 	$acompte=$row[25];
		 	$mode_payement=$row[26];
		 	
		 	$nb_supp_adulte=$row[27];
		 	$nb_supp_enfant=$row[28];
		 	$nb_supp_bebe=$row[29];
		 	
		 	$nb_taxe_adulte=$row[30];
		 	$nb_taxe_enfant=$row[31];
		 	$nb_taxe_bebe=$row[32];
		 	
			$nb_reduc_adulte=$row[33];
		 	$nb_reduc_enfant=$row[34];
		 	$nb_reduc_bebe=$row[35];
		 
		 	$supp_divers=$row[36];
		 	$nb_supp_divers=$row[37]; 
		
		 	$reduc_adulte=$row[38];
		 	$reduc_enfant=$row[39];
		 	$reduc_bebe=$row[40];
		 	
            $ville_dep=$row[41];
            
			$compris=str_replace('&amp;apos;','\'',$row[42]);
			$hausse_carb=$row[43];
			$nb_hausse_carb=$row[44];   
		   // echo $compris;	
		    //infos client
		    //$id_client=$row[45];
		    
		    $nom_client=$row[46];
		    $prenom_client=$row[47];
		    $mail_client=$row[48];
			$adresse1_client=$row[49];
			$adresse2_client=$row[50];
			$ville_client=$row[51];
			$cp_client=$row[52];
			$tel1_client=$row[53];
			$civilite_client=$row[54];
		    
		    //infos assurances
		    $prix_assur=$row[55]; 
		    $assurance=$row[56];
			//infos pays dest
		 	 //$code_pays[57];
			$lpays=$row[58];
		    //infos vendeur
		    $id_vend=$row[59];
		    $nom_vend=$row[60];
		    $prenom_vend=$row[61];

		    
	 }
	        
}

//echo($nb_adultes);

//traitement des  des supplements

$sql1="SELECT type_chambre,supp_adulte_div,supp_enfant_div,supp_bebe_div,
			  nb_supp_adulte_div,nb_supp_enfant_div,nb_supp_bebe_div,
			  intitule_supp_adulte_div,intitule_supp_bebe_div,intitule_supp_enfant_div,
			  hausse_carb_bebe,nb_hausse_carb_bebe,date_transac, date_confirm
	   
	   FROM dossiers WHERE id=$id_dos";
	
$result1 = mysql_query($sql1) or die(mysql_error().$sql1);

if(!$result1){ 
    	echo"Aucune selection : probleme<br/>";
		echo mysql_errno()." : ".mysql_error()."<br/>";
} 
else{
		
		while($row1 = mysql_fetch_array($result1)) {
			       $type_chambre=$row1[0];
				    
				   $supp_adulte_div=$row1[1];
				   $supp_enfant_div=$row1[2];
				   $supp_bebe_div=$row1[3];
				   
				   $nb_supp_adulte_div=$row1[4];
				   $nb_supp_enfant_div=$row1[5];
				   $nb_supp_bebe_div=$row1[6];
				   
				   $intitule_supp_adulte_div=$row1[7];
				   $intitule_supp_enfant_div=$row1[9];
				   $intitule_supp_bebe_div=$row1[8];
				   
				   $hausse_carb_bebe=$row1[10];
				   $nb_hausse_carb_bebe=$row1[11];
				   $date_transac=$row1[12];  
				   $date_confirm=$row1[13];
				   
		}
}
//echo $nb_supp_adulte_div."-".$supp_adulte_div;
//si il y a des passagers autres que le client*/
$passagers=array();
$nb_passagers=$nb_enfants+$nb_bebes+$nb_adultes;

//if($nb_passagers>1){

	$sql2="SELECT * FROM passagers WHERE id_dossier=$id_dos";
	
	$result2 = mysql_query($sql2) or die(mysql_error().$sql2);
   
	if(!$result2){ 
    	echo"Aucune selection : probleme<br/>";
		echo mysql_errno()." : ".mysql_error()."<br/>";
	} 
	else{
		
		while($row2 = mysql_fetch_array($result2)) {
			      // echo $row2[3];
			       //echo $row2[4];
			       //$p_nom=substr($row2[3],0,1);
			       $p_nom=$row2[3];
                   $p_prenom=$row2[4];
                   $passager=$p_nom." ". $p_prenom;
                   //echo "$passager<br>";
                   array_push($passagers,$passager);
             
		}
	}
	$assurance_nom = array();
	$passager_assur =array();
$sql4="SELECT passagers.assur, assurances_new.prix, passagers.nom, passagers.prenom FROM passagers, assurances_new WHERE passagers.id_dossier=$id_dos AND passagers.assur=assurances_new.nom;"; 
$result4 = mysql_query($sql4) or die(mysql_error().$sql4);
		while($row4 = mysql_fetch_array($result4)) 
		{
			$assurance_nom[] = $row4[0];
			$assurance_prix[$row4[0]]= $row4[1];
			$passager=$row4[2]." ".$row4[3];
			$passager_assur[$passager]=$row4[0];	
		}
		//var_dump( $assurance_prix );
$assurance_nom = array_count_values($assurance_nom); 
/***************************************************************/
//RECUPERATION  DY TYPE DE CHAMBRE POUR HOTEL ET CROISIERE et les suppl. divers 2 et 3
$sql5="SELECT type_chambre,supp_adulte_div2,supp_adulte_div3,nb_supp_adulte_div2,nb_supp_adulte_div3,intitule_supp_adulte_div2,intitule_supp_adulte_div3  FROM dossiers WHERE id =$id_dos ;";
$result5 = mysql_query($sql5) or die(mysql_error().$sql5);
while($row5 = mysql_fetch_array($result5)) {
			$type_chambre = $row5[0];
			
			$supp_adulte_div2=$row5[1];
			$supp_adulte_div3=$row5[2];
			
			$nb_supp_adulte_div2=$row5[3];
			$nb_supp_adulte_div3=$row5[4];
			
			$intitule_supp_adulte_div2=$row5[5];
			$intitule_supp_adulte_div3=$row5[6];	
}

/***************************************************************/	
/***************************************************************/		
//RECUPERATION  des trois numero de telephone du client

$sql6="SELECT tel1,tel2,tel3  FROM clients WHERE id =$id_client;";
$result6 = mysql_query($sql6) or die(mysql_error().$sql6);
while($row6 = mysql_fetch_array($result6)) {
	        $telphone="";
	        
	       /* if( strlen(trim($row6[0]))>0  ){
	        	$telphone=$row6[0];
	        }
	        
	        if( strlen(trim($row6[1]))>0 && strlen(trim($row6[0]))==0 ){
				$telphone=$row6[1];
			}
			if( strlen(trim($row6[2]))>0 && strlen(trim($row6[0]))==0 && strlen(trim($row6[1]))==0  ){
				$telphone=$row6[2];
			}*/
			
			if( strlen(trim($row6[2]))>0 ){
				$telphone=$row6[2];
			}
			else {
					 if( strlen(trim($row6[0]))>0  ){
	        				$telphone=$row6[0];
	        		 }
	        		 else{
					 		$telphone=$row6[1];
					 }
			 }
			
				
}		
/***************recuperation du mode payement************************************************/
$sql7="SELECT mode_payement  FROM facturation WHERE id_dossier=$id_dos;";
$result7 = mysql_query($sql7) or die(mysql_error().$sql7);
	$tab_mode_pay=array();
	while($row7 = mysql_fetch_array($result7)) {
	        //echo " mode de payement $row7[0]";
			array_push($tab_mode_pay,$row7[0]);				
	}
  $tab_mode=array_count_values($tab_mode_pay);
  foreach($tab_mode as $c=>$v){
  	switch($c){
  	  case "CB_Internet":
	  $c="CBI";
	  break;
	  case "Ch�que":
	  $c="CH";
	  break;
	  case "Esp�ces":
	  $c="ES";
	  break;
	  case "Virement bancaire":
	  $c="VB";
	  break;
	  case "Ch�que vacances":
	  $c="CV";
	  break;	
	}
  	$mode_paiement=$mode_paiement.", ".$c;
  }

  $mode_paiement=substr($mode_paiement, 1, strlen($mode_paiement)-1); 
  
/***************fin recuperation du mode payement************************************************/	

deconnection($link);

//enum('CB', 'CB_Internet', 'Ch�que', 'Esp�ces', 'Virement bancaire', 'Ch�que vacances')

//echo $type_chambre;
//echo $idpkg;

$total_facture=calcul_total_dossier($id_dos);
$total_acompte=Calcul_total_acompte($id_dos);

//echo $total_facture;
//echo $total_acompte;

//structuratuin des donnees extraites du dossiers
$designation = array("Prix de base adulte"=>"$nb_adultes;$prix_base_adulte",
					 "Suppl�ment 2eme semaine adulte"=>"$nb_supp_adulte;$suppl_adulte",
					 "Suppl�ment single"=>"$nb_taxe_adulte;$taxes_adulte",//supplement single
					 "R�duction adulte"=>"$nb_reduc_adulte;$reduc_adulte",
					 utf8_decode($intitule_supp_adulte_div)=>"$nb_supp_adulte_div;$supp_adulte_div",
					  
					 "Prix de base enfant"=>"$nb_enfants;$prix_base_enfant",
					 "Suppl�ment 2eme semaine enfant"=>"$nb_supp_enfant;$suppl_enfant",
					 "R�duction enfant"=>"$nb_reduc_enfant;$reduc_enfant",
					 "Taxes enfant"=>"$nb_taxe_enfant;$taxes_enfant",
					 utf8_decode($intitule_supp_enfant_div)=>"$nb_supp_enfant_div;$supp_enfant_div",
					 
					 "Prix de base b�b�"=>"$nb_bebes;$prix_base_bebe",
					 "Suppl�ment 2eme semaine b�b�"=>"$nb_supp_bebe;$suppl_bebe",
					// "Taxes b�b�"=>"$nb_taxe_bebe;$taxes_bebe",
					 "Hausse carburant b�b�"=>"$nb_hausse_carb_bebe;$hausse_carb_bebe",
					 utf8_decode($intitule_supp_bebe_div)=>"$nb_supp_bebe_div;$supp_bebe_div",
					 //"Suppl�ment divers"=>"$nb_supp_divers;$supp_divers",//taxes areoport
					 "Taxes d'a�roport"=>"$nb_supp_divers;$supp_divers",
					 "Hausse carburant"=>"$nb_hausse_carb;$hausse_carb");
$aucune_assur=false;					 
foreach ($assurance_nom as $key=>$value)
{

     /*
       //ajour du supplement pour la gripe h1n1
	 //date du debut du supplement
	if ($value >0)// si assurance
	{
		$dateSuppGrippe		='2009-09-09';
		$dateSuppGrippe		= new DateTime($dateSuppGrippe); 
		$dateSuppGrippe		= $dateSuppGrippe->format('Ymd'); 
		$dateConfirmation = new DateTime($date_confirm); 
		$dateConfirmation = $dateConfirmation->format('Ymd'); 
		if( $dateConfirmation > $dateSuppGrippe ) 
		{		
				 $designation["Option grippe H1N1"]=$value."-12";
		}
	}
	//fin ajour du supplement pour la gripe h1n1
      */
      //echo "avant le switch";
	switch($key){
		
		case "Aucune":
		//$key="";
		$aucune_assur=true; //si il n y aucune assurance
		//echo "here";
		break;

		case "Annulation_0-400_2017":
$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
break;
case "Annulation_401-600_2017":
$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
break;
case "Annulation_601-1200_2017":
$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
break;
case "Annulation_1201-1500_2017":
$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
break;
case "Annulation_1501-2000_2017":
$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
break;
case "Annulation_2001-4000_2017":
$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
break;
case "Annulation_4001-8000_2017":
$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
break;
case "Annulation_France_jusqu_a_2000":
$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
break;
case "Annulation_Famille_France_Moyen_Courrier":
$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
break;
case "Annulation_Famille_Long_Courrier":
$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
break;
case "Annulation_Tribu_Moyen_Courrier":
$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
break;
case "Annulation_Tribu_Long_Courrier":
$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
break;
case "Multirisques_0-400_2017":
$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
break;
case "Multirisques_401-600_2017":
$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
break;
case "Multirisques_601-1200_2017":
$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
break;
case "Multirisques_1201-1500_2017":
$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
break;
case "Multirisques_1201-1500_2017":
$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
break;
case "Multirisques_1501-2000_2017":
$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
break;
case "Multirisques_2001-4000_2017":
$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
break;
case "Multirisques_4001-8000_2017":
$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
break;
case "Multirisques_France_jusqu_a_2000":
$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
break;
case "Multirisques_Famille_France_Moyen_Courrier":
$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
break;
case "Multirisques_Famille_Long_Courrier":
$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
break;
case "Multirisques_Tribu_Moyen_Courrier":
$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
break;
case "Multirisques_Tribu_Long_Courrier":
$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
break;

		
		case "Annulation_0-400":
                $designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
                break;

		case "Annulation_0-400":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		case "Annulation_401-600":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		case "Annulation_601-1100":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		case "Annulation_1101-2000":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		case "Annulation_2001-4000":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		case "Annulation_4001-6000":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		case "Presence_annulation_0-300":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		case "Presence_annulation_301-600":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		case "Presence_annulation_601-900":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		case "Presence_annulation_901-1300":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		case "Presence_annulation_1301-1600":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		case "Presence_annulation_1601-2500":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		case "Presence_annulation_2501-3100":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		case "Presence_annulation_3101-4500":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		case "Multirisques_0-400":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		case "Multirisques_401-600":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		case "Multirisques_601-1100":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		case "Multirisques_1101-2000":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		case "Multirisques_2001-4000":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		case "Multirisques_4001-6000":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		case "Presence_multirisques_0-300":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		case "Presence_multirisques_301-600":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		case "Presence_multirisques_601-900":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		case "Presence_multirisques_901-1300":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		case "Presence_multirisques_1301-1600":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		case "Presence_multirisques_1601-2500":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		case "Presence_multirisques_2501-3100":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		case "Presence_multirisques_3100-4500":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		case "Presence_multirisques_3101-4500":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		//jam	
		
		
		
		
		
		// ajout fab
		
		
		case "Assur_multi_0_400_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_401_600_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_601_1200_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_1201_1500_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_1501_2000_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_2001_4000_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_4001_6000_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_6001_8000_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Tribu_Moyen_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Tribu_Long_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Famille_Moyen_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Famille_Long_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		// suite 
		
		
		// 'Aucune','Annulation_0-400_2017','Annulation_401-600_2017','Annulation_601-1200_2017','Annulation_1201-1500_2017','Annulation_1501-2000_2017','Annulation_2001-4000_2017','Annulation_4001-8000_2017','Multirisques_0-400_2017','Multirisques_401-600_2017','Multirisques_601-1200_2017','Multirisques_1201-1500_2017','Multirisques_1201-1500_2017','Multirisques_1501-2000_2017','Multirisques_2001-4000_2017','Multirisques_4001-8000_2017','Assur_multi_0_400_2018','Assur_multi_401_600_2018','Assur_multi_601_1200_2018','Assur_multi_1201_1500_2018','Assur_multi_1501_2000_2018','Assur_multi_2001_4000_2018','Assur_multi_4001_6000_2018','Assur_multi_6001_8000_2018','Famille_Moyen_2018','Famille_Long_2018','Tribu_Moyen_2018','Tribu_Long_2018','Assur_Annulation_0_400_2018','Assur_Annulation_401_600_2018','Assur_Annulation_601_1200_2018','Assur_Annulation_1201_1500_2018','Assur_Annulation_1501_2000_2018','Assur_Annulation_2001_4000_2018','Assur_Annulation_4001_6000_2018','Assur_Annulation_6001_8000_2018','Assur_Annulation_Famille_Moyen_2018','Assur_Annulation_Famille_Long_2018','Assur_Annulation_Tribu_Moyen_2018','Assur_Annulation_Tribu_Long_2018','Assur_Multi_Sans_Motif_0_400_2018','Assur_Multi_Sans_Motif_401_600_2018','Assur_Multi_Sans_Motif_601_1200_2018','Assur_Multi_Sans_Motif_1201_1500_2018','Assur_Multi_Sans_Motif_1501_2000_2018','Assur_Multi_Sans_Motif_2001_4000_2018','Assur_Multi_Sans_Motif_4001_5000_2018','Assur_Annulation_Sans_Motif_0_400_2018','Assur_Annulation_Sans_Motif_401_600_2018','Assur_Annulation_Sans_Motif_601_1200_2018','Assur_Annulation_Sans_Motif_1201_1500_2018','Assur_Annulation_Sans_Motif_1501_2000_2018','Assur_Annulation_Sans_Motif_2001_4000_2018','Assur_Annulation_Sans_Motif_4001_5000_2018'
		
		case "Assur_Annulation_0_400_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Annulation_401_600_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Annulation_601_1200_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Annulation_1201_1500_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Annulation_1501_2000_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Annulation_2001_4000_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Annulation_4001_6000_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Annulation_6001_8000_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Annulation_Famille_Moyen_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Annulation_Famille_Long_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Annulation_Tribu_Moyen_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Annulation_Tribu_Long_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		// ------------------------------------------
		
		
		
		case "Assur_Multi_Sans_Motif_0_400_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Multi_Sans_Motif_401_600_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Multi_Sans_Motif_601_1200_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Multi_Sans_Motif_1201_1500_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Multi_Sans_Motif_1501_2000_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Multi_Sans_Motif_2001_4000_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Multi_Sans_Motif_4001_5000_2018":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		// ---------------------
		
		
		case "Assur_Annulation_Sans_Motif_0_400_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Assur_Annulation_Sans_Motif_401_600_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Assur_Annulation_Sans_Motif_601_1200_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Annulation_Sans_Motif_1201_1500_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Assur_Annulation_Sans_Motif_1501_2000_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Annulation_Sans_Motif_2001_4000_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_Annulation_Sans_Motif_4001_5000_2018":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		
		
		// new 2019
		
		
		case "Assur_multi_0_400_2019":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_401_600_2019":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_601_1200_2019":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_1201_1500_2019":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_1501_2000_2019":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_2001_4000_2019":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_4001_6000_2019":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_6001_8000_2019":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		// ---------------------
		
		
		case "Assur_annulation_bagage_0_400_2019":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Assur_annulation_bagage_401_600_2019":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Assur_annulation_bagage_601_1200_2019":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_1201_1500_2019":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Assur_annulation_bagage_1501_2000_2019":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_2001_4000_2019":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_4001_6000_2019":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_6001_8000_2019":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		
		
		
		
		
		// new 2020
		
		
		case "Assur_multi_0_400_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_401_600_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_601_1200_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_1201_1500_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_1501_2000_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_2001_4000_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_4001_6000_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_6001_8000_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_france_2000_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_famille_france_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_famille_moyen_courrier_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_famille_long_courrier_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_tribu_france_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_tribu_moyen_courrier_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_tribu_long_courrier_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		// ---------------------
		
		
		case "Assur_annulation_bagage_0_400_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Assur_annulation_bagage_401_600_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Assur_annulation_bagage_601_1200_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_1201_1500_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Assur_annulation_bagage_1501_2000_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_2001_4000_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_4001_6000_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_6001_8000_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		
		case "Assur_annulation_bagage_france_2000_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_famille_france_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_moyen_courrier_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_long_courrier_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_tribu_france_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_tribu_moyen_courrier_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_tribu_long_courrier_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		
		
		case "Assur_multi_sans_motif_0_400_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_sans_motif_401_600_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_sans_motif_601_1200_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_sans_motif_1201_1500_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_sans_motif_1501_2000_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_sans_motif_2001_4000_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_sans_motif_4001_5000_2020":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		// ---------------------
		
		
		case "Assur_annulation_sans_motif_0_400_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Assur_annulation_sans_motif_401_600_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Assur_annulation_sans_motif_601_1200_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_sans_motif_1201_1500_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Assur_annulation_sans_motif_1501_2000_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_sans_motif_2001_4000_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_sans_motif_4001_5000_2020":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		

		// fin ajout fab
		
		
				// new 2021
		
		
		case "Assur_multi_0_400_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_401_600_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_601_1200_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_1201_1500_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_1501_2000_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_2001_4000_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_4001_6000_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_6001_8000_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_france_2000_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_famille_france_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_famille_moyen_courrier_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_famille_long_courrier_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_tribu_france_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_tribu_moyen_courrier_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_tribu_long_courrier_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		// ---------------------
		
		
		case "Assur_annulation_bagage_0_400_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Assur_annulation_bagage_401_600_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Assur_annulation_bagage_601_1200_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_1201_1500_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Assur_annulation_bagage_1501_2000_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_2001_4000_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_4001_6000_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_6001_8000_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		
		case "Assur_annulation_bagage_france_2000_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_famille_france_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_moyen_courrier_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_long_courrier_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_tribu_france_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_tribu_moyen_courrier_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_bagage_tribu_long_courrier_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		
		
		case "Assur_multi_sans_motif_0_400_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_sans_motif_401_600_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_sans_motif_601_1200_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_sans_motif_1201_1500_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_sans_motif_1501_2000_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_sans_motif_2001_4000_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_multi_sans_motif_4001_5000_2021":
		$designation["Assurance Multirisques"]=$value.";".$assurance_prix[$key];
		break;
		
		// ---------------------
		
		
		case "Assur_annulation_sans_motif_0_400_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Assur_annulation_sans_motif_401_600_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Assur_annulation_sans_motif_601_1200_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_sans_motif_1201_1500_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;		
		
		case "Assur_annulation_sans_motif_1501_2000_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_sans_motif_2001_4000_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		
		case "Assur_annulation_sans_motif_4001_5000_2021":
		$designation["Assurance d'annulation"]=$value.";".$assurance_prix[$key];
		break;
		

		// fin ajout fab
		
		
		
	}	
}

// ajout des supp adultes divers 2 et 3
$designation[$intitule_supp_adulte_div2]=$nb_supp_adulte_div2.";".$supp_adulte_div2;
$designation[$intitule_supp_adulte_div3]=$nb_supp_adulte_div3.";".$supp_adulte_div3;


$designation["Frais de dossiers"]="1;".$frais_dossiers;

foreach($passager_assur as $passger=>$assurance){
		
	switch($assurance){
		case "Annulation_moyen":
		$assurance="Assurance d'annulation moyen courier";
		$passager_assur[$passger]=$assurance;
		break;
		case "Annulation_long":
		$assurance="Assurance d'annulation long courier";
		$passager_assur[$passger]=$assurance;
		break;
		case "Multirisques_moyen":
		$assurance="Assurance multirisques moyen courier";
		$passager_assur[$passger]=$assurance;
		break;
		case "Multirisques_long":
		$assurance="Assurance multirisques long courier";
		$passager_assur[$passger]=$assurance;
		break;
		//jam
		case "Vol_moyen_annulation":
		$assurance="Assurance d'annulation vol moyen courier";
		$passager_assur[$passger]=$assurance;
		break;
		case "Vol_long_annulation":
		$assurance="Assurance d'annulation vol long courier";
		$passager_assur[$passger]=$assurance;
		break;
		case "Vol_moyen_multirisques":
		$assurance="Assurance multirisques vol moyen courier";
		$passager_assur[$passger]=$assurance;
		break;
		case "Vol_long_multirisques":
		$assurance="Assurance multirisques vol long courier";
		$passager_assur[$passger]=$assurance;
		break;
			
		}
}

//print_r($designation);
//structuratuin des donnees extraites du client 
$client = array("identite_tel"=>"$civilite_client.	$nom_client	$prenom_client       T�l: $telphone",
				 "adresse"=>"$adresse1_client $cp_client $ville_client",
				 "mail"=>"Email: $mail_client");


echo "<center><form method='post' action='search_dossier.php?PHPSESSID=".session_id()."'><input type='hidden' value='".$id_dos."' name='id_dossier'><input type='hidden' value='modifier' name='flag'><input type='submit' class='submit' value='Retour au dossier' style='width:200px;'></form></center>";

function supprimerAccents($chaine){
	$avecaccents=array("À","Á","Â","Ã","Ä","Å","Ç","È","É","Ë","Ê","Ì","Í","Î","Ï","Ò","Ó","Ô","Õ","Ö","Ù","Ú","Û","Ü","Ý","à","á","â","ã","ä","å","ç","è","é","ê","ë","ì","í","î","ï","ð","ò","ó","ô","õ","ö","ù","ú","û","ü","ý","ÿ");
	$sansaccents=array("A","A","A","A","A","A","C","E","E","E","E","I","I","I","I","O","O","O","O","O","U","U","U","U","Y","a","a","a","a","a","a","c","e","e","e","e","i","i","i","i","ð","o","o","o","o","o","u","u","u","u","y","y");
	$chaine=str_replace($avecaccents,$sansaccents,$chaine);
	return $chaine;				 
}
function dateFr($date){
	$tabdate= explode("-",$date);
	$date=$tabdate[2]."/".$tabdate[1]."/".$tabdate[0];
	return $date;	
}
$date_deb=dateFr($date_deb);
$date_fin=dateFr($date_fin);
$date_transac=dateFr($date_transac);

$lpays=html_entity_decode($lpays);
$ville=html_entity_decode($ville);
// total  du dossier

//les options incluses dans le prix				 
$inclus=explode("|",$compris);	

if ($date_confirm == "0000-00-00" || $date_confirm == "")
{
	$date_confirm = date("d/m/Y");
}
//echo "ICI".$date_confirm;

//$aujourdhui=date("d-m-y");

//instanciation de l'objet facture
$facture=new Facture();

//var_dump($passagers);

//$facture->ajoutEntete($etat_dossier,$id_dos,$id_client,$date_confirm,$prenom_vend);

// fab
$facture->ajoutEntete($etat_dossier,$id_dos,$id_client,$aujourdhui,$prenom_vend);
// $facture->ajoutEntete($etat_dossier,$id_dos,$id_client,'28.11.2017',$prenom_vend);
/* $dateF=date($date_confirm, "d/m/Y"); */
//$facture->ajoutEntete($etat_dossier,$id_dos,$id_client, $date_confirm ,$prenom_vend);

$facture->descriptionSejourClient($idpkg,$lpays,$ville,$ville_dep,$date_deb,$date_fin,$hotel,$pension,$passagers,$client,$type_chambre);
//impression des lignes de la facture et recuperation du monatant pour l'autorisation de prelevement
$facture->ajoutLigneQuantitePrix($designation,$mode_paiement,$acompte);//$mode_paiement,$acompte)
$facture->ajoutDesInclus($inclus,$aucune_assur);
//$facture->ajoutFooter();

$today=date("dmy");
$fichier="fact_".$id_dossier."_".$today.".pdf";

if(!file_exists ("facture")){
	mkdir ("facture", 0777);
} 

touch ("facture/".$fichier);
chmod ("facture/".$fichier, 0777);

$facture->Output("facture/$fichier","F");
$facture->Close();
echo "<br/><br/><br/><div align='center'><a href='facture/$fichier' target='_blank'>1.	Visualiser la Facture</a></div><br/>";


//Zev
$fichier_fact = "facture/$fichier";

$today=date("dmy");
///////////autorisation de prelevement///////////////////////////
$autorisation = new Autorisation();
$autorisation->ajoutEntete($prenom_vend);
$destination=$lpays." - ".$ville;
$autorisation->ajoutInfosClientCarte($idpkg,$total_facture,$id_dos,$date_deb,$passagers,$destination,date("d/m/y"),$total_acompte,$montant_partiel,$hotel);

if(!file_exists ("autorisation")){
	mkdir ("autorisation", 0777);
}
$fichier="auto_".$id_dossier."_".$today.".pdf"; 
//$fichier="autorisation.pdf";

touch ("autorisation/".$fichier);
chmod ("autorisation/".$fichier, 0777);

$autorisation->Output("autorisation/$fichier","F");
$autorisation->Close();
echo "<br/><br/><br/><div align='center'><a href='autorisation/$fichier' target='_blank'>2. Visualiser l'autorisation de prelevement</a></div><br/>";


$fichier_auto = "autorisation/$fichier";
///////////fin autorisation de prelevemente///////////////////////////

///////////confirmation de de voyage///////////////////////////
$confirmation=new Confirmation();
$confirmation->ajoutEntete($prenom_vend,$id_dos);
//$type_produit,$date_resa,$destination,$ville_dep,$ville_arrivee,$hotel,$date_deb,$date_fin,$nb_passagers,$formule,$type_chambre,$passager_assurance
$confirmation->ajoutDescriptionProduit($idpkg,$date_transac,$lpays,$ville_dep,$ville,$hotel,$date_deb,$date_fin,$nb_passagers,$pension,$type_chambre,$passager_assur);

/* echo "total:" .$total_facture."<br />- acompte: ".$total_acompte."<br />- montant_partiel :".$montant_partiel; */

$confirmation->ajoutPaiement($total_facture,$total_acompte,$montant_partiel);
$confirmation->ajoutSalutation($civilite_client,$nom_client,$prenom_client);
$confirmation->ajoutCoordonnees();		
if(!file_exists ("confirmation")){
	mkdir ("confirmation", 0777);
} 
$fichier="confir_".$id_dossier."_".$today.".pdf";

touch ("confirmation/".$fichier);
chmod ("confirmation/".$fichier, 0777);


$confirmation->Output("confirmation/$fichier","F");
$confirmation->Close();

echo "<br/><br/><br/><div align='center'><a href='confirmation/$fichier' target='_blank'>3. Visualiser la confirmation du voyage</a></div><br/>";
////////////////fin confirmation de voyage/////////////////
//Zev
$fichier_conf = "confirmation/$fichier";

//Zev
/** Fonction d'envoi par mail **/
echo "<br/><h1>Envoi du mail contenant le devis et l'autorisation de prelevement.</h1><br/><br/><div align='center'><form action='envoi_mail.php?PHPSESSID=".session_id()."' method='post' target='_blank'><input type='hidden' name='file1' value='".$fichier_fact."'><input type='hidden' name='file2' value='".$fichier_auto."'><input type='hidden' name='id_dossier' value='".$id_dos."'>";
echo("<textarea id='msg' name='msg' class='widgEditor nothing'></textarea>");
echo "<input type='submit' value='4. Envoi par mail : autorisation et facture (devis)'></form></div><br/>";


echo "<br/><br/><h1>Envoi du mail contenant la facture et la confirmation</h1><br/><br/><div align='center'><form action='envoi_mail2.php?PHPSESSID=".session_id()."' method='post' target='_blank'><input type='hidden' name='file1' value='".$fichier_fact."'><input type='hidden' name='file2' value='".$fichier_conf."'><input type='hidden' name='id_dossier' value='".$id_dos."'>";
echo("<textarea id='msg2' name='msg2' class='widgEditor nothing'></textarea>");
echo "<input type='submit' value='5. Envoi par mail : confirmation et facture'></form></div><br/>";

echo "<center><form  method='post' action='search_dossier.php?PHPSESSID=".session_id()."'><input type='hidden' value='".$id_dos."' name='id_dossier'><input type='hidden' value='modifier' name='flag'><input type='submit' class='submit' value='Retour au dossier' style='width:200px;'></form></center>";
?>
</td>
<td width="120">&nbsp;</td>
</tr>
</table>



</body>
</html>
