<?php
session_start();
if (!isset($_SESSION["id_vendeur"])) {
		   header("Location:index.php");
		   exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Statistiques Monagence&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="client.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
require_once("fonctions_db.php");
require_once("fonctions.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">
<?php
$jour = $_POST["jour"];
$mois = $_POST["mois"];
$annee = $_POST["annee"];

$montant_achat = $_POST["montant_achat"];
$facture_achat = $_POST["facture_achat"];
$id_dossier = $_POST["id_dossier"];


$jour_courant=date("d");
$mois_courant=date("m");
$annee_courante=date("y");


$date_comm = $_POST["date_comm"];
$type = $_POST["type"];
$commentaire = $_POST["commentaire"];

if (!empty($jour)) {$jour_courant = $jour;};
if (!empty($mois)) {$mois_courant = $mois;};
if (!empty($annee)) {$annee_courante = $annee;};

if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};


	echo "<h1>Visualisation du journal des ventes pour le : ".$jour_courant." / ".$mois_courant." / ".$annee_courante."</h1>";
	echo("<p align='center'><a href='stats2_mois.php'>Visualisation mois en cours</a></p>");
	echo "<p><form method='post' action='".$_SERVER['PHP_SELF']."?PHPSESSID=".session_id()."'><fieldset><legend>Choix de la période</legend>";
	echo ("<label for='jour'>Voir une autre période : </label>\n\r");
	echo ("<select name='jour' id='jour'>\n\r");
	for ($i=1;$i<=31;$i++) {
		if (strlen($i) < 2){$jour="0".$i;} else {$jour=$i;};
		echo ("<option value='".$jour."'");
		if ($jour == $jour_courant) {echo("selected='selected'");};
		echo (">".$jour."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='mois' id='mois'>\n\r");
	for ($i=1;$i<=12;$i++) {
		if (strlen($i) < 2){$mois="0".$i;} else {$mois=$i;};
		echo ("<option value='".$mois."'");
		if ($mois == $mois_courant) {echo("selected='selected'");};
		echo (">".$mois."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='annee' id='annee'>\n\r");
	for ($i=2005;$i<=$annee_courante+1;$i++) {
		echo ("<option value='".$i."'");
		if ($i == $annee_courante) {echo("selected='selected'");};
		echo (">".$i."</option>\n\r");
	}
	echo ("</select><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");
	echo "<input type='submit' name='Voir' id='Voir' value='Voir'></fieldset></form></p>";

	$annee_courante = substr($annee_courante,2,4);




echo("<h1>Journal des ventes du jour </h1>");

	$link = connection(MYDATABASE);
	$requete = "SELECT DISTINCT dossiers.id, dossiers.to, dossiers.dest_ville, dossiers.ville_depart, dossiers.date_deb, dossiers.date_fin, vendeurs.prenom, clients.nom FROM `dossiers`, `clients`, `vendeurs`, `lien_dossier_vendeur` WHERE dossiers.id_client=clients.id AND dossiers.id=lien_dossier_vendeur.id_dossier AND lien_dossier_vendeur.id_vendeur=vendeurs.id AND (etat='En request' OR etat='Confirmé' OR etat='Préconfirmé') AND SUBSTRING(`date_confirm`,9,10)=".$jour_courant." AND SUBSTRING(`date_confirm`,6,7)=".$mois_courant." AND SUBSTRING(`date_confirm`,3,4)=".$annee_courante." ORDER BY dossiers.id;";
	$result=mysql_query($requete) or die(mysql_error());
	$num_rows = mysql_num_rows($result);
	
     //echo  $num_rows;

	if ($num_rows > 0) {
		echo("<table border='1' cellpadding='2' cellspacing='0'>");
		echo("<tr>");
		echo("<td><b>ID dossier</b></td>");
		echo("<td><b>Vendeur</b></td>");
		echo("<td><b>Nom client</b></td>");
		echo("<td><b>TO</b></td>");
		echo("<td><b>Ville Destination</b></td>");
		echo("<td><b>Ville départ</b></td>");
		echo("<td><b>Date de départ</b></td>");
		echo("<td><b>Date de retour</b></td>");
		echo("<td><b>Montant</b></td>");
		echo("<td><b>Assurance</b></td>");
		echo("</tr>");
	}
	else
	{
		echo "Aucun résultat";
	}
	while($row = mysql_fetch_row($result))
	{
		$link = connection(MYDATABASE);
		$requete3 = "SELECT facture_achat, montant_achat FROM achats WHERE id_dossier='".$row[0]."';";
		$result3=mysql_query($requete3) or die(mysql_error());
		$num_rows3 = mysql_num_rows($result3);
		//echo $num_rows3;
		
		$facture_achat_temp = 0;
		$montant_achat_temp = 0;
		
		while($row3 = mysql_fetch_row($result3))
		{
			$facture_achat_temp = $row3[0];
			$montant_achat_temp = $row3[1];
			//echo "ICI : ".$facture_achat.$montant_achat;
		}
		
		$total_assur = Total_assur($row[0]);
		
		echo("<tr>");
		echo("<td>".$row[0]."</td>");
		echo("<td>".$row[6]."</td>");
		echo("<td>".$row[7]."</td>");
		echo("<td>".$row[1]."</td>");
		echo("<td>".$row[2]."</td>");
		echo("<td>".$row[3]."</td>");
		echo("<td>".$row[4]."</td>");
		echo("<td>".$row[5]."</td>");
		echo("<td>".Calcul_total_dossier($row[0])."</td>");
		echo("<td>".$total_assur[1]."</td>");
		echo("</tr>");
	}

	if ($num_rows > 0) {
		echo("</table>");
	}



echo("<hr>");

//Commentaires


if(isset($commentaire))
{
	$link = connection(MYDATABASE);
	$requete = "SELECT * FROM commentaires WHERE type='Vente' AND date='".$date_comm."';";
	//echo $requete;
	$result=mysql_query($requete) or die(mysql_error());
	if (mysql_num_rows($result)>0) 
	{
		$requete2 = "UPDATE commentaires SET `texte` = '".$commentaire."' WHERE type='Vente' AND date='".$date_comm."';";
	}
	else
	{
		$requete2 = "INSERT INTO `commentaires` (`texte` , `date` , `type` ) VALUES ('".$commentaire."', '".$date_comm."', 'Vente');";
	}
	//echo $requete2;
	$result2=mysql_query($requete2) or die(mysql_error());
}
if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};
$date=$annee_courante."/".$mois_courant."/".$jour_courant;

	$link = connection(MYDATABASE);
	$requete = "SELECT texte FROM commentaires WHERE type='Vente' AND date='".$date."';";
	$result=mysql_query($requete) or die(mysql_error());
	if (mysql_num_rows($result)>0) 
	{
		$row=mysql_fetch_row($result);
		$commentaire = $row[0];
	}


//Commentaires
echo ("<form action='".$_SERVER['PHP_SELF']."' name='commentaires' id='commentaires' method='post'>\n\r");
echo("<fieldset><legend>Commentaire pour la journée du : ".$jour_courant." / ".$mois_courant." / ".$annee_courante."</legend>");

echo ("<label for='commentaire'>Commentaire</label>\n\r");
echo ("<textarea name='commentaire' id='commentaire' cols='60' rows='3'>".stripslashes($commentaire)."</textarea><br/>\n\r");
echo ("<br style='clear:both'/>\n\r");
echo("<input type='hidden' name='date_comm' value='".$date."'>");
echo("<input type='hidden' name='type' value='Caisse'>");
echo("<input type='hidden' name='jour' value='".$jour_courant."'>");
echo("<input type='hidden' name='mois' value='".$mois_courant."'>");
echo("<input type='hidden' name='annee' value='".$annee_courante."'>");
echo ("<div class='center'><input type='submit' class='submit' name='Enregistrer' id='Enregistrer' value='Enregistrer'><br/></div>\n\r");
echo("</fieldset></form>");



	$total_dossiers = 0;
	$compteur = 0;
	$link = connection(MYDATABASE);
	$requete = "SELECT id FROM `dossiers` WHERE ( etat='Confirmé' OR etat='Préconfirmé')
	AND 
	SUBSTRING(`date_confirm`,9,10)=".$jour_courant." 
	AND 
	SUBSTRING(`date_confirm`,6,7)=".$mois_courant." 
	AND 
	SUBSTRING(`date_confirm`,3,4)=".substr($annee_courante,2,4)."  GROUP BY dossiers.id;";
	//echo $requete;
	$result=mysql_query($requete) or die(mysql_error());
  while($row = mysql_fetch_row($result))
	{
		//print_r($row);
		$total_courant = Calcul_total_dossier($row[0]);
		$total_dossiers += $total_courant;
		$compteur++;
	}
	
	echo "<hr><h1>Chiffre d'affaire :</h1> ".number_format($total_dossiers,2,',',' ')." ";
	if($compteur > 0)
	{
		echo "<hr><h1>Panier moyen ($compteur dossiers confirmés) :</h1> ".number_format($total_dossiers / $compteur,2,',',' ')." ";
	}

?>
</td>
</tr>
</table>

</body></html>

