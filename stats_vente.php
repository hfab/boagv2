<?php
session_start();
if (!isset($_SESSION["id_vendeur"])) {
		   header("Location:index.php");
		   exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Statistiques Vente Monagence&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="client.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
require_once("fonctions_db.php");
require_once("fonctions.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">
<?php
$mois = $_POST["mois"];
$annee = $_POST["annee"];


$mois_courant=date("m");
$annee_courante=date("y");


if (!empty($mois)) {$mois_courant = $mois;};
if (!empty($annee)) {$annee_courante = $annee;};

if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};


$html_output="";

	echo "<h1>Stats de ventes pour le : ".$mois_courant." / ".$annee_courante."</h1>";
	echo "<p><form method='post' action='".$_SERVER['PHP_SELF']."?PHPSESSID=".session_id()."'><fieldset><legend>Choix de la période</legend>";
	echo ("<label for='mois'>Voir une autre période : </label>\n\r");

	echo ("<select name='mois' id='mois'>\n\r");
	for ($i=1;$i<=12;$i++) {
		if (strlen($i) < 2){$mois="0".$i;} else {$mois=$i;};
		echo ("<option value='".$mois."'");
		if ($mois == $mois_courant) {echo("selected='selected'");};
		echo (">".$mois."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='annee' id='annee'>\n\r");
	for ($i=2005;$i<=$annee_courante;$i++) {
		echo ("<option value='".$i."'");
		if ($i == $annee_courante) {echo("selected='selected'");};
		echo (">".$i."</option>\n\r");
	}
	echo ("</select><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");
	echo "<input type='submit' name='Voir' id='Voir' value='Voir'></fieldset></form></p>";

	$annee_courante = substr($annee_courante,2,4);

unset ($traites);
unset ($confirmes);
for ($i=1;$i<32;$i++)
{
	$link = connection(MYDATABASE);
	$requete = "SELECT vendeurs.id, vendeurs.prenom, vendeurs.nom FROM `vendeurs` WHERE rang='Vendeur' OR rang='Gestionnaire';";
	$result=mysql_query($requete) or die(mysql_error());
	$num_rows = mysql_num_rows($result);
		
	$jour_courant = $i;
	if (strlen($jour_courant) < 2){$jour_courant="0".$jour_courant;};
	if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};	
	echo ("<h2>".$jour_courant." / ".$mois_courant." / ".$annee_courante."</h2>");
	
	while($row = mysql_fetch_row($result))
	{	

		$link = connection(MYDATABASE);
		
		$requete2 = "SELECT dossiers.id FROM `dossiers`, `vendeurs`, `lien_dossier_vendeur` WHERE SUBSTRING(dossiers.date_transac,9,2)='".$jour_courant."' AND SUBSTRING(dossiers.date_transac,6,2)='".$mois_courant."' AND SUBSTRING(dossiers.date_transac,1,4)='".$annee_courante."' AND dossiers.id=lien_dossier_vendeur.id_dossier AND lien_dossier_vendeur.id_vendeur='".$row[0]."' GROUP BY dossiers.id;";
		$requete3 = "SELECT dossiers.id FROM `dossiers`, `vendeurs`, `lien_dossier_vendeur` WHERE SUBSTRING(dossiers.date_transac,9,2)='".$jour_courant."' AND SUBSTRING(dossiers.date_transac,6,2)='".$mois_courant."' AND SUBSTRING(dossiers.date_transac,1,4)='".$annee_courante."' AND dossiers.id=lien_dossier_vendeur.id_dossier AND lien_dossier_vendeur.id_vendeur='".$row[0]."' AND (dossiers.etat='Confirmé' OR dossiers.etat='En request')  GROUP BY dossiers.id;";
		
		/**echo("<hr>");
		echo $requete2;
		echo $requete3;
		echo("<hr>");**/
		
		$result2=mysql_query($requete2) or die(mysql_error());
		$result3=mysql_query($requete3) or die(mysql_error());
		
		$row2 = mysql_fetch_row($result2);
		$num_rows2 = mysql_num_rows($result2);
		
		$row3 = mysql_fetch_row($result3);
		$num_rows3 = mysql_num_rows($result3);
		
		if ($num_rows2 > 0)
		{
			echo "- <b>".$row[1]." ".$row[2]."</b> : ";
			//echo ($row2[0]." / ".$row3[0]." ** ");
			echo ("Traités : ".$num_rows2." / Confirmés : ".$num_rows3." | Pourcentage : ".number_format(($num_rows3*100)/$num_rows2,2,',',' ')." %<br/>");
			$traites[$row[1]." ".$row[2]] += $num_rows2;
			$confirmes[$row[1]." ".$row[2]] += $num_rows3;
		}
	
	}
	echo("<hr>");
}
foreach ($traites as $key=>$value)
{
	echo "<b>".$key."</b> - Total traités : ".$value." / Totals confirmés : ".$confirmes[$key]." | Pourcentage : ".number_format(($confirmes[$key]*100)/$value,2,',',' ')." %<br/>";
}

//echo $html_output;


?>
</td>
</tr>
</table>

</body></html>

