<?php
session_start();
if (!isset($_SESSION["id_vendeur"])) {
		   header("Location:index.php");
		   exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Cr�ation d'un nouveau client de Monagence&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<link href="client.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">
<?php

require_once("fonctions_db.php");
require_once("fonctions.php");

$id_dossier = $_GET["id_dossier"];
$total=$_POST["total"];
$civilite=$_POST["civilite"];
$nom=$_POST["nom"];
$prenom=$_POST["prenom"];
$age=$_POST["age"];
$jour_nais=$_POST["jour_nais"];
$mois_nais=$_POST["mois_nais"];
$annee_nais=$_POST["annee_nais"];

$id_passager=$_POST["id_passager"];
$assur=$_POST["assur"];
$mail=$_POST["mail"];

function Affiche_form_passagers($id_dossier, $id_passager) {
	
	$link = connection(MYDATABASE);
  $requete = "SELECT adultes, enfants, bebes FROM dossiers WHERE id='".$id_dossier."';";
  $result = mysql_query($requete,$link) or die(mysql_error());
	while($row = mysql_fetch_row($result)){
		//1 des adultes est le client !!!
		$adultes = $row[0];
		$enfants = $row[1];
		$bebes = $row[2];
	}

  $query = "SELECT etat FROM dossiers WHERE id=".$id_dossier."";
  $result = mysql_query($query);
  $row = mysql_fetch_assoc($result);
  $etat = $row['etat'];
  
  
//Si le dossier existe deja, on tente de recupere les passagers d�clar�s
  $requete = "SELECT * FROM passagers WHERE id_dossier='".$id_dossier."' ORDER BY id;";
  $result = mysql_query($requete,$link) or die(mysql_error());
	if (mysql_num_rows($result)>0) 
	{
		$i=0;
		while($row = mysql_fetch_row($result)){
			$id[$i]=$row[0];
			$civilite[$i]=$row[2];
			$nom[$i]=$row[3];
			$prenom[$i]=$row[4];
			$age[$i]=$row[5];
			$date_nais[$i]=$row[6];
			$annee_nais[$i]=substr($row[6], 0,4);
			$mois_nais[$i]=substr($row[6], 5,2);
			$jour_nais[$i]=	substr($row[6], 8,2);
			$assur[$i]=$row[7];
			$mail[$i]=$row[9];
			$i++;		
		}
	}
	deconnection($link);
	//echo("<hr>AGE");
	//print_r($age);
	//echo("<hr>ID_PASS");
	//print_r($id);
	//echo("<hr>");
	
	//Affectation des "Ages" � chaque passager.
	for ($i=1;$i<=$adultes;$i++)
	{
		if($id[$i-1]>0)
		{
			$passager[$i]=$age[$i-1];
		}
		else
		{
			$passager[$i]="Adulte";
		}
	}
	$total_temp=$adultes+$enfants;
	for ($i=($adultes+1);$i<=$total_temp;$i++)
	{
		if($id[$i-1]>0)
		{
			$passager[$i]=$age[$i-1];
		}
		else
		{
			$passager[$i]="Enfant";
		}
	}
	$total = $adultes + $enfants + $bebes;
	for ($i=($total_temp+1);$i<=$total;$i++)
	{
		if($id[$i-1]>0)
		{
			$passager[$i]=$age[$i-1];
		}
		else
		{
			$passager[$i]="Bebe";
		}
	}
	
	
	echo ("Adultes : ".$adultes." / Enfants : ".$enfants." / B�b�s : ".$bebes);
	echo " / Total de passagers : ".$total;

  if($etat == 'Confirm�' || $etat == 'Annul�' || $etat =='Pr�confirm�')
  {
	echo "<br /><br /><div style='color:red;font-weight:bold;'>Attention, le dossier est en mode $etat, vous n'avez pas le droit de modifier les passagers ou assurances</div>";
  }
	
	echo ("<form action='".$_SERVER['PHP_SELF']."?id_dossier=".$id_dossier."' name='passagers' id='passagers' method='post'>\n\r");
	echo("<h1>Enregistrement des passagers pour le dossier N� $id_dossier</h1>");
	
	
	for($i=1;$i<=$total;$i++)
	{
	echo ("<fieldset><legend>Passager $i</legend><br/>\n\r");
	
	//Civilit�
	echo ("<label for='civilite'>Civilit�</label>\n\r");
	echo ("<select name='civilite[]' id='civilite'>\n\r");
	echo("<option value='Mr'");
	if($civilite[$i-1]=="Mr"){echo(" selected='selected'");};
	echo(">Mr</option>\n\r");
	
	echo("<option value='Mme'");
	if($civilite[$i-1]=="Mme"){echo(" selected='selected'");};
	echo(">Mme</option>\n\r");
	
	echo("<option value='Melle'");
	if($civilite[$i-1]=="Melle"){echo(" selected='selected'");};
	echo(">Melle</option>\n\r");
	
	echo ("</select><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");
	
	//Nom
	echo ("<label for='nom'>Nom</label>\n\r");
	echo ("<input type='text' name='nom[]' id='nom' value='".$nom[$i-1]."'><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");
	
	//Prenom
	echo ("<label for='prenom'>Pr�nom</label>\n\r");
	echo ("<input type='text' name='prenom[]' id='prenom' value='".$prenom[$i-1]."'><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");
	
	//Age
	echo ("<label for='age'>Age</label>\n\r");
	$fieldOptions = mysql_enum_values('passagers','age');
	echo "<select name='age[]' size='1'>";
	foreach($fieldOptions as $tmp)
	{
	  echo "<option value='$tmp'";
	  if ($tmp == $passager[$i]) {echo(" selected='selected' ");};
	  echo ">$tmp</option>";
	}
	echo "</select><br/>";
	echo ("<br style='clear:both'/>\n\r");	
	
	//Date de naissance
	echo ("<label for='date_nais'>Date de Naissance</label>\n\r");
	echo ("<select name='jour_nais[]' id='jour_nais'>\n\r");
	for ($j=1;$j<=31;$j++) {
		if (strlen($j) < 2){$jour="0".$j;} else {$jour=$j;};
		echo ("<option value='".$jour."'");
		if ($jour == $jour_nais[$i-1]) {echo(" selected='selected' ");};
		echo (">".$jour."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='mois_nais[]' id='mois_nais'>\n\r");
	for ($j=1;$j<=12;$j++) {
		if (strlen($j) < 2){$mois="0".$j;} else {$mois=$j;};
		echo ("<option value='".$mois."'");
		if ($mois == $mois_nais[$i-1]) {echo(" selected='selected' ");};
		echo (">".$mois."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='annee_nais[]' id='annee_nais'>\n\r");
	for ($j=1930;$j<=2016;$j++) {
		echo ("<option value='".$j."'");
		if ($j == $annee_nais[$i-1]) {echo(" selected='selected' ");};
		echo (">".$j."</option>\n\r");
	}
	echo ("</select><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");
	
	//Assurance
	echo ("<label for='assur'>Assurance</label>\n\r");
	$fieldOptions = mysql_enum_values('passagers','assur');
	echo "<select name='assur[]' size='1'>";
	foreach($fieldOptions as $tmp)
	{
	  echo "<option value='$tmp'";
	  if ($tmp == $assur[$i-1]) {echo(" selected='selected' ");};
	  echo ">$tmp</option>";
	}
	echo "</select><br/>";
	echo ("<br style='clear:both'/>\n\r");	
	
	//mail
	echo ("<label for='mail'>Mail</label>\n\r");
	echo ("<input type='text' name='mail[]' id='mail' value='".$mail[$i-1]."'><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");

	echo ("<input type='hidden' name='id_passager[]' id='id_passager' value='".$id[$i-1]."'><br/>\n\r");
	echo ("</fieldset>");
	
	

		
	}
	echo ("<input type='hidden' name='id_dossier' id='id_dossier' value='".$id_dossier."'><br/>\n\r");
	echo ("<input type='hidden' name='total' id='total' value='".$total."'><br/>\n\r");
	echo ("<div class='center'><input type='submit' name='Valider' id='Valider' value='Valider'><br/></div>\n\r");
	echo ("</form><br/>");
	
}

function Insertion_passagers($id_dossier, $civilite, $nom, $prenom, $age, $date_nais, $total, $id_passager, $assur, $mail) 
{
	$link=connection(MYDATABASE);
  
  $query = "SELECT etat FROM dossiers WHERE id=".$id_dossier."";
  $result = mysql_query($query);
  $row = mysql_fetch_assoc($result);
  $etat = $row['etat'];
  //echo $etat;
  
  
  if(($etat != 'Confirm�' && $etat != 'Annul�' && $etat !='Pr�confirm�') || $_SESSION["id_vendeur"] == '17' ||  $_SESSION["id_vendeur"] == '20' ||  $_SESSION["id_vendeur"] == '36' )
  {

	for($i=0;$i<$total;$i++)
	{
		if($id_passager[$i]>0)
		{
			//print_r($prenom);
			$requete = "UPDATE `passagers` SET `id_dossier`= '".$id_dossier."', `civilite`= '".$civilite[$i]."', `nom`= '".strtoupper($nom[$i])."', `prenom`= '".ucfirst(strtolower($prenom[$i]))."', `age`= '".$age[$i]."', `date_naiss`= '".$date_nais[$i]."', `assur`= '".$assur[$i]."', `mail`= '".$mail[$i]."' WHERE id='".$id_passager[$i]."';";
		}
		else
		{
			$requete = "INSERT INTO `passagers` (`id_dossier` , `civilite` , `nom` , `prenom` , `age` , `date_naiss` , `assur`  , `mail` ) VALUES ('".$id_dossier."', '".$civilite[$i]."', '".strtoupper($nom[$i])."', '".ucfirst(strtolower($prenom[$i]))."', '".$age[$i]."', '".$date_nais[$i]."', '".$assur[$i]."', '".$mail[$i]."');";
		}
		echo $requete."<br/><br/>";
		$result = mysql_query($requete,$link) or die(mysql_error());
	}
	deconnection($link);
	echo("Enregistrement effectu�<br/>");
	echo"<p align='center'><form method='post' action='facture.php?PHPSESSID=".session_id()."'><input type='hidden' value='".$id_dossier."' name='id_dossier'><input type='submit' value='Cr�er une facture pour ce dossier'></form></p>";
	}
	else
	{
		echo "T'as pas le droit de faire �a, vilaine fille ! va voir tonton Francis (!!!) ou Eva (?) pour ta fess�e !";
	}
}


if (Isset($id_dossier) && !(Isset($total)))
{
	Affiche_form_passagers($id_dossier, $id_passager);
	Bouton_retour_dossier($id_dossier);
}
else 
{
	for($i=0;$i<$total;$i++)
	{
		$date_nais[$i]=$annee_nais[$i]."-".$mois_nais[$i]."-".$jour_nais[$i];
	}
	Insertion_passagers($id_dossier, $civilite, $nom, $prenom, $age, $date_nais, $total, $id_passager, $assur, $mail);
	Bouton_retour_dossier($id_dossier);
}

?>