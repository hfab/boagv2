<STYLE type="text/css">
		<!--	
		
h4 {
	font-weight:bold;
	font-size:11px;
}

		-->	
</STYLE>
<?php

$mode = $_GET['mode'];

require_once ("fonctions.php");
require_once ("fonctions_db.php");

//Calcul des primes pour les vendeurs de mon agence
$id_vendeur = $_SESSION['id_vendeur'];

$rang = get_vendeur_rang($_SESSION["id_vendeur"]);

$jour_courant = date("d");
$mois_courant = date("m");
$annee_courante = date("y");

if ($rang == "Vendeur" || $id_vendeur == 36)
{

/**	if ($jour_courant < 27 && $mois_courant != 01)
	{
		$debut_periode = date("Y-m-d",mktime(0, 0, 0, $mois_courant-1, 27, $annee_courante));
		$fin_periode = date("Y-m-d",mktime(0, 0, 0, $mois_courant, 26, $annee_courante));
	}
	else if ($jour_courant < 27 && $mois_courant == 01)
	{
		$debut_periode = date("Y-m-d",mktime(0, 0, 0, $mois_courant-1, 27, $annee_courante-1));
		$fin_periode = date("Y-m-d",mktime(0, 0, 0, $mois_courant, 26, $annee_courante));
	}
	else if ($jour_courant > 27 && $mois_courant == 01)
	{
		$debut_periode = date("Y-m-d",mktime(0, 0, 0, $mois_courant, 27, $annee_courante));
		$fin_periode = date("Y-m-d",mktime(0, 0, 0, $mois_courant+1, 26, $annee_courante+1));
	}
	else
	{
		$debut_periode = date("Y-m-d",mktime(0, 0, 0, $mois_courant, 27, $annee_courante));
		$fin_periode = date("Y-m-d",mktime(0, 0, 0, $mois_courant+1, 26, $annee_courante));
	}**/
	
if ($jour_courant > 26 && $mois_courant != "12" && empty($mois) && empty($annee))
{
	$mois_courant++;
} 
else if ($jour_courant > 26 && $mois_courant == "12" && empty($mois) && empty($annee))
{
	$mois_courant = "01";
	$annee_courante++;
}


if ($mois_courant == "01")
{
	$debut_periode = date("Y-m-d",mktime(0, 0, 0, 12, 27, $annee_courante-1));
	$fin_periode = date("Y-m-d",mktime(0, 0, 0, 01, 26, $annee_courante));
}
else if ($mois_courant == "12")
{
	$debut_periode = date("Y-m-d",mktime(0, 0, 0, $mois_courant-1, 27, $annee_courante));
	$fin_periode = date("Y-m-d",mktime(0, 0, 0, $mois_courant, 26, $annee_courante));
}
else
{
	$debut_periode = date("Y-m-d",mktime(0, 0, 0, $mois_courant-1, 27, $annee_courante));
	$fin_periode = date("Y-m-d",mktime(0, 0, 0, $mois_courant, 26, $annee_courante));
}

	
	
	
	//echo date("d-m-y",$debut_periode);
	
	$link = connection(MYDATABASE);
	//Partie 1 : taux de transformation d'assurances
	//Nb total de passagers vendus pour le mois en cours
	//Nb total d'assurances pour la meme periode
	
	$requete_pass = "
	SELECT 
	COUNT(passagers.id) AS total_passagers
	FROM 
	lien_dossier_vendeur, 
	dossiers,
	passagers
	WHERE
	`date_confirmation` BETWEEN DATE '".$debut_periode."' AND DATE '".$fin_periode."' 
	AND
	passagers.id_dossier=lien_dossier_vendeur.id_dossier
	AND
	dossiers.id = lien_dossier_vendeur.id_dossier
	AND
	lien_dossier_vendeur.id_vendeur = '".$id_vendeur."';
	";
	
	$requete_assur = "
	SELECT 
	COUNT(passagers.assur) AS total_assur
	FROM 
	lien_dossier_vendeur, 
	dossiers,
	passagers
	WHERE
	`date_confirmation` BETWEEN DATE '".$debut_periode."' AND DATE '".$fin_periode."' 
	AND
	passagers.id_dossier=lien_dossier_vendeur.id_dossier
	AND
	passagers.assur != 'Aucune'
	AND
	dossiers.id = lien_dossier_vendeur.id_dossier
	AND
	lien_dossier_vendeur.id_vendeur = '".$id_vendeur."';
	";
	
	$result_pass = mysql_query($requete_pass, $link) or die(mysql_error());
	$result_assur = mysql_query($requete_assur, $link) or die(mysql_error());
	
	$row_pass = mysql_fetch_assoc($result_pass);
	$row_assur = mysql_fetch_assoc($result_assur);
	
	$total_passagers = $row_pass['total_passagers'];
	$total_assur = $row_assur['total_assur'];
	
	if ($total_passagers > 0)
	{
		$objectif1 = number_format((($total_assur/$total_passagers)*100),2,',',' ');
	}
	else
	{
		$objectif1 = 0;
	}
	$prime1 = $total_assur;
	
	//echo $total_passagers."/".$total_assur." : ".$objectif1;
	//echo "<hr>";
	//Partie 2 : Primes par Pax
	//Nb total de passagers par TO
	$requete_to = "
	SELECT DISTINCT
	`passagers`.`id`,
	`dossiers`.`to`,
	`prime_to`.`prime`,
	`prime_to`.`blacklist`
	FROM 
	`lien_dossier_vendeur`, 
	`dossiers`,
	`passagers`,
	`prime_to`
	WHERE
	`prime_to`.`to` = `dossiers`.`to`
	AND 
	`date_confirmation` BETWEEN DATE '".$debut_periode."' AND DATE '".$fin_periode."' 
	AND
	`passagers`.`id_dossier`=`lien_dossier_vendeur`.`id_dossier`
	AND
	`dossiers`.`id` = `lien_dossier_vendeur`.`id_dossier`
	AND
	`lien_dossier_vendeur`.`id_vendeur` = '".$id_vendeur."'
	";
	
	//echo $requete_to;
	$result_to = mysql_query($requete_to, $link) or die(mysql_error());
	
	unset($to);
	unset($prime2);
	unset($blacklist);
	
	while($row_to = mysql_fetch_assoc($result_to))
	{
		$to[] = $row_to['to'];
		$to_prime[$row_to['to']] = $row_to['prime'];
		$blacklist[] = $row_to['blacklist'];
		$prime2 += $row_to['prime'];
	}
	if (is_array($to))
	{
		$to = array_count_values($to);
	}
	if(is_array($blacklist))
	{
		$blacklist = array_count_values($blacklist);
	}	
	//print_r($to);
	
	//echo $prime2;
	
	//echo "<hr>";
	//Partie 3 : taux de transformation, voir fonction en commun avec stats_vente.php
	
	$requete2 = "
	SELECT 
	dossiers.id 
	FROM 
	`dossiers`, 
	`vendeurs`, 
	`lien_dossier_vendeur` 
	WHERE  
	`date_transac` BETWEEN DATE '".$debut_periode."' AND DATE '".$fin_periode."'  
	AND 
	dossiers.id=lien_dossier_vendeur.id_dossier 
	AND 
	lien_dossier_vendeur.id_vendeur='".$id_vendeur."' 
	GROUP BY dossiers.id;";
	
	$requete3 = "
	SELECT 
	dossiers.id 
	FROM 
	`dossiers`, 
	`vendeurs`, 
	`lien_dossier_vendeur` 
	WHERE  
	`date_transac` BETWEEN DATE '".$debut_periode."' AND DATE '".$fin_periode."' 
	AND 
	dossiers.id=lien_dossier_vendeur.id_dossier 
	AND 
	lien_dossier_vendeur.id_vendeur='".$id_vendeur."' 
	AND 
	(dossiers.etat='Confirmé' OR dossiers.etat='En request')  
	GROUP BY dossiers.id;";
	
	//echo $requete2;
	//echo $requete3;
	
	$result2=mysql_query($requete2, $link) or die(mysql_error());
	$result3=mysql_query($requete3, $link) or die(mysql_error());
			
	$row2 = mysql_fetch_row($result2);
	$num_rows2 = mysql_num_rows($result2);
			
	$row3 = mysql_fetch_row($result3);
	$num_rows3 = mysql_num_rows($result3);
			
	if ($num_rows2 > 0)
	{
		//echo ("Trait�s : ".$num_rows2." / Confirm�s : ".$num_rows3." | Pourcentage : ".number_format(($num_rows3*100)/$num_rows2,2,',',' ')." %<br/>");
		$traites[] += $num_rows2;
		$confirmes[] += $num_rows3;
	}
	if (isset($traites) && is_array($traites))
		{
		foreach ($traites as $key=>$value)
		{
			$objectif3 = number_format(($confirmes[$key]*100)/$value,2,',',' ');
			//echo "Total trait�s : ".$value." / Totals confirm�s : ".$confirmes[$key]." | Pourcentage : ".$objectif3." %<br/>";
		}
	}
		else
		{
			$objectif3 = 0;
		}
	
	
	
	//echo "<hr>";
	//Partie 4 : Prime sur CA
	//Total du CA g�n�r� par vendeur
	$requete_ca = "
	SELECT
	dossiers.id
	FROM
	dossiers,
	lien_dossier_vendeur
	WHERE
	lien_dossier_vendeur.id_vendeur = '".$id_vendeur."'
	AND
	lien_dossier_vendeur.id_dossier = dossiers.id
	AND 
	`date_confirmation` BETWEEN DATE '".$debut_periode."' AND DATE '".$fin_periode."' 
	AND (dossiers.etat='Confirm�'  OR dossiers.etat='Pr�confirm�' OR dossiers.etat='En request')
	;";
	
	$result_ca=mysql_query($requete_ca, $link) or die(mysql_error());
	
	unset($objectif4);
	
	while($row_ca = mysql_fetch_assoc($result_ca))
	{
		$reqbonus = "SELECT * FROM bonus WHERE id_dossier = '".$row_ca['id']."';";
		$resbonus = mysql_query($reqbonus) or die(mysql_error());

		if (mysql_num_rows($resbonus) > 0)
		{
			$rowbonus = mysql_fetch_row ($resbonus);
			$bonusprime += $rowbonus['bonus'];
		}

		$objectif4 += Calcul_total_dossier($row_ca['id']);
	}
	
	
	//echo ($objectif4);
	
	//Partie 5 : calcul en fonction des 4 premieres parties
	echo "<hr>Taux de transformation des assurances : ".$objectif1." (prime : ".$prime1.")<br/>";
	echo "Primes par Pax/TO : ".$prime2."<br/>";
	echo "Taux de transformation :".$objectif3."<br/>";
	echo "CA r�alis� : ".$objectif4."<br/>";
	
	unset($prime_totale);

	$prime_total += $bonusprime;

	if ($objectif1 > 40)
	{
		$prime_totale += $prime1;
	}
	
	$prime_totale += $prime2;
	
		if($objectif3 >= 40)
		{
			$prime_totale = ($prime_totale * 1.5);
		}
	$pourcentage_blacklist = 0;
	if(is_array($blacklist))
	{
		$pourcentage_blacklist = (($blacklist[1] * 100) / ($blacklist[0] + $blacklist[1]) );
	}


	if($pourcentage_blacklist > 40)
	{
		echo "<br /><span style='color:red;'>D�sol�, trop de PAX en TO blacklist�.. Prime totale divis�e par 2..</span><br /><br />";
		$prime_totale = $prime_totale / 2;
	}

	if($mode != "detail")
	{
		echo "<div style='width:120px;font-family:Arial, Verdana;font-size:9px;text-align:justify;'>";
		
		echo "<h4>P�riode ".$debut_periode." => ".$fin_periode."</h4>";
		
		echo "Si CA &#8805; 100 000 �, <br/>Prime de : ";
		echo "<span style='color:red;'>".$prime_totale." �</span><br/><br/>";
		
		echo "Si CA &#8805; 150 000 � <br/> Prime de : ";
		echo "<span style='color:red;'>".($prime_totale*1.25)." �</span><br/><br/>";
		
		echo "Si CA &#8805; 200 000 �<br/> Prime de : ";
		echo "<span style='color:red;'>".($prime_totale*1.5)." �</span><br/><br/>";
	}
	
	
		if ($objectif4 < 100000)
		{
			$prime_totale = ($prime_totale * 0);
		}
		if ($objectif4 >= 100000 && $objectif4 < 150000)
		{
			$prime_totale = ($prime_totale * 1);
		}
		if ($objectif4 >= 150000 && $objectif4 <= 200000)
		{
			$prime_totale = ($prime_totale * 1.25);
		}
		if ($objectif4 > 200000)
		{
			$prime_totale = ($prime_totale * 1.5);
		}
	if($mode != "detail")
	{
		echo "CA r�alis� jusqu'au ".$jour_courant."-".$mois_courant." :<br/><span style='color:red;'>".number_format($objectif4,2,',',' ')." �</span><br/><br/>";
		echo "Prime actuelle : <br/><span style='color:red;'>".$prime_totale." �</span><br/><br/>";
		echo "<br/><br/><center><a style='font-size:9px;' href='#' Onclick=\"window.open('prime.php?mode=detail','','width=400,height=400,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes')\">Voir le détail</a></center>";
	}
	
	//Affichage des stats d�taill�es
	if ($mode == "detail")
	{
		echo "<div style='border:1px solid black;padding:2px;margin:auto;width:300px;font-family:Arial, Verdana;font-size:9px;text-align:justify;'>";
		
		echo "<h4>P�riode ".$debut_periode." => ".$fin_periode."</h4>";
		
		echo "<h4>Prime pour les assurances</h4>";
		echo "Taux de transformation des assurances : ".$objectif1." %<br/>";
		echo "Nb de passagers : ".$total_passagers."<br/>";
		echo "Nb d'assurances vendues : ".$total_assur."<br/>";
		
		echo "<h4>Prime par Pax et TO : </h4>";
		$i = 0;
		if (isset($to) && is_array($to))
		{
			foreach ($to as $key=>$value)
			{
				
				echo $key." : ".$value." Pax (".$to_prime[$key]." � par Pax)<br/>";
				$i++;
			}
		}
		else
		{
			$prime2 = 0;
		}

		if(is_array($blacklist))
		{
			$pourcentage_blacklist = (($blacklist[1] * 100) / ($blacklist[0] + $blacklist[1]) );
			echo "<br /><br />Pax Blacklist� : ".$blacklist[1];
			echo "<br />Pax OK : ".$blacklist[0];
			echo "<br />% de blacklist : ".number_format($pourcentage_blacklist,2,',',' ').' %';
		}

		echo "<br /><br />Total : ".$prime2." �";
		
		echo "<h4>Transformation des dossiers</h4>";
		echo $objectif3." %";
		
		echo "<h4>Prime sur le CA</h4>";
		echo number_format($objectif4,2,',',' ')." �";
		
		echo "<br/><br/><center><a href='#' Onclick='window.close();'>Cliquez pour fermer la fen�tre</a></center>";
	}
}
?>

</div>