<?php
session_start();
if (!isset($_SESSION["id_vendeur"])) {
		   header("Location:index.php");
		   exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Statistiques Monagence&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="client.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
require_once("fonctions_db.php");
require_once("fonctions.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">

<?php

// pour l'ecriture

define('BASE_PATH',realpath( dirname(__FILE__)));

$file = BASE_PATH.'/venteParDate.csv';

$output  ="ID dossier;Vendeur;Nom client;TO;Ville Destination;Ville départ;Date de départ;Date de retour;Montant;Assurance;\n\r";


$total_dossiers = 0;
$total_assurances = 0;
$compteur = 0;
$montat_achat_total = 0;
$total_ht = 0;

$jour_courant		= date("d");
$mois_courant		= date("m");
$annee_courante	= date("y");

// $annee_courante = $annee_courante + 1;

$to = $_POST["to"];
$cpays = $_POST["cpays"];

$jour1 = $_POST["jourDe"];
$mois1 = $_POST["moisDe"];
$annee1 = $_POST["anneeDe"];

$jour2 = $_POST["jourA"];
$mois2 = $_POST["moisA"];
$annee2 = $_POST["anneeA"];

$dateDebut =$annee1."-".$mois1."-".$jour1;
$dateFin   =$annee2."-".$mois2."-".$jour2;


if (!empty($jour1)) {$jour_courant = $jour1;};
if (!empty($mois1)) {$mois_courant = $mois1;};
if (!empty($annee1)) {$annee_courante = $annee1;};


if (!empty($jour2)) {$jour_courant2 = $jour2;};
if (!empty($mois2)) {$mois_courant2 = $mois2;};
if (!empty($annee2)) {$annee_courante2 = $annee2;};


$date_comm = $_POST["date_comm"];
$type = $_POST["type"];
$commentaire = $_POST["commentaire"];



if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};


	echo "<h1>Visualisation du journal des ventes par Tour operateur : du  ".$dateDebut." au ".$dateFin."</h1>";


	echo "<p><form method='post' action='".$_SERVER['PHP_SELF']."?PHPSESSID=".session_id()."'><fieldset><legend>Choix de la période</legend>";
	
     //  de 
        echo ("<label for='jour'>DU : </label>\n\r");
	echo ("<select name='jourDe' id='jourDe'>\n\r");

	for ($i=1;$i<=31;$i++) {
		if (strlen($i) < 2){$j="0".$i;} else {$j=$i;};
		echo ("<option value='".$j."'");
		if ($i == $jour1) {echo("selected='selected'");};
		echo (">".$j."</option>\n\r");
	}

	echo ("</select>\n\r");
	echo ("<select name='moisDe' id='moisDe'>\n\r");
	for ($i=1;$i<=12;$i++) {
		if (strlen($i) < 2){$mois="0".$i;} else {$mois=$i;};
		echo ("<option value='".$mois."'");
		if ($i == $mois1) {echo("selected='selected'");};
		echo (">".$mois."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='anneeDe' id='anneeDe'>\n\r");
	for ($i=2012;$i<=2025;$i++) {
		echo ("<option value='".$i."'");
		if ($i == $annee1) {echo("selected='selected'");};
		echo (">".$i."</option>\n\r");
	}
	echo ("</select><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");

   // a
       echo ("<label for='jour'>AU : </label>\n\r");
	echo ("<select name='jourA' id='jourA'>\n\r");
	for ($i=1;$i<=31;$i++) {
		if (strlen($i) < 2){$jour="0".$i;} else {$jour=$i;};
		echo ("<option value='".$jour."'");
		if ($jour == $jour2) {echo("selected='selected'");};
		echo (">".$jour."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='moisA' id='moisA'>\n\r");
	for ($i=1;$i<=12;$i++) {
		if (strlen($i) < 2){$mois="0".$i;} else {$mois=$i;};
		echo ("<option value='".$mois."'");
		if ($mois == $mois2) {echo("selected='selected'");};
		echo (">".$mois."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='anneeA' id='anneeA'>\n\r");
	for ($i=2012;$i<=2025;$i++) {
		echo ("<option value='".$i."'");

		if ($i == $annee2) {echo("selected='selected'");};
		echo (">".$i."</option>\n\r");
	}
	echo ("</select><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");

       //to
	echo ("<label for='to'>Tour operateur</label>\n\r");

	$fieldOptions = mysql_enum_values('dossiers','to');
	echo "<select id ='to' name='to' size='1' onchange='Frais_Chronopost(this[this.selectedIndex].value);'>";
	echo("<option value='selectionnez'>Selectionnez</option>\n\r");
	echo ("<option value='toconfondu'> Tout TO confondus </option>");
	asort($fieldOptions);
	foreach($fieldOptions as $tmp)
	{
		echo "<option value='$tmp'";
		if ($tmp == trim($to)) {echo("selected='selected'");};
		echo ">$tmp</option>";
	}
	echo "</select><br/>";
	echo ("<br style='clear:both'/>\n\r");


      /* liste des pays*/
      $link = connection(MYDATABASE);
      $req1= " Select cpays, lpays_fr from code_pays where flag_dest ='1' order by lpays_fr asc";
      $result1=mysql_query($req1) or die(mysql_error());

     echo ("<label for='cpays'>Destination</label>\n\r");

      echo "<select id ='cpays' name='cpays' >";
      echo("<option value='0'>Selectionnez</option>\n\r");

      while($row = mysql_fetch_row($result1))
      {

              echo "<option value='".$row['0']."'";
		if ($row['0']== $cpays) {echo("selected='selected'");};
		echo ">".$row['1']."</option>";

      }
       echo "</select><br/>";
	echo ("<br style='clear:both'/>\n\r");

	echo "<input type='submit' name='Voir' id='Voir' value='RECHERCHER'></fieldset></form></p>";


  
	$annee_courante = substr($annee_courante,2,4);




echo("<h1>Journal des ventes du jour </h1>");

	$link = connection(MYDATABASE);
	$requete = "SELECT DISTINCT dossiers.id, dossiers.to, dossiers.dest_ville, dossiers.ville_depart, dossiers.date_deb, dossiers.date_fin, vendeurs.prenom, clients.nom, dossiers.prix_base_adulte, dossiers.prix_base_bebe, dossiers.adultes, dossiers.enfants
                   FROM `dossiers`, `clients`, `vendeurs`, `lien_dossier_vendeur` 
                   WHERE dossiers.id_client=clients.id 
                   AND dossiers.id=lien_dossier_vendeur.id_dossier 
                   AND lien_dossier_vendeur.id_vendeur=vendeurs.id 
                   AND (etat='En request' OR etat='Confirmé' OR etat='Préconfirmé') 
                   AND `date_confirm` >= '".$dateDebut."'
                   AND `date_confirm` <= '".$dateFin."'";

				   echo $_POST["to"];
                  if ( $_POST["to"] != "" and   $_POST["to"] != "selectionnez" and $_POST["to"] != "toconfondu"  )
                  {
			$requete .= "AND dossiers.to='".$to."'";

                  } 
                  if ( $_POST["cpays"] != "" and   $_POST["cpays"] != "0"  )
                  {
			$requete .= "AND dossiers.dest_pays='".$cpays."'";

                  } 

                     
                   $requete .=" ORDER BY dossiers.id;";


	$result=mysql_query($requete) or die(mysql_error());
	$num_rows = mysql_num_rows($result);

     //echo  $num_rows;

	if ($num_rows > 0) {
		echo("<table border='1' cellpadding='2' cellspacing='0'>");
		echo("<tr>");
		echo("<td><b>ID dossier</b></td>");
		echo("<td><b>Vendeur</b></td>");
		echo("<td><b>Nom client</b></td>");
		echo("<td><b>TO</b></td>");
		echo("<td><b>Ville Destination</b></td>");
		echo("<td><b>Ville départ</b></td>");
		echo("<td><b>Date de départ</b></td>");
		echo("<td><b>Date de retour</b></td>");
		echo("<td><b>Montant</b></td>");
		echo("<td><b>Assurance</b></td>");
		echo("<td><b>Montant H.T.</b></td>");
		echo("</tr>");
	}
	
	while($row = mysql_fetch_row($result))
	{
		$link = connection(MYDATABASE);
		$requete3 = "SELECT facture_achat, montant_achat FROM achats WHERE id_dossier='".$row[0]."';";
		$result3=mysql_query($requete3) or die(mysql_error());
		$num_rows3 = mysql_num_rows($result3);
		//echo $num_rows3;
		
		$facture_achat_temp = 0;
		$montant_achat_temp = 0;
		
		while($row3 = mysql_fetch_row($result3))
		{
			$facture_achat_temp = $row3[0];
			$montant_achat_temp = $row3[1];
			$montat_achat_total += $montant_achat_temp;
			//echo "ICI : ".$facture_achat.$montant_achat;
		}
		
		$total_assur = Total_assur($row[0]);
		$total_courant = Calcul_total_dossier($row[0]);
		$total_dossiers += $total_courant;
		$total_ht += ($row[8]*$row[10]+$row[9]*$row[11]);
		$total_assurances += $total_assur[1];

		$compteur++;


		echo("<tr>");
		echo("<td>".$row[0]."</td>");
		echo("<td>".$row[6]."</td>");
		echo("<td>".$row[7]."</td>");
		echo("<td>".$row[1]."</td>");
		echo("<td>".$row[2]."</td>");
		echo("<td>".$row[3]."</td>");
		echo("<td>".$row[4]."</td>");
		echo("<td>".$row[5]."</td>");
		echo("<td>".Calcul_total_dossier($row[0])."</td>");
		echo("<td>".$total_assur[1]."</td>");
		echo("<td>".($row[8]*$row[10]+$row[9]*$row[11])."</td>");
		echo("</tr>");

              $output.=$row[0].";".$row[6].";".$row[7].";".$row[1].";".$row[2].";".$row[3].";".$row[4].";".$row[5].";".Calcul_total_dossier($row[0]).";".$total_assur[1].";\n\r" ; 
	}

	if ($num_rows > 0) {
		echo("</table>");
	}


file_put_contents($file, $output);

if($total_dossiers > 0)
{
	$marge=100-(100*($montat_achat_total+$total_assurances)/$total_dossiers);
}

echo "<center> <hr><h1>Chiffre d'affaires :</h1> ".number_format($total_dossiers,2,',',' ')." ";
	if($compteur > 0)
	{
		echo "<hr><h1>Panier moyen ($compteur dossiers confirmés) :</h1> ".number_format($total_dossiers / $compteur,2,',',' ')." ";
	}

//echo "<hr><h1>Chiffre d'affaires Assurances :</h1> ".$total_assurances." ";

echo "<hr><h1>Chiffre d'affaires Assurances :</h1> ".$total_assurances." &euro;  </h1> <hr> <h1> Marge : </h1> ".number_format(($total_dossiers-($montat_achat_total+$total_assurances)),2,',', ' ')."&euro; (".number_format($marge,2,',',' ')."%) </center>";

echo "<hr><h1> Montant Total H.T. : ".number_format($total_ht,2,',',' ') ."&euro;</h1>";


?>
</td>
</tr>
</table>

</body></html>

