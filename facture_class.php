
<?php
require('class_fpdf/fpdf.php');

class Facture extends FPDF{
	
	function ajoutEntete($etat_dossier,$id_dossier,$id_client,$date,$vendeur){
		$this->Open();
		$this->SetFillColor(240,52,58);
		$this->SetMargins(15, 15, 15);
		$this->AddPage();
		$this->Image('logo_voyaneo_small.jpg',15,10,65,18,'','http://www.voyaneo.com');
		$this->Ln(20);
		$this->Cell(70,35,'',1,0,'L');
		$this->SetFont('Arial','B',12);
		
		if(($etat_dossier==="Demande")||($etat_dossier==="En request")){
			    $typedoc="Demande de devis";
				$this->SetFont('Arial','B',120);
				$this->SetTextColor(215,207,207); //(226,226,226); 
				$this->Text(50,180,"Devis") ;
				//$this->Text(20,180,"Proforma") ;
				$this->SetFont('Arial','',10);
				$this->SetTextColor(4,4,4);
		}
		else{
			    $typedoc="Facture No : $id_dossier";	
		}
		
		$this->SetFont('Arial','',10);
		$this->Text(18,40,"$typedoc") ;
		//references dossier
		$this->Text(18,45, "Dossier No : $id_dossier") ;
		$this->Text(18,50, "Code client : $id_client") ;
		$this->Text(18,55, "Contact : $vendeur") ;
		$this->Text(50,65, "Date : $date") ;
		$this->Cell(20);
		//references monagence
		$this->Cell(90,35,'',1,1,'L'); //1 bordure 1 =br
		$this->SetFont('Arial','B',14);
		$this->Text(110,40, "Voyaneo - voyaneo.com") ;
		$this->SetFont('Arial','',10);
		$this->Text(110,45, "12 rue Camille Desmoulins") ;
		$this->Text(110,50, "92300 Levallois Perret") ;

		if(($etat_dossier==="Demande")||($etat_dossier==="En request")){
			    $this->Text(110,55, "T�l.: 01 77 69 02 57") ;
		}
		else{
			    $this->Text(110,55, "T�l.: 01 77 69 02 57") ;	
		}


		
		$this->Text(110,60, "Fax : 01 77 69 02 58") ;
		$this->Text(110,65, "E-mail : info@voyaneo.com") ;
		
		//return $id_dossier;
			
	}

	function descriptionSejourClient($type_produit,$pays,$ville_arrivee,$ville_dep,$d_debut,$d_fin,$hotel,$pension,$passagers,$client,$chambre_type){
		$this->SetTextColor(4,4,4); 
		$this->Ln(5);
		$this->Cell(180,0,'',1,1,'');
		$this->SetFont('Arial','B',10);
	    $this->SetFillColor(215,207,207); 
		$this->Cell(90,5,'Description du s�jour',1,0,'L',1);
		$this->Cell(90,5,'Client',1,1,'L',1);
		$this->Ln(2);
		$pays = utf8_decode($pays);
		$ville_arrivee = utf8_decode($ville_arrivee);
		$ville_dep = utf8_decode($ville_dep);
		$hotel = utf8_decode($hotel);
		$pension = utf8_decode($pension);
		
		switch($type_produit){
			case "Vol sec" :
			    $this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Prestation ",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	Vol sec",0,1,'L');
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Destination ",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,": $pays",0,1,'L');
		
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Ville d�part",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$ville_dep",0,1,'L');
		
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Ville arriv�e",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$ville_arrivee",0,1,'L');
			break;
			
			case "Train" :
			    $this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Prestation ",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	Train",0,1,'L');
				
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Destination ",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$pays",0,1,'L');
		
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Ville d�part",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$ville_dep",0,1,'L');
		
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Ville arriv�e",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$ville_arrivee",0,1,'L');
			break;
			case "Hotel seul" :
			    $this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Prestation ",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,": H�tel",0,1,'L');
				
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Destination",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$pays",0,1,'L');
				
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"H�tel",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$hotel",0,1,'L');
				
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Type de chambre",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$chambre_type",0,1,'L');
				
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Formule",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$pension",0,1,'L');
			break;
			case "Croisiere" :
			$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Croisi�re",0,0,'L');
				$this->SetFont('Arial','B',8);
		    	$this->Cell(70,5,":	$hotel",0,1,'L');
				
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Ville d�part",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$ville_dep",0,1,'L');
		
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Ville arriv�e",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$ville_arrivee",0,1,'L');
				
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Type de chambre",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$chambre_type",0,1,'L');
				
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Formule",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$pension",0,1,'L');
			break;
			case "Location" :
			
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Prestation ",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,": Location",0,1,'L');
				
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Destination",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$pays",0,1,'L');
		
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Ville d�part",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$ville_dep",0,1,'L');
		
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Ville arriv�e",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$ville_arrivee",0,1,'L');
		
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Location",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$hotel",0,1,'L');
		
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Formule",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$pension",0,1,'L');
			break;
			default:
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Destination",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$pays",0,1,'L');
		
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Ville d�part",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$ville_dep",0,1,'L');
		
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Ville arriv�e",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$ville_arrivee",0,1,'L');
		
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"H�tel",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$hotel",0,1,'L');
		
				$this->SetFont('Arial','I',9);
				$this->Cell(20,5,"Formule",0,0,'L');
				$this->SetFont('Arial','B',8);
				$this->Cell(70,5,":	$pension",0,1,'L');
		}
	
		$this->SetFont('Arial','I',9);
		$this->Cell(20,5,"Date",0,0,'L');
		$this->SetFont('Arial','B',8);
		$this->Cell(70,5,":	$d_debut au $d_fin",0,1,'L');
		$this->SetFont('Arial','',7);
		$this->Cell(22,4,"",0,0,'L');$this->Cell(70,4,"France - France",0,1,'L');
		
		//ecriture des coordonnes du client
		$this->SetFont('Arial','I',9);
		$this->Text(106,85,$client['identite_tel']) ;
		$this->Text(106,90,$client['adresse']) ;
		$this->Text(106,95,$client['mail']) ;
	
		$nb_passagers=count($passagers);
		$nb_ligne1=round(count($passagers)/2);//nombre de pssagers par ligne
		$text_ligne1=""; //les passagers pour la ligne 
		$nb_ligne2=$nb_ligne1;//nombre de pssagers pour la ligne 2
		$text_ligne2="";
		
	  	for($i=0;$i<=$nb_ligne1;$i++){
	  		$text_ligne1=$text_ligne1.$passagers[$i].", ";
		}
		for($i=$nb_ligne2;$i<=$nb_passagers;$i++){
	  		$text_ligne2=$text_ligne2.$passagers[$i].", ";
		}
		
		$this->SetFont('Arial','B',9);
		$this->Text(106,100,"Passager(s) : ") ;
		$this->SetFont('Arial','I',8);
		//$this->Text(106,105,"$text_ligne1") ;   //ecriture des passagers
		//$this->Text(106,110,"$text_ligne2") ;
		
		for($indice=0;$indice<=round($nb_passagers/2)+2;$indice=$indice+2){
		
			  $lignepass = $passagers[$indice].','.$passagers[$indice+1];
			  $this->Text(106,105+($indice*2), $lignepass);
		}
	
	}
	
	function ajoutLigneQuantitePrix($designation,$mode_paiement,$acompte){

		$total_facture=0;
		$reduction_facture=0;
		
		$this->Ln(5);
		$this->SetFont('Arial','B',10);
		$this->SetFillColor(215,207,207); 
		$this->Cell(100,5,'D�signation',1,0,'C',1);
		$this->Cell(20,5,'Quantit�',1,0,'C',1);
		$this->Cell(30,5,'P.U',1,0,'C',1);
		$this->Cell(30,5,'Total',1,1,'C',1);
		$this->Ln(5);
		
		while (list ($cle, $quantite_prix) = each ($designation)) {
		
			//echo "<br/>$cle ** ".mb_detect_encoding($cle,true)."** ";
			//echo htmlspecialchars_decode(utf8_decode($cle));
			
		   $tab=explode(";",$quantite_prix);
		   $quantite=$tab[0];
		   $pu=$tab[1];
		  
		   if($pu!=="0.00" and $quantite!=="0"){
			   	//$cle = utf8_encode($cle);
			   	/*if (mb_detect_encoding($cle, 'UTF-8') == true)
			   	{ 
				   	 $cle = utf8_decode($cle); 
			    }	*/
				$red = "R�duction";
				$pos = strpos($cle,$red);
				if ($pos===false){   //si la designation n'est pas une r�duction le prix est positif
					$this->SetFont('Arial','',10);
					$this->Cell(100,5,$cle,0,0,'L');
					$this->Cell(20,5,$quantite,0,0,'C');
					$this->Cell(30,5,$pu,0,0,'R');
					$total=$quantite*$pu;
				}
				else{//si la designation est une r�duction le prix est n�gatif
					$this->SetFont('Arial','',10);
					$this->Cell(100,5,$cle,0,0,'L');
					$this->Cell(20,5,$quantite,0,0,'C');
					
				    $total=$quantite*$pu;
				    if($pu!==""){
				   		$pu="-".$pu;
					}
					$this->Cell(30,5,$pu,0,0,'R');
					$reduction_facture=$total;
					$total=	"-$total";
				}
				if($total=="0" ||$total=="-0"){
				   	$total="";
				}
				$this->Cell(30,5,$total,0,1,'R');
			    $total_facture=$total_facture+$total;//somme des totaux des lignes
			  }
			  	
			}
			//
			$this->Ln(5);
			$this->Cell(180,0,'',1,1,'');
			$this->Cell(100,5,"",0,0,'');
			$this->Cell(20,5,"",0,0,'');
			$this->Cell(30,5,"Total facture",0,0,'C');
			$this->SetFont('Arial','B',10);
			$this->Cell(30,5,"$total_facture �",0,1,'R');

			//
			
			
			$this->Ln(2);
			$this->Cell(90,5,"Acompte : $acompte �",0,0,'L');

                     //$this->Cell(70,5,"Acompte : 601.60 �",0,0,'C');// pour les cas 

			$this->SetFont('Arial','B',10);
			$this->Cell(60,5,'Reste � payer',0,0,'C');
			$this->SetFillColor(215,207,207);
			$this->SetFont('Arial','B',12);
			$montant_a_payer= $total_facture - $acompte;


                 	//echo $total_facture;echo '<br />';
                 	//echo $acompte;echo '<br />';
		   	//echo $montant_a_payer;echo '<br />';


                    /* *************
			Pour corriger le bug du total exponetiel -4.54747350886E-13 etc..
                     *************** */

                     if ($montant_a_payer < 0  ||  ctype_alnum($montant_a_payer) || preg_match("/E/i", $montant_a_payer ) )			
                     {
				$this->Cell(30,5,"0 �",1,1,'R',1);
			}
			else
			{
						  $montant_a_payer =round($montant_a_payer, 2);
                          $this->Cell(30,5,"$montant_a_payer �",1,1,'R',1);
			}
                   
                   
			//$this->Cell(180,0,'',1,1,'');
			$this->SetFont('Arial','B',9);
			$this->Cell(50,5,"Mode paiement : $mode_paiement",0,0,'L');
			//$this->Cell(30,5,'',0,0,'L',1);
			$this->Ln(3);
			$this->SetFont('Arial','B',10); 
			

                    // $this->Cell(30,5,"$montant_a_payer �",1,1,'R',1);

			//$this->Cell(30,5,"0 �",1,1,'R',1);


			return round($montant_a_payer, 2);   //retourne le montant � payer
							
	}
	
	
	function ajoutDesInclus($les_inclus,$aucune_assur){
		$this->Ln(3);
		if($aucune_assur===true){
			$this->SetFont('Arial','B',8);
			$this->Cell(180,4,"Assurance(s) refus�e(s) par le client",0,1,'L');	
		}
		$this->Ln(2);
		$this->SetFont('Arial','B',8);
		$this->Cell(180,4,"Le prix comprend :",0,1,'L');
		$this->SetFont('Arial','',7);
		foreach($les_inclus as $inclu){
			 if(!empty($inclu) && $inclu!=" "){
				$inclu = utf8_decode($inclu);
                $this->Cell(180,4," - $inclu",0,1,'L');
             }   
		}
		$this->Ln(3);
	}
	
	function ajoutFooter(){
		$coordonnes[0]="Si�ge Social : 12 rue Camille Desmoulins 92300 Levallois Perret ";
		$coordonnes[1]="Fax : 01 77 69 02 58";
		$coordonnes[2]="Voyaneo est une marque de Ethnosal SARL au capital de 7500 � - RCS Nanterre 751 870 627 ";
		$coordonnes[3]="IM09210053 - RC Gan eurocourtage N� 086833734 / Garantie APST Association ";

		$coordonnes[4]="N� TVA Intracommunautaire FR 8375187062700013";
		$coordonnes[5]="Internet : www.voyaneo.com";
		$this->SetFont('Arial','',6);
		$this->Cell(180,2,"FORMALITES :(A CE JOUR ET SOUS RESERVE DE MODIFICATION) PASSEPORT VALIDE 6 MOIS APRES LA DATE DE RETOUR",0,1,'L',0);
		$this->Cell(180,2,"VACCINS :(A CE JOUR ET SOUS RESERVE DE MODIFICATION )VOIR LE CONSULAT POUR LES VACCINS OBLIGATOIRES",0,1,'L',0);
		$this->Ln(1);
		$this->SetFont('Arial','',7);
		$this->SetFillColor(215,207,207); 
		foreach($coordonnes as $cor){
			$this->Cell(180,3,$cor,0,1,'C',1);
		}
	}
	function Footer(){
    //Positionnement � 1,5 cm du bas
    	$this->SetY(-65);
    //Police Arial italique 8
    //Num�ro de page centr�
    		$coordonnes[0]="Si�ge Social : 12 rue Camille Desmoulins 92300 Levallois Perret ";
		$coordonnes[1]="Fax : 01 77 69 02 58";
		$coordonnes[2]="Voyaneo et AgenceDeVoyage.com sont des marques de Ethnosal SARL au capital de 7500 � - RCS Nanterre 751 870 627 ";
		$coordonnes[3]="IM09210053 - RC Gan eurocourtage N� 086833734 / Garantie APST Association ";

		$coordonnes[4]="N� TVA Intracommunautaire FR 8375187062700013";
		$coordonnes[5]="Internet : www.voyaneo.com";
		$this->SetFont('Arial','',7);
		$this->Cell(180,3,"Si vous avez r�gl� un acompte, le solde de votre dossier sera pr�lev� automatiquement 1 mois avant la date de d�part sur la carte ayant servi � la r�servation.",0,1,'L',0);
		$this->Cell(180,3,"Merci de nous pr�venir par e-mail si vous souhaitez r�gler le solde par un autre moyen.",0,1,'L',0);
		$this->Cell(180,3,"",0,1,'L',0);
		$this->Cell(180,3,"FORMALITES : Passeport valide 6 mois apr�s la date de retour.",0,1,'L',0);
		$this->Cell(180,3,"Pour tous les pays n�cessitant un visa, merci de vous rapprocher du consulat du (des) pays concern�(s).",0,1,'L',0);
		$this->Cell(180,3,"  - Attention : Tous les passagers voyageant vers ou via les USA , doivent �tre en possession d'un passeport � lecture optique.",0,1,'L',0);
		$this->Cell(180,3,"  - Attention : un passeport individuel par adulte, un par enfant, un par b�b�.",0,1,'L',0);
		$this->Cell(180,3,"et suffit pour se rendre aux USA.",0,1,'L',0);
		$this->Cell(180,3,"Pour les passeports d�livr�s apr�s le 26 octobre 2005 : un passeport biom�trique avec puce �lectronique est demand� aux voyageurs. Pour ceux qui n'auraient pas ce type de",0,1,'L',0);
		$this->Cell(180,3,"passeport, ils auront l'obligation de solliciter un visa pour se rendre aux USA (pour tourisme ou affaires). (delai : Tres long � voir avec l'ambassade avant d'acheter votre ",0,1,'L',0);
		$this->Cell(180,3,"Attention � compter du 8 septembre 2010, pour tous voyages aux USA, il est imp�ratif de demander une autorisation �lectronique de voyage 'ESTA' - co�t 14$" ,0,1,'L',0);
		$this->Cell(180,3,"Lien officiel : http://esta.cbp.dhs.gov/" ,0,1,'L',0);
		$this->Cell(180,3,"",0,1,'L',0);
		$this->Cell(180,3,"VACCINS : Merci de consulter votre centre de vaccination (m�decin) et pour plus d'informations : http://www.pasteur.fr/fr/map" ,0,1,'L',0);
		
		$this->Ln(1);
		$this->SetFont('Arial','',7);
		$this->SetFillColor(215,207,207); 
		foreach($coordonnes as $cor){
			$this->Cell(180,3,$cor,0,1,'C',1);
		}
	}
	
}

?> 

