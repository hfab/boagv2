-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Jeu 26 Mars 2015 à 10:30
-- Version du serveur: 5.5.35
-- Version de PHP: 5.4.4-14+deb7u8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `voyagemotion`
--

-- --------------------------------------------------------

--
-- Structure de la table `vendeurs`
--

CREATE TABLE IF NOT EXISTS `vendeurs` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `nom` varchar(50) NOT NULL DEFAULT '',
  `prenom` varchar(50) NOT NULL DEFAULT '',
  `rang` enum('Ancien','Vendeur','BackOffice','Gestionnaire','Admin','Callcenter') NOT NULL DEFAULT 'Vendeur',
  `mail` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=77 ;

--
-- Contenu de la table `vendeurs`
--

INSERT INTO `vendeurs` (`id`, `login`, `password`, `nom`, `prenom`, `rang`, `mail`) VALUES
(76, 'CDD', '0cf3577c1690ebcb3b792e12437a473b', 'CDD', 'Comiteo', 'Callcenter', ''),
(75, 'Houda', 'b44b333e6ced5fc9c068cb8b223d8b07', '', 'Houda', 'Gestionnaire', 'houda@agencedevoyage.com'),
(74, 'natacha', 'de6e5685f289945d0d5ad039e9f5f286', '', 'Natacha', 'Gestionnaire', 'natacha@agencedevoyage.com'),
(73, 'Sandra', '586bfb943e4dd9886a32551dec443fec', 'Bello', 'Sandra', 'Gestionnaire', 'sandra@agencedevoyage.com'),
(72, 'Voni', '1bc1bd6fba54090e49ca2a4d541594b0', '', 'Voni', 'Gestionnaire', 'voni@agencedevoyage.com'),
(71, 'ecall', '7eece9b85c0af4b7b3c0412df39ebb03', 'Ecall', 'Ecall', 'Callcenter', 'info@agencedevoyage.com'),
(70, 'Roxana', '85acaadf8fea166e334382f3ad233170', '', 'Roxana', 'Gestionnaire', 'roxana@agencedevoyage.com'),
(69, 'Eva', '13ad4bc32820213e56dd3405c74e2460', 'Eva', 'Eva', 'Gestionnaire', 'eva@agencedevoyage.com'),
(68, 'Julien', '4a12a67f9b6afc2cfc426f8e1ae6f37b', 'Marliac', 'Julien', 'Admin', 'julien@comptoirdesdeals.com'),
(67, 'Yves', 'ee92632a6b86029eb13eac5b9bbb675e', 'Chaponic', 'Yves', 'Gestionnaire', 'yves@agencedevoyage.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
