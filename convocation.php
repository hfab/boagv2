<?php
session_start();
if (!isset($_SESSION["id_vendeur"])) {
	header("Location:index.php");
	exit();
}


require_once('/home/agencedevoyage/lib/PHPMailer/class.phpmailer.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Envoi de convocations AgenceDeVoyage&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="client.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
require_once("fonctions_db.php");
require_once("fonctions.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px" valign="top">
<?php

$id_dossier_ext =  $_GET["id"];
$id_dossier = $_POST["id_dossier"];
$envoi_convoc = $_POST["envoi_convoc"];


function get_extension($filename)
{
	$parts = explode('.',$filename);
	$last = count($parts) - 1;
	$ext = $parts[$last];
	$ext = strtolower($ext);
	return $ext;
}

if(isset($envoi_convoc))
{

	$link=connection(MYDATABASE);
	$requete = "SELECT vendeurs.mail, clients.mail, vendeurs.nom, vendeurs.prenom, clients.civilite, clients.nom FROM clients, vendeurs, dossiers, lien_dossier_vendeur WHERE dossiers.id=$id_dossier AND dossiers.id=lien_dossier_vendeur.id_dossier AND clients.id=dossiers.id_client AND vendeurs.id=lien_dossier_vendeur.id_vendeur";
	$result=mysql_query($requete) or die(mysql_error().$requete);

	while($row = mysql_fetch_row($result)){
		$destinataire = $row[1];
		$expediteur   = $row[0];
		//$vendeur = $row[3]." ".$row[2];
              $vendeur = $row[3];

		//$hotel = $row[4];
		$civilite=$row[4];
		$nom=$row[5];
	}

	$link=connection(MYDATABASE);
	$requete2 = "SELECT mail FROM passagers WHERE passagers.id_dossier=$id_dossier";
	$result2=mysql_query($requete2) or die(mysql_error());
	while($row2 = mysql_fetch_row($result2)){
		if($row2[0] != "" && strpos($row2[0],"@"))
		{
			$destinataire .= ", ".$row2[0];
		}
	}


	$name_file = $_FILES['convoc']['name'];
	$source=$_FILES['convoc']['tmp_name'];
	$extension=get_extension($name_file);
	//echo $extension;

	$name_file2 = $_FILES['convoc2']['name'];
	$source2=$_FILES['convoc2']['tmp_name'];
	$extension2=get_extension($name_file2);
	//echo $extension2;

	$name_file3 = $_FILES['convoc3']['name'];
	$source3=$_FILES['convoc3']['tmp_name'];
	$extension3=get_extension($name_file3);
	//echo $extension3;

	$name_file4 = $_FILES['convoc4']['name'];
	$source4=$_FILES['convoc4']['tmp_name'];
	$extension4=get_extension($name_file4);
	//echo $extension4;


	$name_file5 = $_FILES['convoc5']['name'];
	$source5=$_FILES['convoc5']['tmp_name'];
	$extension5=get_extension($name_file5);
	//echo $extension5;

	if($extension != "tiff" && $extension != "pdf" && $extension !="doc" && $extension !="rtf" && $extension !="jpg" && $extension !="jpeg")
	{
		die("Extension : $extension. Fichier interdit. La convocation doit etre un PDF, RTF, JPG ou un DOC");
	}
	else
	{
		move_uploaded_file($source, "./convocation/".$name_file);
		$type="text/plain";
		if($extension=="doc")
		{
			$type="application/msword";
		}
		else if($extension=="pdf")
		{
			$type="application/pdf";
		}
		else if($extension=="rtf")
		{
			$type="application/rtf";
		}
		else if($extension=="jpg")
		{
			$type="image/jpeg";
		}
		else if($extension=="jpeg")
		{
			$type="image/jpeg";
		}
		else if($extension=="tiff")
		{
			$type="image/tiff";
		}
	}

	if($name_file2 != "")
	{
		if($extension2 != "pdf" && $extension2 !="doc" && $extension2 !="rtf" && $extension2 !="jpg")
		{
			die("Fichier interdit. La convocation doit etre un PDF, RTF, JPG ou un DOC");
		}
		else
		{
			move_uploaded_file($source2, "./convocation/".$name_file2);
			$type2="text/plain";
			if($extension2=="doc")
			{
				$type2="application/msword";
			}
			else if($extension2=="pdf")
			{
				$type2="application/pdf";
			}
			else if($extension2=="rtf")
			{
				$type2="application/rtf";
			}
			else if($extension2=="jpg")
			{
				$type2="image/jpeg";
			}
			else if($extension=="jpeg")
			{
				$type="image/jpeg";
			}
			else if($extension2=="tiff")
			{
				$type2="image/tiff";
			}
		}
	}



	if($name_file3 != "")
	{
		if($extension3 != "pdf" && $extension3 !="doc" && $extension3 !="rtf" && $extension3 !="jpg")
		{
			die("Fichier interdit. La convocation doit etre un PDF, RTF, JPG ou un DOC");
		}
		else
		{
			move_uploaded_file($source3, "./convocation/".$name_file3);
			$type3="text/plain";
			if($extension3=="doc")
			{
				$type3="application/msword";
			}
			else if($extension3=="pdf")
			{
				$type3="application/pdf";
			}
			else if($extension3=="rtf")
			{
				$type3="application/rtf";
			}
			else if($extension3=="jpg")
			{
				$type3="image/jpeg";
			}
			else if($extension=="jpeg")
			{
				$type="image/jpeg";
			}
			else if($extension3=="tiff")
			{
				$type3="image/tiff";
			}
		}
	}


	if($name_file4 != "")
	{
		if($extension4 != "pdf" && $extension4 !="doc" && $extension4 !="rtf" && $extension4 !="jpg")
		{
			die("Fichier interdit. La convocation doit etre un PDF, RTF, JPG ou un DOC");
		}
		else
		{
			move_uploaded_file($source4, "./convocation/".$name_file4);
			$type4="text/plain";
			if($extension4=="doc")
			{
				$type4="application/msword";
			}
			else if($extension4=="pdf")
			{
				$type4="application/pdf";
			}
			else if($extension4=="rtf")
			{
				$type4="application/rtf";
			}
			else if($extension4=="jpg")
			{
				$type4="image/jpeg";
			}
			else if($extension=="jpeg")
			{
				$type="image/jpeg";
			}
			else if($extension4=="tiff")
			{
				$type4="image/tiff";
			}
		}
	}

	if($name_file5 != "")
	{
		if($extension5 != "pdf" && $extension5 !="doc" && $extension5 !="rtf" && $extension5 !="jpg")
		{
			die("Fichier interdit. La convocation doit etre un PDF, RTF, JPG ou un DOC");
		}
		else
		{
			move_uploaded_file($source5, "./convocation/".$name_file5);
			$type5="text/plain";
			if($extension5=="doc")
			{
				$type5="application/msword";
			}
			else if($extension5=="pdf")
			{
				$type5="application/pdf";
			}
			else if($extension5=="rtf")
			{
				$type5="application/rtf";
			}
			else if($extension5=="jpg")
			{
				$type5="image/jpeg";
			}
			else if($extension=="jpeg")
			{
				$type="image/jpeg";
			}
			else if($extension5=="tiff")
			{
				$type5="image/tiff";
			}
		}
	}
	//----------------------------------
	// Construction de l'entête
	//----------------------------------

	$boundary = "-----=".md5(uniqid(rand()));

	$header = "MIME-Version: 1.0\r\n";

	$header .= "Content-Type: multipart/mixed; boundary=\"$boundary\"\r\n";
	$header .= "\r\n";



	$msg = "Votre lecteur de mail ne semble pas compatible. Merci de contacter AgenceDeVoyage.com au 01 77 69 02 57\r\n";


	$msg .= "--$boundary\r\n";


	$msg .= "Content-Type: text/html; charset=\"utf-8\"\r\n";

	$msg .= "Content-Transfer-Encoding:8bit\r\n";
	$msg .= "\r\n";

	$msg .= "<style>*{font-family:Verdana}</style><center><img src='http://www.agencedevoyage.com/img/header/logo.jpg'></center><br/><br/>";
	$msg .= "\r\n";
	$msg .= "Bonjour ".$civilite." ".$nom.",<br/><br/>\r\n";
	//$msg .= "Veuillez trouver ci-joint votre convocation aéroport.<br/>\r\n";
	//$msg .= "Imprimez la et rendez vous avec à l'aéroport. <br/><br/>\r\n\r\n";
	//$msg .= "Vous pouvez retrouver toutes les informations concernant votre voyage sur monagence.com (rubrique suivi de commande) ou en <a href='http://www.monagence.com/consultation_dossier/'>cliquant ici</a>. <br/><br/>\r\n\r\n";
	//$msg .= "<br/>En vous souhaitant un bon voyage. <br/>\r\n";
	$msg .= " Vous trouverez ci-joints les documents de voyage concernant votre réservation. <br/>\r\n
<br/>\r\n
			VEUILLEZ LES IMPRIMER EN INTEGRALITE : ils seront demandes tout au long du voyage. Ils sont indispensables au bon déroulement des vacances. <br/>\r\n
<br/>\r\n
En vous souhaitant un bon voyage,";
	$msg .= "<br/>\r\n";
	$msg .= "Cordialement, <br/>\r\n";
	if($vendeur!="Ecall")
		$msg .= $vendeur."<br/>\r\n";
	$msg .= "Conseiller voyage <br/>\r\n";
	$msg .= "Agencedevoyage.com <br/>\r\n";
	$msg .= "Tél : 01 77 69 02 57<br/>\r\n";
	$msg .= "Fax : 01 77 69 02 58 <br/>\r\n";
	$msg .= $expediteur."<br/>\r\n";
	$msg .= "\r\n\r\n";

	if($name_file != "")
	{
		$file = "./convocation/".$name_file;
		$fp = fopen($file, "rb");
		$attachment = fread($fp, filesize($file));
		fclose($fp);

		$attachment = chunk_split(base64_encode($attachment));

		$msg .= "\r\n\r\n";
		$msg .= "--$boundary\r\n";
		$msg .= "Content-Type: $type; name=\"$name_file\"\r\n";
		$msg .= "Content-Transfer-Encoding: base64\r\n";
		$msg .= "Content-Disposition: attachment; filename=\"$name_file\"\r\n";
		$msg .= "\r\n";
		$msg .= $attachment . "\r\n";
		$msg .= "\r\n\r\n";
	}



	if($name_file2 != "")
	{
		$file2 = "./convocation/".$name_file2;
		$fp2 = fopen($file2, "rb");
		$attachment2 = fread($fp2, filesize($file2));
		fclose($fp2);

		$attachment2 = chunk_split(base64_encode($attachment2));

		$msg .= "--$boundary\r\n";
		$msg .= "Content-Type: $type2; name=\"$name_file2\"\r\n";
		$msg .= "Content-Transfer-Encoding: base64\r\n";
		$msg .= "Content-Disposition: attachment; filename=\"$name_file2\"\r\n";
		$msg .= "\r\n";
		$msg .= $attachment2 . "\r\n";
		$msg .= "\r\n\r\n";
	}


	if($name_file3 != "")
	{
		$file3 = "./convocation/".$name_file3;
		$fp3 = fopen($file3, "rb");
		$attachment3 = fread($fp3, filesize($file3));
		fclose($fp3);

		$attachment3 = chunk_split(base64_encode($attachment3));

		$msg .= "--$boundary\r\n";
		$msg .= "Content-Type: $type3; name=\"$name_file3\"\r\n";
		$msg .= "Content-Transfer-Encoding: base64\r\n";
		$msg .= "Content-Disposition: attachment; filename=\"$name_file3\"\r\n";
		$msg .= "\r\n";
		$msg .= $attachment3 . "\r\n";
		$msg .= "\r\n\r\n";
	}


	if($name_file4 != "")
	{
		$file4 = "./convocation/".$name_file4;
		$fp4 = fopen($file4, "rb");
		$attachment4 = fread($fp4, filesize($file4));
		fclose($fp4);

		$attachment4 = chunk_split(base64_encode($attachment4));

		$msg .= "--$boundary\r\n";
		$msg .= "Content-Type: $type4; name=\"$name_file4\"\r\n";
		$msg .= "Content-Transfer-Encoding: base64\r\n";
		$msg .= "Content-Disposition: attachment; filename=\"$name_file4\"\r\n";
		$msg .= "\r\n";
		$msg .= $attachment4 . "\r\n";
		$msg .= "\r\n\r\n";
	}


	if($name_file5 != "")
	{
		$file5 = "./convocation/".$name_file5;
		$fp5 = fopen($file5, "rb");
		$attachment5 = fread($fp5, filesize($file5));
		fclose($fp5);

		$attachment5 = chunk_split(base64_encode($attachment5));

		$msg .= "--$boundary\r\n";
		$msg .= "Content-Type: $type5; name=\"$name_file5\"\r\n";
		$msg .= "Content-Transfer-Encoding: base64\r\n";
		$msg .= "Content-Disposition: attachment; filename=\"$name_file5\"\r\n";
		$msg .= "\r\n";
		$msg .= $attachment5 . "\r\n";
		$msg .= "\r\n\r\n";
	}



	//Fichier D'info aux voyageurs
	$file6 = "./pdf/Informations_aux_voyageurs.pdf";
	$fp6 = fopen($file6, "rb");
	$attachment6 = fread($fp6, filesize($file6));
	fclose($fp6);

	$attachment6 = chunk_split(base64_encode($attachment6));

	$msg .= "--$boundary\r\n";
	$msg .= "Content-Type: application/pdf; name=\"Informations_aux_voyageurs.pdf\"\r\n";
	$msg .= "Content-Transfer-Encoding: base64\r\n";
	$msg .= "Content-Disposition: attachment; filename=\"Informations_aux_voyageurs.pdf\"\r\n";
	$msg .= "\r\n";
	$msg .= $attachment6 . "\r\n";
	$msg .= "\r\n\r\n";


	$reponse = $expediteur;

	//Hack de test a commenter pour envoyer au client
	//$destinataire="webmaster@medisite.fr";
	// && mail($expediteur, "COPIE - Monagence.com : Envoi de convocation (Dossier n°$id_dossier) - $destinataire", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header)
	if (mail($destinataire, "AgenceDeVoyage.com : Envoi de convocation (Dossier n°$id_dossier)", $msg, "Reply-to: ".$reponse."\r\nFrom: ".$expediteur."\r\n".$header, "-f".$expediteur."") && mail($expediteur, "COPIE - agencedevoyage.com : Envoi de convocation (Dossier n°$id_dossier) - $destinataire", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header))
	{
		echo "Le mail a bien été envoyé à <b>$destinataire</b> de la part de <b>$expediteur</b> avec la convocation : <b>$file</b>, <b>$file2</b>, <b>$file3</b>, <b>$file4</b>, <b>$file5</b> et <b>$file6</b><br/>";
		$link = connection(MYDATABASE);
		$requete = "SELECT *  FROM `envoi` WHERE id_dossier='".$id_dossier."';";
		$result=mysql_query($requete) or die(mysql_error());
		$num_rows = mysql_num_rows($result);

		$jour_courant=date("d");
		$mois_courant=date("m");
		$annee_courante=date("Y");
		$date_envoi = $annee_courante."-".$mois_courant."-".$jour_courant;



		//echo $date_envoi;

		if ($num_rows > 0) {
			$requete2 = "UPDATE envoi SET mode_envoi='Mail', date_envoi='".$date_envoi."' WHERE id_dossier='".$id_dossier."'";
		}
		else
		{
			$requete2 = "INSERT INTO envoi (id_dossier, mode_envoi, date_envoi) VALUES ('".$id_dossier."','Mail','".$date_envoi."');";
		}

		//echo $requete2;

		$result2=mysql_query($requete2) or die(mysql_error());
		if ($result2) {echo("Envoi de la convocation enregistré<br/>");};


		//mailConvocSent($destinataire);

	}






	if($name_file != "")
	{
		//upload_convoc($name_file, $id_dossier);
		unlink("./convocation/".$name_file);
	}

	if($name_file2 != "")
	{
		//upload_convoc($name_file2, $id_dossier);
		unlink("./convocation/".$name_file2);
	}

	if($name_file3 != "")
	{
		//upload_convoc($name_file3, $id_dossier);
		unlink("./convocation/".$name_file3);
	}

	if($name_file4 != "")
	{
		//upload_convoc($name_file4, $id_dossier);
		unlink("./convocation/".$name_file4);
	}

	if($name_file5 != "")
	{
		//upload_convoc($name_file5, $id_dossier);
		unlink("./convocation/".$name_file5);
	}

	echo("<br/><a href='convocation.php'>Envoyer une autre convocation</a>");
}
else
{
	echo("<form enctype='multipart/form-data' action='' method='post'>");
	echo("<fieldset><legend>Envoi de la convocation</legend>");
	echo("<label for='id_dossier'>ID Dossier</label><input type='text' name='id_dossier' id='id_dossier' value='".$id_dossier_ext."'><br>");
	echo("<label for='convoc'>Convocation pièce N°1</label><input type='file' id='convoc' name='convoc'><br>");
	echo("<label for='convoc2'>Convocation pièce N°2</label><input type='file' id='convoc2' name='convoc2'>");
	echo("<label for='convoc3'>Convocation pièce N°3</label><input type='file' id='convoc3' name='convoc3'>");
	echo("<label for='convoc4'>Convocation pièce N°4</label><input type='file' id='convoc4' name='convoc4'>");
	echo("<label for='convoc5'>Convocation pièce N°5</label><input type='file' id='convoc5' name='convoc5'>");
	echo("<input type='hidden' name='envoi_convoc' id='envoi_convoc' value='ok'><br>");
	echo("<input type='submit' value='Envoyer'>");
	echo("</fieldset></form>");
}


function SuppAccents($chaine){
	$tofind = "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ";
	$replac = "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn";
	return(strtr($chaine,$tofind,$replac));
}

function upload_convoc($file, $id_dossier)
{
	$file_dest = str_replace(' ', '', $file);
	$file_dest = SuppAccents($file_dest);

	//Param ftp
	$ftp_server = "lamporchestra.orchestra-platform.com";
	$ftp_user_name = "monagence_serviceclt";
	$ftp_user_pass = "5AXekA85uh";

	// set up basic connection
	$conn_id = ftp_connect($ftp_server);

	// login with username and password
	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

	// check connection
	if ((!$conn_id) || (!$login_result)) {echo "FTP connection has failed!<br/>"; echo "Attempted to connect to $ftp_server for user $ftp_user_name"; exit;} else {echo "Connected to $ftp_server, for user $ftp_user_name";}

	$dest = "/convocations/".$id_dossier."/".$file_dest;
	$source = "./convocation/".$file;
	// upload the file
	@ftp_mkdir($conn_id, "/convocations/".$id_dossier);
	$upload = ftp_put($conn_id, $dest, $source, FTP_BINARY);
	// check upload status
	if (!$upload) {echo "FTP upload has failed!";}
	else
	{
		echo "Uploaded $source_file to $ftp_server as $destination_file";
		$link = connection(MYDATABASE);
		$requete = "
			INSERT INTO `convocations` (`id_dossier` , `nom` )
			VALUES (
			'".$id_dossier."', '".$file_dest."'
			);";

		$result=mysql_query($requete) or die(mysql_error());
	}

	// close the FTP stream
	ftp_close($conn_id);
}



function mailConvocSent($email)
{

	$mail = new PHPMailer();

	$mail->From     = "resa@agencedevoyage.com";
	$mail->FromName = "Agencedevoyage.com";
	$mail->Subject = "Votre convocation vient d'etre envoyee";

	$mail->ConfirmReadingTo = "info@agencedevoyage.com";
	$mail->addCustomHeader("Return-Receipt-To: <info@agencedevoyage.com>");

	$text = 'Votre lecteur d\'email ne comprend pas l\'html.\n\r\n\r';
	$text .= 'Nous venons de vous envoyer votre convocation et nous vous en souhaitons une bonne reception.\n\r\n\r';
	$text .= 'Merci, et &agrave; bientot sur Agencedevoyage.com !';

	//Html
	$html = "<style>*{font-family:Verdana}</style><center><a href='http://www.agencedevoyage.com/'><img src='http://www.agencedevoyage.com/img/header/logo.jpg'></a></center><br/><br/>Bonjour, <br /><br />";

	$html .= "Nous venons de vous envoyer votre convocation et nous vous en souhaitons une bonne reception.<br /><br />";

	$html .= 'Merci, et &agrave; bientot sur Agencedevoyage.com !<br /><br /></body></html>';

	$mail->Body    = $html;
	$mail->AltBody = $text;

	$mail->AddAddress($email);
	if(!$mail->Send())
	{
		echo "There has been a mail error sending to " . $email . "<br>";
		return false;
	}
	else
	{
		return true;
	}
}
?>
</td>
</tr>
</table>

</body></html>

