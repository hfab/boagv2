<?php
session_start();
if (!session_is_registered("id_vendeur")) {
		   header("Location:index.php");
		   exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Statistiques Monagence&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<link href="client.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
require_once("fonctions_db.php");
require_once("fonctions.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">
<?php

$jour = $_POST["jour"];
$mois = $_POST["mois"];
$annee = $_POST["annee"];

/*
$jour1 = $_POST["jourDe"];
$mois1 = $_POST["moisDe"];
$annee1 = $_POST["anneeDe"];

$jour2 = $_POST["jourA"];
$mois2 = $_POST["moisA"];
$annee2 = $_POST["anneeA"];

$dateDebut =$annee1."-".$mois1."-".$jour1;
$dateFin   =$annee2."-".$mois2."-".$jour2;

if (!empty($jour1)) {$jour_courant = $jour1;};
if (!empty($mois1)) {$mois_courant = $mois1;};
if (!empty($annee1)) {$annee_courante = $annee1;};

if (!empty($jour2)) {$jour_courant2 = $jour2;};
if (!empty($mois2)) {$mois_courant2 = $mois2;};
if (!empty($annee2)) {$annee_courante2 = $annee2;};

*/
$jour_courant=date("d");
$mois_courant=date("m");
$annee_courante=date("y");

$id_dossier=$_POST["id_dossier"];
$mode_envoi=$_POST["mode_envoi"];
$jour_envoi=$_POST["jour_envoi"];
$mois_envoi=$_POST["mois_envoi"];
$annee_envoi=$_POST["annee_envoi"];


if (!empty($jour)) {$jour_courant = $jour;};
if (!empty($mois)) {$mois_courant = $mois;};
if (!empty($annee)) {$annee_courante = $annee;};

$date_envoi = $annee_envoi."-".$mois_envoi."-".$jour_envoi;

if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};

//Si un formulaire a ete envoye, on insere la date d'envoi
if (isset($id_dossier))
{
	$link = connection(MYDATABASE);
	$requete = "SELECT *  FROM `envoi` WHERE id_dossier='".$id_dossier."';";
	$result=mysql_query($requete) or die(mysql_error());
	
	$num_rows = mysql_num_rows($result);
	if ($num_rows > 0) {
		$requete2 = "UPDATE envoi SET mode_envoi='".$mode_envoi."', date_envoi='".$date_envoi."' WHERE id_dossier='".$id_dossier."'";
	}
	else
	{
		$requete2 = "INSERT INTO envoi (id_dossier, mode_envoi, date_envoi) VALUES ('".$id_dossier."','".$mode_envoi."','".$date_envoi."');";
	}
	$result2=mysql_query($requete2) or die(mysql_error());
	if ($result2) {echo("Requete inseree<br/>");};
}
	
	

	echo "<h1>Visualisation des dossiers - date de d�part le : ".$jour_courant." / ".$mois_courant." / ".$annee_courante."</h1>";
	echo("<p align='center'><a href='stats4_mois.php'>Visualisation tous les dossiers par date de d�part</a></p>");
	echo "<p><form method='post' action='".$_SERVER['PHP_SELF']."?PHPSESSID=".session_id()."'><fieldset><legend>Choix de la p�riode</legend>";
	

echo ("<label for='jour'>Voir une autre p�riode : </label>\n\r");
	echo ("<select name='jour' id='jour'>\n\r");
	for ($i=1;$i<=31;$i++) {
		if (strlen($i) < 2){$jour="0".$i;} else {$jour=$i;};
		echo ("<option value='".$jour."'");
		if ($jour == $jour_courant) {echo("selected='selected'");};
		echo (">".$jour."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='mois' id='mois'>\n\r");
	for ($i=1;$i<=12;$i++) {
		if (strlen($i) < 2){$mois="0".$i;} else {$mois=$i;};
		echo ("<option value='".$mois."'");
		if ($mois == $mois_courant) {echo("selected='selected'");};
		echo (">".$mois."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='annee' id='annee'>\n\r");
	for ($i=2005;$i<=($annee_courante+1);$i++) {
		echo ("<option value='".$i."'");
		if ($i == $annee_courante) {echo("selected='selected'");};
		echo (">".$i."</option>\n\r");
	}
	echo ("</select><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");

  

	echo "<input type='submit' name='Voir' id='Voir' value='Voir'></fieldset></form></p>";



	echo("<h1>Dossiers avec d�parts proches</h1>");
	$link = connection(MYDATABASE);
	$requete = "
	SELECT 
	dossiers.id, 
	dossiers.date_deb, 
	dossiers.id_client, 
	clients.nom, 
	dossiers.to AS tour_op, 
	dossiers.ref_to  
	FROM 
	`dossiers`, 
	`clients` 
	WHERE 
	clients.id=dossiers.id_client 
	AND 
	(dossiers.etat='Confirm�' OR dossiers.etat='Pr�confirm�') 
	AND
	YEAR(dossiers.date_deb) = '".$annee_courante."'
	AND
	MONTH(dossiers.date_deb) = '".$mois_courant."'
	AND
	DAY(dossiers.date_deb) = '".$jour_courant."'
	AND
	TO_DAYS(dossiers.date_deb)-TO_DAYS(now()) > -1 
	ORDER BY dossiers.date_deb;";
	//echo $requete;
	$result=mysql_query($requete) or die(mysql_error());
	$num_rows = mysql_num_rows($result);
	if ($num_rows > 0) {
		echo("<table border='1' cellpadding='2' cellspacing='0'>");
		echo("<tr>");
		echo("<td><b>ID dossier</b></td>");
		echo("<td><b>Date de d�part</b></td>");
		echo("<td><b>Nom du client</b></td>");
		echo("<td><b>Solde</b></td>");
		echo("<td><b>TO</b></td>");
		echo("<td><b>Envoi convocation</b></td>");
		echo("</tr>");
	}
	while($row = mysql_fetch_row($result))
	{


		$total_dossier = Calcul_total_dossier($row[0]);
		$total_acompte = Calcul_total_acompte($row[0]);
		$restant=$total_dossier-$total_acompte;
		if($restant > 0) {$bgcolor = 'red';} else {$bgcolor='white'; }
		echo("<tr bgcolor='".$bgcolor."'>");
		echo("<td>".$row[0]."<br/>
<form  target='_blank' method='post' action='search_dossier.php?PHPSESSID=".session_id()."'>
<input type='hidden' value='".$row[0]."' name='id_dossier'>
<input type='hidden' value='modifier' name='flag'>
<input type='submit' class='submit' value='Dossier' style='width:60px;'>
</form>
<br/>
<form action='/scripts/gestion_clients/search_client.php?PHPSESSID=".session_id()."' target='_blank' name='recherche' id='recherche' method='post'>
<input type='hidden' name='id_client' id='id_client' value='".$row[2]."'>
<input type='hidden' name='flag' id='flag' value='modifier'>
<input type='submit' class='submit' name='Client' id='Client' value='Client' style='width:60px;'>
</form><br/>
<a href='convocation.php?id=".$row[0]."' target='_blank'>Convocation</a>
</td>");
		echo("<td>".$row[1]."</td>");
		echo("<td>".$row[3]."</td>");
		echo("<td>".$restant."</td>");
		echo("<td>".$row[4]."<br/>Ref. : ".$row['5']."</td>");

		
		
		$mode_envoi="";
		$date_envoi="";
		$bg_envoi="white";
		//Envoye ou non ?
		$link = connection(MYDATABASE);
		$requete2 = "SELECT *  FROM `envoi` WHERE id_dossier='".$row[0]."';";
		$result2=mysql_query($requete2) or die(mysql_error());
		if(mysql_num_rows($result2)>0)
		{
			while($row2 = mysql_fetch_row($result2))
			{
				$mode_envoi = $row2[3];
				$date_envoi = $row2[2];
				$jour_envoi = substr($date_envoi, 8,2);
				$mois_envoi = substr($date_envoi, 5,2);
				$annee_envoi = substr($date_envoi, 0,4);
				$bg_envoi = "#DFDFDF";
			}
		}
		else
		{
			$jour_envoi=date("d");
			$mois_envoi=date("m");
			$annee_envoi=date("y");			
		}
		echo("<td width='300' bgcolor='".$bg_envoi."'><form action='".$_SERVER["PHP_SELF"]."?PHPSESSID=".session_id()."' name='envoi' id='envoi' method='post'>");
		
		echo ("<label for='mail'>Mail</label>\n\r");
		echo ("<input type='radio' name='mode_envoi' id='mail' value='Mail'");
		if ($mode_envoi == "Mail") {echo ("checked='checked'");}
		echo ("><br/>\n\r");
		echo ("<label for='fax'>Fax</label>\n\r");
		echo ("<input type='radio' name='mode_envoi' id='fax' value='Fax'");
		if ($mode_envoi == "Fax") {echo ("checked='checked'");}
		echo ("><br/>\n\r");
		echo ("<label for='courrier'>Courrier</label>\n\r");
		echo ("<input type='radio' name='mode_envoi' id='courrier' value='Courrier'");
		if ($mode_envoi == "Courrier") {echo ("checked='checked'");}
		echo ("><br/>\n\r");
		
		echo ("<label for='mainpropre'>En main propre</label>\n\r");
		echo ("<input type='radio' name='mode_envoi' id='mainpropre' value='En main propre'");
		if ($mode_envoi == "En main propre") {echo ("checked='checked'");}
		echo ("><br/>\n\r");		
		
		echo ("<br style='clear:both'/>\n\r");
		
		echo ("<label for='date_envoi'>Date d'envoi</label>\n\r");
		echo ("<select name='jour_envoi' id='jour_envoi'>\n\r");
		for ($i=1;$i<=31;$i++) {
			if (strlen($i) < 2){$jour="0".$i;} else {$jour=$i;};
			echo ("<option value='".$jour."'");
			if ($jour == $jour_envoi) {echo("selected='selected'");};
			echo (">".$jour."</option>\n\r");
		}
		echo ("</select>\n\r");
		echo ("<select name='mois_envoi' id='mois_envoi'>\n\r");
		for ($i=1;$i<=12;$i++) {
			if (strlen($i) < 2){$mois="0".$i;} else {$mois=$i;};
			echo ("<option value='".$mois."'");
			if ($mois == $mois_envoi) {echo("selected='selected'");};
			echo (">".$mois."</option>\n\r");
		}
		echo ("</select>\n\r");
		echo ("<select name='annee_envoi' id='annee_envoi'>\n\r");
		for ($i=2009;$i<=2011;$i++) {
			echo ("<option value='".$i."'");
			if ($i == $annee_envoi) {echo("selected='selected'");};
			echo (">".$i."</option>\n\r");
		}
		echo ("</select><br/>\n\r");


		echo ("<input type='hidden' name='id_dossier' id='id_dossier' value='".$row[0]."'><br/>\n\r");
		
		echo ("<br style='clear:both'/>\n\r");
		
		echo("<input type='submit' class='submit' name='Envoi' id='Envoi' value='Envoi'><br/></form></td>");
		echo("</tr>");
	}



	if ($num_rows2 > 0) {
		echo("</table>");
	}
	


function diff_date($jour , $mois , $an , $jour2 , $mois2 , $an2){  
 
$date = mktime(0, 0, 0, $mois, $jour, $an);  
$date2 = mktime(0, 0, 0, $mois2, $jour2, $an2);  
     
$diff = floor(($date - $date2) / (3600 * 24));  
return $diff;
}


	
?>
</td>
</tr>
</table>

</body></html>

