<?php
	require_once("fonctions_db.php");
	require_once("fonctions.php");
	
//$mois_courant = $_GET['mois'];
//$annee_courante = $_GET['annee'];

//print_r($_GET);


function Mise_a_jour_compta($journal, $id_dossier)
{
	$requete_confirm_compta = "
	INSERT INTO 
	import_compta
	(journal, id_dossier)
	VALUES
	('".$journal."', '".$id_dossier."');";
	$result_confirm_compta = mysql_query($requete_confirm_compta);
}

function Journal_ve($mois_courant, $annee_courante)
{
	
	$date_fin = date('Y-m-d',mktime(0, 0, 0, $mois_courant+1, 01, $annee_courante));
	
	$link = connection(MYDATABASE);
	//Requetes TVA Fr=>Fr / Fr=>Eu Fr=>Autre
	unset($fr_eu);
	unset($fr_fr);
	$fr_fr = array();
	$fr_eu = array();
	$requete = "
	SELECT 
	dossiers.id,
	dossiers.dest_ville,
	clients.ville
	FROM 
	dossiers, 
	clients,
	achats
	WHERE 
	dossiers.dest_pays='FR' 
	AND 
	dossiers.id_client=clients.id 
	AND 
	clients.pays='FR' 
	AND etat='Confirm�' 
	AND 
	achats.date BETWEEN ('".$annee_courante."-".$mois_courant."-01' - INTERVAL 6 MONTH) AND '".$date_fin."'
	AND
	achats.id_dossier = dossiers.id
	AND
	dossiers.id
	NOT IN 
	(SELECT id_dossier FROM import_compta WHERE journal = 'j_ve')
	;";
	
	//echo $requete;
	
	$result = mysql_query($requete) or die(mysql_error().$requete);
	
	while ($row = mysql_fetch_assoc($result))
	{
		$fr_fr[] = $row['id'];
	}


	$requete = "
	SELECT 
	dossiers.id,
	dossiers.dest_ville,
	clients.ville,
	code_pays.lpays_fr AS pays
	FROM 
	dossiers, 
	clients,
	achats,
	code_pays
	WHERE 
	dossiers.dest_pays=code_pays.cpays
	AND
	code_pays.europe_compta = '1' 
	AND 
	dossiers.id_client=clients.id 
	AND 
	clients.pays='FR' 
	AND etat='Confirm�' 
	AND 
	achats.date BETWEEN ('".$annee_courante."-".$mois_courant."-01' - INTERVAL 6 MONTH) AND '".$date_fin."'
	AND
	achats.id_dossier = dossiers.id
	AND
	dossiers.id
	NOT IN 
	(SELECT id_dossier FROM import_compta WHERE journal = 'j_ve')
	;";
	$result = mysql_query($requete) or die(mysql_error().$requete);
	while ($row = mysql_fetch_assoc($result))
	{
		$fr_eu[] = $row['id'];
	}
	
	
	
	
	//Requete de creation du journal
	$requete = "
	SELECT DISTINCT 
	dossiers.id AS id_dossier,  
	clients.nom,
	clients.prenom,
	clients.id AS id_client,
	achats.date AS date_achat,
	achats.montant_achat,
	tour_op_compta.id_compta AS code_to
	FROM 
	`dossiers`, 
	`clients`, 
	`vendeurs`, 
	`lien_dossier_vendeur`,
	achats,
	tour_op_compta
	WHERE 
	dossiers.id_client=clients.id
	AND 
	achats.date BETWEEN ('".$annee_courante."-".$mois_courant."-01' - INTERVAL 6 MONTH) AND '".$date_fin."' 
	AND
	dossiers.to = tour_op_compta.nom COLLATE latin1_general_cs
	AND
	achats.id_dossier = dossiers.id
	AND 
	dossiers.id
	NOT IN 
	(SELECT id_dossier FROM import_compta WHERE journal = 'j_ve')
	ORDER BY 
	achats.date;
	";
	//echo $requete;
	
	unset ($out);
	$result = mysql_query($requete) or die (mysql_error().$requete);
	while($row = mysql_fetch_assoc($result))
	{
		$total_dossier = Calcul_total_dossier($row['id_dossier']);
		$total_assur = Total_assur($row['id_dossier']);
		
		//$n_piece = str_replace('180','',$row['id_dossier']);
		$n_piece = substr ($row['id_dossier'],-5);
		$compte = "9".$n_piece;
		
		
		//print_r($row);
		//echo $total_dossier;
		//print_r($total_assur);
		//echo "<br/><hr/>";
		if ($total_assur[0] > 0)
		{
			//Assurances ligne 1
			$out .= $row['date_achat'].";";
			$out .= "VE;";
			$out .= $compte.";";
			$out .= $n_piece.";";
			$out .= $row['prenom']." ".$row['nom'].";";
			$out .= number_format($total_assur[0], 2, ',', ' ').";";
			$out .= ";<br/>";
		
			//Assurances ligne 2
			$out .= $row['date_achat'].";";
			$out .= "VE;";
			$out .= "706901;";
			$out .= $n_piece.";";
			$out .= $row['prenom']." ".$row['nom'].";";
			$out .= ";";
			$out .= number_format($total_assur[0], 2, ',', ' ').";<br/>";			
			
		}
		
		// Ligne total vente
		$out .= $row['date_achat'].";";
		$out .= "VE;";
		$out .= $compte.";";
		$out .= $n_piece.";";
		$out .= $row['prenom']." ".$row['nom'].";";
		$out .= number_format(($total_dossier-$total_assur[0]), 2, ',', ' ').";";
		$out .= ";";
		$out .= "E;<br/>";
		
		//Ligne total achat
		$out .= $row['date_achat'].";";
		$out .= "VE;";
		$out .= $row['code_to'].";";
		$out .= $n_piece.";";
		$out .= $row['prenom']." ".$row['nom'].";";
		$out .= ";";
		$out .= number_format($row['montant_achat'], 2, ',', ' ').";";
		$out .= "E;<br/>";


		if(in_array($row['id_dossier'], $fr_fr))
		{
			$compte_marge = "706700";
		}
		else if (in_array($row['id_dossier'], $fr_eu))
		{
			$compte_marge = "706800";
		}
		else
		{
			$compte_marge = "706900";
		}


		//Ligne marge
		$out .= $row['date_achat'].";";
		$out .= "VE;";
		$out .= $compte_marge.";";
		$out .= $n_piece.";";
		$out .= $row['prenom']." ".$row['nom'].";";
		$out .= ";";
		$out .= number_format((($total_dossier-$total_assur[0])-$row['montant_achat']), 2, ',', ' ').";";
		$out .= "E;<br/>";
		
		Mise_a_jour_compta('j_ve',$row['id_dossier']);
	}

return $out;
	
}



function Journal_acv($mois_courant, $annee_courante)
{
	$link = connection(MYDATABASE);
	
	unset ($out);
	
	$date_fin = date('Y-m-d',mktime(0, 0, 0, $mois_courant+1, 01, $annee_courante));

	$requete1 = "
	SELECT 
	id_dossier, 
	facture_achat, 
	montant_achat, 
	date, 
	date_payement,
	code_fournisseur,
	id_compta,
	nom
	FROM 
	achats,
	tour_op_compta,
	dossiers
	WHERE
	`date` BETWEEN ('".$annee_courante."-".$mois_courant."-01' - INTERVAL 6 MONTH) AND '".$date_fin."'
	AND
	dossiers.`to` = tour_op_compta.nom COLLATE latin1_general_cs
	AND 
	dossiers.id = achats.id_dossier
	AND
	dossiers.id
	NOT IN 
	(SELECT id_dossier FROM import_compta WHERE journal = 'j_acv')
	ORDER BY 
	date;";
	
	echo $requete1;
	
	$result1 = mysql_query($requete1) or die (mysql_error().$requete1);
	
	while($row1 = mysql_fetch_assoc($result1))
	{
		
		$n_piece = substr ($row1['id_dossier'],-5);
		$compte = "9".$n_piece;				
		$date = $row1['date_payement'];
		
		//Ligne ACV
		$out .= $row1['date'].";";
		$out .= "ACV;";
		$out .= $row1['code_fournisseur'].";";
		$out .= $row1['facture_achat'].";";
		$out .= $row1['nom'].";";
		$out .= ";";
		$out .= number_format($row1['montant_achat'], 2, ',', ' ').";";
		$out .= "E;";
		$out .= "<br/>";
		
		$out .= $row1['date'].";";
		$out .= "ACV;";
		$out .= $row1['id_compta'].";";
		$out .= $row1['facture_achat'].";";
		$out .= $row1['nom'].";";
		$out .= number_format($row1['montant_achat'], 2, ',', ' ').";;";
		$out .= "E;<br/>";
		
		Mise_a_jour_compta('j_acv',$row1['id_dossier']);
	}
	
	return $out;
}

function Journal_fc ($mois_courant, $annee_courante)
{
		if (strlen($annee_courante) > 2)
	{
		$annee_courante = substr($annee_courante,-2);
	}
	
	$link = connection(MYDATABASE);

	unset ($out);
	
	$date_fin = date('Y-m-d',mktime(0, 0, 0, $mois_courant+1, 01, $annee_courante));
	
for ($i = 1;$i <=32; $i++)
{
	$requete_englobante = "
	SELECT nom, 
	id_compta, 
	code 
	FROM 
	mode_payement_compta;";


	if (strlen($i) < 2)
	{
		$jour_courant="0".$i;
	}
	else
	{
		$jour_courant = $i;
	}
	
	$result_englobante = mysql_query($requete_englobante) or die (mysql_error().$requete_englobante);
	
	while($row_englobante = mysql_fetch_assoc($result_englobante))
	{
		$requete = "
		SELECT DISTINCT
		facturation.montant_payement,
		dossiers.id AS id_dossier, 
		dossiers.id_client,  
		clients.nom,
		clients.prenom,  
		facturation.mode_payement,
		facturation.date_payement, 
		dossiers.etat   
		FROM 
		`dossiers`, 
		`clients`, 
		`vendeurs`, 
		`facturation` 
		WHERE 
		dossiers.id_client=clients.id 
		AND 
		SUBSTRING(facturation.date_payement,1,2)=".$jour_courant."
		AND 
		SUBSTRING(facturation.date_payement,4,2)=".$mois_courant." 
		AND 
		SUBSTRING(facturation.date_payement,7,2)=".$annee_courante." 
		AND 
		facturation.mode_payement = '".$row_englobante['nom']."'
		AND
		dossiers.id=facturation.id_dossier 
		AND
		dossiers.id
		NOT IN 
		(SELECT id_dossier FROM import_compta WHERE journal = 'j_fc')
		ORDER BY 
		facturation.date_payement;";
		
		//echo "<hr>";
		//echo $requete."<br/>";
		//echo"<hr>";
		unset($total_mode);
		$result = mysql_query($requete) or die (mysql_error().$requete);
		
		//echo(mysql_num_rows($result));
		
		while($row = mysql_fetch_assoc($result))
		{
			//print_r($row);
			$ids[] = $row['id_dossier'];
			
			$n_piece = substr ($row['id_dossier'],-5);
			$compte = "9".$n_piece;	
			//print_r($row);
			//echo "<br/><hr/>";
			
			$date = $row['date_payement'];
			
			//Ligne FC
			$out .= $row['date_payement'].";";
			$out .= "FC;";
			$out .= $compte.";";
			$out .= $row_englobante['code'].";";
			$out .= $row['prenom']." ".$row['nom'].";";
			$out .= "0;";
			$out .= number_format($row['montant_payement'], 2, ',', ' ').";";		
			$out .= "E;<br/>";
			
			$total_mode += $row['montant_payement'];
			
			
		}
		//$out .= "<hr>";
		//echo "<hr><hr>";
		if($total_mode > 0)
		{
			//Ligne TOTAL MODE
			$out .= $date.";";
			$out .= "FC;";
			$out .= $row_englobante['id_compta'].";";
			$out .= $row_englobante['code'].";";
			$out .= $row_englobante['nom'].";";
			$out .= number_format($total_mode, 2, ',', ' ').";";
			$out .= "0;";
			$out .= "E;<br/>";
		}
	}
}

if (is_array($ids))
{
	foreach($ids as $id)
	{
		Mise_a_jour_compta('j_fc',$id);
	}
}
	return $out;
}

function Journal_ccf($mois_courant, $annee_courante)
{
	$link = connection(MYDATABASE);
	
	unset ($out);
	
	$date_fin = date('Y-m-d',mktime(0, 0, 0, $mois_courant+1, 01, $annee_courante));	

	$requete1 = "
	SELECT 
	id_dossier, 
	date_payement,
	banque,
	code_fournisseur,
	id_compta,
	num_cheque,
	nom,
	montant_achat
	FROM 
	achats,
	tour_op_compta,
	dossiers
	WHERE
	`date_payement` BETWEEN ('".$annee_courante."-".$mois_courant."-01' - INTERVAL 6 MONTH) AND '".$date_fin."'
	AND
	dossiers.`to` = tour_op_compta.nom  COLLATE latin1_general_cs
	AND
	banque='CCF'
	AND
	date_payement > '0000-00-00'
	AND 
	dossiers.id = achats.id_dossier
	AND
	dossiers.id
	NOT IN 
	(SELECT id_dossier FROM import_compta WHERE journal = 'j_ccf')
	ORDER BY 
	date_payement;";
	
	//echo $requete1;
	
	$result1 = mysql_query($requete1) or die (mysql_error().$requete1);
	
	while($row1 = mysql_fetch_assoc($result1))
	{
				
		$date = $row1['date_payement'];
		
		//Ligne ACV
		$out .= $row1['date_payement'].";";
		$out .= "CCF;";
		$out .= $row1['code_fournisseur'].";";
		$out .= $row1['num_cheque'].";";
		$out .= $row1['nom'].";";
		$out .= number_format($row1['montant_achat'], 2, ',', ' ').";";
		$out .= ";";
		$out .= "E;";
		$out .= "<br/>";
		
		$out .= $row1['date_payement'].";";
		$out .= "CCF;";
		$out .= "512200;";
		$out .= $row1['num_cheque'].";";
		$out .= $row1['nom'].";";
		$out .= ";";
		$out .= number_format($row1['montant_achat'], 2, ',', ' ').";";
		$out .= "E;<br/>";
		
		
		Mise_a_jour_compta('j_ccf',$row1['id_dossier']);
	}
	
	return $out;	
}

function Journal_ca($mois_courant, $annee_courante)
{
	$link = connection(MYDATABASE);
	
	unset ($out);
	
	$date_fin = date('Y-m-d',mktime(0, 0, 0, $mois_courant+1, 01, $annee_courante));
	
	
	$requete1 = "
	SELECT 
	id_dossier, 
	date_payement,
	banque,
	code_fournisseur,
	id_compta,
	num_cheque,
	nom,
	montant_achat
	FROM 
	achats,
	tour_op_compta,
	dossiers
	WHERE
	`date_payement` BETWEEN ('".$annee_courante."-".$mois_courant."-01' - INTERVAL 6 MONTH) AND '".$date_fin."'
	AND
	dossiers.`to` = tour_op_compta.nom  COLLATE latin1_general_cs
	AND
	banque='CA'
	AND
	date_payement > '0000-00-00'
	AND 
	dossiers.id = achats.id_dossier
	AND
	dossiers.id
	NOT IN 
	(SELECT id_dossier FROM import_compta WHERE journal = 'j_ca')
	ORDER BY 
	date_payement;";
	
	//echo $requete1;
	
	$result1 = mysql_query($requete1) or die (mysql_error().$requete1);
	
	while($row1 = mysql_fetch_assoc($result1))
	{
				
		$date = $row1['date_payement'];
		
		//Ligne ACV
		$out .= $row1['date_payement'].";";
		$out .= "CA;";
		$out .= $row1['code_fournisseur'].";";
		$out .= $row1['num_cheque'].";";
		$out .= $row1['nom'].";";
		$out .= number_format($row1['montant_achat'], 2, ',', ' ').";;";
		$out .= "E;";
		$out .= "<br/>";
		
		$out .= $row1['date_payement'].";";
		$out .= "CA;";
		$out .= "512100;";
		$out .= $row1['num_cheque'].";";
		$out .= $row1['nom'].";";
		$out .= ";";
		$out .= number_format($row1['montant_achat'], 2, ',', ' ').";";
		$out .= "E;<br/>";
		//echo $row1['id_dossier'];
		
		Mise_a_jour_compta('j_ca',$row1['id_dossier']);
	}
	
	return $out;		
}

$temps_debut_acv = microtime(true);
$acv = Journal_acv($mois_courant, $annee_courante);
$temps_fin_acv = microtime(true);

$temps_debut_ve = microtime(true);
$ve = Journal_ve($mois_courant, $annee_courante);
$temps_fin_ve = microtime(true);

$temps_debut_fc = microtime(true);
$fc = Journal_fc($mois_courant, $annee_courante);
$temps_fin_fc = microtime(true);

$temps_debut_ccf = microtime(true);
$ccf = Journal_ccf($mois_courant, $annee_courante);
$temps_fin_ccf = microtime(true);

$temps_debut_ca = microtime(true);
$ca = Journal_ca($mois_courant, $annee_courante);
$temps_fin_ca = microtime(true);

$temps_debut = microtime(true);
$temps_fin = microtime(true);


//echo 'Temps d\'execution : '.round($temps_fin - $temps_debut, 4);


echo $acv;
echo "<hr>";
echo $ve;
echo "<hr>";
echo $fc;
echo "<hr>";
echo $ccf;
echo "<hr>";
echo $ca;



echo 'Temps d\'execution ACV : '.round($temps_fin_acv - $temps_debut_acv, 4)."<br/>";
echo 'Temps d\'execution VE : '.round($temps_fin_ve - $temps_debut_ve, 4)."<br/>";
echo 'Temps d\'execution FC : '.round($temps_fin_fc - $temps_debut_fc, 4)."<br/>";
echo 'Temps d\'execution CCF : '.round($temps_fin_ccf - $temps_debut_ccf, 4)."<br/>";
echo 'Temps d\'execution CA : '.round($temps_fin_ca - $temps_debut_ca, 4)."<br/>";

?>