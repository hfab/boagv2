<?php
session_start();
if (!isset($_SESSION["id_vendeur"])) {
		   header("Location:index.php");
		   exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Réglement des fournisseurs d'Agencedevoyage&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />



<link href="client.css" rel="stylesheet" type="text/css"/>


<style>

th {
	background-color: #e9e9da;
}

.largeHeaders {
		background: 
			#e9e9da
			url('/js/img/green_arrows.gif')
			no-repeat
			center left;
		color: #333;
		padding: 5px;
		padding-left: 25px;
		text-align: left;
		cursor: pointer;	
}
.sortUp {
	background:
		#e9e900 
		url('/js/img/green_decending.gif')		
		no-repeat
		center left;
}

.sortDown {
	background:
		#e9e900 

		url('/js/img/green_acending.gif')
		no-repeat
		center left;
}
#sorting {
	position: absolute;
	width: 100%;
	height: 100%;
	left:0px;
	top:0px;
	text-align: center;
	display: none;
	margin:0px;
	padding:0px;
}
#sorting div {
	border: 4px solid #000;
	padding:30px;
	margin: auto;
	text-align: center;
	width: 600px;
	top:500px;
	position: relative;
	background-color: #FFF;
	color: #333;
	font-weight: bold;
}
.even {
	background-color:#FFD760;
}
.odd
{
	background-color:#60C3FF;
}
</style>

	<script type="text/javascript" src="/js/jquery-svn.js"></script>
	<script type="text/javascript" src="/js/jquery.tablesorter.js"></script>
	
	<script type="text/javascript">
	$(document).ready(function() {
		$("table#recherche").tableSorter({
			sortClassAsc: 'sortUp', // class name for asc sorting action
			sortClassDesc: 'sortDown', // class name for desc sorting action
			headerClass: 'largeHeaders', // class name for headers (th's)
			disableHeader: ['lien'], // disable column can be a string / number or array containing string or number. 
			dateFormat: 'dd/mm/yyyy', // set date format for non iso dates default us, in this case override and set uk-format
			//stripingRowClass: ['even','odd'],
		  	//stripRowsOnStartUp: true
		})
	});
	$(document).sortStart(function(){
		$("div#sorting").show();
	}).sortStop(function(){
		$("div#sorting").hide();
	});
	</script>


</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
require_once("fonctions_db.php");
require_once("fonctions.php");
require_once("comptabilite/mailto.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">
<?php

$temps_debut = microtime(true);

$mois = $_POST["mois"];
$annee = $_POST["annee"];

$jour_facturation = $_POST["jour_facturation"];
$mois_facturation = $_POST["mois_facturation"];
$annee_facturation = $_POST["annee_facturation"];

$jour_payement = $_POST["jour_payement"];
$mois_payement = $_POST["mois_payement"];
$annee_payement = $_POST["annee_payement"];

$date_facturation = $annee_facturation."-".$mois_facturation."-".$jour_facturation;

$date_payement = $annee_payement."-".$mois_payement."-".$jour_payement;

$banque = $_POST['banque'];

$montant_achat = $_POST["montant_achat"];
$facture_achat = $_POST["facture_achat"];
$id_dossier = $_POST["id_dossier"];
$mode_achat = $_POST["mode_achat"];
$num_cheque = $_POST["num_cheque"];

$mois_courant=date("m");
$annee_courante=date("y");


if (!empty($mois)) {$mois_courant = $mois;};
if (!empty($annee)) {$annee_courante = $annee;};

if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};

$envoi_mail = $_POST["envoi_mail"];
$html = $_POST["html"];


$file_csv_ce   ='comptabilite/journalAchat'.$mois_courant.'_'.$annee_courante.'CE.csv';
//$file_joint_ce = 'journalCaisse'.$mois_courant.'_'.$annee_courante.'CE.csv';
$file_csv_hce = 'comptabilite/journalAchat'.$mois_courant.'_'.$annee_courante.'HorsCE.csv';
$output_ce_csv = "";
$output_hce_csv = "";

//echo $file_csv_ce."<br>";
//echo $file_csv_hce;

$destinataires =array('yveschaponic@gmail.com',"info@voyaneo.com",'fabien@lead-factory.net','hernoux.fabien@gmail.com');
//$destinataires =array('julien@comptoirdesdeals.com');

if($envoi_mail=="ok")
{
	if ( mailToCompta($destinataires, "achat",$file_csv_ce,$file_csv_hce) )
	{
		echo "Le mail a bien été envoyé à <b>$destinataire</b> de la part de <b>$expediteur</b> <br/>";
	}
}


/*

if($envoi_mail=="ok")
{

	$boundary = "-----=".md5(uniqid(rand()));
	
	$header = "MIME-Version: 1.0\r\n";
	
	$header .= "Content-Type: multipart/mixed; boundary=\"$boundary\"\r\n";
	$header .= "\r\n";
	
	
	
	$msg = "Votre lecteur de mail ne semble pas compatible. Merci de contacter AgenceDeVoyage.com au 01 77 69 02 52\r\n";
	
	
	$msg .= "--$boundary\r\n";
	$msg .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
	
	$msg .= "Content-Transfer-Encoding:8bit\r\n";
	$msg .= "\r\n";

	$msg .= "<style>*{font-family:Verdana}</style><center><img src='http://www.agencedevoyage.com/img/header/logo.jpg'></center><br/><br/>";
	$msg .= "\r\n";
	$msg .= "Bonjour <br/><br/>\r\n";
	$msg .= "\r\n";
	$msg .= "Veuillez trouver ci-joint les réglements fournisseurs de AgenceDeVoyage.com<br/>\r\n";
	$msg .= "<br/>\r\n";
	$msg .= "Cordialement, <br/>\r\n";
	$msg .= "L'équipe d'Agencedevoyage.com <br/>\r\n";
	$msg .= "Tél : 01 77 69 02 52<br/>\r\n";
	$msg .= "Fax : 01 77 69 02 14 <br/>\r\n";

	$msg .= "\r\n\r\n";
	//Texte
	$msg .= "--$boundary\r\n";

	$msg .= "Content-Type: text/html; name=\"reglement_fournisseurs.html\"\r\n";
	$msg .= "Content-Transfer-Encoding: 8bit\r\n";
	
	$msg .= "Content-Disposition: attachment; filename=\"reglement_fournisseurs.html\"\r\n";
	
	$msg .= "\r\n";
	
	$msg .= stripslashes($html). "\r\n";
	$msg .= "\r\n\r\n";
	
	
	$expediteur="info@agencedevoyage.com";
	$reponse = $expediteur;
	$destinataire="yves@agencedevoyage.com";
	//Hack de test a commenter pour envoyer au client
	//$destinataire="webmaster@medisite.fr";
	// && mail($expediteur, "COPIE - Réglement fournisseur - Monagence.com - $destinataire", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header)
	if (mail($destinataire, "Réglements fournisseurs - AgenceDeVoyage.com", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header) 
	&& mail($expediteur, "COPIE - Réglement fournisseur - AgenceDeVoyage.com - $destinataire", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header)
	)
	{
		echo "Le mail a bien été envoyé à <b>$destinataire</b> de la part de <b>$expediteur</b> <br/>";	
	}
	
}


*/

$html_output="";
$html_output_mail="";
$html_output_mail .= "<h1>Réglements fournisseurs</h1>";



	echo "<h1>Réglements fournisseurs pour le : ".$mois_courant." / ".$annee_courante."</h1>";
	echo "<p><form method='post' action='".$_SERVER['PHP_SELF']."?PHPSESSID=".session_id()."'><fieldset><legend>Choix de la période</legend>";
	echo ("<label for='mois'>Voir une autre période : </label>\n\r");
	echo ("<select name='mois' id='mois'>\n\r");
	for ($i=1;$i<=12;$i++) {
		if (strlen($i) < 2){$mois="0".$i;} else {$mois=$i;};
		echo ("<option value='".$mois."'");
		if ($mois == $mois_courant) {echo("selected='selected'");};
		echo (">".$mois."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='annee' id='annee'>\n\r");
	for ($i=2005;$i<=$annee_courante+1;$i++) {
		echo ("<option value='".$i."'");
		if ($i == $annee_courante) {echo("selected='selected'");};
		echo (">".$i."</option>\n\r");
	}
	echo ("</select><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");
	echo "<input type='submit' name='Voir' id='Voir' value='Voir'></fieldset></form></p>";

	$annee_courante = substr($annee_courante,2,4);



$montant_total_dossier=0;
$montant_total_achat=0;



		$html_output .= "<table border='1' cellpadding='2' cellspacing='0' id='recherche'>\n\r";
		$html_output .= "<tr>\n\r";
		$html_output .= "<th><b>Date</b></th>\n\r";
		$html_output .= "<th><b>ID dossier</b></th>\n\r";
		//$html_output .= "<th><b>Nom client</b></th>\n\r";
		$html_output .= "<th><b>TO</b></th>\n\r";
		$html_output .= "<th><b>Ref facture TO</b></th>\n\r";
		$html_output .= "<th><b>Client</b></th>\n\r";
		$html_output .= "<th><b>Départ</b></th>\n\r";
		$html_output .= "<th><b>Montant</b></th>\n\r";
		//$html_output .= "<th><b>Date de facturation</b></th>\n\r";
		//$html_output .= "<th><b>N° de facture achat</b></th>";
		$html_output .= "<th><b>Montant achat</b></th>";
		//$html_output .= "<th><b>Date payement</b></th>";
		$html_output .= "<th><b>Assur</b></th>";
		//$html_output .= "<th><b>Banque</b></th>";
		//$html_output .= "<th><b>Numéro de cheque</b></th>";
		$html_output .= "<th>Marge (&euro;)</th>";
		$html_output .= "<th>Marge (%)</th>";
		$html_output .= "</tr>\n\r";	
		
		$output_ce_csv .= "Date ;";
		$output_ce_csv .= "ID dossier ;";
		$output_ce_csv .= "TO ;";
		$output_ce_csv .= "Ref facture TO ;";
		$output_ce_csv .= "Client ;";
		$output_ce_csv .= "Départ ;";
		$output_ce_csv .= "Montant ;";
		$output_ce_csv .= "Montant achat ;";
		$output_ce_csv .= "Assur ;";
		$output_ce_csv .= "Marge ; \n";
		
		$output_hce_csv .= "Date ;";
		$output_hce_csv .= "ID dossier ;";
		$output_hce_csv .= "TO ;";
		$output_hce_csv .= "Ref facture TO ;";
		$output_hce_csv .= "Client ;";
		$output_hce_csv .= "Départ ;";
		$output_hce_csv .= "Montant ;";
		$output_hce_csv .= "Montant achat ;";
		$output_hce_csv .= "Assur ;";
		$output_hce_csv .= "Marge ; \n";
	

for ($i=1;$i<32;$i++)
{
$jour_courant = $i;
if (strlen($jour_courant) < 2){$jour_courant="0".$jour_courant;};

	$date_courante="20".$annee_courante."-".$mois_courant."-".$jour_courant;
	
	$link = connection(MYDATABASE);
	$requete = "SELECT 
	DISTINCT 
	dossiers.id, 
	dossiers.to, 
	dossiers.dest_ville, 
	dossiers.ville_depart, 
	dossiers.date_deb, 
	dossiers.date_fin, 
	vendeurs.prenom, 
	clients.nom, 
	dossiers.ref_to,
	code_pays.europe_compta
	FROM 
	`dossiers`, 
	`clients`, 
	`vendeurs`, 
	`lien_dossier_vendeur`,
	`code_pays`
	WHERE 
	dossiers.id_client=clients.id 
	AND 
	dossiers.id=lien_dossier_vendeur.id_dossier
	AND
	dossiers.dest_pays=code_pays.cpays
	AND 
	lien_dossier_vendeur.id_vendeur=vendeurs.id 
	AND 
	dossiers.etat='Confirmé' 
	AND 
	dossiers.date_confirm='".$date_courante."' 
	ORDER BY 
	dossiers.id;";
	
/* 	echo $requete."<br><br>"; */
	
	$result=mysql_query($requete) or die(mysql_error());
	$num_rows = mysql_num_rows($result);
	//echo $i."=>".$num_rows."<br/>".$requete."<br/><br/>";
	
	
	if ($num_rows > 0) {
		//Html Output mail
		$html_output_mail .= "<h2>$jour_courant / $mois_courant / $annee_courante</h2>\n\r";
		$html_output_mail .= "<table border='1' cellpadding='2' cellspacing='0'>\n\r";
		$html_output_mail .= "<tr>\n\r";
		$html_output_mail .= "<td><b>ID dossier</b></td>\n\r";
		//$html_output_mail .= "<td><b>Nom client</b></td>\n\r";
		$html_output_mail .= "<td><b>TO</b></td>\n\r";
		$html_output_mail .= "<td><b>Ref facture TO</b></td>\n\r";
		$html_output_mail .= "<td><b>Client</b></td>\n\r";
		$html_output_mail .= "<td><b>Départ</b></td>\n\r";
		$html_output_mail .= "<td><b>Montant</b></td>\n\r";
		
		//$html_output_mail .= "<td><b>Date de facturation</b></td>\n\r";
		//$html_output_mail .= "<td><b>N° de facture achat</b></td>";
		$html_output_mail .= "<td><b>Montant achat</b></td>";
		//$html_output_mail .= "<td><b>Date payement</b></td>";
		$html_output_mail .= "<td><b>Assur</b></td>";
		//$html_output_mail .= "<td><b>Banque</b></td>";
		//$html_output_mail .= "<td><b>Numéro de cheque</b></td>";
		$html_output_mail .= "<td>Marge (%)</td>";
		$html_output_mail .= "</tr>\n\r";
	}
	
	while($row = mysql_fetch_row($result))
	{
		$link = connection(MYDATABASE);
		$requete3 = "SELECT facture_achat, montant_achat, date, mode_achat, num_cheque, date_payement, banque FROM achats WHERE id_dossier='".$row[0]."' AND montant_achat >= 0;";
		
		//echo $row[0]." ";

		$result3=mysql_query($requete3) or die(mysql_error());
		$num_rows3 = mysql_num_rows($result3);
		//echo $num_rows3;
		
		$facture_achat_temp = 0;
		$montant_achat_temp = 0;
		$montant_achat_assur = 0;
		$mode_achat_temp = "";
		$date="0000-00-00";
		$total_assur = Total_assur($row[0]);
		
		unset($row3);
		unset($jour_courant2);
		unset($mois_courant2);
		unset($annee_courante2);
		
		unset($date_payement);
		
		unset($jour_courant3);
		unset($mois_courant3);
		unset($annee_courante3);
				
		$num_cheque=0;
				
		while($row3 = mysql_fetch_row($result3))
		{
			$facture_achat_temp = $row3[0];
			$montant_achat_temp = $row3[1];
			$mode_achat_temp = $row3[3];
			$date = $row3[2];
			
			
			//echo "ICI : ".$facture_achat.$montant_achat;
			//echo "ICI : ".$date;
			//echo $row[0]."<br>";
			
			$montant_total_dossier+=Calcul_total_dossier($row[0]);
			$montant_total_achat+=$montant_achat_temp;
			$montant_total_assur+=$total_assur[1];
			$num_cheque = $row3[4];
			$date_payement = $row3[5];
			$banque = $row3[6];
		}
			if ($date != "0000-00-00") 
			{
				$jour_courant2 = substr($date,8,2);
				$mois_courant2 = substr($date,5,2);
				$annee_courante2 = substr($date,0,4);
			}
			else
			{
				$date = date("Y-m-d");
				$jour_courant2 = substr($date,8,2);
				$mois_courant2 = substr($date,5,2);
				$annee_courante2 = substr($date,0,4);
			}
			
			if ($date_payement != "0000-00-00" && isset($date_payement)) 
			{
				$jour_courant3 = substr($date_payement,8,2);
				$mois_courant3 = substr($date_payement,5,2);
				$annee_courante3 = substr($date_payement,0,4);
			}
			else
			{
				//$date_payement = date("Y-m-d");
				
				$jour_courant3 = '00';
				$mois_courant3 = '00';
				$annee_courante3 = '0000';
			}
		
		//echo "Debut : ".$row[4]." -- Fin : ".$row[5]."<br>";
		
		if($row[9])
			$current = "output_ce_csv";
		else
			$current = "output_hce_csv";
		
		$marge=100-(100*($montant_achat_temp+$total_assur[1])/Calcul_total_dossier($row[0]));
		if($montant_achat_temp > 0){$bgcolor="#CCCCCC";} else {$bgcolor="#FFFFFF";}
		if($marge < 0 && $row[1] != 'Jancarthier' && $row[1] != 'Go Voyages' && $row[1] != 'Océania' ){$bgcolor="red";} else if($marge < 9  && $row[1] != 'Jancarthier' && $row[1] != 'Go Voyages' && $row[1] != 'Océania') {$bgcolor="orange";}
		
		$html_output .= "<tr bgcolor='".$bgcolor."'>\n\r";
		$html_output .= "<td nowrap='nowrap'>$jour_courant/$mois_courant/$annee_courante</td>\n\r";
		$html_output .= "<td>".$row[0]."</td>\n\r";
		$html_output .= "<td>".$row[1]."</td>\n\r";
		
		$html_output .= "<td>".$row[8]."</td>\n\r";
		$html_output .= "<td>".$row[7]."</td>\n\r";
		$html_output .= "<td>".$row[4]."</td>\n\r";
		$html_output .= "<td>".Calcul_total_dossier($row[0])."&euro;</td>\n\r";
		//$html_output .= "<form method='post' action='".$_SERVER["PHP_SELF"]."?PHPSESSID=".session_id()."'>";
		//$html_output .= "<td nowrap='nowrap'>$jour_courant2/$mois_courant2/$annee_courante2\n\r";
		
		
		${$current} .= "$jour_courant/$mois_courant/$annee_courante;" ;
		${$current} .= $row[0].";";
		${$current} .= $row[1].";";
		${$current} .= $row[8].";";
		${$current} .= $row[7].";";
		${$current} .= $row[5].";";
		${$current} .= Calcul_total_dossier($row[0]).";";
		

		/**$html_output .= "<select name='jour_facturation' id='jour_facturation'>\n\r";
		for ($j=1;$j<=31;$j++) {
			if (strlen($j) < 2){$jour2="0".$j;} else {$jour2=$j;};
			$html_output .= "<option value='".$jour2."'";
			if ($jour2 == $jour_courant2) {$html_output .= "selected='selected'";};
			$html_output .= ">".$jour2."</option>\n\r";
		}
		$html_output .= "</select>\n\r";
		$html_output .= "<select name='mois_facturation' id='mois_facturation'>\n\r";
		for ($j=1;$j<=12;$j++) {
			if (strlen($j) < 2){$mois2="0".$j;} else {$mois2=$j;};
			$html_output .= "<option value='".$mois2."'";
			if ($mois2 == $mois_courant2) {$html_output .= "selected='selected'";};
			$html_output .= ">".$mois2."</option>\n\r";
		}
		$html_output .= "</select>\n\r";
		$html_output .= "<select name='annee_facturation' id='annee_facturation'>\n\r";
		for ($j=2005;$j<=$annee_courante2;$j++) {
			$html_output .= "<option value='".$j."'";
			if ($j == $annee_courante2) {$html_output .= "selected='selected'";};
			$html_output .= ">".$j."</option>\n\r";
		}
		$html_output .= "</select><br/>\n\r";
		$html_output .= "<br style='clear:both'/>\n\r";**/
		
		
		//$html_output .= "</td>\n\r";
		
		//$html_output .= "<td>".$facture_achat_temp."</td>";
		$html_output .= "<td>".$montant_achat_temp."</td>";	
		${$current} .=$montant_achat_temp.";";

		//$html_output .= "<td nowrap='nowrap'>$jour_courant3/$mois_courant3/$annee_courante3\n\r";

		/**$fieldOptions = mysql_enum_values('achats','mode_achat');
		$html_output .= "<select name='mode_achat' size='1'>";
		foreach($fieldOptions as $tmp)
		{
		  $html_output .= "<option value='$tmp'";
		  if ($tmp == $mode_achat_temp) {$html_output .=" selected='selected' ";};
		  $html_output .= ">$tmp</option>";
		}
		$html_output .= "</select>";**/
		
		
		
		
		

		/**$html_output .= "<select name='jour_payement' id='jour_payement'>\n\r";
		for ($j=0;$j<=31;$j++) {
			if (strlen($j) < 2){$jour3="0".$j;} else {$jour3=$j;};
			$html_output .= "<option value='".$jour3."'";
			if ($jour3 == $jour_courant3) {$html_output .= "selected='selected'";};
			$html_output .= ">".$jour3."</option>\n\r";
		}
		$html_output .= "</select>\n\r";
		$html_output .= "<select name='mois_payement' id='mois_payement'>\n\r";
		for ($j=0;$j<=12;$j++) {
			if (strlen($j) < 2){$mois3="0".$j;} else {$mois3=$j;};
			$html_output .= "<option value='".$mois3."'";
			if ($mois3 == $mois_courant3) {$html_output .= "selected='selected'";};
			$html_output .= ">".$mois3."</option>\n\r";
		}
		$html_output .= "</select>\n\r";
		$html_output .= "<select name='annee_payement' id='annee_payement'>\n\r";
		
		$html_output .= "<option value='0000'";
			if ($j == $annee_courante3) {$html_output .= "selected='selected'";};
			$html_output .= ">0000</option>\n\r";
		
		if($annee_courante3 == '0000'){$j_max = date("Y");};
		for ($j=2005;$j<=$j_max;$j++) {
			$html_output .= "<option value='".$j."'";
			if ($j == $annee_courante3) {$html_output .= "selected='selected'";};
			$html_output .= ">".$j."</option>\n\r";
		}
		$html_output .= "</select><br/>\n\r";
		$html_output .= "<br style='clear:both'/>\n\r";**/

		//$html_output .= "</td>";
		
		
		
		$html_output .= "<td>".$total_assur[1]."&euro;</td>";
		${$current} .= $total_assur[1].";";

		//$html_output .= "<td nowrap='nowrap'>$banque\n\r";

		/**$fieldOptions = mysql_enum_values('achats','banque');
		$html_output .= "<select name='banque' size='1'>";
		foreach($fieldOptions as $tmp)
		{
		  $html_output .= "<option value='$tmp'";
		  if ($tmp == $banque) {$html_output .=" selected='selected' ";};
		  $html_output .= ">$tmp</option>";
		}
		$html_output .= "</select>";**/
		
		//$html_output .= "</td>";

		//$html_output .= "<td>".$num_cheque."</td>";

		$marge = number_format($marge,2,',',' ');
		if($marge < 10 && $marge > 0)
		{
			$marge = '0'.$marge;
		}

		if($montant_achat_temp > 0)
		{
			
			$html_output .= "<td>".(Calcul_total_dossier($row[0]) - ($montant_achat_temp+$total_assur[1]))." &euro;</td>";
			${$current} .= (Calcul_total_dossier($row[0]) - ($montant_achat_temp+$total_assur[1])).";";
		}
		else
		{
			$html_output .= "<td>N/A</td>";
			${$current} .="N/A;";
		}

		if($montant_achat_temp > 0)
		{
			
			$html_output .= "<td>".$marge." %</td>";
			//${$current} .= $marge.";";
		}
		else
		{
			$html_output .= "<td>N/A</td>";
			//${$current} .= "N/A;";
		}
		${$current} .="\n";
		$html_output .= "</tr>";
		
		$html_output_mail .= "<tr bgcolor='".$bgcolor."'>\n\r";
		$html_output_mail .= "<td>".$row[0]."</td>\n\r";
		$html_output_mail .= "<td>".$row[1]."</td>\n\r";
		$html_output_mail .= "<td>".$row[8]."</td>\n\r";
		$html_output_mail .= "<td>".Calcul_total_dossier($row[0])."&euro;</td>\n\r";
		//$html_output_mail .= "<td>".$date."</td>\n\r";
		//$html_output_mail .= "<td>".$facture_achat_temp."</td>";
		$html_output_mail .= "<td>".$montant_achat_temp."</td>";
		//$html_output_mail .= "<td>".$date_payement."</td>\n\r";
		$html_output_mail .= "<td>".$total_assur[1]."&euro;</td>";
		//$html_output_mail .= "<td>".$banque."</td>";
		//$html_output_mail .= "<td>".$num_cheque."</td>";
		
		if($montant_achat_temp > 0)
		{
			$marge=100-(100*($montant_achat_temp+$total_assur[1])/Calcul_total_dossier($row[0]));
			$html_output_mail .= "<td>".(Calcul_total_dossier($row[0]) - ($montant_achat_temp+$total_assur[1]))."&euro;</td>";
		}
		else
		{
			$html_output_mail .= "<td>N/A</td>";
		}

	}

	if ($num_rows > 0) {
		
		$html_output_mail .= "</table>\n\r";
	}
	

//$html_output .= "<hr>\n\r";
$html_output_mail .= "<hr>\n\r";
}


$html_output .= "</table><div id='sorting'><div>La table est en cours de tri, merci de patienter...</div></div>\n\r";


if($montant_total_dossier > 0)
{
$marge=100-(100*($montant_total_achat+$montant_total_assur)/$montant_total_dossier);
}
$html_output .= "Total des ventes : ".$montant_total_dossier."&euro; / Total des achats : ".($montant_total_achat+$montant_total_assur)."&euro; / Marge : ".($montant_total_dossier-($montant_total_achat+$montant_total_assur))."&euro; (".number_format($marge,2,',',' ')."%)";
$html_output_mail .= "Total des ventes : ".$montant_total_dossier."&euro; / Total des achats : ".($montant_total_achat+$montant_total_assur)."&euro; / Marge : ".($montant_total_dossier-($montant_total_achat+$montant_total_assur))."&euro; (".number_format($marge,2,',',' ')."%)";

echo $html_output;

//echo $html_output_mail;

	echo ("<form action='".$_SERVER['PHP_SELF']."' name='envoi' id='envoi' method='post'>\n\r");
	echo("<fieldset><legend>Envoi mail à yveschaponic@gmail.com</legend>");
	echo("<textarea name='html' onFocus='this.blur()' cols='1' rows='1' style='visibility:hidden;'>$html_output_mail</textarea>");
	echo("<input type='hidden' name='envoi_mail' value='ok'>\n\r");
	echo("<input type='hidden' name='mois' value='".$_POST['mois']."'>\n\r");
	echo("<input type='hidden' name='annee' value='".$_POST['annee']."'>\n\r");
	echo ("<div class='center'><input type='submit' class='submit' name='Envoi du mail' id='Envoi du mail' value='Envoi du mail'><br/></div>\n\r");
	echo("</fieldset></form>");	
	
	
$temps_fin = microtime(true);	
echo 'Temps d\'execution : '.round($temps_fin - $temps_debut, 4)."<br/>";
file_put_contents($file_csv_ce, $output_ce_csv);
file_put_contents($file_csv_hce,$output_hce_csv);
?>
</td>
</tr>
</table>

</body></html>

