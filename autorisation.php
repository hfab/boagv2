<!doctype html public "-//W3C//DTD HTML 4.0 //EN">
<html>
<head>
       <title>Title here!</title>
</head>
<body>
<?php
require('class_fpdf/fpdf.php');

class Autorisation extends FPDF{
	
	function ajoutEntete($vendeur){
		$this->Open();
		$this->SetFillColor(240,52,58);
		$this->SetMargins(15, 15, 15);
		$this->AddPage();
		$this->Image('logo_monagence.jpg',15,10,72,18,'','http://www.monagence.com');
		$this->Ln(30);
		
		$this->SetFont('Arial','B',12);
	    $this->Cell(180,3,"AUTORISATION DE PRELEVEMENT",0,1,'C',0);
	    $this->Ln(3);
	    $this->Cell(180,3,"CARTE BANCAIRE",0,1,'C',0);
	    $this->Ln(3);
		$this->SetFont('Arial','B',10);
		$this->Ln(3);
		$this->Cell(180,3,"A nous faxer au : 01 80 91 52 29",0,1,'C',0);
		$this->SetFont('Arial','B',10);
		$this->Ln(3);
		$this->Cell(180,3,"A l'attention de	$vendeur",0,1,'C',0);
			
			
			
	}

	function ajoutInfosClientCarte($montant,$numero_dossier,$date_depart,$passagers,$destination){
		
		$declaration[0]="Je soussign� (e) Mme, Mlle, Mr ......................................................autorise";
		$declaration[1]="par la pr�sente la soci�t� MONAGENCE.COM, 14, rue de Tolbiac";
		$declaration[2]="75013 Paris, � d�biter ma carte bancaire pour r�glement de ma cr�ance.";

		$coordonnes[0]="N� CARTE :";
		$coordonnes[1]="DATE D' EXPIRATION :";
		$coordonnes[2]="NOM & PRENOM DU PORTEUR :";
		$coordonnes[3]="NOM DE LA BANQUE DU PORTEUR :";
		$coordonnes[4]="CRYPTOGRAMME :";
		$coordonnes[6]="TYPE DE CARTE :  Visa , Mastercard :";
		$coordonnes[7]="MONTANT : *** $montant  � ***";
		$coordonnes[8]="N� DE DOSSIER : $numero_dossier";
		$coordonnes[9]="DATE DE DEPART : $date_depart";
		$coordonnes[10]="DESTINATION : $destination";
		$coordonnes[11]="NOM DU OU DES PAX : (2 personnes) :";
		
	    $test="NOM DU OU DES PAX : (2 personnes) :";
		$this->Ln(20);
		$this->SetFont('Arial','I',12);
		
		foreach($declaration as $dec){
			$this->Cell(180,3,$dec,0,1,'L',0);
			$this->Ln(2);
		}
		$this->Ln(15);
		$this->SetFont('Arial','',13);
		$this->SetFillColor(215,207,207); 
	
		foreach($coordonnes as $cor){
			$this->SetFont('Arial','',13);
			if($cor!=="CRYPTOGRAMME :"){
				$this->Cell(180,3,$cor,0,1,'L',0);
			}
			else{
				$this->SetFont('Arial','',13);
				$this->Cell(180,3,$cor,0,1,'L',0);
				$this->SetFont('Arial','',10);
				$this->Ln(1);
				$this->Cell(180,3,"(3 derniers chiffres du num�ro au dos de la carte de cr�dit)",0,1,'L',0);	
			}
			if($cor!=="$test"){
				$this->Ln(5);
			}
			else{
				$this->Ln(2);
			}
			 
				
		}
		foreach($passagers as $p){
			$passager=$passager." ".$p;
		}
		$this->SetFont('Arial','I',12);
		$this->Cell(180,5,"$passager",0,1,'L',0);
		$this->Ln(8);
		$this->SetFont('Arial','I',12);
		$this->Cell(180,3,"Fait le :",0,1,'L',0);
		$this->Ln(1);
		$this->Cell(180,3,"Signature :",0,1,'L',0);

		

	}

	function ajoutFooter(){
		$coordonnes[0]="Si�ge Social : 90 rue Baudin 92300 Levallois-Perret";
		$coordonnes[1]="Tel : 08  92 23 05 06 - Fax : 01 55 46 84 35";
		$coordonnes[2]="Sarl Mon Agence.com au capital de 400 000 �  - R.C.S. NANTERRE B 442 958 443";
		$coordonnes[3]="LI  092 05 0014 - Garantie APS - RC G�n�rali  AA 0706218 - Adh�rent SNAV";
		$coordonnes[4]="N� TVA Intra communautaire FR 76442958443";
		$coordonnes[5]="Internet : www.monagence.com    *Prix d'appel 0.34�";
		$this->SetFont('Arial','',7);
		$this->SetFillColor(215,207,207); 
		$this->Ln(20);
		foreach($coordonnes as $cor){
			$this->Cell(180,3,$cor,0,1,'C',1);
		}
	}
	function Footer(){
    //Positionnement � 1,5 cm du bas
    	$this->SetY(-30);
    //Police Arial italique 8
    //Num�ro de page centr�
    	$coordonnes[0]="Si�ge Social : 90 rue Baudin 92300 Levallois-Perret";
		$coordonnes[1]="Tel : 08  92 23 05 06 - Fax : 01 80 91 52 29";
		$coordonnes[2]="Sarl Mon Agence.com au capital de 10 000 �  - R.C.S. NANTERRE B 442 958 443";
		$coordonnes[3]="LI  092 05 0014 - Garantie APS - RC G�n�rali  AA 0706218 - Adh�rent SNAV";
		$coordonnes[4]="N� TVA Intracommunautaire FR 76442958443";
		$coordonnes[5]="Internet : www.monagence.com";
		$this->SetFont('Arial','',6);
		$this->SetFont('Arial','',7);
		$this->SetFillColor(215,207,207); 
		foreach($coordonnes as $cor){
			$this->Cell(180,3,$cor,0,1,'C',1);
		}
	}	
}

?> 
</body>
</html>
