<?php
session_start();
if (!(Isset($_SESSION["id_vendeur"]))) {
		   header("Location:index.php");
		   exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Effacer des clients - Agence de Voyage&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<link href="client.css" rel="stylesheet" type="text/css"/>

<SCRIPT LANGUAGE="JavaScript">
function basicFunction (id) {
	var confirmation=confirm ("Etes vous sur de vouloir effacer ce client : " + id +" ?");
	if (confirmation=="1") {
		
		window.open("effacer_clients.php?delete=confirm&id="+id,'efface_client','directories=no,location = no, menubar = no, resizable = no, scrollbars = no, status = no, toolbar = no,width = 800, height = 400')
	}
}



</SCRIPT>

</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
require_once("fonctions_db.php");
require_once("fonctions.php");
?>
</td>
<td width="750" style="border-left:1px solid black;padding:5px">
<h1>Gestion des clients de Agence de Voyage</h1>
<?php
$debut = $_GET['debut'];
$ids = $_GET['ids'];

$id = $_GET['id'];
$delete = $_GET['delete'];

if ($delete == "confirm")
{
	$link= connection(MYDATABASE);
	$requete2 = "DELETE FROM clients WHERE id='".$id."';";
	$result2 = mysql_query($requete2) or die (mysql_error());
	//echo $requete2."<br/>";
	echo("Client effac� !<br/>");
	die("<a href='javascript:window.close();'>Fermer cette fenetre</a>");
}

if (!empty($ids[0]))
{
	
	foreach ($ids as $id)
	{
		if(!is_array($id))
		{
			$link= connection(MYDATABASE);
			$requete1 = "SELECT id FROM dossiers WHERE id_client ='".$id."'";
			//echo $requete1;
			$result1 = mysql_query($requete1) or die (mysql_error());
			$num_rows1 = mysql_num_rows($result1);
			//$row1 = mysql_fetch_row($result1);
			//echo $row1[0];
			//echo ("<hr>");
			
			if ($num_rows1 > 0)
			{
				echo("<hr>");
				echo ("- Attention : le client ".$id." a un ou plusieurs dossiers enregistr�s - Impossible de l'effacer directement<br/>");
				
				Bouton_fiche_client($id);
				while ($dossiers = mysql_fetch_assoc($result1))
				{
					Bouton_retour_dossier($dossiers['id'], "_blank");
					Bouton_effacer_dossier($dossiers['id']);
				}
				echo("<hr>");
			}
			else
			{
				echo("<hr>");
				echo("<a href=\"javascript:basicFunction('".$id."');\">Effacer ce client (N�".$id.")</a><br/>");
				Bouton_fiche_client($id);
				echo("<hr>");
			}
			
		}

	}
}
$link= connection(MYDATABASE);
$requete="SELECT * FROM clients";
$result = mysql_query($requete);
$total_clients = mysql_num_rows($result);
$nb_pages=ceil($total_clients/30);


if(empty($debut)){$debut=0;};
$requete = "SELECT id, nom, prenom, mail, adresse1, adresse2, ville, cp, tel1, tel2, tel3, fax FROM clients  ORDER BY nom LIMIT ".$debut.",30;";
$result = mysql_query($requete) or die (mysql_error());

echo("<form action='".$_SERVER['PHP_SELF']."' method='get'>");
echo ("<input type='hidden' name='debut' value='".$debut."'>");
echo("<table style='border:1px solid black;margin:0;padding:0px;border-collapse:collapse;width:500px;'>\n\r");
echo("<tr style='border:1px solid black;font-weight:bold;'>\n\r");
echo("<td style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'>ID</td>\n\r");
echo("<td style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'>Nom</td>\n\r");
echo("<td style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'>Pr�nom</td>\n\r");
echo("<td style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'>Mail</td>\n\r");
echo("<td style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'>Adresse</td>\n\r");
//echo("<td style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'>T�l 1</td>\n\r");
//echo("<td style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'>T�l 2</td>\n\r");
//echo("<td style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'>Fiche client</td>\n\r");
echo("<td style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'>&nbsp;</td>\n\r");
echo("</tr>\n\r");

while($row = mysql_fetch_assoc($result))
{
	echo("<tr style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'>\n\r");
	echo("<td style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'>".$row['id']."</td>\n\r");
	echo("<td style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'>".$row['nom']."</td>\n\r");
	echo("<td style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'>".$row['prenom']."</td>\n\r");
	echo("<td style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'>".$row['mail']."</td>\n\r");
	echo("<td style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'>".$row['adresse1']." ".$row['adresse2']." ".$row['cp']." ".$row['ville']."</td>\n\r");
	//echo("<td style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'>".$row['tel1']."</td>\n\r");
	//echo("<td style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'>".$row['tel2']."</td>\n\r");
	//echo("<td style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'>".Bouton_fiche_client($row['id'])."</td>\n\r");
	echo("<td style='border:1px solid black;margin:0;padding:2px;border-collapse:collapse;'><input type='checkbox' name='ids[]' value='".$row['id']."' style='width:50px;'></td>\n\r");
	echo("</tr>\n\r");	
}
echo("</table><br/>\n\r");
echo("<center><input type='submit' name='Effacer' value='Effacer'></center>");
echo("</form>");
for($i=1;$i<=$nb_pages;$i++)
{
	$page_en_cours = (($debut+30)/30);
	if ($i == $page_en_cours)
	{
		echo("<strong><a href='".$PHP_SELF."?debut=".($i*30-30)."'>".$i."</a></strong> - ");
	}
	else
	{
		echo("<a href='".$PHP_SELF."?debut=".($i*30-30)."'>".$i."</a> - ");
	}
	
}

?>
</td>
</tr>
</table>

</body></html>

