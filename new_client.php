<?php
session_start();
if (!isset($_SESSION["id_vendeur"])) {
		   header("Location:index.php");
		   exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Cr�ation d'un nouveau client de Monagence&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="client.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">
<?php
require_once("fonctions_db.php");
require_once("fonctions.php");


$flag = $_POST["flag"];
$nom = $_POST["nom"];
$prenom = $_POST["prenom"];
$mail = $_POST["mail"];
$adresse1 = $_POST["adresse1"];
$adresse2 = $_POST["adresse2"];
$tel1 = $_POST["tel1"];
$tel2 = $_POST["tel2"];
$tel3 = $_POST["tel3"];
$fax = $_POST["fax"];
$ville = $_POST["ville"];
$cp = $_POST["cp"];
$pays = $_POST["pays"];
$sexe = $_POST["sexe"];
$civilite = $_POST["civilite"];

$jour_naissance = $_POST["jour_naissance"];
$mois_naissance = $_POST["mois_naissance"];
$annee_naissance = $_POST["annee_naissance"];
$connu_par = $_POST["connu_par"];
$avis_cli = $_POST["avis_cli"];
$commentaire = $_POST["commentaire"];



//Logique
if (!empty($flag)){
	$verification = Verif_form_new_client($nom, $prenom, $mail, $adresse1, $adresse2, $tel1, $tel2, $tel3, $fax, $ville, $cp, $pays, $sexe, $jour_naissance, $mois_naissance, $annee_naissance, $connu_par, $avis_cli, $commentaire, $civilite);
	if($verification == "TRUE"){
		echo("<br/>Insertion du nouveau client...<br/>");
		if(Creation_client($nom, $prenom, $mail, $adresse1, $adresse2, $tel1, $tel2, $tel3, $fax, $ville, $cp, $pays, $sexe, $jour_naissance, $mois_naissance, $annee_naissance, $connu_par, $avis_cli, $commentaire, $civilite)) {
			echo("<br/>Retour � l'index de l'interface d'admin : <a href='menu.php'>cliquez ici</a><br/>");
			echo("<br/>Retour � l'enregistrement de nouveaux clients : <a href='new_client.php'>cliquez ici</a><br/>");
		}
	}
	else {
		echo "<br/><font style='color:red;font-weight:bold'>ERREURS DANS LE FORMULAIRE<br/>".$verification."</font><br/><br/>";
		Form_new_client($nom, $prenom, $mail, $adresse1, $adresse2, $tel1, $tel2, $tel3, $fax, $ville, $cp, $pays, $sexe, $jour_naissance, $mois_naissance, $annee_naissance, $connu_par, $avis_cli, $commentaire, $civilite);
	}
}
else {
	Form_new_client($nom, $prenom, $mail, $adresse1, $adresse2, $tel1, $tel2, $tel3, $fax, $ville, $cp, $pays, $sexe, $jour_naissance, $mois_naissance, $annee_naissance, $connu_par, $avis_cli, $commentaire, $civilite);
}

?>
</td>
</tr>
</table>

</body></html>

