<?php
session_start();
if (!session_is_registered("id_vendeur")) {
		   header("Location:index.php");
		   exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Statistiques Monagence&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<link href="client.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
require_once("fonctions_db.php");
require_once("fonctions.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">
<?php
$jour = $_POST["jour"];
$mois = $_POST["mois"];
$annee = $_POST["annee"];

$montant_achat = $_POST["montant_achat"];
$facture_achat = $_POST["facture_achat"];
$id_dossier = $_POST["id_dossier"];


$jour_courant=date("d");
$mois_courant=date("m");
$annee_courante=date("y");


$date_comm = $_POST["date_comm"];
$type = $_POST["type"];
$commentaire = $_POST["commentaire"];

if (!empty($jour)) {$jour_courant = $jour;};
if (!empty($mois)) {$mois_courant = $mois;};
if (!empty($annee)) {$annee_courante = $annee;};

if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};


	echo "<h1>Visualisation du journal des ventes pour le : ".$jour_courant." / ".$mois_courant." / ".$annee_courante."</h1>";
	echo("<p align='center'><a href='stats2_mois.php'>Visualisation mois en cours</a></p>");
	echo "<p><form method='post' action='".$_SERVER['PHP_SELF']."?PHPSESSID=".session_id()."'><fieldset><legend>Choix de la p�riode</legend>";
	echo ("<label for='jour'>Voir une autre p�riode : </label>\n\r");
	echo ("<select name='jour' id='jour'>\n\r");
	for ($i=1;$i<=31;$i++) {
		if (strlen($i) < 2){$jour="0".$i;} else {$jour=$i;};
		echo ("<option value='".$jour."'");
		if ($jour == $jour_courant) {echo("selected='selected'");};
		echo (">".$jour."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='mois' id='mois'>\n\r");
	for ($i=1;$i<=12;$i++) {
		if (strlen($i) < 2){$mois="0".$i;} else {$mois=$i;};
		echo ("<option value='".$mois."'");
		if ($mois == $mois_courant) {echo("selected='selected'");};
		echo (">".$mois."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='annee' id='annee'>\n\r");
	for ($i=2005;$i<=$annee_courante;$i++) {
		echo ("<option value='".$i."'");
		if ($i == $annee_courante) {echo("selected='selected'");};
		echo (">".$i."</option>\n\r");
	}
	echo ("</select><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");
	echo "<input type='submit' name='Voir' id='Voir' value='Voir'></fieldset></form></p>";

	$annee_courante = substr($annee_courante,2,4);


unset($liste_client_total);
unset($liste_dossier_total);
echo("<h1>Historique du $jour_courant / $mois_courant / $annee_courante</h1>");

$link = connection(MYDATABASE);
$requete_vend = "SELECT vendeurs.id, vendeurs.prenom, vendeurs.nom FROM `vendeurs` WHERE rang != 'Admin' ORDER BY vendeurs.id;";
$result_vend=mysql_query($requete_vend) or die(mysql_error());
while ($row_vend = mysql_fetch_assoc($result_vend))
{
	
	unset($liste_dossier);
	unset($liste_client);
	$link = connection(MYDATABASE);
	$requete_dossier = "SELECT dossiers.id, clients.id AS client_id FROM `dossiers`,`clients`, lien_dossier_vendeur WHERE DAY(`date_transac`)=".$jour_courant." AND MONTH(`date_transac`)=".$mois_courant." AND YEAR(`date_transac`)='20".$annee_courante."' AND dossiers.id_client=clients.id  AND dossiers.id=lien_dossier_vendeur.id_dossier AND lien_dossier_vendeur.id_vendeur = '".$row_vend['id']."' ORDER BY dossiers.id;";
	$result_dossier=mysql_query($requete_dossier) or die(mysql_error());
	//echo $requete_dossier;
	$num_rows1 = mysql_num_rows($result_dossier);
	if ($num_rows1 > 0)
	{
		echo "<h3 style='text-align:center;color:yellow;background-color:red;'>".$row_vend['prenom']." ".$row_vend['nom']."</h3>";
		echo("<h3>Dossiers</h3>");
		while ($row_dossier = mysql_fetch_assoc($result_dossier))
		{
				$liste_dossier[] = $row_dossier['id'];
				$liste_client[] = $row_dossier['client_id'];
				$liste_client_total[] = $row_dossier['client_id'];
				$liste_dossier_total[] = $row_dossier['id'];
				
				
				echo $row_dossier['id']." : ";
				Bouton_retour_dossier($row_dossier['id'], '_blank');
				Bouton_fiche_client($row_dossier['client_id']);
				echo "<hr>";
		}
	}
	
	
	$requete_client = "SELECT clients.id FROM `clients` WHERE DAY(`date_inscription`)=".$jour_courant." AND MONTH(`date_inscription`)=".$mois_courant." AND YEAR(`date_inscription`)='20".$annee_courante."' AND clients.id_vendeur = '".$row_vend['id']."' ORDER BY clients.id;";
	$result_client=mysql_query($requete_client) or die(mysql_error());
	//echo $requete_client;
	$num_rows2 = mysql_num_rows($result_client);
	if ($num_rows2 > 0)
	{
		echo("<h3>Clients</h3>");
		while ($row_client = mysql_fetch_assoc($result_client))
		{
			if(is_array($liste_client))
			{
					if(!(in_array($row_client['id'], $liste_client)))
					{
						
						$requete_dossier2 = "
						SELECT DISTINCT
						dossiers.id AS id_dossier,
						dossiers.date_transac,
						vendeurs.id AS id_vendeur, 
						vendeurs.nom,
						vendeurs.prenom
						FROM 
						`dossiers`,
						`clients`,
						vendeurs,
						lien_dossier_vendeur 
						WHERE 
						dossiers.id_client='".$row_client['id']."'  
						AND 
						dossiers.id=lien_dossier_vendeur.id_dossier
						AND 
						lien_dossier_vendeur.id_vendeur = vendeurs.id  
						ORDER BY dossiers.id;";
						$result_dossier2=mysql_query($requete_dossier2) or die(mysql_error());
						$num_rows_dossier2 = mysql_num_rows($result_dossier2);
						
						//echo $requete_dossier2;
						
						$liste_client[] = $row_client['id'];
						$liste_client_total[] = $row_client['client_id'];
						echo $row_client['id']." : ";
						Bouton_fiche_client($row_client['id']);
						
						if($num_rows_dossier2 > 0)
						{
							while ($row_dossier2 = mysql_fetch_assoc($result_dossier2))
							{
								echo "Dossier ".$row_dossier2['id_dossier']." cr�e par ".$row_dossier2['prenom'].", le ".$row_dossier2['date_transac'];
							}
						}
						
						echo "<hr>";
					}
				}
		}
	}
	
	if (count($liste_dossier) > 0 || count($liste_client) > 0 )
	{
		echo("<h3>Total</h3>");
		echo ("<p>- Nb de dossiers cr�es : ".count($liste_dossier)."<br/>");
		echo ("- Nb de clients cr�es : ".count($liste_client)."</p>");
	}
}

		echo("<h3>Total du jour, tous vendeurs confondus</h3>");
		echo ("<p>- Nb de dossiers cr�es : ".count($liste_dossier_total)."<br/>");
		echo ("- Nb de clients cr�es : ".count($liste_client_total)."</p>");
?>
</td>
</tr>
</table>

</body></html>

