<?php
session_start();
if (!isset($_SESSION["id_vendeur"])) {
    header("Location:index.php");
    exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Recherche / Modification des dossiers de AgenceDeVoyage&copy;</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <link href="client.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <table class="generale">
            <tr>
                <td width="120" valign="top" class="menu">
                    <?php
                    include("menu.php");
                    ?>
                </td>
                <td width="680" style="border-left:1px solid black;padding:5px">
                    <?php
                    require_once("fonctions_db.php");
                    require_once("fonctions.php");

					print_r($_POST);
                    $id_dossier = $_POST["id_dossier"];

                    $recherche = $_POST["recherche"];
                    $champ = $_POST["champ"];
                    $partie = $_POST["partie"];

                    $id_vendeur = $_SESSION["id_vendeur"];

                    $id_client = $_POST["id_client"];
                    $id_pkg = $_POST["id_pkg"];
                    $etat = $_POST["etat"];
                    $dest_pays = $_POST["dest_pays"];
                    $dest_ville = $_POST["dest_ville"];
                    $dest_hotel = $_POST["dest_hotel"];
                    $duree = $_POST["duree"];
                    $duree_autre = $_POST["duree_autre"];
                    $cat_hot = $_POST["cat_hot"];
                    $adultes = $_POST["adultes"];
                    $enfants = $_POST["enfants"];
                    $bebes = $_POST["bebes"];
                    $pension = $_POST["pension"];
                    $assur = $_POST["assur"];
                    $jour_depart = $_POST["jour_depart"];
                    $mois_depart = $_POST["mois_depart"];
                    $annee_depart = $_POST["annee_depart"];
                    $jour_retour = $_POST["jour_retour"];
                    $mois_retour = $_POST["mois_retour"];
                    $annee_retour = $_POST["annee_retour"];
                    $prix_base_adulte = $_POST["prix_base_adulte"];
                    $prix_base_enfant = $_POST["prix_base_enfant"];
                    $prix_base_bebe = $_POST["prix_base_bebe"];
                    $supp_adulte = $_POST["supp_adulte"];
                    $supp_enfant = $_POST["supp_enfant"];
                    $supp_bebe = $_POST["supp_bebe"];
                    $taxes_adulte = $_POST["taxes_adulte"];
                    $taxes_enfant = $_POST["taxes_enfant"];
                    $taxes_bebe = $_POST["taxes_bebe"];
                    $frais_dossiers = $_POST["frais_dossiers"];

                    $acompte = $_POST["acompte"];
                    $mode_payement = $_POST["mode_payement"];
                    $nb_supp_adulte = $_POST["nb_supp_adulte"];
                    $nb_supp_enfant = $_POST["nb_supp_enfant"];
                    $nb_supp_bebe = $_POST["nb_supp_bebe"];
                    $nb_taxe_adulte = $_POST["nb_taxe_adulte"];
                    $nb_taxe_enfant = $_POST["nb_taxe_enfant"];
                    $nb_taxe_bebe = $_POST["nb_taxe_bebe"];
                    $nb_reduc_adulte = $_POST["nb_reduc_adulte"];
                    $nb_reduc_enfant = $_POST["nb_reduc_enfant"];
                    $nb_reduc_bebe = $_POST["nb_reduc_bebe"];
                    $supp_divers = $_POST["supp_divers"];
                    $nb_supp_divers = $_POST["nb_supp_divers"];

                    $reduc_adulte = $_POST["reduc_adulte"];
                    $reduc_enfant = $_POST["reduc_enfant"];
                    $reduc_bebe = $_POST["reduc_bebe"];

                    $commentaires = $_POST["commentaires"];



                    $ville_depart=$_POST["ville_depart"];

                    $etape = $_POST["etape"];
                    $flag = $_POST["flag"];

                    $compris=$_POST["compris"];
                    $compris=@implode("|",$compris);
                    $compris=str_replace("|Array", "", $compris);

                    $to = $_POST["to"];
                    $hausse_carb = $_POST["hausse_carb"];
                    $nb_hausse_carb = $_POST["nb_hausse_carb"];


                    $commentaires_cli = $_POST['commentaires_cli'];
                    $type_chambre = $_POST['type_chambre'];
                    $supp_adulte_div = $_POST['supp_adulte_div'];
                    $supp_enfant_div = $_POST['supp_enfant_div'];
                    $supp_bebe_div = $_POST['supp_bebe_div'];
                    $nb_supp_adulte_div = $_POST['nb_supp_adulte_div'];
                    $nb_supp_enfant_div = $_POST['nb_supp_enfant_div'];
                    $nb_supp_bebe_div = $_POST['nb_supp_bebe_div'];
                    $intitule_supp_adulte_div = $_POST['intitule_supp_adulte_div'];
                    $intitule_supp_enfant_div = $_POST['intitule_supp_enfant_div'];
                    $intitule_supp_bebe_div = $_POST['intitule_supp_bebe_div'];
                    $hausse_carb_bebe = $_POST['hausse_carb_bebe'];
                    $nb_hausse_carb_bebe = $_POST['nb_hausse_carb_bebe'];

                    $assur1 = $_POST['assur1'];
                    $nb_assur = $_POST['nb_assur'];
                    $nb_assur1 = $_POST['nb_assur1'];

                    $type_chambre1 = $_POST['type_chambre1'];
                    $nb_type_chambre = $_POST['nb_type_chambre'];
                    $nb_type_chambre1 =$_POST['nb_type_chambre1'];


                    $supp_adulte_div2 = $_POST['supp_adulte_div2'];
                    $supp_adulte_div3 = $_POST['supp_adulte_div3'];
                    $nb_supp_adulte_div2 = $_POST['nb_supp_adulte_div2'];
                    $nb_supp_adulte_div3 = $_POST['nb_supp_adulte_div3'];
                    $intitule_supp_adulte_div2 = $_POST['intitule_supp_adulte_div2'];
                    $intitule_supp_adulte_div3 = $_POST['intitule_supp_adulte_div3'];

                    $provenance_dossier =$_POST['provenance_dossier'];

                    $ref_to =$_POST['ref_to'];
					
					$debugref_fab = $_POST['ref_to'];

                    //$factureTO = $_POST['factureTO'];
                    $montantTo = $_POST['montantTo'];

                    function Afficher_form_recherche_dossier() {

                        echo "<script type='text/javascript'>";
                        echo "function selectByRefTo() {";
                        echo "for(i=0;i<document.recherche.champ.length;++i)
                                                {
                                                  if(document.recherche.champ.options[i].value == 'ref_to')
                                                    {
                                                       document.recherche.champ.options[i].selected = true;
                                                    }
                                                }
                                                }";
                        echo "</script>";

                        echo("<h1>recherche / modification d'un dossier AgenceDeVoyage</h1>");
                        echo ("<form action='".$_SERVER['PHP_SELF']."' name='recherche' id='recherche' method='post'>\n\r");

                        //Fieldset Coordonnees
                        echo ("<fieldset><legend>Rechercher :</legend><br/>\n\r");
                        //Champ texte
                        echo ("<label for='recherche'>Terme recherche</label>\n\r");
                        echo ("<input type='text' name='recherche' id='recherche' value='".$recherche."'><br/>\n\r");
                        echo ("<br style='clear:both'/>\n\r");

                        //Champ select
                        echo ("<label for='champ'>Dans quel champ ?</label>\n\r");
                        echo ("<select name='champ' id='champ'>\n\r");
                        $link = connection(MYDATABASE);
                        $requete = "SELECT * FROM dossiers LIMIT 1;";
                        $result = mysql_query($requete,$link) or die(mysql_error());

                        $nbr_champ = mysql_num_fields($result);

                        for ($i=0;$i<$nbr_champ;$i++)
                        {
                            echo("<option value='".mysql_field_name($result, $i)."'");
                            echo(">".mysql_field_name($result, $i)."</option>\n\r");
                        }
                        deconnection($link);
                        echo ("</select><a href='javascript:void(0);' onclick='javascript:selectByRefTo();'>Recherche par Ref. TO</a><br/>\n\r");
                        echo ("<br style='clear:both'/>\n\r");

                        //Recherche exacte ou partielle
                        echo ("<label for='partie1'>Egal à</label>\n\r");
                        echo ("<input type='radio' name='partie' id='partie1' value='egal'><br/>\n\r");
                        echo ("<label for='partie2'>Commençant par</label>\n\r");
                        echo ("<input type='radio' name='partie' id='partie2' value='commence' checked='checked'><br/>\n\r");
                        echo ("<br style='clear:both'/>\n\r");


                        echo ("</fieldset>");

                        echo ("<input type='hidden' name='flag' id='flag' value='chercher'><br/>\n\r");
                        echo ("<div class='center'><input type='submit' name='Rechercher' id='Rechercher' value='Rechercher'><br/></div>\n\r");
                        echo ("</form><br/>");
                    }


                    function Recherche_dossier($recherche, $champ, $partie) {
                        $link = connection(MYDATABASE);
                        if ($partie == "commence"){$append="%";};
                        $requete = "SELECT * FROM dossiers WHERE LOWER(".$champ.") LIKE '".strtolower($recherche).$append."';";
                        //echo $requete;
                        $result = mysql_query($requete,$link) or die(mysql_error() ." : ". $requete);
                        if (mysql_num_rows($result)>0)
                        {
                            echo("<h1>Choisissez le dossier à modifier :</h1><br/>");
                            //echo("<ul>");
                            echo ("<form action='".$_SERVER['PHP_SELF']."' name='recherche' id='recherche' method='post'>\n\r");
                            //Fieldset
                            echo ("<fieldset><legend>Sélection dossier</legend><br/>\n\r");

                            while($row = mysql_fetch_row($result))
                            {
                                echo ("<label for='id_dossier".$row[0]."' style='width:80%'>".$row[0]." / ".$row[1]." ".$row[2]." ".$row[3]." (".$row[4].") - Date conf : ".$row[67]."</label><input type='radio' name='id_dossier' id='id_dossier".$row[0]."' value='".$row[0]."' style='width:10%;'><br/>\n\r");
                                echo ("<hr style='height:1px'><br style='clear:both'/>\n\r");
                            }
                            //echo("</ul>");
                            echo ("</fieldset>");

                            echo ("<input type='hidden' name='flag' id='flag' value='modifier'><br/>\n\r");
                            echo ("<div align='center'><input type='submit' name='Modifier' id='Modifier' value='Modifier'><br/></div>\n\r");
                            echo ("</form><br/>");
                        }
                        else
                        {
                            echo("<h1>Recherche sans résultats</h1><br/>");
                            echo("<p><a href='search_dossier.php?PHPSESSID=".session_id()."'>Retour au formulaire de recherche</a></p>");
                        }


                        deconnection($link);
                    }



                    $verif = Verif_form_new_dossier($id_dossier, $id_client, $id_pkg, $etat, $dest_pays, $dest_ville, $dest_hotel, $duree, $duree_autre, $cat_hot, $adultes, $enfants, $bebes, $pension, $assur, $jour_depart, $mois_depart, $annee_depart, $jour_retour, $mois_retour, $annee_retour, $prix_base_adulte, $prix_base_enfant, $prix_base_bebe, $supp_adulte, $supp_enfant, $supp_bebe, $taxes_adulte, $taxes_enfant, $taxes_bebe, $frais_dossiers, $acompte, $mode_payement, $nb_supp_adulte, $nb_supp_enfant, $nb_supp_bebe, $nb_taxe_adulte, $nb_taxe_enfant, $nb_taxe_bebe, $nb_reduc_adulte, $nb_reduc_enfant, $nb_reduc_bebe, $supp_divers, $nb_supp_divers, $commentaires,  $reduc_adulte, $reduc_enfant, $reduc_bebe, $ville_depart, $compris, $to, $hausse_carb, $nb_hausse_carb, $commentaires_cli, $type_chambre, $supp_adulte_div, $supp_enfant_div, $supp_bebe_div, $nb_supp_adulte_div, $nb_supp_enfant_div, $nb_supp_bebe_div, $intitule_supp_adulte_div, $intitule_supp_enfant_div, $intitule_supp_bebe_div, $hausse_carb_bebe, $nb_hausse_carb_bebe, $assur1, $nb_assur, $nb_assur1, $type_chambre1, $nb_type_chambre, $nb_type_chambre1, $supp_adulte_div2, $supp_adulte_div3, $nb_supp_adulte_div2, $nb_supp_adulte_div3, $intitule_supp_adulte_div2, $intitule_supp_adulte_div3, $provenance_dossier, $ref_to);


                    if ($flag == "modification") {
                        Modifier_dossier();
                    }
                    else if ($flag == "modifier") {
                        if(!empty($id_dossier)){
                            $link = connection(MYDATABASE);
                            $requete = "SELECT * FROM dossiers WHERE id = '$id_dossier';";
                            //echo $requete;
                            $result = mysql_query($requete,$link) or die(mysql_error() ." : ". $requete);
                            while($row = mysql_fetch_row($result))
                            {
                                //print_r($row);
                                //Affectation des données pour le traitement
                                $id = $row[0] ;
                                $id_client =$row[2];
                                $id_pkg = $row[1];
                                $etat = $row[3];
                                $dest_pays = $row[4];
                                $dest_ville = $row[5];
                                $dest_hotel = $row[6];
                                $duree = $row[7];
                                $duree_autre = $row[8];
                                $cat_hot = $row[9];
                                $adultes = $row[10];
                                $enfants = $row[11];
                                $bebes = $row[12];
                                $pension = $row[13];
                                $assur = $row[14];
                                $date_deb = $row[15];
                                $date_fin = $row[16];
                                $prix_base_adulte = $row[17];
                                $prix_base_enfant = $row[18];
                                $prix_base_bebe = $row[19];
                                $supp_adulte = $row[20];
                                $supp_enfant = $row[21];
                                $supp_bebe = $row[22];
                                $taxes_adulte = $row[23];
                                $taxes_enfant = $row[24];
                                $taxes_bebe = $row[25];
                                $frais_dossiers = $row[26];

                                $acompte = $row[28];


                                $mode_payement = $row[29];
                                $nb_supp_adulte = $row[30];
                                $nb_supp_enfant = $row[31];
                                $nb_supp_bebe = $row[32];
                                $nb_taxe_adulte = $row[33];
                                $nb_taxe_enfant = $row[34];
                                $nb_taxe_bebe = $row[35];
                                $nb_reduc_adulte = $row[36];
                                $nb_reduc_enfant = $row[37];
                                $nb_reduc_bebe = $row[38];
                                $supp_divers = $row[39];
                                $nb_supp_divers = $row[40];

                                $commentaires = $row[41];

                                $reduc_adulte = $row[42];
                                $reduc_enfant = $row[43];
                                $reduc_bebe = $row[44];

                                $ville_depart = $row[45];

                                $compris = $row[46];

                                $to = $row[47];
                                $hausse_carb = $row[48];
                                $nb_hausse_carb = $row[49];

                                $commentaires_cli = $row[50];
                                $type_chambre = $row[51];
                                $supp_adulte_div = $row[52];
                                $supp_enfant_div = $row[53];
                                $supp_bebe_div = $row[54];
                                $nb_supp_adulte_div = $row[55];
                                $nb_supp_enfant_div = $row[56];
                                $nb_supp_bebe_div = $row[57];
                                $intitule_supp_adulte_div = $row[58];
                                $intitule_supp_enfant_div = $row[59];
                                $intitule_supp_bebe_div = $row[60];
                                $hausse_carb_bebe = $row[61];
                                $nb_hausse_carb_bebe = $row[62];

                                $assur1 = $row[64];
                                $nb_assur = $row[65];
                                $nb_assur1 = $row[66];

                                $type_chambre1 = $row[68];
                                $nb_type_chambre = $row[69];
                                $nb_type_chambre1 = $row[70];


                                $supp_adulte_div2 = $row[71];
                                $supp_adulte_div3 = $row[72];
                                $nb_supp_adulte_div2 = $row[73];
                                $nb_supp_adulte_div3 = $row[74];
                                $intitule_supp_adulte_div2 = $row[75];
                                $intitule_supp_adulte_div3 = $row[76];

                                $provenance_dossier = $row[78];

                                $ref_to = $row[79];
								
								// var_dump('ligne 346: ' . $row[79]);

                                $jour_depart = substr($date_deb, 8,2);
                                $mois_depart = substr($date_deb, 5,2);
                                $annee_depart = substr($date_deb, 0,4);



                                $jour_retour = substr($date_fin, 8,2);
                                $mois_retour = substr($date_fin, 5,2);
                                $annee_retour = substr($date_fin, 0,4);

								// die();
                            }
                            deconnection($link);
                            $rang = get_vendeur_rang($_SESSION["id_vendeur"]);
                            // echo("($id_client, $id, $id_pkg, $etat, $dest_pays, $dest_ville, $dest_hotel, $duree, $duree_autre, $cat_hot, $adultes, $enfants, $bebes, $pension, $assur, $jour_depart, $mois_depart, $annee_depart, $jour_retour, $mois_retour, $annee_retour, $prix_base_adulte, $prix_base_enfant, $prix_base_bebe, $supp_adulte, $supp_enfant, $supp_bebe, $taxes_adulte, $taxes_enfant, $taxes_bebe, $frais_dossiers, 'modification', $acompte, $mode_payement, $nb_supp_adulte, $nb_supp_enfant, $nb_supp_bebe, $nb_taxe_adulte, $nb_taxe_enfant, $nb_taxe_bebe, $nb_reduc_adulte, $nb_reduc_enfant, $nb_reduc_bebe, $supp_divers, $nb_supp_divers, $commentaires, $reduc_adulte, $reduc_enfant, $reduc_bebe, $ville_depart,$compris, $to, $hausse_carb, $nb_hausse_carb, $commentaires_cli, $type_chambre, $supp_adulte_div, $supp_enfant_div, $supp_bebe_div, $nb_supp_adulte_div, $nb_supp_enfant_div, $nb_supp_bebe_div, $intitule_supp_adulte_div, $intitule_supp_enfant_div, $intitule_supp_bebe_div, $hausse_carb_bebe, $nb_hausse_carb_bebe, $assur1, $nb_assur, $nb_assur1, $type_chambre1, $nb_type_chambre, $nb_type_chambre1)");
								
							// echo ('ref to = ' .$ref_to);
							// die();
							
							Form_new_dossier($id_client, $id, $id_pkg, $etat, $dest_pays, $dest_ville, $dest_hotel, $duree, $duree_autre, $cat_hot, $adultes, $enfants, $bebes, $pension, $assur, $jour_depart, $mois_depart, $annee_depart, $jour_retour, $mois_retour, $annee_retour, $prix_base_adulte, $prix_base_enfant, $prix_base_bebe, $supp_adulte, $supp_enfant, $supp_bebe, $taxes_adulte, $taxes_enfant, $taxes_bebe, $frais_dossiers, "modification", $acompte, $mode_payement, $nb_supp_adulte, $nb_supp_enfant, $nb_supp_bebe, $nb_taxe_adulte, $nb_taxe_enfant, $nb_taxe_bebe, $nb_reduc_adulte, $nb_reduc_enfant, $nb_reduc_bebe, $supp_divers, $nb_supp_divers, $commentaires, $reduc_adulte, $reduc_enfant, $reduc_bebe, $ville_depart,$compris, $to, $hausse_carb, $nb_hausse_carb, $commentaires_cli, $type_chambre, $supp_adulte_div, $supp_enfant_div, $supp_bebe_div, $nb_supp_adulte_div, $nb_supp_enfant_div, $nb_supp_bebe_div, $intitule_supp_adulte_div, $intitule_supp_enfant_div, $intitule_supp_bebe_div, $hausse_carb_bebe, $nb_hausse_carb_bebe, $assur1, $nb_assur, $nb_assur1, $type_chambre1, $nb_type_chambre, $nb_type_chambre1, $supp_adulte_div2, $supp_adulte_div3, $nb_supp_adulte_div2, $nb_supp_adulte_div3, $intitule_supp_adulte_div2, $intitule_supp_adulte_div3, $provenance_dossier, $ref_to);
                            Bouton_facture($id, $rang);
                            echo("<center><p><a href='passagers.php?PHPSESSID=".session_id()."&id_dossier=".$id_dossier."' target='_blank'>Modifier les passagers, cliquez ici</a></p></center>");

                        }
                        else {
                            echo("Vous devez selectionner un dossier pour le modifier !!<br/>");
                            echo("<a href='search_dossier.php?PHPSESSID=".session_id()."'>Recherche de dossiers, cliquez ici</a><br/>");
                        }
                    }
                    else if ($etape == "modification") {
                        if($verif=="TRUE")
                        {
                            Update_dossier($id_client, $id_pkg, $etat, $dest_pays, $dest_ville, $dest_hotel, $duree, $duree_autre, $cat_hot, $adultes, $enfants, $bebes, $pension, $assur, $jour_depart, $mois_depart, $annee_depart, $jour_retour, $mois_retour, $annee_retour, $prix_base_adulte, $prix_base_enfant, $prix_base_bebe, $supp_adulte, $supp_enfant, $supp_bebe, $taxes_adulte, $taxes_enfant, $taxes_bebe, $frais_dossiers, $id_vendeur, $id_dossier, $acompte, $mode_payement, $nb_supp_adulte, $nb_supp_enfant, $nb_supp_bebe, $nb_taxe_adulte, $nb_taxe_enfant, $nb_taxe_bebe, $nb_reduc_adulte, $nb_reduc_enfant, $nb_reduc_bebe, $supp_divers, $nb_supp_divers, $commentaires,  $reduc_adulte, $reduc_enfant, $reduc_bebe, $ville_depart, $compris, $to, $hausse_carb, $nb_hausse_carb, $commentaires_cli, $type_chambre, $supp_adulte_div, $supp_enfant_div, $supp_bebe_div, $nb_supp_adulte_div, $nb_supp_enfant_div, $nb_supp_bebe_div, $intitule_supp_adulte_div, $intitule_supp_enfant_div, $intitule_supp_bebe_div, $hausse_carb_bebe, $nb_hausse_carb_bebe, $assur1, $nb_assur, $nb_assur1, $type_chambre1, $nb_type_chambre, $nb_type_chambre1, $supp_adulte_div2, $supp_adulte_div3, $nb_supp_adulte_div2, $nb_supp_adulte_div3, $intitule_supp_adulte_div2, $intitule_supp_adulte_div3, $provenance_dossier, /* $ref_to */ $debugref_fab, $montantTo);
                            echo("<a href='passagers.php?PHPSESSID=".session_id()."&id_dossier=".$id_dossier."'>Modifier les passagers, cliquez ici</a><br/>");

                        }
                        else
                        {
                            echo "<font color='red'><strong>".$verif."</strong></font>";
                            Form_new_dossier($id_client, $id_dossier, $id_pkg, $etat, $dest_pays, $dest_ville, $dest_hotel, $duree, $duree_autre, $cat_hot, $adultes, $enfants, $bebes, $pension, $assur, $jour_depart, $mois_depart, $annee_depart, $jour_retour, $mois_retour, $annee_retour, $prix_base_adulte, $prix_base_enfant, $prix_base_bebe, $supp_adulte, $supp_enfant, $supp_bebe, $taxes_adulte, $taxes_enfant, $taxes_bebe, $frais_dossiers, "modification", $acompte, $mode_payement, $nb_supp_adulte, $nb_supp_enfant, $nb_supp_bebe, $nb_taxe_adulte, $nb_taxe_enfant, $nb_taxe_bebe, $nb_reduc_adulte, $nb_reduc_enfant, $nb_reduc_bebe, $supp_divers, $nb_supp_divers, $commentaires, $reduc_adulte, $reduc_enfant, $reduc_bebe, $ville_depart, $compris, $to, $hausse_carb, $nb_hausse_carb, $commentaires_cli, $type_chambre, $supp_adulte_div, $supp_enfant_div, $supp_bebe_div, $nb_supp_adulte_div, $nb_supp_enfant_div, $nb_supp_bebe_div, $intitule_supp_adulte_div, $intitule_supp_enfant_div, $intitule_supp_bebe_div, $hausse_carb_bebe, $nb_hausse_carb_bebe, $assur1, $nb_assur, $nb_assur1, $type_chambre1, $nb_type_chambre, $nb_type_chambre1, $supp_adulte_div2, $supp_adulte_div3, $nb_supp_adulte_div2, $nb_supp_adulte_div3, $intitule_supp_adulte_div2, $intitule_supp_adulte_div3, $provenance_dossier, $ref_to);
                            echo("\n\r<center><p><a href='passagers.php?PHPSESSID=".session_id()."&id_dossier=".$id_dossier."' target='_blank'>Modifier les passagers, cliquez ici</a></p></center>\n\r");
                        }
                    }
                    else if ($flag == "chercher") {
                        Recherche_dossier($recherche, $champ, $partie);
                    }
                    else {
                        Afficher_form_recherche_dossier();
                    }

                    if(isset($_FILES['factureTO']))
                    {
                        $dossier = 'facturesTO/';
                        @mkdir($dossier . $id_dossier);
                        $dossier = $dossier . $id_dossier.'/';
                        @chmod($dossier, 0777);
                        //echo $dossier;
                        $fichier = basename($_FILES['factureTO']['name']);
                        if(!isset($erreur)) //S'il n'y a pas d'erreur, on upload
                        {
                            //On formate le nom du fichier ici...
                            $fichier = strtr($fichier,
                              'ÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃ Ã¡Ã¢Ã£Ã¤Ã¥Ã§Ã¨Ã©ÃªÃ«Ã¬Ã­Ã®Ã¯Ã°Ã²Ã³Ã´ÃµÃ¶Ã¹ÃºÃ»Ã¼Ã½Ã¿',
                              'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
                            $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);

                            if(move_uploaded_file($_FILES['factureTO']['tmp_name'], $dossier . $fichier)) //Si la fonction renvoie TRUE, c'est que Ã§a a fonctionnÃ©...
                            {
                                echo "facture TO  bien upload&eacute;e";
                            }
                            else //Sinon (la fonction renvoie FALSE).
                            {
                                echo "Probleme facture TO";
                            }
                        }
                        else
                        {
                            echo $erreur;
                        }
                    }
                    //echo "1";
                    // || !empty($montantTo)
                    if( isset($montantTo) )
                    {
                       // echo "2";
                        
                        //$montantTo = (float)$_POST['montantTo'];
                        //est-ce que l'achat est deja enregistrÃ©.. ?
                        $link = Connection(MYDATABASE);
                        $sql = "SELECT id FROM achats WHERE id_dossier='".$id_dossier."' LIMIT 1;";
                        $res = mysql_query($sql) or die(mysql_error());
                        if(mysql_num_rows($res) > 0)
                        {
                            $row = mysql_fetch_assoc($res);
                            $sql = "UPDATE achats SET montant_achat='".$montantTo."' WHERE id_dossier = '".$id_dossier." '; ";
                            $res = mysql_query($sql) or die(mysql_error());
                        }
                        else
                        {
                           // $row = mysql_fetch_assoc($res);
                            $sql = "INSERT INTO achats (id_dossier, montant_achat, date, date_payement) VALUES ('".$id_dossier." ', '".$montantTo."', NOW(), NOW()); ";
                            $res = mysql_query($sql) or die(mysql_error());
                        }
                    }
                    ?>
                </td>
            </tr>
        </table>

</body></html>

