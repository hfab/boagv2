<?php
	require_once("fonctions_db.php");
	require_once("fonctions.php");
	
	//$mois_courant = $_GET['mois'];
	//$annee_courante = $_GET['annee'];

	$link = connection(MYDATABASE);
	//Requetes TVA Fr=>Fr / Fr=>Eu Fr=>Autre
	unset($fr_eu);
	unset($fr_fr);
	
	$requete = "
	SELECT 
	dossiers.id,
	dossiers.dest_ville,
	clients.ville
	FROM 
	dossiers, 
	clients,
	achats
	WHERE 
	dossiers.dest_pays='FR' 
	AND 
	dossiers.id_client=clients.id 
	AND 
	clients.pays='FR' 
	AND etat='Confirm�' 
	AND 
	MONTH(achats.date)='".$mois_courant."' 
	AND 
	YEAR(achats.date)='".$annee_courante."'
	AND
	achats.id_dossier = dossiers.id
	;";
	$result = mysql_query($requete) or die(mysql_error().$requete);
	
	while ($row = mysql_fetch_assoc($result))
	{
		$fr_fr[] = $row['id'];
	}


	$requete = "
	SELECT 
	dossiers.id,
	dossiers.dest_ville,
	clients.ville,
	code_pays.lpays_fr AS pays
	FROM 
	dossiers, 
	clients,
	achats,
	code_pays
	WHERE 
	dossiers.dest_pays=code_pays.cpays
	AND
	code_pays.europe_compta = '1' 
	AND 
	dossiers.id_client=clients.id 
	AND 
	clients.pays='FR' 
	AND etat='Confirm�' 
	AND 
	MONTH(achats.date)='".$mois_courant."' 
	AND 
	YEAR(achats.date)='".$annee_courante."'
	AND
	achats.id_dossier = dossiers.id
	;";
	$result = mysql_query($requete) or die(mysql_error().$requete);
	while ($row = mysql_fetch_assoc($result))
	{
		$fr_eu[] = $row['id'];
	}
	
	
	
	
	//Requete de creation du journal
	$requete = "
	SELECT DISTINCT 
	dossiers.id AS id_dossier,  
	clients.nom,
	clients.prenom,
	clients.id AS id_client,
	achats.date AS date_achat,
	achats.montant_achat,
	tour_op_compta.id_compta AS code_to
	FROM 
	`dossiers`, 
	`clients`, 
	`vendeurs`, 
	`lien_dossier_vendeur`,
	achats,
	tour_op_compta
	WHERE 
	dossiers.id_client=clients.id
	AND 
	MONTH(achats.date)='".$mois_courant."' 
	AND 
	YEAR(achats.date)='".$annee_courante."' 
	AND
	achats.id_dossier = dossiers.id
	AND
	dossiers.to = tour_op_compta.nom
	ORDER BY 
	achats.date;
	";
	//echo $requete;
	unset ($out);
	$result = mysql_query($requete) or die (mysql_error().$requete);
	while($row = mysql_fetch_assoc($result))
	{
		$total_dossier = Calcul_total_dossier($row['id_dossier']);
		$total_assur = Total_assur($row['id_dossier']);
		
		//$n_piece = str_replace('180','',$row['id_dossier']);
		$n_piece = substr ($row['id_dossier'],-5);
		$compte = "9".$n_piece;
		
		
		//print_r($row);
		//echo $total_dossier;
		//print_r($total_assur);
		//echo "<br/><hr/>";
		if ($total_assur[0] > 0)
		{
			//Assurances ligne 1
			$out .= $row['date_achat'].";";
			$out .= "VE;";
			$out .= $compte.";";
			$out .= $n_piece.";";
			$out .= $row['prenom']." ".$row['nom'].";";
			$out .= number_format($total_assur[0], 2, ',', ' ').";";
			$out .= ";<br/>";
		
			//Assurances ligne 2
			$out .= $row['date_achat'].";";
			$out .= "VE;";
			$out .= "706901;";
			$out .= $n_piece.";";
			$out .= $row['prenom']." ".$row['nom'].";";
			$out .= ";";
			$out .= number_format($total_assur[0], 2, ',', ' ').";<br/>";			
			
		}
		
		// Ligne total vente
		$out .= $row['date_achat'].";";
		$out .= "VE;";
		$out .= $compte.";";
		$out .= $n_piece.";";
		$out .= $row['prenom']." ".$row['nom'].";";
		$out .= number_format(($total_dossier-$total_assur[0]), 2, ',', ' ').";";
		$out .= ";<br/>";
		
		//Ligne total achat
		$out .= $row['date_achat'].";";
		$out .= "VE;";
		$out .= $row['code_to'].";";
		$out .= $n_piece.";";
		$out .= $row['prenom']." ".$row['nom'].";";
		$out .= ";";
		$out .= number_format($row['montant_achat'], 2, ',', ' ').";<br/>";

				
		if(in_array($row['id_dossier'], $fr_fr))
		{
			$compte_marge = "706700";
		}
		else if (in_array($row['id_dossier'], $fr_eu))
		{
			$compte_marge = "706800";
		}
		else
		{
			$compte_marge = "706900";
		}
		
		//Ligne marge
		$out .= $row['date_achat'].";";
		$out .= "VE;";
		$out .= $compte_marge.";";
		$out .= $n_piece.";";
		$out .= $row['prenom']." ".$row['nom'].";";
		$out .= ";";
		$out .= number_format((($total_dossier-$total_assur[0])-$row['montant_achat']), 2, ',', ' ').";<br/>";
		
	}

echo $out;

?> 

