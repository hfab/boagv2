<h1>Import des clients/dossiers de Monagence.com</h1>
<?php
require_once("fonctions_db.php");
require_once("fonctions.php");

$ftp_server = 'lamporchestra.orchestra-platform.com';
$ftp_user_name = 'monagence_mcto';
$ftp_user_pass = 'rtoc85ag';
$conn_id = ftp_connect($ftp_server);
$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

function ftp_is_dir($folder) {
   global $conn_id;
   if (@ftp_chdir($conn_id, $folder)) {
      ftp_chdir($conn_id, '..');
      return true;
   } else {
      return false;
   }
}

function xdir($path) {
   global $conn_id;
   $contents = ftp_nlist($conn_id, $path);
   foreach($contents as $file) {
      if ($file!='.'&&$file!='..') {
         if (ftp_is_dir($file)) {
            echo $file . ' is a folder<br>';
         } else {
         		$command1 = 'RNFR '.$file;
         		$command2 = 'RNTO /olds/'.$file;

						echo ftp_raw($conn_id,$command1);
						echo ftp_raw($conn_id,$command2);
         		
            echo $file . ' is a file<br>';
         }
      }
   }
}

xdir('.');




/**
for($i=1;$i<$nb;$i++)
{
	echo ("<br>".$urls['href'][$i]."<br>");
}



for($i=1;$i<$nb;$i++)
{
	//$nb++;
	if ($urls['href'][$i] != "" && $urls['href'][$i] !="/site-admin/reservation_info/")
	{
		$url_courante = $url_base.$urls['href'][$i]."quotation/content.qx_oqn";
		echo $url_courante."<br/>";
		if($file2 = file_get_contents($url_courante))
		{
			
			echo "Recuperation du ".$urls['href'][$i]."<br/>";
			//USER
			$user[$i]['id_qts']= $urls['href'][$i];
			
			$xml = simplexml_load_string($file2);
			//Echo $url_courante." : OK !<br/>";
			$user[$i]['email']=strtolower((String)$xml->to->coords->email);
			if (strtolower((String)$xml->to->name["prefix"]) == "m")
			{
				$user[$i]['civilite']= "Mr";
			}
			else if (strtolower((String)$xml->to->name["prefix"]) == "mme")
			{
				$user[$i]['civilite']= "Mme";
			}
			else
			{
				$user[$i]['civilite']= "Melle";
			}
			$user[$i]['nom']=strtoupper(secureString2((String)$xml->to->name["last"]));
			$user[$i]['prenom']=secureString2(ucfirst(strtolower((String)$xml->to->name["first"])));
			if($user[$i]['civilite'] == "Mr")
			{
				$user[$i]['sexe'] = "h";
			}
			else
			{
			
				$user[$i]['sexe'] = "f";
			}
			$user[$i]['date_naiss']=str_replace('/','',str_replace('-','',(String)$xml->to["birthDate"]));
			
			foreach ($xml->to->coords->address->addressLine as $adress)
			{
				$user[$i]['adresse1'] .= secureString2($adress)." ";
			}
			//$user[$i]['adresse1'] = (String)$xml->to->coords->address->addressLine[0];
			$user[$i]['ville'] = secureString2((String)$xml->to->coords->address->city["name"]);
			$user[$i]['cp'] = (String)$xml->to->coords->address->postalZone["code"];
			$user[$i]['pays'] = secureString2((String)$xml->to->coords->address->country["name"]);
			//echo $user[$i]['cp']."<br/>";
			foreach ($xml->to->coords->phone as $phone)
			{
				if ($phone["nature"] == "home")
				{
					$user[$i]['tel1'] = str_replace(' ','',str_replace('-','',str_replace('.','',$phone->number)));
				}
				elseif ($phone["nature"] == "office")
				{
					$user[$i]['tel2'] = str_replace(' ','',str_replace('-','',str_replace('.','',$phone->number)));
				}
				elseif ($phone["nature"] == "mobile")
				{
					$user[$i]['tel3'] = str_replace(' ','',str_replace('-','',str_replace('.','',$phone->number)));
				}
				
			}
			//$user[$i]['tel1'] = ["nature"]=="home"->number;
			//$user[$i]['tel2'] = (String)$xml->to->coords->phone["office"]->number;
			//$user[$i]['tel3'] = (String)$xml->to->coords->phone["mobile"]->number;
			
			
			$user[$i]['fax'] = (String)$xml->to->coords->fax;
			
			//\USER
			
			//DOSSIER



			$dossier[$i]['id_pkg'] = (String)$xml->bookingInfo->reservationInfo->package["code"];
			
			$dossier[$i]['total'] = (String)$xml->bookingInfo->invoiceDetails->total['amount'];
			$dossier[$i]['date'] = (String)$xml->issued['dateTime'];
			
			
			$donnes_import = Import_donnees($dossier[$i]['id_pkg']);
			
			$dossier[$i]['etat'] = "Import";
			
			$dest_pays="";
			$dest_pays = (String)$xml->bookingInfo->reservationInfo->package->itinerary->journey->outwardTravel->choice->route->destination->city["code"];
			$dossier[$i]['dest_pays'] = substr($dest_pays,0,2);
			
			$dest_ville="";
			$dest_ville = secureString2((String)$xml->bookingInfo->reservationInfo->package->itinerary->journey->outwardTravel->choice->route->destination->city["name"]);
			$dossier[$i]['dest_ville'] = ucfirst(strtolower($dest_ville));
			
			$dest_hotel="";
			$dest_hotel = secureString2((String)$xml->bookingInfo->reservationInfo->package->itinerary->accomodation->choice["name"]);
			$dossier[$i]['dest_hotel'] = secureString2(ucfirst(strtolower($dest_hotel))) ;
			
			$dossier[$i]['duree'] = $donnes_import[6];
			
			$dossier[$i]['cat_hot'] =$donnes_import[3];
			
			foreach ($xml->bookingInfo->reservationInfo->passengers->headCount->ageGroup as $age)
			{
				if ($age == "adult")
				{
					$dossier[$i]['adultes'] = (String)$age["countStd"];
				}
				if ($age == "child")
				{
					$dossier[$i]['enfants'] = (String)$age["countStd"];
				}
				if ($age == "baby")
				{
					$dossier[$i]['bebes'] =  (String)$age["countStd"];
				}
			}
			
			$dossier[$i]['nb_supp_adulte'] = $dossier[$i]['adultes'];
			$dossier[$i]['nb_taxe_adulte'] = $dossier[$i]['adultes'];
			$dossier[$i]['nb_reduc_adulte'] = $dossier[$i]['adultes'];
			$dossier[$i]['nb_supp_enfant'] = $dossier[$i]['enfants'];
			$dossier[$i]['nb_taxe_enfant'] = $dossier[$i]['enfants'];
			$dossier[$i]['nb_reduc_enfant'] = $dossier[$i]['enfants'];
			$dossier[$i]['nb_supp_bebe'] = $dossier[$i]['bebes'];
			$dossier[$i]['nb_reduc_bebe'] = $dossier[$i]['bebes'];
 
			 					
			

				$pension=$xml->bookingInfo->reservationInfo->package->itinerary->accomodation->choice->hotelStay->choice->hotel->options->mealplan->choice['name'];
			
			if ($pension == "all inclusive") {$pension = "All Inclusive";}
			if ($pension == "petit d�jeuner") {$pension = "Petit-d�jeuner";}
			if ($pension == "demi pension") {$pension = "Demi-pension";}
			if ($pension == "pension compl�te") {$pension = "Pensions compl�te";}	
			
			$dossier[$i]['pension'] = $donnes_import[4];
			
			$dossier[$i]['date_deb'] = (String)$xml->bookingInfo->reservationInfo->package->itinerary->journey->outwardTravel->choice->routeOccurence->departure["date"];
			$dossier[$i]['date_fin'] = (String)$xml->bookingInfo->reservationInfo->package->itinerary->journey->returnTravel->choice->routeOccurence->departure["date"];
			

			$dossier[$i]['ville_depart'] = ucfirst(strtolower($donnes_import[5])); 
			$dossier[$i]['compris'] = $donnes_import[7]; 
			$dossier[$i]['to'] = $donnes_import[8]; 
			if ($dossier[$i]['to'] == "Ilios tour") {$dossier[$i]['to'] = "Ilios";}			
			
			
			unset($type);
			$type=array();
			foreach($xml->bookingInfo->reservationInfo->package->itinerary->accomodation->choice->hotelStay->choice->roomStay as $room)
			{
				$type[] = (String)$room->roomType["code"];
			}
			
			//$type = (String)$xml->bookingInfo->reservationInfo->package->itinerary->accomodation->choice->hotelStay->choice->roomStay[0]->roomType["code"];
			$nb_type = (String)$xml->bookingInfo->reservationInfo->package->itinerary->accomodation->choice->hotelStay->choice->roomStay[0]->guestCounts->total["countStd"];
			//$type1 = (String)$xml->bookingInfo->reservationInfo->package->itinerary->accomodation->choice->hotelStay->choice->roomStay[1]->roomType["code"];
			$nb_type1 = (String)$xml->bookingInfo->reservationInfo->package->itinerary->accomodation->choice->hotelStay->choice->roomStay[1]->guestCounts->total["countStd"];
			
			$type_count = array_count_values($type);
			
			$dossier[$i]['type_chambre'] = ucfirst(strtolower(key($type_count)));
			$dossier[$i]['nb_type_chambre'] = current($type_count);
			next($type_count);
			$dossier[$i]['type_chambre1'] = ucfirst(strtolower(key($type_count))); 
			$dossier[$i]['nb_type_chambre1'] = current($type_count); 

			$j=0;
			foreach($xml->bookingInfo->reservationInfo->passengers->passenger as $passager)
			{
				//Passagers
				 
				$dossier[$i][$j]['civilite'] = secureString2((String)$passager->name["title"]); 
				if($dossier[$i][$j]['civilite'] == "M")
				{
					$dossier[$i][$j]['civilite'] = "Mr";
				}
				else if ($dossier[$i][$j]['civilite'] == "Mme")
				{
					$dossier[$i][$j]['civilite'] = "Mme";
				}
				else
				{
					$dossier[$i][$j]['civilite'] = "Melle";
				}
				$dossier[$i][$j]['nom'] = secureString2((String)$passager->name["last"]);  
				$dossier[$i][$j]['prenom'] = secureString2((String)$passager->name["first"]); 

				if (secureString2((String)$passager["ageGroup"]) == "adult")
				{
					$dossier[$i][$j]['age'] = "Adulte";
				}
				else if (secureString2((String)$passager["ageGroup"]) == "child")
				{
					$dossier[$i][$j]['age'] = "Enfant";
				}
				else if (secureString2((String)$passager["ageGroup"]) == "baby")
				{
					$dossier[$i][$j]['age'] =  "Bebe";
				}
				
				$dossier[$i][$j]['date_nais'] = secureString2((String)$passager["birthDate"]); 
				
				
				//EN TEST
				//Moyen ou long courrier ?
				$link = connection(MYDATABASE);
				$requete_courrier = "SELECT courrier FROM code_pays WHERE cpays='".$dossier[$i]['dest_pays']."';";
				$result_courrier = mysql_query($requete_courrier) or die($requete_courrier.mysql_error().$requete_courrier);
				$row_courrier = mysql_fetch_assoc($result_courrier);
				
				//\EN TEST
				
				
				if($row_courrier['courrier'] == 'moyen')
				{
					if ((String)$xml->bookingInfo->reservationInfo->package->options->insurance->choice["code"] == "assur-sans")
					{
						$dossier[$i][$j]['assur'] = "Aucune";
					}
					else if ((String)$xml->bookingInfo->reservationInfo->package->options->insurance->choice["code"] == "assur-multirisque")
					{
						$dossier[$i][$j]['assur'] = "Multirisques_moyen";
					}
					else
					{
						$dossier[$i][$j]['assur'] = "Annulation_moyen";
					}
				}
				else
				{
					if ((String)$xml->bookingInfo->reservationInfo->package->options->insurance->choice["code"] == "assur-sans")
					{
						$dossier[$i][$j]['assur'] = "Aucune";
					}
					else if ((String)$xml->bookingInfo->reservationInfo->package->options->insurance->choice["code"] == "assur-multirisque")
					{
						$dossier[$i][$j]['assur'] = "Multirisques_long";
					}
					else
					{
						$dossier[$i][$j]['assur'] = "Annulation_long";
					}
				}
				
				
				$j++;
			}
			





			$dossier[$i]['commentaires']="";
			foreach($xml->bookingInfo->invoiceDetails->line as $line)
			{
				$type = $line->invoiceItem["code"];
				switch ($type)
				{
				
				//Prix de base
				case("PRX-BASE"):
				$dossier[$i]['prix_base_adulte'] = (String)$line->unitPrice["amount"];
				break;
				
				//Prix taxe aeroport
				case("PRX-TXA-PAR"):
				//Taxes a�roports
				$dossier[$i]['supp_divers'] = (String)$line->unitPrice["amount"];
				$dossier[$i]['nb_supp_divers'] = (String)$line["quantity"];
				break;
				

				//Prix de base b�b�
				case("PRX-BASE-BEBE"):
				$dossier[$i]['prix_base_bebe'] = (String)$line->unitPrice["amount"];
				
				break;
				
				//Frais de dossiers (fixes par defaut)
				case("PRX-FD"):
				
				break;
								
				//Prix r�duc enfant
				case("PRX-RED-1ENF"):
				$dossier[$i]['prix_base_enfant'] = ($dossier[$i]['prix_base_adulte'] - (String)$line->unitPrice["amount"]);
				break;
				
				//Prix 3e adulte
				case("PRX-RED-3ADU"):
				
				break;
				
				//Supp chambre indiv
				case("PRX-SUP-CHMB-IND"):
				$dossier[$i]['taxes_adulte'] = (String)$line->unitPrice["amount"];
				$dossier[$i]['nb_taxe_adulte'] = (String)$line["quantity"];
				break;

				
				//$line["quantity"]
				//$line->unitPrice["amount"]
				//$line->invoiceItem["code"]
				default:
				$dossier[$i]['commentaires'] .= (String)$line["label"]." : ";
				$dossier[$i]['commentaires'] .= (String)$line->unitPrice["amount"]." � *";
				$dossier[$i]['commentaires'] .= (String)$line["quantity"];
				$dossier[$i]['commentaires'] .= "\n\r";
			}
		}

			$dossier[$i]['commentaires'] .= "Total dossier : ".$dossier[$i]['total'];
			$dossier[$i]['commentaires'] .= "\n\r";
			$dossier[$i]['commentaires'] .= "ID QTS : ".$user[$i]['id_qts'];
			$dossier[$i]['commentaires'] .= "\n\r";
			
			$dossier[$i]['frais_dossiers'] = "15"; 
			
			
 
			
			
			//\DOSSIER



			
		}
		else
		{
			echo "Erreur dans le file get contents";
		}
		
		$url_courante_sips = $url_base.$urls['href'][$i]."sips_ops/";
		echo $url_courante_sips."<br/>";
		
		if($file_sips = file_get_contents($url_courante_sips))
		{
			$urls_sips = get_urls($file_sips);
			print_r($urls_sips);
			$nb_sips=(count($urls_sips['href'])-1);			
				$url_courante_sips2 = $url_courante_sips.$urls_sips['href'][1]."response_auto.qx_msg";
				if($file2_sips = file_get_contents($url_courante_sips2))
				{
					
					echo "Recuperation du SIPS<br/>";
					echo $url_courante_sips2.'<br/>';
					//SIPS
					$xml_sips = simplexml_load_string($file2_sips);
					//var_dump($xml_sips);
					$total_paye = $xml_sips->operation->sips['amount_sips'];
					$total_paye = $total_paye / 100;
					
					$dossier[$i]['commentaires'] .= "TOTAL PAYE : ".$total_paye;
					echo $dossier[$i]['commentaires'];
				}				
		}
	}
}


require_once("fonctions_db.php");
//echo count($user);

echo "NB : ".$nb;
for($i=1;$i<$nb;$i++)
{

	
	echo ("<hr>");
	$requete_verif = "SELECT id FROM `id_qts` WHERE id_qts='".$user[$i]['id_qts']."';";
	echo $requete_verif;
	echo("<br/>");
	$link = connection(MYDATABASE);
	$result_verif = mysql_query($requete_verif) or die ($requete_verif.mysql_error());
	$num_rows_verif = mysql_num_rows($result_verif);
	if($user[$i]['nom'] != "" && $user[$i]['email'] != "" && $user[$i]['nom'] != "KARCZEWSKI" && $user[$i]['nom'] != "NETBOOSTER" && $user[$i]['nom'] != "JAMAL" && $user[$i]['nom'] != "GEZEN" && $user[$i]['nom'] != "CHAPONIC" && $user[$i]['nom'] != "DFRTDFGH" && $num_rows_verif < 1)
	{
		
		echo ($i." : ");
		$requete = "INSERT INTO `clients_import` 
		(`civilite` , `nom` , `prenom` , `mail` , `sexe` , `date_nais` , `adresse1` , `adresse2` , `ville` , `cp` , `pays` , `tel1` , `tel2` , `tel3` , `fax` )
		 VALUES 
		 (
		 '".secureString2($user[$i]['civilite'])."',
		 '".secureString2($user[$i]['nom'])."',
		 '".secureString2($user[$i]['prenom'])."', 
		 '".secureString2($user[$i]['email'])."', 
		 '".secureString2($user[$i]['sexe'])."', 
		 '".secureString2($user[$i]['date_naiss'])."', 
		 '".secureString2($user[$i]['adresse1'])."', 
		 '".secureString2($user[$i]['adresse2'])."', 
		 '".secureString2($user[$i]['ville'])."', 
		 '".secureString2($user[$i]['cp'])."', 
		 '".secureString2($user[$i]['pays'])."', 
		 '".secureString2($user[$i]['tel1'])."', 
		 '".secureString2($user[$i]['tel2'])."', 
		 '".secureString2($user[$i]['tel3'])."', 
		 '".secureString2($user[$i]['fax'])."');";
		 echo $requete;
		 $link = connection(MYDATABASE);
		 $result = mysql_query($requete);
		 print_r($result);
		 echo("<br/>");
		 $id_client = mysql_insert_id();
		 //echo "ID CLIENT : ".$id_client;
		 if ($id_client > 0)
		 {
			$requete2 = "INSERT INTO `import_qts` 
			(`id_qts` , `id_client`,`date`,`total` )
			 VALUES 
			 ('".secureString2($user[$i]['id_qts'])."',
			 '".$id_client."','".secureString2($dossier[$i]['date'])."','".secureString2($dossier[$i]['total'])."');";
			 echo $requete2;
			 echo("<br/>");
			 $result2 = mysql_query($requete2);
			 print_r($result2);
			 
			$requete2bis = "INSERT INTO `id_qts` 
			(`id_qts`)
			 VALUES 
			 ('".secureString2($user[$i]['id_qts'])."');";
			 echo $requete2bis;
			 echo("<br/>");
			 $result2bis = mysql_query($requete2bis);
			 print_r($result2bis);
			//echo ("<hr>");

		}
		
		//A traiter dans les INSERT
		$dossier[$i]['derniere_modif'] = date("d/m/y \� H:i:s"); 
		$dossier[$i]['id_client'] = $id_client;		
		if ($id_client > 0)
		 {
		$requete3 = "
		INSERT INTO `dossiers_import` 
		(`id_pkg` , 
		`id_client` , 
		`etat` , 
		`dest_pays` , 
		`dest_ville` , 
		`dest_hotel` , 
		`duree` , 
		`duree_autre` , 
		`cat_hot` , 
		`adultes` , 
		`enfants` , 
		`bebes` , 
		`pension` , 
		`date_deb` , 
		`date_fin` , 
		`prix_base_adulte` , 
		`prix_base_enfant` , 
		`prix_base_bebe` , 
		`suppl_adulte` , 
		`suppl_enfant` , 
		`suppl_bebe` , 
		`taxes_adulte` , 
		`taxes_enfant` , 
		`taxes_bebe` , 
		`frais_dossiers` , 
		`date_transac`, 
		`nb_supp_adulte`, 
		`nb_supp_enfant`, 
		`nb_supp_bebe`, 
		`nb_taxe_adulte`, 
		`nb_taxe_enfant`, 
		`nb_taxe_bebe`, 
		`nb_reduc_adulte`, 
		`nb_reduc_enfant`, 
		`nb_reduc_bebe`, 
		`supp_divers`, 
		`nb_supp_divers`, 
		`commentaires`, 
		`reduc_adulte`,
		`reduc_enfant`,
		`reduc_bebe`, 
		`ville_depart`,
		`compris`,
		`to`,
		`hausse_carb`,
		`nb_hausse_carb`,
		`commentaires_cli`,
		`type_chambre`,
		`supp_adulte_div`,
		`supp_enfant_div`,
		`supp_bebe_div`,
		`nb_supp_adulte_div`,
		`nb_supp_enfant_div`,
		`nb_supp_bebe_div`,
		`intitule_supp_adulte_div`,
		`intitule_supp_enfant_div`,
		`intitule_supp_bebe_div`,
		`hausse_carb_bebe`, 
		`nb_hausse_carb_bebe`, 
		`derniere_modif`, 
		`date_confirm`,
		`type_chambre1`, 
		`nb_type_chambre`,
		`nb_type_chambre1`
		) 
		VALUES (
		'".$dossier[$i]['id_pkg']."',
		'".$dossier[$i]['id_client']."',
		'".$dossier[$i]['etat']."',
		'".$dossier[$i]['dest_pays']."',
		'".$dossier[$i]['dest_ville']."',
		'".$dossier[$i]['dest_hotel']."',
		'".$dossier[$i]['duree']."',
		'".$dossier[$i]['duree_autre']."',
		'".$dossier[$i]['cat_hot']."',
		'".$dossier[$i]['adultes']."',
		'".$dossier[$i]['enfants']."',
		'".$dossier[$i]['bebes']."',
		'".$dossier[$i]['pension']."',
		'".$dossier[$i]['date_deb']."',
		'".$dossier[$i]['date_fin']."',
		'".$dossier[$i]['prix_base_adulte']."',
		'".$dossier[$i]['prix_base_enfant']."',
		'".$dossier[$i]['prix_base_bebe']."',
		'".$dossier[$i]['supp_adulte']."',
		'".$dossier[$i]['supp_enfant']."',
		'".$dossier[$i]['supp_bebe']."',
		'".$dossier[$i]['taxes_adulte']."',
		'".$dossier[$i]['taxes_enfant']."',
		'".$dossier[$i]['taxes_bebe']."',
		'".$dossier[$i]['frais_dossiers']."', 
		NOW(), 
		'".$dossier[$i]['nb_supp_adulte']."', 
		'".$dossier[$i]['nb_supp_enfant']."', 
		'".$dossier[$i]['nb_supp_bebe']."', 
		'".$dossier[$i]['nb_taxe_adulte']."', 
		'".$dossier[$i]['nb_taxe_enfant']."', 
		'".$dossier[$i]['nb_taxe_bebe']."', 
		'".$dossier[$i]['nb_reduc_adulte']."', 
		'".$dossier[$i]['nb_reduc_enfant']."', 
		'".$dossier[$i]['nb_reduc_bebe']."', 
		'".$dossier[$i]['supp_divers']."', 
		'".$dossier[$i]['nb_supp_divers']."', 
		'".addslashes($dossier[$i]['commentaires'])."', 
		'".$dossier[$i]['reduc_adulte']."', 
		'".$dossier[$i]['reduc_enfant']."', 
		'".$dossier[$i]['reduc_bebe']."', 
		'".$dossier[$i]['ville_depart']."', 
		'".addslashes($dossier[$i]['compris'])."', 
		'".addslashes($dossier[$i]['to'])."', 
		'".$dossier[$i]['hausse_carb']."', 
		'".$dossier[$i]['nb_hausse_carb']."', 
		'".$dossier[$i]['commentaires_cli']."', 
		'".$dossier[$i]['type_chambre']."',
		'".$dossier[$i]['supp_adulte_div']."',
		'".$dossier[$i]['supp_enfant_div']."',
		'".$dossier[$i]['supp_bebe_div']."',
		'".$dossier[$i]['nb_supp_adulte_div']."',
		'".$dossier[$i]['nb_supp_enfant_div']."',
		'".$dossier[$i]['nb_supp_bebe_div']."',
		'".$dossier[$i]['intitule_supp_adulte_div']."',
		'".$dossier[$i]['intitule_supp_enfant_div']."',
		'".$dossier[$i]['intitule_supp_bebe_div']."',
		'".$dossier[$i]['hausse_carb_bebe']."', 
		'".$dossier[$i]['nb_hausse_carb_bebe']."', 
		'".$dossier[$i]['derniere_modif']."', 
		'".$dossier[$i]['date_confirm']."', 
		'".$dossier[$i]['type_chambre1']."', 
		'".$dossier[$i]['nb_type_chambre']."', 
		'".$dossier[$i]['nb_type_chambre1']."'
		);";
	
		 //echo $requete3;
		 //echo("<br/>");
		$result3 = mysql_query($requete3) or die($requete3.mysql_error());
		//echo("<br/>RESULTAT : ");
		print_r($result3);		 
		}
		 $id_dossier = mysql_insert_id();
		 if ($id_dossier > 0 && $id_client > 0)
		 {
			for($j=0;$j<count($dossier[$i][$j]);$j++)
			{
				$civilite= $dossier[$i][$j]['civilite'];
				$requete4 = "
				INSERT INTO `passagers_import` (
				`id_dossier` , 
				`civilite` , 
				`nom` , 
				`prenom` , 
				`age` , 
				`date_naiss` , 
				`assur`  , 
				`mail` ) 
				VALUES (
				'".$id_dossier."', 
				'".$civilite."', 
				'".$dossier[$i][$j]['nom']."', 
				'".$dossier[$i][$j]['prenom']."', 
				'".$dossier[$i][$j]['age']."', 
				'".$dossier[$i][$j]['date_nais']."', 
				'".$dossier[$i][$j]['assur']."', 
				'".$dossier[$i][$j]['mail']."'
				);";
			
				//echo $requete4;
			$result4 = mysql_query($requete4) or die($requete4.mysql_error());
			//echo("<br/>RESULTAT : ");
			print_r($result4);
			
		 		//echo("<br/><br/><br/>");
			}
		}		
			//echo ("<hr>");

	}
	echo "Dossier deja int�gr�";
}
echo ("<h1>Importation termin�e</h1>");
echo ("<br/><a href='javascript:window.close();'>Cliquez ici pour fermer la fenetre</a>");
function secureString2($str)
{
		$str = trim($str);
		$str = stripslashes($str);
		$str = strip_tags($str);
		$str = htmlspecialchars($str);
		$str = htmlentities($str, ENT_NOQUOTES, 'utf-8');
		$str = html_entity_decode($str, ENT_NOQUOTES, 'iso-8859-1');
		$str = addslashes($str);
		return $str;
}

function get_urls($string, $strict=true) {

   $types = array("href", "src", "url");
   while(list(,$type) = each($types)) {
       $innerT = $strict?'[a-z0-9:?=&@/._-]+?':'.+?';
       preg_match_all ("|$type\=([\"'`])(".$innerT.")\\1|i", $string, &$matches);
       $ret[$type] = $matches[2];
   }

return $ret;
}**/
?>

