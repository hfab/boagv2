<?php
session_start();
if (!session_is_registered("id_vendeur")) {
		   header("Location:index.php");
		   exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Réglement des fournisseurs de AgencedeVoyage&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />



<link href="client.css" rel="stylesheet" type="text/css"/>


<style>

th {
	background-color: #e9e9da;
}

.largeHeaders {
		background:
			#e9e9da
			url('/js/img/green_arrows.gif')
			no-repeat
			center left;
		color: #333;
		padding: 5px;
		padding-left: 25px;
		text-align: left;
		cursor: pointer;
}
.sortUp {
	background:
		#e9e900
		url('/js/img/green_decending.gif')
		no-repeat
		center left;
}

.sortDown {
	background:
		#e9e900

		url('/js/img/green_acending.gif')
		no-repeat
		center left;
}
#sorting {
	position: absolute;
	width: 100%;
	height: 100%;
	left:0px;
	top:0px;
	text-align: center;
	display: none;
	margin:0px;
	padding:0px;
}
#sorting div {
	border: 4px solid #000;
	padding:30px;
	margin: auto;
	text-align: center;
	width: 600px;
	top:500px;
	position: relative;
	background-color: #FFF;
	color: #333;
	font-weight: bold;
}
.even {
	background-color:#FFD760;
}
.odd
{
	background-color:#60C3FF;
}
</style>

	<script type="text/javascript" src="/js/jquery-svn.js"></script>
	<script type="text/javascript" src="/js/jquery.tablesorter.js"></script>

	<script type="text/javascript">
	$(document).ready(function() {
		$("table#recherche").tableSorter({
			sortClassAsc: 'sortUp', // class name for asc sorting action
			sortClassDesc: 'sortDown', // class name for desc sorting action
			headerClass: 'largeHeaders', // class name for headers (th's)
			disableHeader: ['lien'], // disable column can be a string / number or array containing string or number.
			dateFormat: 'dd/mm/yyyy', // set date format for non iso dates default us, in this case override and set uk-format
			//stripingRowClass: ['even','odd'],
		  	//stripRowsOnStartUp: true
		})
	});
	$(document).sortStart(function(){
		$("div#sorting").show();
	}).sortStop(function(){
		$("div#sorting").hide();
	});
	</script>


</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
require_once("../fonctions_db.php");
require_once("../fonctions.php");
require_once("mailto.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">
<?php

$temps_debut = microtime(true);

$mois = $_POST["mois"];
$annee = $_POST["annee"];

$jour_facturation = $_POST["jour_facturation"];
$mois_facturation = $_POST["mois_facturation"];
$annee_facturation = $_POST["annee_facturation"];

$jour_payement = $_POST["jour_payement"];
$mois_payement = $_POST["mois_payement"];
$annee_payement = $_POST["annee_payement"];

$date_facturation = $annee_facturation."-".$mois_facturation."-".$jour_facturation;

$date_payement = $annee_payement."-".$mois_payement."-".$jour_payement;

$banque = $_POST['banque'];

$montant_achat = $_POST["montant_achat"];
$facture_achat = $_POST["facture_achat"];
$id_dossier = $_POST["id_dossier"];
$mode_achat = $_POST["mode_achat"];
$num_cheque = $_POST["num_cheque"];

$mois_courant=date("m");
$annee_courante=date("y");


if (!empty($mois)) {$mois_courant = $mois;};
if (!empty($annee)) {$annee_courante = $annee;};

if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};

$envoi_mail = $_POST["envoi_mail"];
$html = $_POST["html"];

/*
if($envoi_mail=="ok")
{

	if (mail($destinataire, "Réglements fournisseurs - Agencedevoyage.com", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header)
	&& mail($expediteur, "COPIE - Réglement fournisseur - Agencedevoyage.com - $destinataire", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header)
	)
	{
		echo "Le mail a bien été envoyé à <b>$destinataire</b> de la part de <b>$expediteur</b> <br/>";
	}

}

*/



// pour l'ecriture
define('BASE_PATH',realpath( dirname(__FILE__)));
$file_csv_ce   = BASE_PATH.'/journalAchat'.$mois_courant.'_'.$annee_courante.'CE.csv';
//$file_joint_ce = 'journalCaisse'.$mois_courant.'_'.$annee_courante.'CE.csv';
$file_csv_hce = BASE_PATH.'/journalAchat'.$mois_courant.'_'.$annee_courante.'HorsCE.csv';


//$destinataires =array('yveschaponic@gmail.com');
$destinataires =array('julien@comptoirdesdeals.com');

if($envoi_mail=="ok")
{
	if ( mailToCompta($destinataires, "achat",$file_csv_ce,$file_csv_hce) )
	{
		echo "Le mail a bien été envoyé à <b>$destinataire</b> de la part de <b>$expediteur</b> <br/>";
	}
}

$output_ce_csv  ="ID DOSSIER; NOM CLIENT ;VILLE ARRIVEE; MONTANT D'ACHAT; ASSURANCES\n\r";
$output_hce_csv ="ID DOSSIER; NOM CLIENT; VILLE ARRIVEE; MONTANT D'ACHAT; ASSURANCES\n\r";



$html_output="";
$html_output_mail="";
$html_output_mail .= "<h1>Réglements fournisseurs</h1>";



	echo "<h1>Réglements fournisseurs pour le : ".$mois_courant." / ".$annee_courante."</h1>";
	echo "<p><form method='post' action='".$_SERVER['PHP_SELF']."?PHPSESSID=".session_id()."'><fieldset><legend>Choix de la période</legend>";
	echo ("<label for='mois'>Voir une autre période : </label>\n\r");
	echo ("<select name='mois' id='mois'>\n\r");
	for ($i=1;$i<=12;$i++) {
		if (strlen($i) < 2){$mois="0".$i;} else {$mois=$i;};
		echo ("<option value='".$mois."'");
		if ($mois == $mois_courant) {echo("selected='selected'");};
		echo (">".$mois."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='annee' id='annee'>\n\r");
	for ($i=2005;$i<=$annee_courante+1;$i++) {
		echo ("<option value='".$i."'");
		if ($i == $annee_courante) {echo("selected='selected'");};
		echo (">".$i."</option>\n\r");
	}
	echo ("</select><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");
	echo "<input type='submit' name='Voir' id='Voir' value='Voir'></fieldset></form></p>";

	$annee_courante = substr($annee_courante,2,4);



$montant_total_dossier=0;
$montant_total_achat=0;


		$html_output .= "<table border='1' cellpadding='2' cellspacing='0' id='recherche'>\n\r";
		$html_output .= "<tr>\n\r";
		$html_output .= "<th><b>ID dossier</b></th>\n\r";
		$html_output .= "<th><b>Nom client</b></th>\n\r";
	    $html_output .= "<th><b>Ville d'arrivee</b></th>\n\r";
		$html_output .= "<th><b>Montant achat</b></th>";
		$html_output .= "<th><b>Assurances</b></th>";
		$html_output .= "</tr>\n\r";

for ($i=1;$i<32;$i++)
{
$jour_courant = $i;
if (strlen($jour_courant) < 2){$jour_courant="0".$jour_courant;};

	$date_courante="20".$annee_courante."-".$mois_courant."-".$jour_courant;

	$link = connection(MYDATABASE);
/*
$requete = "SELECT
	DISTINCT
	dossiers.id,
	dossiers.to,
	dossiers.dest_ville,
	dossiers.ville_depart,
	dossiers.date_deb,
	dossiers.date_fin,
	vendeurs.prenom,
	clients.nom,
	dossiers.ref_to,
	code_pays.europe_compta
	FROM
	`dossiers`,
	`clients`,
	`vendeurs`,
	`lien_dossier_vendeur`,
	`code_pays`
	WHERE
	dossiers.id_client=clients.id
	AND
	dossiers.id=lien_dossier_vendeur.id_dossier
	AND
	dossiers.dest_pays=code_pays.cpays
	AND
	lien_dossier_vendeur.id_vendeur=vendeurs.id
	AND
	dossiers.etat='Confirmé'
	AND
	dossiers.date_deb='".$date_courante."'
	ORDER BY
	dossiers.id;";
*/


	$requete = "SELECT
	DISTINCT
	dossiers.id,
	dossiers.to,
	dossiers.dest_ville,
	dossiers.ville_depart,
	dossiers.date_deb,
	dossiers.date_fin,
	vendeurs.prenom,
	clients.nom,
	dossiers.ref_to,
	code_pays.europe_compta
	FROM
	`dossiers`,
	`clients`,
	`vendeurs`,
	`lien_dossier_vendeur`,
	`code_pays`
	WHERE
	dossiers.id_client=clients.id
	AND
	dossiers.id=lien_dossier_vendeur.id_dossier
	AND
	dossiers.dest_pays=code_pays.cpays
	AND
	lien_dossier_vendeur.id_vendeur=vendeurs.id
	AND
	dossiers.etat='Confirmé'
	AND
	dossiers.date_confirm ='".$date_courante."'
	ORDER BY
	dossiers.id;";



	//echo $requete;
	$result=mysql_query($requete) or die(mysql_error());
	$num_rows = mysql_num_rows($result);

	while($row = mysql_fetch_row($result))
	{
		
		$link = connection(MYDATABASE);
		$requete3 = "SELECT facture_achat, montant_achat, date, mode_achat, num_cheque, date_payement, banque FROM achats WHERE id_dossier='".$row[0]."' AND montant_achat > 0;";
		$result3=mysql_query($requete3) or die(mysql_error());
		$num_rows3 = mysql_num_rows($result3);
		//echo $num_rows3;

		$facture_achat_temp = 0;
		$montant_achat_temp = 0;
		$montant_achat_assur = 0;
		$mode_achat_temp = "";
		$date="0000-00-00";
		$total_assur = Total_assur($row[0]);

		unset($row3);
		unset($jour_courant2);
		unset($mois_courant2);
		unset($annee_courante2);

		unset($date_payement);

		unset($jour_courant3);
		unset($mois_courant3);
		unset($annee_courante3);

		$num_cheque=0;

		while($row3 = mysql_fetch_row($result3))
		{
			$facture_achat_temp = $row3[0];
			$montant_achat_temp = $row3[1];
			$mode_achat_temp = $row3[3];
			$date = $row3[2];

			//echo "ICI : ".$facture_achat.$montant_achat;
			//echo "ICI : ".$date;

			$montant_total_dossier+=Calcul_total_dossier($row[0]);
			$montant_total_achat+=$montant_achat_temp;
			$montant_total_assur+=$total_assur[1];
			$num_cheque = $row3[4];
			$date_payement = $row3[5];
			$banque = $row3[6];

		}
			if ($date != "0000-00-00")
			{
				$jour_courant2 = substr($date,8,2);
				$mois_courant2 = substr($date,5,2);
				$annee_courante2 = substr($date,0,4);
			}
			else
			{
				$date = date("Y-m-d");
				$jour_courant2 = substr($date,8,2);
				$mois_courant2 = substr($date,5,2);
				$annee_courante2 = substr($date,0,4);
			}

			if ($date_payement != "0000-00-00" && isset($date_payement))
			{
				$jour_courant3 = substr($date_payement,8,2);
				$mois_courant3 = substr($date_payement,5,2);
				$annee_courante3 = substr($date_payement,0,4);
			}
			else
			{
				//$date_payement = date("Y-m-d");

				$jour_courant3     = '00'; 
				$mois_courant3     = '00';
				$annee_courante3   = '0000';
			}

		$marge=100-(100*($montant_achat_temp+$total_assur[1])/Calcul_total_dossier($row[0]));
		if($montant_achat_temp > 0){$bgcolor="#CCCCCC";} else {$bgcolor="#FFFFFF";}
		if($marge < 0 && $row[1] != 'Jancarthier' && $row[1] != 'Go Voyages' && $row[1] != 'Océania' ){$bgcolor="red";} else if($marge < 9  && $row[1] != 'Jancarthier' && $row[1] != 'Go Voyages' && $row[1] != 'Océania') {$bgcolor="orange";}



		if( trim($row[9]) == '1')
		{
			$output_ce_csv .= $row[0].";";
			$output_ce_csv .= $row[7].";";
			$output_ce_csv .= "(CE)".$row[4]." - ".$row[2].";";
			$output_ce_csv .= $montant_achat_temp.";";
			$output_ce_csv .=  $total_assur[1]."\n\r";

		}
		else
		{

			$output_hce_csv .= $row[0].";";
			$output_hce_csv .= $row[8].";";
			$output_hce_csv .= "(Hors CE)".$row[2]." - ".$row[2].";";
			$output_hce_csv .= $montant_achat_temp.";";
			$output_hce_csv .= $total_assur[1]."\n\r";
		}


		$html_output .= "<tr bgcolor='".$bgcolor."'>\n\r";
	//	$html_output .= "<td nowrap='nowrap'>$jour_courant/$mois_courant/$annee_courante</td>\n\r";
		$html_output .= "<td>".$row[0]."</td>\n\r";
		$html_output .= "<td>".$row[7]."</td>\n\r";
		$html_output .= "<td>".$row[2]."</td>\n\r";
		//$html_output .= "<td>".Calcul_total_dossier($row[0])."&euro;</td>\n\r";
		//$html_output .= "<form method='post' action='".$_SERVER["PHP_SELF"]."?PHPSESSID=".session_id()."'>";
		//$html_output .= "<td nowrap='nowrap'>$jour_courant2/$mois_courant2/$annee_courante2\n\r";
		$html_output .= "<td>".$montant_achat_temp."</td>";
		$html_output .= "<td>".$total_assur[1]."&euro;</td>";


/*
		$marge = number_format($marge,2,',',' ');
		if($marge < 10 && $marge > 0)
		{
			$marge = '0'.$marge;
		}

		if($montant_achat_temp > 0)
		{

			$html_output .= "<td>".(Calcul_total_dossier($row[0]) - ($montant_achat_temp+$total_assur[1]))." &euro;</td>";
		}
		else
		{
			//$html_output .= "<td>N/A</td>";
		}*/

	/*	if($montant_achat_temp > 0)
		{

			$html_output .= "<td>".$marge." %</td>";
		}
		else
		{
			$html_output .= "<td>N/A</td>";
		}*/

		$html_output .= "</tr>";


	}


//$html_output .= "<hr>\n\r";
$html_output_mail .= "<hr>\n\r";
}


$html_output .= "</table><div id='sorting'><div>La table est en cours de tri, merci de patienter...</div></div>\n\r";


if($montant_total_dossier > 0)
{
$marge=100-(100*($montant_total_achat+$montant_total_assur)/$montant_total_dossier);
}

$html_output .= "Total des ventes : ".$montant_total_dossier."&euro; / Total des achats : ".($montant_total_achat+$montant_total_assur)."&euro; / Marge : ".($montant_total_dossier-($montant_total_achat+$montant_total_assur))."&euro; (".number_format($marge,2,',',' ')."%)";


echo $html_output;

file_put_contents($file_csv_ce, $output_ce_csv);
file_put_contents($file_csv_hce,$output_hce_csv);

//echo $html_output_mail;

	echo ("<form action='".$_SERVER['PHP_SELF']."' name='envoi' id='envoi' method='post'>\n\r");
	echo("<fieldset><legend>Envoi mail à yveschaponic@gmail.com</legend>");
	echo("<textarea name='html' onFocus='this.blur()' cols='1' rows='1' style='visibility:hidden;'>$html_output_mail</textarea>");
	echo("<input type='hidden' name='envoi_mail' value='ok'>\n\r");
	echo ("<div class='center'><input type='submit' class='submit' name='Envoi du mail' id='Envoi du mail' value='Envoi du mail'><br/></div>\n\r");
	echo("</fieldset></form>");


$temps_fin = microtime(true);
echo 'Temps d\'execution : '.round($temps_fin - $temps_debut, 4)."<br/>";

?>
</td>
</tr>
</table>

</body></html>
