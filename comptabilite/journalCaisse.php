<?php
session_start();
if (!session_is_registered("id_vendeur")) {
		   header("Location:index.php");
		   exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Statistiques AgenceDeVoyage&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="client.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
//include("../menu.php");
require_once("../fonctions_db.php");
require_once("../fonctions.php");
require_once("mailto.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">
<?php

$mois = $_POST["mois"];
$annee = $_POST["annee"];


$mois_courant=date("m");
$annee_courante=date("y");


if (!empty($mois)) {$mois_courant = $mois;};
if (!empty($annee)) {$annee_courante = $annee;};

if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};


$envoi_mail = $_POST["envoi_mail"];
$html = $_POST["html"];

// pour l'ecriture
define('BASE_PATH',realpath( dirname(__FILE__)));
$file_csv_ce   = BASE_PATH.'/journalCaisse'.$mois_courant.'_'.$annee_courante.'CE.csv';
//$file_joint_ce = 'journalCaisse'.$mois_courant.'_'.$annee_courante.'CE.csv';

$file_csv_hce = BASE_PATH.'/journalCaisse'.$mois_courant.'_'.$annee_courante.'HorsCE.csv';


$output_ce_csv  ="ID DOSSIER; NOM CLIENT ;VILLE ARRIVEE; MONTANT; ASSURANCES\n\r";
$output_hce_csv ="ID DOSSIER; NOM CLIENT; VILLE ARRIVEE; MONTANT; ASSURANCES\n\r";

//giles.amsellem@club-internet.fr 'yveschaponic@gmail.com'
//$destinataires =array('yveschaponic@gmail.com');
$destinataires =array('julien@comptoirdesdeals.com');

//Préparation du mail et envoi

if($envoi_mail=="ok")
{
	if ( mailToCompta($destinataires, "caisse",$file_csv_ce,$file_csv_hce) )
	{
		echo "Le mail a bien été envoyé à <b>$destinataire</b> de la part de <b>$expediteur</b> <br/>";
	}
}



$html_output="";

	echo "<h1>Visualisation du journal des caisses pour le : ".$mois_courant." / ".$annee_courante."</h1>";
	echo "<p><form method='post' action='".$_SERVER['PHP_SELF']."?PHPSESSID=".session_id()."'><fieldset><legend>Choix de la période</legend>";
	echo ("<label for='mois'>Voir une autre période : </label>\n\r");

	echo ("<select name='mois' id='mois'>\n\r");
	for ($i=1;$i<=12;$i++) {
		if (strlen($i) < 2){$mois="0".$i;} else {$mois=$i;};
		echo ("<option value='".$mois."'");
		if ($mois == $mois_courant) {echo("selected='selected'");};
		echo (">".$mois."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='annee' id='annee'>\n\r");
	for ($i=2005;$i<=$annee_courante;$i++) {
		echo ("<option value='".$i."'");
		if ($i == $annee_courante) {echo("selected='selected'");};
		echo (">".$i."</option>\n\r");
	}
	echo ("</select><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");
	echo "<input type='submit' name='Voir' id='Voir' value='Voir'></fieldset></form></p>";

	$annee_courante = substr($annee_courante,2,4);

	$html_output .= "<h1>Journal de caisse pour le ".$mois_courant." / ".$annee_courante."</h1>";

for ($i=0;$i<32;$i++)
{
$jour_courant = $i;
if (strlen($jour_courant) < 2){$jour_courant="0".$jour_courant;};

	$link = connection(MYDATABASE);
	$requete2 = "SELECT
	dossiers.id,
	dossiers.id_client,
	dossiers.mode_payement,
	dossiers.acompte,
	dossiers.dest_ville,
	dossiers.dest_pays,
	clients.nom,
	facturation.montant_payement,
	facturation.mode_payement,
	dossiers.etat,
	code_pays.europe_compta

	FROM `dossiers`,
	`clients`,
	`vendeurs`,
	`lien_dossier_vendeur`,
	`facturation`,
	`code_pays`
	WHERE dossiers.id=facturation.id_dossier
	AND dossiers.id_client=clients.id
	AND dossiers.id=lien_dossier_vendeur.id_dossier
	AND dossiers.dest_pays=code_pays.cpays
	AND lien_dossier_vendeur.id_vendeur=vendeurs.id
	AND (`etat`='Confirmé' OR etat='Préconfirmé' OR `etat`='En request' OR etat='Annulé')
	AND SUBSTRING(facturation.date_payement,1,2)=".$jour_courant."
	AND SUBSTRING(facturation.date_payement,4,5)=".$mois_courant."
	AND SUBSTRING(facturation.date_payement,7,8)=".$annee_courante."
	ORDER BY dossiers.id;";

	$result2=mysql_query($requete2) or die(mysql_error());
	$num_rows2 = mysql_num_rows($result2);
	if ($num_rows2 > 0) {
		$html_output .= "<h2>$jour_courant / $mois_courant / $annee_courante</h2>";
		$html_output .= "<table border='1' cellpadding='2' cellspacing='0'>";
		$html_output .= "<tr>";
		$html_output .= "<td><b>ID dossier</b></td>";
		$html_output .= "<td><b>Nom client</b></td>";
		$html_output .= "<td><b>Ville d'arivee</b></td>";


		//$html_output .= "<td><b>Montant du paiement</b></td>";


		$html_output .= "<td><b>Montant restant</b></td>";
		$html_output .= "<td><b>Asurrances</b></td>";
		$html_output .= "</tr>";
	}
	while($row2 = mysql_fetch_row($result2))
	{
		if($row2[7] == "Annulé")
		{
			$bgcolor="red";
		}
		else
		{
			$bgcolor="#FFFFFF";
		}

		$total_assur = Total_assur($row2[0]);
        $mode_compta = $row2[10];

		if($mode_compta ==1)
		{

			$output_ce_csv .= $row2[0].";";
			$output_ce_csv .= $row2[6].";";
			$output_ce_csv .= "(CE )".$row2[4].";";
			$output_ce_csv .= $row2[3].";";
			//	$html_output .= "<td>".(Calcul_total_dossier($row2[0])-$row2[3])."</td>";
			$output_ce_csv .= $total_assur[1]."\n\r";

		}
		else
		{
			$output_hce_csv .= $row2[0].";";
			$output_hce_csv .= $row2[6].";";
			$output_hce_csv .= "(HORS CE )".$row2[4].";";
			$output_hce_csv .= $row2[3].";";
			//	$html_output .= "<td>".(Calcul_total_dossier($row2[0])-$row2[3])."</td>";
			$output_hce_csv .= $total_assur[1]."\n\r";

		}

		$html_output .= "<tr style='background-color:".$bgcolor."'>";
		$html_output .= "<td>".$row2[0]."</td>";
		$html_output .= "<td>".$row2[6]."</td>";
		$html_output .= "<td>(".$row2[10].") ".$row2[4]."</td>";
		$html_output .= "<td>".$row2[3]."</td>";
		//	$html_output .= "<td>".(Calcul_total_dossier($row2[0])-$row2[3])."</td>";
		$html_output .= "<td>".$total_assur[1]."</td>";
		$html_output .= "</tr>";

	}


	if ($num_rows2 > 0) {
		$html_output .= "</table>";
	}


if ($num_rows2>0){
$html_output .= "<h1>Total des ventes, réparties par moyen de payement</h1>";
$fieldOptions = mysql_enum_values('facturation','mode_payement');
foreach($fieldOptions as $tmp)
{
	$link = connection(MYDATABASE);
	$requete = "SELECT SUM(montant_payement) FROM facturation WHERE mode_payement='".$tmp."' AND SUBSTRING(date_payement,1,2)=".$jour_courant." AND SUBSTRING(date_payement,4,2)=".$mois_courant." AND SUBSTRING(date_payement,7,2)=".$annee_courante.";";
	$result=mysql_query($requete) or die(mysql_error());
	$row = mysql_fetch_row($result);
	if (!empty($row[0]))
	{
		$html_output .= $tmp." : ".$row[0]." <br/>";
	}
}

}


$annee_courante2 = $annee_courante;
//Commentaires
if (strlen($annee_courante2) < 3){$annee_courante2="20".$annee_courante2;};
$date=$annee_courante2."/".$mois_courant."/".$jour_courant;
	$link = connection(MYDATABASE);
	$requete = "SELECT texte FROM commentaires WHERE type='Caisse' AND date='".$date."';";
	$result=mysql_query($requete) or die(mysql_error());
	if (mysql_num_rows($result)>0)
	{
		$row=mysql_fetch_row($result);
		$commentaire = $row[0];
		$html_output .= "<p>Commentaire : $commentaire</p>";
	}
$html_output .= "<hr>";
}


file_put_contents($file_csv_ce, $output_ce_csv);
file_put_contents($file_csv_hce,$output_hce_csv);

echo $html_output;

	echo ("<form action='".$_SERVER['PHP_SELF']."' name='envoi' id='envoi' method='post'>\n\r");
	echo("<fieldset><legend>Envoi mail à yveschaponic@gmail.com</legend>");
	echo("<textarea name='html' onFocus='this.blur()' cols='1' rows='1' style='visibility:hidden;'>$html_output</textarea>");
	echo("<input type='hidden' name='envoi_mail' value='ok'>\n\r");
	echo ("<div class='center'><input type='submit' class='submit' name='Envoi du mail' id='Envoi du mail' value='Envoi du mail'><br/></div>\n\r");
	echo("</fieldset></form>");

?>
</td>
</tr>
</table>

</body></html>