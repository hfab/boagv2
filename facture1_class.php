
<?php
require('class_fpdf/fpdf.php');

class Facture extends FPDF{
	
	function ajoutEntete($etat_dossier,$id_dossier,$id_client,$date,$vendeur){
		$this->Open();
		$this->SetFillColor(240,52,58);
		$this->SetMargins(15, 15, 15);
		$this->AddPage();
		$this->Image('logo_monagence.jpg',15,10,72,18,'','http://www.monagence.com');
		$this->Ln(20);
		$this->Cell(70,35,'',1,0,'L');
		$this->SetFont('Arial','B',12);
		
		if(($etat_dossier==="Demande")||($etat_dossier==="En attente")){
			    $typedoc="Demande de devis";
				$this->SetFont('Arial','B',120);
				$this->SetTextColor(215,207,207); //(226,226,226); 
				$this->Text(50,180,"Devis") ;
				$this->SetFont('Arial','',10);
				$this->SetTextColor(4,4,4);
		}
		else{
			    $typedoc="Facture No : $id_dossier";	
		}
		
		$this->SetFont('Arial','',10);
		$this->Text(18,40,"$typedoc") ;
		//references dossier
		$this->Text(18,45, "Dossier No : $id_dossier") ;
		$this->Text(18,50, "Code client : $id_client") ;
		$this->Text(18,55, "Contact : $vendeur") ;
		$this->Text(50,65, "Date : $date") ;
		$this->Cell(20);
		//references monagence
		$this->Cell(90,35,'',1,1,'L'); //1 bordure 1 =br
		$this->SetFont('Arial','B',14);
		$this->Text(110,40, "Monagence.com") ;
		$this->SetFont('Arial','',10);
		$this->Text(110,45, "78 BD de La R�publique") ;
		$this->Text(110,50, "92100 Boulogne") ;
		$this->Text(110,55, "T�l: 08 92 23 05 06*") ;
		$this->Text(110,60, "Fax: 01 46 21 02 88") ;
		$this->Text(110,65, "E-mail resa@monagence.com") ;
		
		//return $id_dossier;
			
	}

	function descriptionSejourClient($pays,$ville,$ville_dep,$d_debut,$d_fin,$hotel,$pension,$passagers,$client){
		$this->SetTextColor(4,4,4); 
		$this->Ln(5);
		$this->Cell(180,0,'',1,1,'');
		$this->SetFont('Arial','B',10);
	    $this->SetFillColor(215,207,207); 
		$this->Cell(90,5,'Description du s�jour',1,0,'L',1);
		$this->Cell(90,5,'Client',1,1,'L',1);
		$this->Ln(2);
		$this->SetFont('Arial','I',9);
		$this->Cell(20,5,"Destination",0,0,'L');
		$this->SetFont('Arial','B',8);
		$this->Cell(70,5,":	$pays",0,1,'L');
		
		$this->SetFont('Arial','I',9);
		$this->Cell(20,5,"Ville d�part",0,0,'L');
		$this->SetFont('Arial','B',8);
		$this->Cell(70,5,":	$ville_dep",0,1,'L');
		
		$this->SetFont('Arial','I',9);
		$this->Cell(20,5,"Ville arriv�e",0,0,'L');
		$this->SetFont('Arial','B',8);
		$this->Cell(70,5,":	$ville",0,1,'L');
		
		$this->SetFont('Arial','I',9);
		$this->Cell(20,5,"H�tel",0,0,'L');
		$this->SetFont('Arial','B',8);
		$this->Cell(70,5,":	$hotel",0,1,'L');
		
		$this->SetFont('Arial','I',9);
		$this->Cell(20,5,"Formule",0,0,'L');
		$this->SetFont('Arial','B',8);
		$this->Cell(70,5,":	$pension",0,1,'L');
		
		$this->SetFont('Arial','I',9);
		$this->Cell(20,5,"Date",0,0,'L');
		$this->SetFont('Arial','B',8);
		$this->Cell(70,5,":	$d_debut au $d_fin",0,1,'L');
		
		//ecriture des coordonnes du client
		$this->SetFont('Arial','I',9);
		$this->Text(106,85,$client['identite_tel']) ;
		$this->Text(106,90,$client['adresse']) ;
		$this->Text(106,95,$client['mail']) ;
	
		$nb_passagers=count($passagers);
		$nb_ligne1=round(count($passagers)/2);//nombre de pssagers par ligne
		$text_ligne1=""; //les passagers pour la ligne 
		$nb_ligne2=$nb_ligne1;//nombre de pssagers pour la ligne 2
		$text_ligne2="";
		
	  	for($i=0;$i<$nb_ligne1;$i++){
	  		$text_ligne1=$text_ligne1.$passagers[$i].", ";
		}
		for($i=$nb_ligne2;$i<$nb_passagers;$i++){
	  		$text_ligne2=$text_ligne2.$passagers[$i].", ";
		}
		$this->SetFont('Arial','B',9);
		$this->Text(106,100,"Passager(s) : ") ;
		$this->SetFont('Arial','I',8);
		$this->Text(106,105,"$text_ligne1") ;   //ecriture des passagers
		$this->Text(106,110,"$text_ligne2") ;
	
	}
	
	function ajoutLigneQuantitePrix($designation,$mode_paiement,$acompte){

		$total_facture=0;
		$reduction_facture=0;
		
		$this->Ln(5);
		$this->SetFont('Arial','B',10);
		$this->SetFillColor(215,207,207); 
		$this->Cell(100,5,'D�signation',1,0,'C',1);
		$this->Cell(20,5,'Quantit�',1,0,'C',1);
		$this->Cell(30,5,'P.U',1,0,'C',1);
		$this->Cell(30,5,'Total',1,1,'C',1);
		$this->Ln(5);
		
		while (list ($cle, $quantite_prix) = each ($designation)) {
			
		   $tab=explode("-",$quantite_prix);
		   $quantite=$tab[0];
		   $pu=$tab[1];
		  
		   if($pu!=="0.00" and $quantite!=="0"){		
				$red="R�duction";
				$pos = strpos($cle,$red);
				if ($pos===false){   //si la designation n'est pas une r�duction le prix est positif
					$this->SetFont('Arial','',10);
					$this->Cell(100,5,$cle,0,0,'L');
					$this->Cell(20,5,$quantite,0,0,'C');
					$this->Cell(30,5,$pu,0,0,'R');
					$total=$quantite*$pu;
				}
				else{//si la designation est une r�duction le prix est n�gatif
					$this->SetFont('Arial','',10);
					$this->Cell(100,5,$cle,0,0,'L');
					$this->Cell(20,5,$quantite,0,0,'C');
					
				    $total=$quantite*$pu;
				    if($pu!==""){
				   		$pu="-".$pu;
					}
					$this->Cell(30,5,$pu,0,0,'R');
					$reduction_facture=$total;
					$total=	"-$total";
				}
				if($total=="0" ||$total=="-0"){
				   	$total="";
				}
				$this->Cell(30,5,$total,0,1,'R');
			    $total_facture=$total_facture+$total;//somme des totaux des lignes
			  }
			  	
			}
			$this->Ln(5);
			$this->Cell(180,0,'',1,1,'');
			$this->Cell(40,5,"Mode paiement : $mode_paiement",0,0,'L');
			$this->Cell(80,5,"Acompte : $acompte �",0,0,'C');
			$this->SetFont('Arial','B',10);
			$this->Cell(30,5,'Montant � payer',0,0,'C');
			$this->SetFillColor(215,207,207);
			$this->SetFont('Arial','B',12);
			$montant_a_payer=$total_facture-$acompte;
			$this->Cell(30,5,"$montant_a_payer �",1,1,'C',1);
			
			return $montant_a_payer;   //retourne le montant � payer
							
	}
	
	
	function ajoutDesInclus($les_inclus,$aucune_assur){
		$this->Ln(3);
		if($aucune_assur===true){
			$this->SetFont('Arial','B',8);
			$this->Cell(180,4,"Assurance refus�e par le client",0,1,'L');	
		}
		$this->Ln(2);
		$this->SetFont('Arial','B',8);
		$this->Cell(180,4,"Le prix comprend :",0,1,'L');
		$this->SetFont('Arial','',7);
		foreach($les_inclus as $inclu){
			$this->Cell(180,4," - $inclu",0,1,'L');
		}
		$this->Ln(3);
	}
	
	function ajoutFooter(){
		$coordonnes[0]="Si�ge Social : 78 boulevard de la R�publique 92100 Boulogne";
		$coordonnes[1]="Tel : 08  92 23 05 06 - Fax : 01 46 21 02 88";
		$coordonnes[2]="Sarl Mon Agence.com au capital de 400 000 �  - R.C.S. NANTERRE B 442 958 443";
		$coordonnes[3]="LI  092 05 0014 - Garantie APS - RC G�n�rali  AA 0706218 - Adh�rent SNAV";
		$coordonnes[4]="N� TVA Intracommunautaire FR 76442958443";
		$coordonnes[5]="Internet : www.monagence.com    *Prix d'appel 0.34�";
		$this->SetFont('Arial','',6);
		$this->Cell(180,2,"FORMALITES :(A CE JOUR ET SOUS RESERVE DE MODIFICATION) PASSEPORT VALIDE 6 MOIS APRES LA DATE DE RETOUR",0,1,'L',0);
		$this->Cell(180,2,"VACCINS :(A CE JOUR ET SOUS RESERVE DE MODIFICATION )VOIR LE CONSULAT POUR LES VACCINS OBLIGATOIRES",0,1,'L',0);
		$this->Ln(1);
		$this->SetFont('Arial','',7);
		$this->SetFillColor(215,207,207); 
		foreach($coordonnes as $cor){
			$this->Cell(180,3,$cor,0,1,'C',1);
		}
	}
	function Footer(){
    //Positionnement � 1,5 cm du bas
    	$this->SetY(-30);
    //Police Arial italique 8
    //Num�ro de page centr�
    	$coordonnes[0]="Si�ge Social : 78 boulevard de la R�publique 92100 Boulogne";
		$coordonnes[1]="Tel : 08  92 23 05 06 - Fax : 01 46 21 02 88";
		$coordonnes[2]="Sarl Mon Agence.com au capital de 400 000 �  - R.C.S. NANTERRE B 442 958 443";
		$coordonnes[3]="LI  092 05 0014 - Garantie APS - RC G�n�rali  AA 0706218 - Adh�rent SNAV";
		$coordonnes[4]="N� TVA Intracommunautaire FR 76442958443";
		$coordonnes[5]="Internet : www.monagence.com    *Prix d'appel 0.34�";
		$this->SetFont('Arial','',6);
		$this->Cell(180,2,"FORMALITES :(A CE JOUR ET SOUS RESERVE DE MODIFICATION) PASSEPORT VALIDE 6 MOIS APRES LA DATE DE RETOUR",0,1,'L',0);
		$this->Cell(180,2,"VACCINS :(A CE JOUR ET SOUS RESERVE DE MODIFICATION )VOIR LE CONSULAT POUR LES VACCINS OBLIGATOIRES",0,1,'L',0);
		$this->Ln(1);
		$this->SetFont('Arial','',7);
		$this->SetFillColor(215,207,207); 
		foreach($coordonnes as $cor){
			$this->Cell(180,3,$cor,0,1,'C',1);
		}
	}
	
}

?> 

