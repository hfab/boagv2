<?php
session_start();
if (!(Isset($_SESSION["id_vendeur"]))) {
    header("Location:index.php");
    exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Création d'un nouveau dossier de Monagence&copy;</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <link href="client.css" rel="stylesheet" type="text/css"/>

    </head>
    <body>
        <table class="generale">
            <tr>
                <td width="120" valign="top" class="menu">
                    <?php
                    include("menu.php");
                    ?>
                </td>
                <td width="680" style="border-left:1px solid black;padding:5px">
                    <?php

                    require_once("fonctions_db.php");
                    require_once("fonctions.php");


                    $id_vendeur = $_SESSION["id_vendeur"];

                    $id = $_POST["id"];
                    $id_client = $_POST["id_client"];
                    $id_pkg = $_POST["id_pkg"];
                    $etat = $_POST["etat"];
                    $dest_pays = $_POST["dest_pays"];
                    $dest_ville = $_POST["dest_ville"];
                    $dest_hotel = $_POST["dest_hotel"];
                    $duree = $_POST["duree"];
                    $duree_autre = $_POST["duree_autre"];
                    $cat_hot = $_POST["cat_hot"];
                    $adultes = $_POST["adultes"];
					
					if(!is_int($_POST["enfants"])){
						$enfants = 0;
					} else {
						$enfants = $_POST["enfants"];
					}					
					
                    if(!is_int($_POST["bebes"])){
						$bebes = 0;
					} else {
						$bebes = $_POST["bebes"];
					}
					
                    $pension = $_POST["pension"];
                    $assur = $_POST["assur"];
                    $jour_depart = $_POST["jour_depart"];
                    $mois_depart = $_POST["mois_depart"];
                    $annee_depart = $_POST["annee_depart"];
                    $jour_retour = $_POST["jour_retour"];
                    $mois_retour = $_POST["mois_retour"];
                    $annee_retour = $_POST["annee_retour"];
                    
					
					
					$prix_base_adulte = $_POST["prix_base_adulte"];
					
					if(!is_numeric($_POST["prix_base_enfant"])){
						$prix_base_enfant = 0;
					} else {
						$prix_base_enfant = $_POST["prix_base_enfant"];
					}
					
					
					if(!is_numeric($_POST["prix_base_bebe"])){
					$prix_base_bebe = 0;
					} else {
                    $prix_base_bebe = $_POST["prix_base_bebe"];
					}
					
					if(!is_numeric($_POST["supp_adulte"])){
						$supp_adulte = 0;
					} else {
						$supp_adulte = $_POST["supp_adulte"];
					}
					
					
					if(!is_numeric($_POST["supp_enfant"])){
						$supp_enfant = 0;
					} else {
						$supp_enfant = $_POST["supp_enfant"];
					}
					
                    
                    if(!is_numeric($_POST["supp_bebe"])){
						$supp_bebe = 0;
					} else {
						$supp_bebe = $_POST["supp_bebe"];
					}					
					
					if(!is_numeric($_POST["taxes_adulte"])){
						$taxes_adulte = 0;
					} else {
						$taxes_adulte = $_POST["taxes_adulte"];
					}			
                    
					if(!is_numeric($_POST["taxes_enfant"])){
						$taxes_enfant = 0;
					} else {
						$taxes_enfant = $_POST["taxes_enfant"];
					}
					
					if(!is_numeric($_POST["taxes_bebe"])){
						$taxes_bebe = 0;
					} else {
						$taxes_bebe = $_POST["taxes_bebe"];
					}
					
                    
					
					
                    
					
					
					
                    $frais_dossiers = $_POST["frais_dossiers"];

                    $acompte = $_POST["acompte"];
					
					
                    $mode_payement = $_POST["mode_payement"];
					
					if(!is_numeric($_POST["nb_supp_adulte"])){
						$nb_supp_adulte = 0;
					} else {
						$nb_supp_adulte = $_POST["nb_supp_adulte"];
					}
                    
					if(!is_numeric($_POST["nb_supp_enfant"])){
						$nb_supp_enfant = 0;
					} else {
						$nb_supp_enfant = $_POST["nb_supp_enfant"];
					}
					
                    if(!is_numeric($_POST["nb_supp_bebe"])){
						$nb_supp_bebe = 0;
					} else {
						$nb_supp_bebe = $_POST["nb_supp_bebe"];
					}

                    if(!is_numeric($_POST["nb_taxe_adulte"])){
						$nb_taxe_adulte = 0;
					} else {
						$nb_taxe_adulte = $_POST["nb_taxe_adulte"];
					}					
                    
                    if(!is_numeric($_POST["nb_taxe_enfant"])){
						$nb_taxe_enfant = 0;
					} else {
						$nb_taxe_enfant = $_POST["nb_taxe_enfant"];
					}						
                    
					
                    if(!is_numeric($_POST["nb_taxe_bebe"])){
						$nb_taxe_bebe = 0;
					} else {
                    $nb_taxe_bebe = $_POST["nb_taxe_bebe"];
					}	                    
					
                    if(!is_numeric($_POST["nb_reduc_adulte"])){
						$nb_reduc_adulte = 0;
					} else {
						$nb_reduc_adulte = $_POST["nb_reduc_adulte"];
					}	
					
                    if(!is_numeric($_POST["nb_reduc_enfant"])){
						$nb_reduc_enfant = 0;
					} else {
						$nb_reduc_enfant = $_POST["nb_reduc_enfant"];
					}	                    
					
                    
					 if(!is_numeric($_POST["nb_reduc_bebe"])){
						$nb_reduc_bebe = 0;
					} else {
						$nb_reduc_bebe = $_POST["nb_reduc_bebe"];
					}	

					if(!is_numeric($_POST["supp_divers"])){
						$supp_divers = 0;
					} else {
						$supp_divers = $_POST["supp_divers"];
					}	
					
					if(!is_numeric($_POST["nb_supp_divers"])){
						$nb_supp_divers = 0;
					} else {
						$nb_supp_divers = $_POST["nb_supp_divers"];
					}               
                    
					if(!is_numeric($_POST["reduc_adulte"])){
						$reduc_adulte = 0;
					} else {
						$reduc_adulte = $_POST["reduc_adulte"];
					}

                    if(!is_numeric($_POST["reduc_enfant"])){
						$reduc_enfant = 0;
					} else {
						$reduc_enfant = $_POST["reduc_enfant"];
					}
					
					
					 if(!is_numeric($_POST["reduc_bebe"])){
						$reduc_bebe = 0;
					} else {
						$reduc_bebe = $_POST["reduc_bebe"];
					}

                    $ville_depart=$_POST["ville_depart"];

                    $compris[]=$_POST["compris"];
                    $compris=implode("|",$compris);
                    $compris=str_replace("|Array", "", $compris);

                    $to = $_POST["to"];
					
					if(!is_numeric($_POST["hausse_carb"])){
						$hausse_carb = 0;
					} else {
						$hausse_carb = $_POST["hausse_carb"];
					}
                    
					
					
					if(!is_numeric($_POST["nb_hausse_carb"])){
						$nb_hausse_carb = 0;
					} else {
						$nb_hausse_carb = $_POST["nb_hausse_carb"];
					}
                    

                    $commentaires_cli = $_POST['commentaires_cli'];
                    $type_chambre = $_POST['type_chambre'];
					
					if(!is_numeric($_POST["supp_adulte_div"])){
						$supp_adulte_div = 0;
					} else {
						$supp_adulte_div = $_POST['supp_adulte_div'];
					}                   

					if(!is_numeric($_POST["supp_enfant_div"])){
						$supp_enfant_div = 0;
					} else {
						$supp_enfant_div = $_POST['supp_enfant_div'];
					}
                    
					if(!is_numeric($_POST["supp_bebe_div"])){
						$supp_bebe_div = 0;
					} else {
						$supp_bebe_div = $_POST['supp_bebe_div'];
					}
					
					
					if(!is_numeric($_POST["nb_supp_adulte_div"])){
						$nb_supp_adulte_div = 0;
					} else {
						$nb_supp_adulte_div = $_POST['nb_supp_adulte_div'];
					}
					
					
					if(!is_numeric($_POST["nb_supp_enfant_div"])){
						$nb_supp_enfant_div = 0;
					} else {
						$nb_supp_enfant_div = $_POST['nb_supp_enfant_div'];
					}
					
					if(!is_numeric($_POST["nb_supp_bebe_div"])){
						$nb_supp_bebe_div = 0;
					} else {
						$nb_supp_bebe_div = $_POST['nb_supp_bebe_div'];
					}
                    
                    
                    
                    $intitule_supp_adulte_div = $_POST['intitule_supp_adulte_div'];
                    $intitule_supp_enfant_div = $_POST['intitule_supp_enfant_div'];
                    $intitule_supp_bebe_div = $_POST['intitule_supp_bebe_div'];
                    
					if(!is_numeric($_POST["hausse_carb_bebe"])){
						$hausse_carb_bebe = 0;
					} else {
						$hausse_carb_bebe = $_POST['hausse_carb_bebe'];
					}


					if(!is_numeric($_POST["nb_hausse_carb_bebe"])){
						$nb_hausse_carb_bebe = 0;
					} else {
						$nb_hausse_carb_bebe = $_POST['nb_hausse_carb_bebe'];
					}					
                    

                    $assur1 = $_POST['assur1'];
					
					if(!is_numeric($_POST["nb_assur"])){
						$nb_assur = 0;
					} else {
						$nb_assur = $_POST['nb_assur'];
					}		
					
					if(!is_numeric($_POST["nb_assur1"])){
						$nb_assur1 = 0;
					} else {
						$nb_assur1 = $_POST['nb_assur1'];
					}	
                    
                    


                    $type_chambre1 = $_POST['type_chambre1'];
					
					
					
					if(!is_numeric($_POST["nb_assur1"])){
						$nb_assur1 = 0;
					} else {
						$nb_assur1 = $_POST['nb_assur1'];
					}	
					
					if(!is_numeric($_POST["nb_type_chambre1"])){
						$nb_type_chambre1 = 0;
					} else {
						$nb_type_chambre1 =$_POST['nb_type_chambre1'];
					}	
					
					if(!is_numeric($_POST["nb_type_chambre"])){
						$nb_type_chambre = 0;
					} else {
						$nb_type_chambre = $_POST['nb_type_chambre'];
					}


                    $etape = $_POST["etape"];
                    $flag = $_POST["flag"];

                    $provenance_dossier = $_POST["provenance_dossier"];
					
					if(!is_numeric($_POST["supp_adulte_div2"])){
					$supp_adulte_div2 = 0;
					} else {
					$supp_adulte_div2 = $_POST['supp_adulte_div2'];
					}
					
					
					if(!is_numeric($_POST["supp_adulte_div3"])){
					$supp_adulte_div3 = 0;
					} else {
					$supp_adulte_div3 = $_POST['supp_adulte_div3'];
					}
					
					
					if(!is_numeric($_POST["nb_supp_adulte_div2"])){
						$nb_supp_adulte_div2 = 0;
					} else {
						$nb_supp_adulte_div2 = $_POST['nb_supp_adulte_div2'];
					}
					
					if(!is_numeric($_POST["nb_supp_adulte_div3"])){
						$nb_supp_adulte_div3 = 0;
					} else {
						$nb_supp_adulte_div3 = $_POST['nb_supp_adulte_div3'];
					}
					
					
                    $intitule_supp_adulte_div2 = $_POST['intitule_supp_adulte_div2'];
                    $intitule_supp_adulte_div3 = $_POST['intitule_supp_adulte_div3'];

                    $provenance =$_POST['provenance'];
                    $ref_to =$_POST['ref_to'];

                    function Form_prepa_dossier($id_client)
                    {
                        echo ("<form action='".$_SERVER['PHP_SELF']."' name='pkg' id='pkg' method='post'>\n\r");
                        //Fieldset Coordonnees
                        echo ("<fieldset><legend>Dossier</legend><br/>\n\r");

                        //Provenance : Pack ou TO
                        echo ("<label for='provenance1' style='width:50%'>Pack QTS</label>\n\r");
                        echo ("<input type='radio' name='provenance' id='provenance1' value='qts' checked='checked' style='width:10%'>\n\r");
                        echo ("<input type='text' name='id_pkg' id='id_pkg' value='' style='width:30%;'><br/>\n\r");
                        echo ("<label for='provenance2' style='width:50%'>Demande directe</label>\n\r");
                        echo ("<input type='radio' name='provenance' id='provenance2' value='to' style='width:10%'><br/>\n\r");
                        echo ("<label for='provenance3' style='width:50%'>Vol, Hôtel, Croisière, Location, train, Vol+hotel, Train+Hotel</label>\n\r");
                        echo ("<input type='radio' name='provenance' id='provenance3' value='autre' style='width:10%'><br/>\n\r");
                        echo ("<br style='clear:both'/>\n\r");

                        echo ("</fieldset>");
                        echo ("<input type='hidden' name='id_client' id='id_client' value='".$id_client."'><br/>\n\r");
                        echo ("<div class='center'><input type='submit' name='Choisir' id='Choisir' value='Choisir'><br/></div>\n\r");
                        echo ("</form><br/>");
                    }





                    $verif = Verif_form_new_dossier($id, $id_client, $id_pkg, $etat, $dest_pays, $dest_ville, $dest_hotel, $duree, $duree_autre, $cat_hot, $adultes, $enfants, $bebes, $pension, $assur, $jour_depart, $mois_depart, $annee_depart, $jour_retour, $mois_retour, $annee_retour, $prix_base_adulte, $prix_base_enfant, $prix_base_bebe, $supp_adulte, $supp_enfant, $supp_bebe, $taxes_adulte, $taxes_enfant, $taxes_bebe, $frais_dossiers, $acompte, $mode_payement, $nb_supp_adulte, $nb_supp_enfant, $nb_supp_bebe, $nb_taxe_adulte, $nb_taxe_enfant, $nb_taxe_bebe, $nb_reduc_adulte, $nb_reduc_enfant, $nb_reduc_bebe, $supp_divers, $nb_supp_divers, $commentaires, $reduc_adulte, $reduc_enfant, $reduc_bebe, $ville_depart, $compris, $to, $hausse_carb, $nb_hausse_carb, $commentaires_cli, $type_chambre, $supp_adulte_div, $supp_enfant_div, $supp_bebe_div, $nb_supp_adulte_div, $nb_supp_enfant_div, $nb_supp_bebe_div, $intitule_supp_adulte_div, $intitule_supp_enfant_div, $intitule_supp_bebe_div, $hausse_carb_bebe, $nb_hausse_carb_bebe, $assur1, $nb_assur, $nb_assur1, $type_chambre1, $nb_type_chambre, $nb_type_chambre1, $supp_adulte_div2, $supp_adulte_div3, $nb_supp_adulte_div2, $nb_supp_adulte_div3, $intitule_supp_adulte_div2, $intitule_supp_adulte_div3, $provenance_dossier, $ref_to);

                    if (!($etape)) {
                        if(empty($id_client)) {
                            echo("Vous devez selectionner un client pour ce dossier !!<br/>");
                            echo("<a href='search_client.php?PHPSESSID=".session_id()."'>Recherche de clients, cliquez ici</a><br/>");
                        }
                        else if (empty($provenance))
                        {
                            //Insertion d'un dossier venant du PKG QTS ou autre produit ?
                            Form_prepa_dossier($id_client);
                        }
                        else if ($provenance == "qts")
                        {
                            $donnes_import = Import_donnees($id_pkg);
                            Form_new_dossier($id_client, $id='',  $id_pkg, $etat='', $donnes_import[0], $donnes_import[1], $donnes_import[2], $donnes_import[6], $duree_autre='', $donnes_import[3], $adultes='', $enfants='', $bebes='',  $donnes_import[4], $assur='', $jour_depart='', $mois_depart='', $annee_depart='', $jour_retour='', $mois_retour='', $annee_retour='', $prix_base_adulte='', $prix_base_enfant='', $prix_base_bebe='', $supp_adulte='', $supp_enfant='', $supp_bebe='', $taxes_adulte='', $taxes_enfant='', $taxes_bebe='', $frais_dossiers='15', $etape='1', $acompte='', $mode_payement='', $nb_supp_adulte='', $nb_supp_enfant='', $nb_supp_bebe='', $nb_taxe_adulte='', $nb_taxe_enfant='', $nb_taxe_bebe='', $nb_reduc_adulte='', $nb_reduc_enfant='', $nb_reduc_bebe='', $supp_divers='', $nb_supp_divers='',$commentaires='', $reduc_adulte='', $reduc_enfant='', $reduc_bebe='', $donnes_import[5], $donnes_import[7], $donnes_import[8], $hausse_carb, $nb_hausse_carb, $commentaires_cli, $type_chambre, $supp_adulte_div, $supp_enfant_div, $supp_bebe_div, $nb_supp_adulte_div, $nb_supp_enfant_div, $nb_supp_bebe_div, $intitule_supp_adulte_div, $intitule_supp_enfant_div, $intitule_supp_bebe_div, $hausse_carb_bebe, $nb_hausse_carb_bebe, $assur1, $nb_assur, $nb_assur1,  $type_chambre1, $nb_type_chambre, $nb_type_chambre1);
                        }
                        else if ($provenance == "to")
                        {
                            $id_pkg="";
                            Form_new_dossier($id_client,'', $id_pkg);
                        }
                        else
                        {
                            $id_pkg="Autre";
                            Form_new_dossier($id_client,'', $id_pkg);
                        }
                    }
                    else if (!($verif == "TRUE")) {
                        echo "<font color='red'><strong>".$verif."</strong></font>";
                        $etape="2";
                        //echo $compris;
                        Form_new_dossier($id_client, $id,  $id_pkg, $etat, $dest_pays, $dest_ville, $dest_hotel, $duree, $duree_autre, $cat_hot, $adultes, $enfants, $bebes, $pension, $assur, $jour_depart, $mois_depart, $annee_depart, $jour_retour, $mois_retour, $annee_retour, $prix_base_adulte, $prix_base_enfant, $prix_base_bebe, $supp_adulte, $supp_enfant, $supp_bebe, $taxes_adulte, $taxes_enfant, $taxes_bebe, $frais_dossiers, $etape, $acompte, $mode_payement, $nb_supp_adulte, $nb_supp_enfant, $nb_supp_bebe, $nb_taxe_adulte, $nb_taxe_enfant, $nb_taxe_bebe, $nb_reduc_adulte, $nb_reduc_enfant, $nb_reduc_bebe, $supp_divers, $nb_supp_divers,$commentaires, $reduc_adulte, $reduc_enfant, $reduc_bebe, $ville_depart, $compris, $to, $hausse_carb, $nb_hausse_carb, $commentaires_cli, $type_chambre, $supp_adulte_div, $supp_enfant_div, $supp_bebe_div, $nb_supp_adulte_div, $nb_supp_enfant_div, $nb_supp_bebe_div, $intitule_supp_adulte_div, $intitule_supp_enfant_div, $intitule_supp_bebe_div, $hausse_carb_bebe, $nb_hausse_carb_bebe, $assur1, $nb_assur, $nb_assur1, $type_chambre1, $nb_type_chambre, $nb_type_chambre1, $supp_adulte_div2, $supp_adulte_div3, $nb_supp_adulte_div2, $nb_supp_adulte_div3, $intitule_supp_adulte_div2, $intitule_supp_adulte_div3, $provenance_dossier, $ref_to);
                    }
                    else {
                        echo("<h1>Formulaire OK : Insertion du dossier dans la base</h1>");
                        $id_dossier = Creation_dossier($id_client, $id_pkg, $etat, $dest_pays, $dest_ville, $dest_hotel, $duree, $duree_autre, $cat_hot, $adultes, $enfants, $bebes, $pension, $assur, $jour_depart, $mois_depart, $annee_depart, $jour_retour, $mois_retour, $annee_retour,  $prix_base_adulte, $prix_base_enfant, $prix_base_bebe, $supp_adulte, $supp_enfant, $supp_bebe, $taxes_adulte, $taxes_enfant, $taxes_bebe, $frais_dossiers, $id_vendeur, $acompte, $mode_payement, $nb_supp_adulte, $nb_supp_enfant, $nb_supp_bebe, $nb_taxe_adulte, $nb_taxe_enfant, $nb_taxe_bebe, $nb_reduc_adulte, $nb_reduc_enfant, $nb_reduc_bebe, $supp_divers, $nb_supp_divers, $commentaires, $reduc_adulte, $reduc_enfant, $reduc_bebe, $ville_depart, $compris, $to, $hausse_carb, $nb_hausse_carb,$commentaires_cli, $type_chambre, $supp_adulte_div, $supp_enfant_div, $supp_bebe_div, $nb_supp_adulte_div, $nb_supp_enfant_div, $nb_supp_bebe_div, $intitule_supp_adulte_div, $intitule_supp_enfant_div, $intitule_supp_bebe_div, $hausse_carb_bebe, $nb_hausse_carb_bebe, $assur1, $nb_assur, $nb_assur1, $type_chambre1, $nb_type_chambre, $nb_type_chambre1, $supp_adulte_div2, $supp_adulte_div3, $nb_supp_adulte_div2, $nb_supp_adulte_div3, $intitule_supp_adulte_div2, $intitule_supp_adulte_div3, $provenance_dossier, $_SESSION['id_vendeur'], $ref_to);
                        echo("<a href='passagers.php?PHPSESSID=".session_id()."&id_dossier=".$id_dossier."'>Enregistrer les passagers, cliquez ici</a><br/>");
                    }


                    if(isset($_FILES['factureTO']))
                    {
                        $dossier = 'facturesTO/';
                        @mkdir($dossier . $id_dossier);
                        $dossier = $dossier . $id_dossier.'/';
                        @chmod($dossier, 0777);
                        //echo $dossier;
                        $fichier = basename($_FILES['factureTO']['name']);
                        if(!isset($erreur)) //S'il n'y a pas d'erreur, on upload
                        {
                            //On formate le nom du fichier ici...
                            $fichier = strtr($fichier,
                              'ÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃ Ã¡Ã¢Ã£Ã¤Ã¥Ã§Ã¨Ã©ÃªÃ«Ã¬Ã­Ã®Ã¯Ã°Ã²Ã³Ã´ÃµÃ¶Ã¹ÃºÃ»Ã¼Ã½Ã¿',
                              'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
                            $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);

                            if(move_uploaded_file($_FILES['factureTO']['tmp_name'], $dossier . $fichier)) //Si la fonction renvoie TRUE, c'est que Ã§a a fonctionnÃ©...
                            {
                                echo "facture TO  bien upload&eacute;e";
                            }
                            else //Sinon (la fonction renvoie FALSE).
                            {
                                echo "Probleme facture TO";
                            }
                        }
                        else
                        {
                            echo $erreur;
                        }
                    }
                    //echo "1";
                    if(isset($montantTo))
                    {
                       // echo "2";

                        //$montantTo = (float)$_POST['montantTo'];
                        //est-ce que l'achat est deja enregistrÃ©.. ?
                        $link = Connection(MYDATABASE);
                        $sql = "SELECT id FROM achats WHERE id_dossier='".$id_dossier."' LIMIT 1;";
                        $res = mysql_query($sql) or die(mysql_error());
                        if(mysql_num_rows($res) > 0)
                        {
                            $row = mysql_fetch_assoc($res);
                            $sql = "UPDATE achats SET montant_achat='".$montantTo."' WHERE id_dossier = '".$id_dossier." '; ";
                            $res = mysql_query($sql) or die(mysql_error());
                        }
                        else
                        {
                           // $row = mysql_fetch_assoc($res);
                            $sql = "INSERT INTO achats (id_dossier, montant_achat, date, date_payement) VALUES ('".$id_dossier." ', '".$montantTo."', NOW(), NOW()); ";
                            $res = mysql_query($sql) or die(mysql_error());
                        }
                    }
                    ?>
                </td>
            </tr>
        </table>

</body></html>

