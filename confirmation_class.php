<!doctype html public "-//W3C//DTD HTML 4.0 //EN">
<html>
<head>
       <title>Title here!</title>
</head>
<body>
<?php
require('class_fpdf/fpdf.php');

class Confirmation extends FPDF{
	function ajoutEntete($vendeur,$id_dossier){
		$this->Open();
		$this->SetFillColor(240,52,58);
		$this->SetMargins(15, 15, 15);
		$this->AddPage();
		$this->Image('logo_voyaneo_small.jpg',15,10,50,18,'','http://www.voyaneo.com');
		$this->Ln(30);
		$this->SetFont('Arial','B',12);
	    $this->Cell(50,3,"Objet :",0,0,'L',0);
	    $this->SetFont('Arial','',12);
	    $this->Cell(80,3,"Confirmation de voyage",0,1,'L',0);
	    $this->Ln(3);
	    $this->SetFont('Arial','B',12);
	    $this->Cell(50,3,"Dossier trait� par :",0,0,'L',0);
	    $this->SetFont('Arial','',12);
	    $this->Cell(80,3,"$vendeur",0,1,'L',0);
	    $this->Ln(3);
	    $this->SetFont('Arial','B',12);
	    $this->Cell(50,3,"Num�ro du dossier :",0,0,'L',0);
	    $this->SetFont('Arial','',12);
	    $this->Cell(80,3,"$id_dossier",0,1,'L',0);
        $today=date("d/m/y");
        $this->Ln(8);
	    $this->Cell(180,3,"Levallois Perret, le $today ",0,1,'R',0);
	    $this->Ln(3);
				
	}

	function ajoutDescriptionProduit($type_produit,$date_resa,$destination,$ville_dep,$ville_arrivee,$hotel,$date_deb,$date_fin,$nb_passagers,$formule,$type_chambre,$passager_assurance){
		$declaration[0]="Bonjour,";
		$declaration[1]="Nous vous confirmons par la pr�sente votre voyage r�serv� le $date_resa.";
		$declaration[2]="R�capitulatif de votre r�servation :";
		
		switch($type_produit){
			case "Vol sec" :
				  $produit["Prestation"]="Vol sec";
				  $produit["Destination"]="$destination";
				  $produit["Ville d�part"]=$ville_dep;
				  $produit["Ville arriv�e"]=$ville_arrivee;
				  $produit["Date aller"]=$date_deb;
				  $produit["Date retour"]=$date_fin;
		    break;
		    case "Hotel seul" :
				  $produit["Prestation"]="H�tel";
				  $produit["Destination"]="$destination";
				  $produit["H�tel"]=$hotel;
				  $produit["Type de chambre"]=$type_chambre;
				  $produit["Formule"]=$formule;
				  $produit["Date"]=$date_deb."-".$date_fin;
		    break;
		    case "Croisiere" :
				  $produit["Prestation"]="Croisi�re";
				  $produit["Croisi�re"]=$hotel;
				  $produit["Ville d�part"]=$ville_dep;
				  $produit["Ville arriv�e"]=$ville_arrivee;
				  $produit["Type de chambre"]=$type_chambre;
				  $produit["Formule"]=$formule;
				  $produit["Date"]=$date_deb."-".$date_fin;
		    break;
		    default:
		    	  $produit["Prestation"]="S�jour";
		    	  $produit["H�tel"]=$hotel;
				  $produit["Destination"]="$destination";
				  $produit["Ville d�part"]=$ville_dep;
				  $produit["Ville arriv�e"]=$ville_arrivee;
				  $produit["Date aller"]=$date_deb;
				  $produit["Date retour"]=$date_fin;
				  $produit["Nombre de personnes"]=$nb_passagers;
				  $produit["Formule"]=$formule;  
		}


		$this->Ln(20);
		
		foreach($declaration as $dec){
			$this->SetFont('Arial','',12);
			$this->Cell(180,3,$dec,0,1,'L',0);
			$this->Ln(3);
		}
		$this->Ln(7);
		foreach($produit as $cle=>$val){
			$this->SetFont('Arial','B',12);
			$this->Cell(50,3,$cle,0,0,'L',0);
			$this->SetFont('Arial','',12);
			$this->Cell(50,3,$val,0,1,'L',0);
			$this->Ln(2);
		}
		$this->SetFont('Arial','B',12);
		$this->Cell(180,3,"Assurances :",0,1,'L',0);
		$this->Ln(2);
		//$passager_assurance=array();
		foreach($passager_assurance as $cle=>$val){
			$this->SetFont('Arial','B',10);
			$this->Cell(50,3,"  - $cle",0,0,'L',0);
			$this->SetFont('Arial','',12);
			$this->Cell(50,3,$val,0,1,'L',0);
			$this->Ln(2);
		}

	}
	function ajoutPaiement($montant,$accompte,$montant_partiel){
		
		$solde = $montant-($montant_partiel+$accompte);		
		//$solde=$montant-$montant_partiel;
		//$solde=$montant-($montant_partiel+$acompte);
			
		if ($solde <0)
		{
			$solde = 0;
		}
		
		$this->Ln(5);
		$this->SetFont('Arial','',12);
		//$this->Cell(180,3,"Suite � votre accord,nous vous d�bitons ce jour la somme de $a_payer �.",0,1,'L',0);
		//$this->Ln(2);
	//	$this->Cell(180,3,"$totalite_acompte",0,1,'L',0);
		$this->Ln(2);
		
		/*if($solde>0){
			$this->Cell(180,3,"Solde : ** $solde � **  � r�gler 1 mois avant le d�part.",0,1,'L',0);
			$this->Ln(3);		
		}
		else{
			$this->Cell(180,3,"Reste � solder : $solde �.",0,1,'L',0);
			$this->Ln(3);
		}*/
		
		$this->Cell(180,3,"Vous recevrez votre convocation 72 heures avant le d�part.",0,1,'L',0);
		$this->Ln(4);
		$this->SetFont('Arial','',12);
		$this->Cell(180,3,"Pour toute information, n'h�sitez pas � contacter le service client du lundi au vendredi",0,1,'L',0);
		$this->Ln(2);
		$this->Cell(180,3,"de 10h � 18h au 01 77 69 02 57 ",0,1,'L',0);
		$this->Ln(2);
		$this->Cell(180,3,"ou par mail : info@voyaneo.com",0,1,'L',0); 
	}
	function ajoutSalutation($civilite,$nom_client,$prenom_client){
		$this->Ln(4);
		$this->SetFont('Arial','',12);
		$this->Cell(180,3,"Veuillez agr�er, $civilite $nom_client $prenom_client, nos sinc�res salutations. ",0,1,'L',0);
		$this->Ln(3);
		$this->Cell(180,3,"En vous souhaitant un bon voyage.",0,1,'L',0); 
	}
	function ajoutCoordonnees(){
		$this->Ln(2);
		$coordonnes[0]="";
		$coordonnes[1]=" ";
		$coordonnes[2]="Service Client";
		$coordonnes[3]="Voyaneo.com";
	    $this->Ln(4);
		$this->SetFont('Arial','',10);
		foreach($coordonnes as $cor){
			$this->Cell(180,3,$cor,0,1,'L',0);
			$this->Ln(1);
		}

	}

	function Footer(){
    //Positionnement � 1,5 cm du bas
    	$this->SetY(-30);
    	$$coordonnes[0]="Si�ge Social : 90 rue Baudin 92300 Levallois Perret ";
		$coordonnes[1]="Fax : 01 77 69 02 58";
		$coordonnes[2]="Voyaneo.com est une marque de Ethnosal SARL au capital de 7500 � - RCS Nanterre 751 870 627 ";
		$coordonnes[3]="IM09210053 - RC Gan eurocourtage N� 086833734 / Garantie APST Association ";

		$coordonnes[4]="N� TVA Intracommunautaire FR 8375187062700013";
		$coordonnes[5]="Internet : www.voyaneo.com";
		$this->SetFont('Arial','',6);
		$this->SetFont('Arial','',7);
		$this->SetFillColor(215,207,207); 
		foreach($coordonnes as $cor){
			$this->Cell(180,3,$cor,0,1,'C',1);
		}
	}	
}


?> 
</body>
</html>
