<?php
session_start();
if (!isset($_SESSION["id_vendeur"])) {
		   header("Location:index.php");
		   exit();
		}
		
		error_reporting(e_all);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Statistiques Monagence&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="client.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
require_once("fonctions_db.php");
require_once("fonctions.php");
require_once("comptabilite/mailto.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">
<?php

$mois = $_POST["mois"];
$annee = $_POST["annee"];

$montant_achat = $_POST["montant_achat"];
$facture_achat = $_POST["facture_achat"];
$id_dossier = $_POST["id_dossier"];






if (!empty($mois)) {$mois_courant = $mois;};
if (!empty($annee)) {$annee_courante = $annee;};

if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};

//echo $mois."//".$annee;
$envoi_mail = $_POST["envoi_mail"];
$html = $_POST["html"];

//Préparation du mail et envoi
$file_csv_ce  = 'comptabilite/journalVentes'.$mois_courant.'_'.$annee_courante.'CE.csv';
$file_csv_hce = 'comptabilite/journalVentes'.$mois_courant.'_'.$annee_courante.'HorsCE.csv';
$output_ce_csv = "";
$output_hce_csv = "";

//$destinataires =array('julien@comptoirdesdeals.com');
$destinataires =array('yveschaponic@gmail.com',"info@voyaneo.com",'fabien@lead-factory.net','hernoux.fabien@gmail.com');

//Préparation du mail et envoi

if($envoi_mail=="ok")
{
	if ( mailToCompta($destinataires, "vente",$file_csv_ce,$file_csv_hce) )
	{
		echo "Le mail a bien été envoyé à <b>$destinataire</b> de la part de <b>$expediteur</b> <br/>";
	}
}



/*
if($envoi_mail=="ok")
{

	$boundary = "-----=".md5(uniqid(rand()));
	
	$header = "MIME-Version: 1.0\r\n";
	
	$header .= "Content-Type: multipart/mixed; boundary=\"$boundary\"\r\n";
	$header .= "\r\n";
	
	
	
	$msg = "Votre lecteur de mail ne semble pas compatible. Merci de contacter Agencedevoyage.com au 01 77 69 02 52\r\n";
	
	
	$msg .= "--$boundary\r\n";
	$msg .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
	
	$msg .= "Content-Transfer-Encoding:8bit\r\n";
	$msg .= "\r\n";

	$msg .= "<style>*{font-family:Verdana}</style><center><img src='http://www.agencedevoyage.com/img/header/logo.jpg'></center><br/><br/>";
	$msg .= "\r\n";
	$msg .= "Bonjour <br/><br/>\r\n";
	$msg .= "\r\n";
	$msg .= "Veuillez trouver ci-joint le journal de vente de Agencedevoyage.com<br/>\r\n";
	$msg .= "<br/>\r\n";
	$msg .= "Cordialement, <br/>\r\n";
	$msg .= "L'équipe de Agencedevoyage.com <br/>\r\n";
	$msg .= "Tél : 01 77 69 02 52<br/>\r\n";
	$msg .= "Fax : 01 77 69 02 17 <br/>\r\n";

	$msg .= "\r\n\r\n";
	//Texte
	$msg .= "--$boundary\r\n";

	$msg .= "Content-Type: text/html; name=\"journal_vente.html\"\r\n";
	$msg .= "Content-Transfer-Encoding: 8bit\r\n";
	
	$msg .= "Content-Disposition: attachment; filename=\"journal_vente.html\"\r\n";
	
	$msg .= "\r\n";
	
	$msg .= stripslashes($html). "\r\n";
	$msg .= "\r\n\r\n";
	
	
	$expediteur="info@agencedevoyage.com";
	$reponse = $expediteur;
	$destinataire="yves@agencedevoyage.com";
	//Hack de test a commenter pour envoyer au client
	//$destinataire="webmaster@medisite.fr";
	// && mail($expediteur, "COPIE - Journal de vente - Monagence.com - $destinataire", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header)
	if (mail($destinataire, "Journal de vente - Agencedevoyage.com", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header) && mail($expediteur, "COPIE - Journal de vente - Agencedevoyage.com - $destinataire", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header))
	{
		echo "Le mail a bien été envoyé à <b>$destinataire</b> de la part de <b>$expediteur</b> <br/>";	
	}
	
}
*/



$html_output="";

	echo "<h1>Visualisation du journal des ventes pour le : ".$mois_courant." / ".$annee_courante."</h1>";
	echo "<p><form method='post' action='".$_SERVER['PHP_SELF']."?PHPSESSID=".session_id()."'><fieldset><legend>Choix de la période</legend>";
	echo ("<label for='mois'>Voir une autre période : </label>\n\r");
	echo ("<select name='mois' id='mois'>\n\r");
	for ($i=1;$i<=12;$i++) {
		if (strlen($i) < 2){$mois="0".$i;} else {$mois=$i;};
		echo ("<option value='".$mois."'");
		if ($mois == $mois_courant) {echo("selected='selected'");};
		echo (">".$mois."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='annee' id='annee'>\n\r");
	for ($i=2005;$i<=$annee_courante;$i++) {
		echo ("<option value='".$i."'");
		if ($i == $annee_courante) {echo("selected='selected'");};
		echo (">".$i."</option>\n\r");
	}
	echo ("</select><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");
	echo "<input type='submit' name='Voir' id='Voir' value='Voir'></fieldset></form></p>";

	$annee_courante = substr($annee_courante,2,4);

if(isset($id_dossier)) {
	$link = connection(MYDATABASE);
	$requete = "SELECT * FROM achats WHERE id_dossier='".$id_dossier."';";
	$result=mysql_query($requete) or die(mysql_error());
	$num_rows = mysql_num_rows($result);
	if ($num_rows > 0) {
		$requete2 = "UPDATE achats SET montant_achat='".$montant_achat."', facture_achat='".$facture_achat."' WHERE id_dossier='".$id_dossier."';";
		
	}
	else
	{
		$requete2 = "INSERT INTO achats (id_dossier, montant_achat, facture_achat) VALUES ('".$id_dossier."','".$montant_achat."', '".$facture_achat."');";
	}
	echo $requete2;
	$link = connection(MYDATABASE);
	$result2=mysql_query($requete2) or die(mysql_error());
	if ($result2) {
		echo("Facture / Montant mis à jour.");
	}
}


$html_output .= "<h1>Journal des ventes pour le ".$mois_courant." / ".$annee_courante."</h1>";

for ($i=1;$i<32;$i++)
{
$jour_courant = $i;
if (strlen($jour_courant) < 2){$jour_courant="0".$jour_courant;};

	$link = connection(MYDATABASE);
	$requete = "
	SELECT DISTINCT 
	dossiers.id, 
	dossiers.to, 
	dossiers.dest_ville, 
	dossiers.ville_depart, 
	dossiers.date_deb, 
	dossiers.date_fin, 
	vendeurs.prenom, 
	clients.nom,
	code_pays.europe_compta
	FROM 
	`dossiers`, 
	`clients`, 
	`vendeurs`, 
	`lien_dossier_vendeur`,
	`code_pays`
	WHERE 
	dossiers.id_client=clients.id
	AND 
	dossiers.id=lien_dossier_vendeur.id_dossier 
	AND
	dossiers.dest_pays=code_pays.cpays
	AND 
	lien_dossier_vendeur.id_vendeur=vendeurs.id 
	AND 
	(etat='En request' OR etat='Confirmé' OR etat='Préconfirmé' OR etat='Annulé') 
	AND 
	SUBSTRING(`date_confirm`,9,10)=".$jour_courant." 
	AND 
	SUBSTRING(`date_confirm`,6,7)=".$mois_courant." 
	AND 
	SUBSTRING(`date_confirm`,3,4)=".$annee_courante." 
	ORDER BY 
	dossiers.id;
	";

	$result=mysql_query($requete) or die(mysql_error());
	$num_rows = mysql_num_rows($result);
	if ($num_rows > 0) {
		$html_output .= "<h2>$jour_courant / $mois_courant / $annee_courante</h2>\n\r";
		$html_output .= "<table border='1' cellpadding='2' cellspacing='0'>\n\r";
		$html_output .= "<tr>\n\r";
		$html_output .= "<td><b>ID dossier</b></td>\n\r";
		$html_output .= "<td><b>Vendeur</b></td>\n\r";
		$html_output .= "<td><b>Nom client</b></td>\n\r";
		$html_output .= "<td><b>TO</b></td>\n\r";
		$html_output .= "<td><b>Ville Destination</b></td>\n\r";
		$html_output .= "<td><b>Ville départ</b></td>\n\r";
		$html_output .= "<td><b>Date de départ</b></td>\n\r";
		$html_output .= "<td><b>Date de retour</b></td>\n\r";
		$html_output .= "<td><b>Montant</b></td>\n\r";
		$html_output .= "<td><b>Montant Achat</b></td>\n\r";
		$html_output .= "<td><b>Assurances</b></td>\n\r";
		$html_output .= "</tr>\n\r";
		
		$output_ce_csv .= "Date ;";
		$output_ce_csv .= "ID dossier ;";
		$output_ce_csv .= "Vendeur ;";
		$output_ce_csv .= "Nom client ;";
		$output_ce_csv .= "TO ;";
		$output_ce_csv .= "Ville Destination ;";
		$output_ce_csv .= "Ville départ ;";
		$output_ce_csv .= "Date de départ ;";
		$output_ce_csv .= "Date de retour ;";
		$output_ce_csv .= "Montant ;";
		$output_ce_csv .= "Montant Achat ;";
		$output_ce_csv .= "Assurances ;\n" ;
		
		$output_hce_csv .= "Date ;";
		$output_hce_csv .= "ID dossier ;";
		$output_hce_csv .= "Vendeur ;";
		$output_hce_csv .= "Nom client ;";
		$output_hce_csv .= "TO ;";
		$output_hce_csv .= "Ville Destination ;";
		$output_hce_csv .= "Ville départ ;";
		$output_hce_csv .= "Date de départ ;";
		$output_hce_csv .= "Date de retour ;";
		$output_hce_csv .= "Montant ;";
		$output_hce_csv .= "Montant Achat ;";
		$output_hce_csv .= "Assurances ;\n" ;
	}
	
	
	while($row = mysql_fetch_row($result))
	{
		if($row[8])
			$current = "output_ce_csv";
		else
			$current = "output_hce_csv";
		$link = connection(MYDATABASE);
		$requete3 = "SELECT facture_achat, montant_achat FROM achats WHERE id_dossier='".$row[0]."';";
		$result3=mysql_query($requete3) or die(mysql_error());
		$num_rows3 = mysql_num_rows($result3);
		//echo $num_rows3;
		
		$facture_achat_temp = 0;
		$montant_achat_temp = 0;
		
		while($row3 = mysql_fetch_row($result3))
		{
			$facture_achat_temp = $row3[0];
			$montant_achat_temp = $row3[1];
			//echo "ICI : ".$facture_achat.$montant_achat;
		}
		
		$total_assur = Total_assur($row[0]);
		
		$html_output .= "<tr>\n\r";
		$html_output .= "<td>".$row[0]."</td>\n\r";
		$html_output .= "<td>".$row[6]."</td>\n\r";
		$html_output .= "<td>".$row[7]."</td>\n\r";
		$html_output .= "<td>".$row[1]."</td>\n\r";
		$html_output .= "<td>".$row[2]."</td>\n\r";
		$html_output .= "<td>".$row[3]."</td>\n\r";
		$html_output .= "<td>".$row[4]."</td>\n\r";
		$html_output .= "<td>".$row[5]."</td>\n\r";
		$html_output .= "<td>".Calcul_total_dossier($row[0])."</td>\n\r";
		$html_output .= "<td>".$montant_achat_temp."</td>\n\r";
		$html_output .= "<td>".$total_assur[1]."</td>\n\r";
		$html_output .= "</tr>\n\r";
		
		${$current} .= "$jour_courant/$mois_courant/$annee_courante ;";
		${$current} .= $row[0].";";
		${$current} .= $row[6].";";
		${$current} .= $row[7].";";
		${$current} .= $row[1].";";
		${$current} .= $row[2].";";
		${$current} .= $row[3].";";
		${$current} .= $row[4].";";
		${$current} .= $row[5].";";
		${$current} .= Calcul_total_dossier($row[0]).";";
		${$current} .= $montant_achat_temp.";";
		${$current} .= $total_assur[1].";\n";
		
	}

	if ($num_rows > 0) {
		$html_output .= "</table>\n\r";
	}


$annee_courante2 = $annee_courante;
//Commentaires
if (strlen($annee_courante2) < 3){$annee_courante2="20".$annee_courante2;};
$date=$annee_courante2."/".$mois_courant."/".$jour_courant;
	$link = connection(MYDATABASE);
	$requete = "SELECT texte FROM commentaires WHERE type='Vente' AND date='".$date."';";
	$result=mysql_query($requete) or die(mysql_error());
	if (mysql_num_rows($result)>0) 
	{
		$row=mysql_fetch_row($result);
		$commentaire = $row[0];
		$html_output .= "<p>Commentaire : $commentaire</p>\n\r";
	}

$html_output .= "<hr>\n\r";
}

echo $html_output;

//echo " <pre> CE -- $output_ce_csv </pre>";
//echo "<pre> HCE -- $output_hce_csv </pre>";

	file_put_contents($file_csv_ce, $output_ce_csv);
	file_put_contents($file_csv_hce,$output_hce_csv);
	
	echo ("<form action='".$_SERVER['PHP_SELF']."' name='envoi' id='envoi' method='post'>\n\r");
	echo("<fieldset><legend>Envoi mail à yves@agencedevoyage.com</legend>");
	echo("<textarea name='html' onFocus='this.blur()' cols='1' rows='1' style='visibility:hidden;'>$html_output</textarea>");
	echo("<input type='hidden' name='envoi_mail' value='ok'>\n\r");
	echo("<input type='hidden' name='mois' value='".$_POST['mois']."'>\n\r");
	echo("<input type='hidden' name='annee' value='".$_POST['annee']."'>\n\r");
	echo ("<div class='center'><input type='submit' class='submit' name='Envoi du mail' id='Envoi du mail' value='Envoi du mail'><br/></div>\n\r");
	echo("</fieldset></form>");	

?>
</td>
</tr>
</table>

</body></html>

