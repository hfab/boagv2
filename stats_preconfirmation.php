<?php
session_start();
if (!session_is_registered("id_vendeur")) {
		   header("Location:index.php");
		   exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Statistiques Monagence&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<link href="client.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
require_once("fonctions_db.php");
require_once("fonctions.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">
<?php
$jour = $_POST["jour"];
$mois = $_POST["mois"];
$annee = $_POST["annee"];

$montant_achat = $_POST["montant_achat"];
$facture_achat = $_POST["facture_achat"];
$id_dossier = $_POST["id_dossier"];


$jour_courant=date("d");
$mois_courant=date("m");
$annee_courante=date("y");


$date_comm = $_POST["date_comm"];
$type = $_POST["type"];
$commentaire = $_POST["commentaire"];

if (!empty($jour)) {$jour_courant = $jour;};
if (!empty($mois)) {$mois_courant = $mois;};
if (!empty($annee)) {$annee_courante = $annee;};

if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};


	echo "<h1>Visualisation du tableau des ventes pour le : ".$jour_courant." / ".$mois_courant." / ".$annee_courante."</h1>";
	echo "<p><form method='post' action='".$_SERVER['PHP_SELF']."?PHPSESSID=".session_id()."'><fieldset><legend>Choix de la période</legend>";
	echo ("<label for='jour'>Voir une autre période : </label>\n\r");
	echo ("<select name='jour' id='jour'>\n\r");
	for ($i=1;$i<=31;$i++) {
		if (strlen($i) < 2){$jour="0".$i;} else {$jour=$i;};
		echo ("<option value='".$jour."'");
		if ($jour == $jour_courant) {echo("selected='selected'");};
		echo (">".$jour."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='mois' id='mois'>\n\r");
	for ($i=1;$i<=12;$i++) {
		if (strlen($i) < 2){$mois="0".$i;} else {$mois=$i;};
		echo ("<option value='".$mois."'");
		if ($mois == $mois_courant) {echo("selected='selected'");};
		echo (">".$mois."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='annee' id='annee'>\n\r");
	for ($i=2005;$i<=$annee_courante;$i++) {
		echo ("<option value='".$i."'");
		if ($i == $annee_courante) {echo("selected='selected'");};
		echo (">".$i."</option>\n\r");
	}
	echo ("</select><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");
	echo "<input type='submit' name='Voir' id='Voir' value='Voir'></fieldset></form></p>";

	$annee_courante = substr($annee_courante,2,4);




echo("<h1>Tableau des ventes/préconfirmations du jour</h1>");

	$link = connection(MYDATABASE);
	$requete = "SELECT DISTINCT dossiers.id, dossiers.to, dossiers.dest_ville, dossiers.ville_depart, dossiers.date_deb, dossiers.date_fin, vendeurs.prenom, clients.nom, dossiers.etat FROM `dossiers`, `clients`, `vendeurs`, `lien_dossier_vendeur` WHERE dossiers.id_client=clients.id AND dossiers.id=lien_dossier_vendeur.id_dossier AND lien_dossier_vendeur.id_vendeur=vendeurs.id AND (etat='Préconfirmé' OR etat='En request' OR etat='Confirmé') AND SUBSTRING(`date_confirm`,1,2)=".$jour_courant." AND SUBSTRING(`date_confirm`,4,2)=".$mois_courant." AND SUBSTRING(`date_confirm`,7,2)=".$annee_courante." ORDER BY vendeurs.prenom, dossiers.id;";
	$result=mysql_query($requete) or die(mysql_error());
	$num_rows = mysql_num_rows($result);
	if ($num_rows > 0) {
		echo("<table border='1' cellpadding='2' cellspacing='0'>");
		echo("<tr>");
		echo("<td><b>ID dossier</b></td>");
		echo("<td><b>Etat</b></td>");
		echo("<td><b>Vendeur</b></td>");
		echo("<td><b>Nom client</b></td>");
		echo("<td><b>TO</b></td>");
		echo("<td><b>Ville Destination</b></td>");
		echo("<td><b>Ville départ</b></td>");
		echo("<td><b>Date de départ</b></td>");
		echo("<td><b>Date de retour</b></td>");
		echo("<td><b>Montant</b></td>");
		echo("<td><b>Assurance</b></td>");
		echo("</tr>");
	}
	
	unset($total);
	unset($nb_dossiers);
	
	while($row = mysql_fetch_row($result))
	{
		
		$total_assur = Total_assur($row[0]);
		$total_courant = Calcul_total_dossier($row[0]);
		$total[$row[6]] += $total_courant;
		$nb_dossiers[$row[6]]++;
		
		echo("<tr>");
		echo("<td>".$row[0]."</td>");
		echo("<td>".$row[8]."</td>");
		echo("<td>".$row[6]."</td>");
		echo("<td>".$row[7]."</td>");
		echo("<td>".$row[1]."</td>");
		echo("<td>".$row[2]."</td>");
		echo("<td>".$row[3]."</td>");
		echo("<td>".$row[4]."</td>");
		echo("<td>".$row[5]."</td>");
		echo("<td>".Calcul_total_dossier($row[0])."</td>");
		echo("<td>".$total_assur[1]."</td>");
		echo("</tr>");
	}

	if ($num_rows > 0) {
		echo("</table>");
	}



echo("<hr>");

foreach($total as $key=>$value)
{
	echo $key." : ".$value." € (".$nb_dossiers[$key]." dossier(s))<br/>";
}

if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};
$date=$annee_courante."/".$mois_courant."/".$jour_courant;


	
	echo "<hr><h1>Chiffre d'affaire :</h1> ".number_format(array_sum($total),2,',',' ')." €";
	echo "<hr><h1>Panier moyen (".array_sum($nb_dossiers)." dossiers confirmés/préconfirmés) :</h1> ".number_format(array_sum($total) / array_sum($nb_dossiers),2,',',' ')." €";
?>
</td>
</tr>
</table>

</body></html>

