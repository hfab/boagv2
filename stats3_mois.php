<?php
session_start();
if (!isset($_SESSION["id_vendeur"])) {
		   header("Location:index.php");
		   exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Statistiques Monagence&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="client.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
require_once("fonctions_db.php");
require_once("fonctions.php");
require_once("comptabilite/mailto.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">
<?php

$mois = $_POST["mois"];
$annee = $_POST["annee"];


$mois_courant=date("m");
$annee_courante=date("y");



if (!empty($mois)) {$mois_courant = $mois;};
if (!empty($annee)) {$annee_courante = $annee;};

if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};

$file_csv_ce   = 'comptabilite/journalCaisse'.$mois_courant.'_'.$annee_courante.'CE.csv';
$file_csv_hce = 'comptabilite/journalCaisse'.$mois_courant.'_'.$annee_courante.'HorsCE.csv';
$output_ce_csv = "";
$output_hce_csv = "";
$envoi_mail = $_POST["envoi_mail"];
$html = $_POST["html"];



//$destinataires =array('julien@comptoirdesdeals.com');
$destinataires =array('yveschaponic@gmail.com',"info@voyaneo.com",'fabien@lead-factory.net','hernoux.fabien@gmail.com');

//Préparation du mail et envoi

if($envoi_mail=="ok")
{
	if ( mailToCompta($destinataires, "caisse",$file_csv_ce,$file_csv_hce) )
	{
		echo "Le mail a bien été envoyé à <b>$destinataire</b> de la part de <b>$expediteur</b> <br/>";
	}
}



//Préparation du mail et envoi
/*
if($envoi_mail=="ok")
{

	$boundary = "-----=".md5(uniqid(rand()));
	
	$header = "MIME-Version: 1.0\r\n";
	
	$header .= "Content-Type: multipart/mixed; boundary=\"$boundary\"\r\n";
	$header .= "\r\n";
	
	
	
	$msg = "Votre lecteur de mail ne semble pas compatible. Merci de contacter Agencedevoyage.com au 01 77 69 02 52\r\n";
	
	
	$msg .= "--$boundary\r\n";
	$msg .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
	
	$msg .= "Content-Transfer-Encoding:8bit\r\n";
	$msg .= "\r\n";

	$msg .= "<style>*{font-family:Verdana}</style><center><img src='http://www.agencedevoyage.com/img/header/logo.jpg'></center><br/><br/>";
	$msg .= "\r\n";
	$msg .= "Bonjour <br/><br/>\r\n";

	$msg .= "\r\n";
	$msg .= "Veuillez trouver ci-joint le journal de caisse de Agencedevoyage.com<br/>\r\n";
	$msg .= "<br/>\r\n";
	$msg .= "Cordialement, <br/>\r\n";
	$msg .= "L'équipe de Agencedevoyage.com <br/>\r\n";
	$msg .= "Tél :  01 77 69 02 52<br/>\r\n";
	$msg .= "Fax :  01 77 69 02 14 <br/>\r\n";

	$msg .= "\r\n\r\n";
	//Texte
	$msg .= "--$boundary\r\n";

	$msg .= "Content-Type: text/html; name=\"journal_caisse.html\"\r\n";
	$msg .= "Content-Transfer-Encoding: 8bit\r\n";
	
	$msg .= "Content-Disposition: attachment; filename=\"journal_caisse.html\"\r\n";
	
	$msg .= "\r\n";
	
	$msg .= stripslashes($html). "\r\n";
	$msg .= "\r\n\r\n";
	
	
	$expediteur="info@agencedevoyage.com";
	$reponse = $expediteur;
	$destinataire="yves@agencedevoyage.com";
	//Hack de test a commenter pour envoyer au client
	//$destinataire="webmaster@medisite.fr";
	//&& mail($expediteur, "COPIE - Journal de caisse Monagence.com - $destinataire", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header)
	if (mail($destinataire, "Journal de caisse Agencedevoyage.com", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header) && mail($expediteur, "COPIE - Journal de caisse Agencedevoyage.com - $destinataire", $msg, "Reply-to: $reponse\r\nFrom: $expediteur\r\n".$header))
	{
		echo "Le mail a bien été envoyé à <b>$destinataire</b> de la part de <b>$expediteur</b> <br/>";	
	}
	
}
*/




$html_output="";

	echo "<h1>Visualisation du journal des caisses pour le : ".$mois_courant." / ".$annee_courante."</h1>";
	echo "<p><form method='post' action='".$_SERVER['PHP_SELF']."?PHPSESSID=".session_id()."'><fieldset><legend>Choix de la période</legend>";
	echo ("<label for='mois'>Voir une autre période : </label>\n\r");

	echo ("<select name='mois' id='mois'>\n\r");
	for ($i=1;$i<=12;$i++) {
		if (strlen($i) < 2){$mois="0".$i;} else {$mois=$i;};
		echo ("<option value='".$mois."'");
		if ($mois == $mois_courant) {echo("selected='selected'");};
		echo (">".$mois."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='annee' id='annee'>\n\r");
	for ($i=2005;$i<=$annee_courante;$i++) {
		echo ("<option value='".$i."'");
		if ($i == $annee_courante) {echo("selected='selected'");};
		echo (">".$i."</option>\n\r");
	}
	echo ("</select><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");
	echo "<input type='submit' name='Voir' id='Voir' value='Voir'></fieldset></form></p>";

	$annee_courante = substr($annee_courante,2,4);
	
	$html_output .= "<h1>Journal de caisse pour le ".$mois_courant." / ".$annee_courante."</h1>";
	
	$output_ce_csv .= "Date ;" ;
		$output_ce_csv .= "ID dossier ;";
		$output_ce_csv .= "Nom client ;";
		$output_ce_csv .= "Montant du payement ;";
		$output_ce_csv .= "Moyen de réglement ;\n";
		//$output_ce_csv .= "Montant restant ;\n";
		
		
		$output_hce_csv .= "Date ;" ;
		$output_hce_csv .= "ID dossier ;";
		$output_hce_csv .= "Nom client ;";
		$output_hce_csv .= "Montant du payement ;";
		$output_hce_csv .= "Moyen de réglement ;\n";
		//$output_hce_csv .= "Montant restant ;\n";
	
for ($i=0;$i<32;$i++)
{
$jour_courant = $i;
if (strlen($jour_courant) < 2){$jour_courant="0".$jour_courant;};
	
	$link = connection(MYDATABASE);
	$requete2 = "SELECT dossiers.id, dossiers.id_client, dossiers.mode_payement, dossiers.acompte, clients.nom, facturation.montant_payement, facturation.mode_payement, dossiers.etat ,code_pays.europe_compta  FROM `dossiers`, `clients`, `vendeurs`, `lien_dossier_vendeur`, `facturation`,`code_pays` WHERE dossiers.id=facturation.id_dossier AND dossiers.id_client=clients.id AND dossiers.dest_pays=code_pays.cpays AND dossiers.id=lien_dossier_vendeur.id_dossier AND lien_dossier_vendeur.id_vendeur=vendeurs.id AND (`etat`='Confirmé' OR etat='Préconfirmé' OR `etat`='En request' OR etat='Annulé') AND SUBSTRING(facturation.date_payement,1,2)=".$jour_courant." AND SUBSTRING(facturation.date_payement,4,2)=".$mois_courant." AND SUBSTRING(facturation.date_payement,7,2)=".$annee_courante." ORDER BY dossiers.id;";
	$result2=mysql_query($requete2) or die(mysql_error());
	$num_rows2 = mysql_num_rows($result2);
	if ($num_rows2 > 0) {
		$html_output .= "<h2>$jour_courant / $mois_courant / $annee_courante</h2>";
		$html_output .= "<table border='1' cellpadding='2' cellspacing='0'>";
		$html_output .= "<tr>";
		$html_output .= "<td><b>ID dossier</b></td>";
		$html_output .= "<td><b>Nom client</b></td>";
		$html_output .= "<td><b>Montant du payement</b></td>";
		$html_output .= "<td><b>Moyen de réglement</b></td>";
		$html_output .= "<td><b>Montant restant</b></td>";
		$html_output .= "</tr>";
		

		

	}
	while($row2 = mysql_fetch_row($result2))
	{
		if($row2[7] == "Annulé")
		{
			$bgcolor="red";
		}
		else
		{
			$bgcolor="#FFFFFF";
		}
		$html_output .= "<tr style='background-color:".$bgcolor."'>";
		$html_output .= "<td>".$row2[0]."</td>";
		$html_output .= "<td>".$row2[4]."</td>";
		$html_output .= "<td>".$row2[5]."</td>";
		$html_output .= "<td>".$row2[6]."</td>";
		$html_output .= "<td>".(Calcul_total_dossier($row2[0])-$row2[3])."</td>";
		$html_output .= "</tr>";
		
		$mode_compta = $row2[8];

		if($mode_compta =="1")
		{
			$output_ce_csv .= "$jour_courant/$mois_courant/$annee_courante ;" ;
			$output_ce_csv .= $row2[0].";";
			$output_ce_csv .= $row2[4].";";
			$output_ce_csv .= $row2[5].";";
			$output_ce_csv .= $row2[6].";\n";
			//$output_ce_csv .= (Calcul_total_dossier($row2[0])-$row2[3]).";\n";
		}
		else
		{
			$output_hce_csv .= "$jour_courant/$mois_courant/$annee_courante ;" ;
			$output_hce_csv .= $row2[0].";";
			$output_hce_csv .= $row2[4].";";
			$output_hce_csv .= $row2[5].";";
			$output_hce_csv .= $row2[6].";\n";
			//$output_hce_csv .= (Calcul_total_dossier($row2[0])-$row2[3]).";\n";
		}
		
		//echo "<pre>";
		//print_r($row2);
		//echo "</pre>";
		
	}
	
	

	/**$link = connection(MYDATABASE);
	$requete_a = "SELECT dossiers.id, dossiers.id_client, dossiers.mode_payement, dossiers.acompte, clients.nom, facturation.montant_payement, facturation.mode_payement   FROM `dossiers`, `clients`, `vendeurs`, `lien_dossier_vendeur`, `facturation` WHERE dossiers.id=facturation.id_dossier AND dossiers.id_client=clients.id AND dossiers.id=lien_dossier_vendeur.id_dossier AND lien_dossier_vendeur.id_vendeur=vendeurs.id AND `etat`='Annulé' AND SUBSTRING(facturation.date_payement,1,2)=".$jour_courant." AND SUBSTRING(facturation.date_payement,4,2)=".$mois_courant." AND SUBSTRING(facturation.date_payement,7,2)=".$annee_courante." ORDER BY dossiers.id;";
	$result_a=mysql_query($requete_a) or die(mysql_error());
	while($row_a = mysql_fetch_row($result_a))
	{
		$html_output .= "<tr bgcolor='red'>";
		$html_output .= "<td>".$row_a[0]."</td>";
		$html_output .= "<td>".$row_a[4]."</td>";
		$html_output .= "<td>".$row_a[5]."</td>";
		$html_output .= "<td>".$row_a[6]."</td>";
		$html_output .= "<td>".(Calcul_total_dossier($row_a[0])-$row_a[3])."</td>";
		$html_output .= "</tr>";
	}**/
	if ($num_rows2 > 0) {
		$html_output .= "</table>";
	}
	
	
if ($num_rows2>0){	
$html_output .= "<h1>Total des ventes, réparties par moyen de payement</h1>";
$fieldOptions = mysql_enum_values('facturation','mode_payement');
foreach($fieldOptions as $tmp)
{
	$link = connection(MYDATABASE);
	$requete = "SELECT SUM(montant_payement) FROM facturation WHERE mode_payement='".$tmp."' AND SUBSTRING(date_payement,1,2)=".$jour_courant." AND SUBSTRING(date_payement,4,2)=".$mois_courant." AND SUBSTRING(date_payement,7,2)=".$annee_courante.";";
	$result=mysql_query($requete) or die(mysql_error());
	$row = mysql_fetch_row($result);
	if (!empty($row[0]))
	{
		$html_output .= $tmp." : ".$row[0]." <br/>";
	}
}

}


$annee_courante2 = $annee_courante;
//Commentaires
if (strlen($annee_courante2) < 3){$annee_courante2="20".$annee_courante2;};
$date=$annee_courante2."/".$mois_courant."/".$jour_courant;
	$link = connection(MYDATABASE);
	$requete = "SELECT texte FROM commentaires WHERE type='Caisse' AND date='".$date."';";
	$result=mysql_query($requete) or die(mysql_error());
	if (mysql_num_rows($result)>0) 
	{
		$row=mysql_fetch_row($result);
		$commentaire = $row[0];
		$html_output .= "<p>Commentaire : $commentaire</p>";
	}
$html_output .= "<hr>";
}
	
	
	echo $html_output;
	
	file_put_contents($file_csv_ce, $output_ce_csv);
	file_put_contents($file_csv_hce,$output_hce_csv);
	
	echo ("<form action='".$_SERVER['PHP_SELF']."' name='envoi' id='envoi' method='post'>\n\r");
	echo("<fieldset><legend>Envoi mail à yveschaponic@gmail.com</legend>");
	echo("<textarea name='html' onFocus='this.blur()' cols='1' rows='1' style='visibility:hidden;'>$html_output</textarea>");
	echo("<input type='hidden' name='envoi_mail' value='ok'>\n\r");
		echo("<input type='hidden' name='mois' value='".$_POST['mois']."'>\n\r");
	echo("<input type='hidden' name='annee' value='".$_POST['annee']."'>\n\r");
	echo ("<div class='center'><input type='submit' class='submit' name='Envoi du mail' id='Envoi du mail' value='Envoi du mail'><br/></div>\n\r");
	echo("</fieldset></form>");	

?>
</td>
</tr>
</table>

</body></html>

