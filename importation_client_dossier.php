                                                                     
                                                                     
                                                                     
                                             
<h1>Import des clients/dossiers de Monagence.com</h1>
<?php
require_once("fonctions_db.php");
require_once("fonctions.php");

/* ip du serveur ftp ou nom de la hote*/
$ftp_server = '00. 00. 00. 00';
$ftp_user_name = 'username';
$ftp_user_pass = 'mot_de_passe';


$conn_id = ftp_connect($ftp_server);
$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
//echo "LOGIN : ".$login_result."<br />";
//echo $conn_id;
function ftp_is_dir($folder)
{
	global $conn_id;
	if (@ftp_chdir($conn_id, $folder))
	{
		ftp_chdir($conn_id, '..');
		return true;
	}
	else
	{
		return false;
	}
}

function parse_rawlist( $array )
{
    foreach($array as $curraw)
    {
        $struc = array();
        $current = preg_split("/[\s]+/",$curraw,9);

        $struc['perms']  = $current[0];
        $struc['number'] = $current[1];
        $struc['owner']  = $current[2];
        $struc['group']  = $current[3];
        $struc['size']  = $current[4];
        $struc['month']  = $current[5];
        $struc['day']    = $current[6];
        $struc['time']  = $current[7];
        $struc['name']  = $current[8];
	//$struc['name']  = $current[9];
        $struc['raw']  = $curraw;
        $structure[] = $struc;
    }
   return $structure;

}
function xdir($path)
{
	global $conn_id;
	//echo $conn_id.$path;
	ftp_pasv($conn_id, true);
	$contents = ftp_nlist($conn_id, '-a' . $path);
	$buff = ftp_rawlist($conn_id, '/');
	var_dump($buff);
	echo "<hr />";
	var_dump($contents);
	$structure = parse_rawlist($buff);
	print_r($structure);
	foreach($structure as $k=>$v)
	{
		$contents[] = $v['name'];
	}
	var_dump($contents);
	foreach($contents as $file)
	{
       

		if ($file!='.'&&$file!='..')
		{
			if (ftp_is_dir($file))
			{
				echo $file . ' Dossier - On passe<br>';
			}
			else
			{
				traiterMcto($conn_id, $file);

				$command1 = 'RNFR '.$file;
				$command2 = 'RNTO /olds/'.$file;
				print_r(ftp_raw($conn_id,$command1));
				print_r(ftp_raw($conn_id,$command2));

				echo $file . ' Fichier traite<br>';
			}
		}
	}
}

xdir('.');

echo ("<h1>Importation terminee</h1>");
echo "<br/><a href='javascript:window.close();'>Cliquez ici pour fermer la fenetre</a>";

function ftp_fetch($ftp_stream, $remote_file) {
	ob_end_flush();
	ob_start();
	$out = fopen('php://output', 'w');
	if (!ftp_fget($ftp_stream, $out, $remote_file, FTP_ASCII))
	{
		return false;
	}
	fclose($out);
	$data = ob_get_clean();
	return $data;
}

function traiterMcto ($conn_id, $file)
{
     //donnees pour  Mvep  option developpement photo
	$client_mvep		= array();
	$startCounter		= 0;
	$code_produit_mvep  = 0;
	$prix_produit_mvep  = 0;

	if($data = ftp_fetch($conn_id,$file))
	{

		unset($user);

		echo "Recuperation du ".$file."<br/>";
		$user['id_qts']= $file;
		//print_r($data);
		$xml = simplexml_load_string($data);
		//print_r($xml);
		$user['email']=strtolower((String)$xml->dossier->details_clients->detail_client->client_email);
		$user['civilite']= (String)$xml->dossier->details_clients->detail_client->client_titre;
		$user['nom']=strtoupper(secureString2((String)$xml->dossier->details_clients->detail_client->client_nom));
		$user['prenom']=secureString2(ucfirst(strtolower((String)$xml->dossier->details_clients->detail_client->client_prenom)));

		if($user['civilite'] == "Mr")
		{
			$user['sexe'] = "h";
		}
		else
		{

			$user['sexe'] = "f";
		}

		$user['adresse1'] .= secureString2((String)$xml->dossier->details_clients->detail_client->client_adr1)." ";
		$user['adresse1'] .= secureString2((String)$xml->dossier->details_clients->detail_client->client_adr2)." ";

		$user['ville'] = secureString2((String)$xml->dossier->details_clients->detail_client->client_ville);
		$user['cp'] = (String)$xml->dossier->details_clients->detail_client->client_cp;
		$user['pays'] = secureString2((String)$xml->dossier->details_clients->detail_client->client_pays);

		$user['tel1'] = str_replace(' ','',str_replace('-','',str_replace('.','',$xml->dossier->details_clients->detail_client->client_tel1)));
		$user['tel3'] = str_replace(' ','',str_replace('-','',str_replace('.','',$xml->dossier->details_clients->detail_client->client_tel2)));

		print_r($user);
		//
		//	//\USER
		//
		//	//DOSSIER
		//
		//
		//
		$dossier['id_pkg'] = (String)$xml->dossier->produit_principal->references_produit->interne_to->libelle;

		$dossier['total'] = ((String)$xml->dossier->montants_dossier->montant_brut / 100);

		$dossier['date'] = (String)$xml->entete->origine->date_emission->date_hms->date['annee'].'-'.(String)$xml->entete->origine->date_emission->date_hms->date['mois'].'-'.(String)$xml->entete->origine->date_emission->date_hms->date['jour'];


		//$donnes_import = Import_donnees_Orch($dossier['id_pkg']);

		$dossier['etat'] = "Import";
		$dest_pays = $xml->xpath('//dossier/voyage/pays_sejour/pays/*');
		$dossier['dest_pays'] = (String)$dest_pays[0]['value'];
		$dossier['dest_ville'] = secureString2((String)$xml->dossier->voyage->lieu_sejour->ville->iata['value']);

		$dest_hotel="";
		$dest_hotel = secureString2((String)$xml->dossier->produit_principal->nom_produit->libelle);
		$dossier['dest_hotel'] = secureString2(ucfirst(strtolower($dest_hotel))) ;

		$dossier['date_deb'] = (String)$xml->dossier->voyage->periode_voyage->periode->date_debut['annee'].'-'.(String)$xml->dossier->voyage->periode_voyage->periode->date_debut['mois'].'-'.(String)$xml->dossier->voyage->periode_voyage->periode->date_debut['jour'];

		$dossier['date_fin'] = (String)$xml->dossier->voyage->periode_voyage->periode->date_fin['annee'].'-'.(String)$xml->dossier->voyage->periode_voyage->periode->date_fin['mois'].'-'.(String)$xml->dossier->voyage->periode_voyage->periode->date_fin['jour'];

		$dossier['duree'] = diffDate($dossier['date_deb'], $dossier['date_fin']);

		//$dossier['cat_hot'] =$donnes_import[3];

		$dossier['adultes'] = (String)$xml->dossier->terrestres->terrestre->nombre_passagers->adultes;
		$dossier['enfants'] = (String)$xml->dossier->terrestres->terrestre->nombre_passagers->enfants;
		$dossier['bebes'] =  (String)$xml->dossier->terrestres->terrestre->nombre_passagers->bebes;


		$dossier['nb_supp_adulte'] = $dossier['adultes'];
		$dossier['nb_taxe_adulte'] = $dossier['adultes'];
		$dossier['nb_reduc_adulte'] = $dossier['adultes'];
		$dossier['nb_supp_enfant'] = $dossier['enfants'];
		$dossier['nb_taxe_enfant'] = $dossier['enfants'];
		$dossier['nb_reduc_enfant'] = $dossier['enfants'];
		$dossier['nb_supp_bebe'] = $dossier['bebes'];
		$dossier['nb_reduc_bebe'] = $dossier['bebes'];




		//Pension !
		//A rajouter

		$dossier['ville_depart'] = (String)$xml->dossier->voyage->depart_voyage->ville->iata['value'];
		$dossier['compris'] = $donnes_import[7];
		$dossier['to'] = $xml->entete->origine->emetteur->libelle;


		unset($type);
		$type=array();
		foreach($xml->dossier->terrestres->terrestre->hebergements->hebergement as $r)
		{
			$type[] = (String)$r->type_hebergement->libelle;
			$nb_type = (String)$r->nombre_prestations;
		}


		$type_count = array_count_values($type);

		$dossier['type_chambre'] = ucfirst(strtolower(key($type_count)));
		$dossier['nb_type_chambre'] = current($type_count);
		next($type_count);
		$dossier['type_chambre1'] = ucfirst(strtolower(key($type_count)));
		$dossier['nb_type_chambre1'] = current($type_count);

		foreach($xml->dossier->details_tarifs->detail_tarif as $line)
		{
			if ((String)$line->libelle_tarif->libelle == 'Annulation Early Booking') {
				$early_booking = true;
			}

		}


		$j=0;
		foreach($xml->dossier->passagers->passager as $passager)
		{
			//Passagers

			$dossier[$j]['civilite'] = secureString2((String)$passager->titre_personne["value"]);
			if($dossier[$j]['civilite'] == "MR")
			{
				$dossier[$j]['civilite'] = "Mr";
			}
			else if ($dossier[$j]['civilite'] == "MME")
			{
				$dossier[$j]['civilite'] = "Mme";
			}
			else
			{
				$dossier[$j]['civilite'] = "Melle";
			}

			$dossier[$j]['nom'] = secureString2((String)$passager->nom);
			$dossier[$j]['prenom'] = secureString2((String)$passager->prenom);


			//EN TEST
			//Moyen ou long courrier ?
			$link = connection(MYDATABASE);
			$requete_courrier = "SELECT courrier FROM code_pays WHERE cpays='".$dossier['dest_pays']."';";
			$result_courrier = mysql_query($requete_courrier) or die($requete_courrier.mysql_error().$requete_courrier);
			$row_courrier = mysql_fetch_assoc($result_courrier);


			if ($early_booking)
			{
				if($row_courrier['courrier'] == 'moyen')
				{
					$dossier[$j]['assur'] = "Annulation_moyen";
				}
				else
				{
					$dossier[$j]['assur'] = "Annulation_long";
				}
			}


			//\EN TEST


			//			if($row_courrier['courrier'] == 'moyen')
			//			{
			//				if ((String)$xml->bookingInfo->reservationInfo->package->options->insurance->choice["code"] == "assur-sans")
			//				{
			//					$dossier[$j]['assur'] = "Aucune";
			//				}
			//				else if ((String)$xml->bookingInfo->reservationInfo->package->options->insurance->choice["code"] == "assur-multirisque")
			//				{
			//					$dossier[$j]['assur'] = "Multirisques_moyen";
			//				}
			//				else
			//				{
			//					$dossier[$j]['assur'] = "Annulation_moyen";
			//				}
			//			}
			//			else
			//			{
			//				if ((String)$xml->bookingInfo->reservationInfo->package->options->insurance->choice["code"] == "assur-sans")
			//				{
			//					$dossier[$j]['assur'] = "Aucune";
			//				}
			//				else if ((String)$xml->bookingInfo->reservationInfo->package->options->insurance->choice["code"] == "assur-multirisque")
			//				{
			//					$dossier[$j]['assur'] = "Multirisques_long";
			//				}
			//				else
			//				{
			//					$dossier[$j]['assur'] = "Annulation_long";
			//				}
			//			}


			$j++;
		}






		unset($dossier['commentaires']);
		foreach($xml->dossier->details_tarifs->detail_tarif as $line)
		{
			$type = $line["type"];
			switch ($type)
			{

				//Prix de base
				case("PRX-BASE"):
					$dossier['prix_base_adulte'] = (String)$line->unitPrice["amount"];
					break;

					//Prix taxe aeroport
				case("PRX-TXA-PAR"):
					//Taxes a�roports
					$dossier['supp_divers'] = (String)$line->unitPrice["amount"];
					$dossier['nb_supp_divers'] = (String)$line["quantity"];
					break;


					//Prix de base b�b�
				case("PRX-BASE-BEBE"):
					$dossier['prix_base_bebe'] = (String)$line->unitPrice["amount"];

					break;

					//Frais de dossiers (fixes par defaut)
				case("PRX-FD"):

					break;

					//Prix r�duc enfant
				case("PRX-RED-1ENF"):
					$dossier['prix_base_enfant'] = ($dossier['prix_base_adulte'] - (String)$line->unitPrice["amount"]);
					break;

					//Prix 3e adulte
				case("PRX-RED-3ADU"):

					break;

					//Supp chambre indiv
				case("PRX-SUP-CHMB-IND"):
					$dossier['taxes_adulte'] = (String)$line->unitPrice["amount"];
					$dossier['nb_taxe_adulte'] = (String)$line["quantity"];
					break;

					//Supp chambre indiv
				case("RED"):
					//$dossier['taxes_adulte'] = (String)$line->unitPrice["amount"];
					//$dossier['nb_taxe_adulte'] = (String)$line["quantity"];
					require_once("fonctions_db.php");

					$sql = "INSERT INTO `codes_promos` (`libelle` , `montant` , `date` )
							VALUES (
							'".(String)$line->libelle_tarif->libelle."', '".((String)$line->montant_brut / 100)."', '".date("Y-m-d")."'
							);";
					$link = connection(MYDATABASE);
					$res = mysql_query($sql) or die ($sql.mysql_error());		
					break;
					
					//$line["quantity"]
					//$line->unitPrice["amount"]
					//$line->invoiceItem["code"]

					//Options complementaires developpement photo MVEP >> appel du webservice
				    case("OPT"):

                                  $libelle = (String)$line->libelle_tarif->libelle;

                                  //echo"<hr /> $libelle<hr />";

                                  
                                  if (eregi("^(.*)tirage (500|100) photos$", $libelle, $regs)   ) 
                                  {


                                        echo"<hr />ok option<hr />";

						require_once("webService_mvep.php");

						if($user['civilite'] == "Mr")
						{
							 $client_mvep['civilite'] = 1;
						}
						elseif($user['civilite'] == "Mme")
						{
							 $client_mvep['civilite'] = 2;
						}
						elseif($user['civilite'] == "Mlle")
						{
							 $client_mvep['civilite'] = 3;
						}
						
						$client_mvep['email'] 		= $user['email'];
						$client_mvep['nom'] 			= $user['nom'];
						$client_mvep['prenom'] 		= $user['prenom'];
						$client_mvep['cp'] 			= $user['cp'];
						$client_mvep['ville'] 		= $user['ville'];
						$client_mvep['tel'] 			= $user['tel1'];
					
						//numero de la rue
						if (eregi("^([0-9]*)(.*)", $user['adresse1'] , $regs)   ) 
						{   
   							$client_mvep['numero'] 	= $regs[1] ;
                                        	 $client_mvep['adresse1'] 	= $regs[2] ;
						} 
						//startCounter de devveloppment photo 
						if ((String)$line->code == '75100')
						{
					 	 $startCounter	=100;//nbr de tirage photo
						}
						elseif ((String)$line->code == '75500')
						{
					  	$startCounter	=500;//nbr de tirage photo			
						}
					
                                  
						$code_produit_mvep   = 75;
                                 		// $code_produit_mvep   = (int)$line->code;
						$prix_produit_mvep   = ((String)$line->montant_brut / 100);

						//creation du compte client sur Mvep

						createUserSav($client_mvep,$code_produit_mvep,$startCounter);

                                  		$option_mvep='OptionMvep;code:'.$code_produit_mvep.';prix:'.$prix_produit_mvep.'euro;qte:'.(int)$line->quantite."\n\r";

                                  		//echo $option_mvep;
                                  		$dossier['commentaires'] .= 'OptionMvep;code:'.$code_produit_mvep.';prix:'.$prix_produit_mvep.'euro;qte:'.(int)$line->quantite."\n\r";
				        }

					break;
				default:
					$dossier['commentaires'] .= (String)$line->libelle_tarif->libelle." : ";
					$dossier['commentaires'] .= ((String)$line->montant_brut / 100)." &euro; *";
					$dossier['commentaires'] .= (String)$line->quantite;
					$dossier['commentaires'] .= "\n\r";
			}
		}

		$dossier['commentaires'] .= "Total dossier : ".$dossier['total'];
		$dossier['commentaires'] .= "\n\r";
		$dossier['commentaires'] .= "ID QTS : ".$user['id_qts'];
		$dossier['commentaires'] .= "\n\r";

             // echo $dossier['commentaires'] ;

		foreach ($xml->dossier->details_informations->detail_information as $v)
		{
			if ($v->libelle == 'PAYED') {
				$total_paye = $v->detail / 100;
			}
		}

		$dossier['commentaires'] .= "TOTAL PAYE : ".$total_paye;

		$dossier['commentaires'] = human($dossier['commentaires']);

		$dossier['frais_dossiers'] = "15";





		//\DOSSIER




	}
	else
	{
		echo "Erreur dans le ftp_fetch";
	}


	//print_r($dossier);




	require_once("fonctions_db.php");


	echo ("<hr>");
	$requete_verif = "SELECT id FROM `id_qts` WHERE id_qts='".$user['id_qts']."';";
	echo $requete_verif;
	echo("<br/>");
	$link = connection(MYDATABASE);
	$result_verif = mysql_query($requete_verif) or die ($requete_verif.mysql_error());
	$num_rows_verif = mysql_num_rows($result_verif);
	if($num_rows_verif < 1)
	{

		$requete = "INSERT INTO `clients_import`
				(`civilite` , `nom` , `prenom` , `mail` , `sexe` , `date_nais` , `adresse1` , `adresse2` , `ville` , `cp` , `pays` , `tel1` , `tel2` , `tel3` , `fax` )
				 VALUES
				 (
				 '".secureString2($user['civilite'])."',
				 '".secureString2($user['nom'])."',
				 '".secureString2($user['prenom'])."',
				 '".secureString2($user['email'])."',
				 '".secureString2($user['sexe'])."',
				 '".secureString2($user['date_naiss'])."',
				 '".secureString2($user['adresse1'])."',
				 '".secureString2($user['adresse2'])."',
				 '".secureString2($user['ville'])."',
				 '".secureString2($user['cp'])."',
				 '".secureString2($user['pays'])."',
				 '".secureString2($user['tel1'])."',
				 '".secureString2($user['tel2'])."',
				 '".secureString2($user['tel3'])."',
				 '".secureString2($user['fax'])."');";
		echo $requete;
		$link = connection(MYDATABASE);
		$result = mysql_query($requete);
		print_r($result);
		echo("<br/>");
		$id_client = mysql_insert_id();
		//$id_client = 1;
		echo "ID CLIENT : ".$id_client;
		if ($id_client > 0)
		{
			$requete2 = "INSERT INTO `import_qts`
					(`id_qts` , `id_client`,`date`,`total` )
					 VALUES
					 ('".secureString2($user['id_qts'])."',
					 '".$id_client."','".secureString2($dossier['date'])."','".secureString2($dossier['total'])."');";
			echo $requete2;
			echo("<br/>");
			$result2 = mysql_query($requete2);
			print_r($result2);

			$requete2bis = "INSERT INTO `id_qts`
					(`id_qts`)
					 VALUES
					 ('".secureString2($user['id_qts'])."');";
			echo $requete2bis;
			echo("<br/>");
			$result2bis = mysql_query($requete2bis);
			print_r($result2bis);
			echo ("<hr>");

		}


		//A traiter dans les INSERT
		$dossier['derniere_modif'] = date("d/m/y \� H:i:s");
		$dossier['id_client'] = $id_client;
		if ($id_client > 0)
		{
			$requete3 = "
				INSERT INTO `dossiers_import`
				(`id_pkg` ,
				`id_client` ,
				`etat` ,
				`dest_pays` ,
				`dest_ville` ,
				`dest_hotel` ,
				`duree` ,
				`duree_autre` ,
				`cat_hot` ,
				`adultes` ,
				`enfants` ,
				`bebes` ,
				`pension` ,
				`date_deb` ,
				`date_fin` ,
				`prix_base_adulte` ,
				`prix_base_enfant` ,
				`prix_base_bebe` ,
				`suppl_adulte` ,
				`suppl_enfant` ,
				`suppl_bebe` ,
				`taxes_adulte` ,
				`taxes_enfant` ,
				`taxes_bebe` ,
				`frais_dossiers` ,
				`date_transac`,
				`nb_supp_adulte`,
				`nb_supp_enfant`,
				`nb_supp_bebe`,
				`nb_taxe_adulte`,
				`nb_taxe_enfant`,
				`nb_taxe_bebe`,
				`nb_reduc_adulte`,
				`nb_reduc_enfant`,
				`nb_reduc_bebe`,
				`supp_divers`,
				`nb_supp_divers`,
				`commentaires`,
				`reduc_adulte`,
				`reduc_enfant`,
				`reduc_bebe`,
				`ville_depart`,
				`compris`,
				`to`,
				`hausse_carb`,
				`nb_hausse_carb`,
				`commentaires_cli`,
				`type_chambre`,
				`supp_adulte_div`,
				`supp_enfant_div`,
				`supp_bebe_div`,
				`nb_supp_adulte_div`,
				`nb_supp_enfant_div`,
				`nb_supp_bebe_div`,
				`intitule_supp_adulte_div`,
				`intitule_supp_enfant_div`,
				`intitule_supp_bebe_div`,
				`hausse_carb_bebe`,
				`nb_hausse_carb_bebe`,
				`derniere_modif`,
				`date_confirm`,
				`type_chambre1`,
				`nb_type_chambre`,
				`nb_type_chambre1`
				)
				VALUES (
				'".$dossier['id_pkg']."',
				'".$dossier['id_client']."',
				'".$dossier['etat']."',
				'".$dossier['dest_pays']."',
				'".$dossier['dest_ville']."',
				'".$dossier['dest_hotel']."',
				'".$dossier['duree']."',
				'".$dossier['duree_autre']."',
				'".$dossier['cat_hot']."',
				'".$dossier['adultes']."',
				'".$dossier['enfants']."',
				'".$dossier['bebes']."',
				'".$dossier['pension']."',
				'".$dossier['date_deb']."',
				'".$dossier['date_fin']."',
				'".$dossier['prix_base_adulte']."',
				'".$dossier['prix_base_enfant']."',
				'".$dossier['prix_base_bebe']."',
				'".$dossier['supp_adulte']."',
				'".$dossier['supp_enfant']."',
				'".$dossier['supp_bebe']."',
				'".$dossier['taxes_adulte']."',
				'".$dossier['taxes_enfant']."',
				'".$dossier['taxes_bebe']."',
				'".$dossier['frais_dossiers']."',
				NOW(),
				'".$dossier['nb_supp_adulte']."',
				'".$dossier['nb_supp_enfant']."',
				'".$dossier['nb_supp_bebe']."',
				'".$dossier['nb_taxe_adulte']."',
				'".$dossier['nb_taxe_enfant']."',
				'".$dossier['nb_taxe_bebe']."',
				'".$dossier['nb_reduc_adulte']."',
				'".$dossier['nb_reduc_enfant']."',
				'".$dossier['nb_reduc_bebe']."',
				'".$dossier['supp_divers']."',
				'".$dossier['nb_supp_divers']."',
				'".addslashes(human($dossier['commentaires']))."',
				'".$dossier['reduc_adulte']."',
				'".$dossier['reduc_enfant']."',
				'".$dossier['reduc_bebe']."',
				'".$dossier['ville_depart']."',
				'".addslashes($dossier['compris'])."',
				'".addslashes($dossier['to'])."',
				'".$dossier['hausse_carb']."',
				'".$dossier['nb_hausse_carb']."',
				'".$dossier['commentaires_cli']."',
				'".$dossier['type_chambre']."',
				'".$dossier['supp_adulte_div']."',
				'".$dossier['supp_enfant_div']."',
				'".$dossier['supp_bebe_div']."',
				'".$dossier['nb_supp_adulte_div']."',
				'".$dossier['nb_supp_enfant_div']."',
				'".$dossier['nb_supp_bebe_div']."',
				'".$dossier['intitule_supp_adulte_div']."',
				'".$dossier['intitule_supp_enfant_div']."',
				'".$dossier['intitule_supp_bebe_div']."',
				'".$dossier['hausse_carb_bebe']."',
				'".$dossier['nb_hausse_carb_bebe']."',
				'".$dossier['derniere_modif']."',
				'".$dossier['date_confirm']."',
				'".$dossier['type_chambre1']."',
				'".$dossier['nb_type_chambre']."',
				'".$dossier['nb_type_chambre1']."'
				);";

			echo $requete3;
			echo("<br/>");
			$result3 = mysql_query($requete3) or die($requete3.mysql_error());
			echo("<br/>RESULTAT : ");
			print_r($result3);
		}
		$id_dossier = mysql_insert_id();

		//$id_dossier = 1;

		if ($id_dossier > 0 && $id_client > 0)
		{
			for($j=0;$j<count($dossier[$j]);$j++)
			{
				$civilite= $dossier[$j]['civilite'];
				$requete4 = "
						INSERT INTO `passagers_import` (
						`id_dossier` ,
						`civilite` ,
						`nom` ,
						`prenom` ,
						`age` ,
						`date_naiss` ,
						`assur`  ,
						`mail` )
						VALUES (
						'".$id_dossier."',
						'".$civilite."',
						'".$dossier[$j]['nom']."',
						'".$dossier[$j]['prenom']."',
						'".$dossier[$j]['age']."',
						'".$dossier[$j]['date_nais']."',
						'".$dossier[$j]['assur']."',
						'".$dossier[$j]['mail']."'
						);";

				echo $requete4;
				$result4 = mysql_query($requete4) or die($requete4.mysql_error());
				echo("<br/>RESULTAT : ");
				print_r($result4);

				echo("<br/><br/><br/>");
			}
		}
		echo ("<hr>");

	}

}


function secureString2($str)
{
	$str = trim($str);
	$str = stripslashes($str);
	$str = strip_tags($str);
	$str = htmlspecialchars($str);
	$str = htmlentities($str, ENT_NOQUOTES, 'utf-8');
	$str = html_entity_decode($str, ENT_NOQUOTES, 'iso-8859-1');
	$str = addslashes($str);
	return $str;
}

function get_urls($string, $strict=true)
{

	$types = array("href", "src", "url");
	while(list(,$type) = each($types)) {
		$innerT = $strict?'[a-z0-9:?=&@/._-]+?':'.+?';
		preg_match_all ("|$type\=([\"'`])(".$innerT.")\\1|i", $string, &$matches);
		$ret[$type] = $matches[2];
	}

	return $ret;
}



function diffDate($debut, $fin) {

	$tDeb = explode("-", $debut);
	$tFin = explode("-", $fin);

	$diff = mktime(0, 0, 0, $tFin[1], $tFin[2], $tFin[0]) -
	mktime(0, 0, 0, $tDeb[1], $tDeb[2], $tDeb[0]);

	return(($diff / 86400));

}

function human($msg)
{
	$msg = addslashes($msg);
	$msg = htmlentities($msg);
	$msg = str_replace("&Atilde;&copy;", "&eacute;", $msg);
	$msg = str_replace("&Atilde;&uml;", "&egrave;", $msg);
	$msg = str_replace("&Atilde;&ordf;", "&ecirc;", $msg);
	$msg = str_replace("&Atilde;&laquo;", "&euml;", $msg);
	$msg = str_replace("&Atilde;&nbsp;", "&agrave;", $msg);
	$msg = str_replace("&Atilde;&curren;", "&auml;", $msg);
	$msg = str_replace("&Atilde;&cent;", "&acirc;", $msg);
	$msg = str_replace("&Atilde;&sup1;", "&ugrave;", $msg);
	$msg = str_replace("&Atilde;&raquo;", "&ucirc;", $msg);
	$msg = str_replace("&Atilde;&frac14;", "&uuml;", $msg);
	$msg = str_replace("&Atilde;&acute;", "&ocirc;", $msg);
	$msg = str_replace("&Atilde;&para;", "&ouml;", $msg);
	$msg = str_replace("&Atilde;&reg;", "&icirc;", $msg);
	$msg = str_replace("&Atilde;&macr;", "&iuml;", $msg);
	$msg = str_replace("&Atilde;&sect;", "&ccedil;", $msg);
	$msg = str_replace("&amp;", "&", $msg);
	$msg = html_entity_decode($msg);
	return $msg;
}
?>