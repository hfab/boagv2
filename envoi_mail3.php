<?php
session_start();
if (!isset($_SESSION["id_vendeur"])) {
		   header("Location:index.php");
		   exit();
		}
		header('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Envoi de mails de retour - Monagence&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<link href="client.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
require_once("fonctions_db.php");
require_once("fonctions.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">
<center>
<?php

$jour = $_POST["jour"];
$mois = $_POST["mois"];
$annee = $_POST["annee"];

$jour_courant=date("d");
$mois_courant=date("m");
$annee_courante=date("y");

if (!empty($jour)) {$jour_courant = $jour;};
if (!empty($mois)) {$mois_courant = $mois;};
if (!empty($annee)) {$annee_courante = $annee;};

if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};


	echo "<p><form method='post' action='".$_SERVER['PHP_SELF']."?PHPSESSID=".session_id()."'><fieldset><legend>Choix de la période</legend>";
	echo ("<label for='jour'>Voir une autre période : </label>\n\r");
	echo ("<select name='jour' id='jour'>\n\r");
	for ($i=1;$i<=31;$i++) {
		if (strlen($i) < 2){$jour="0".$i;} else {$jour=$i;};
		echo ("<option value='".$jour."'");
		if ($jour == $jour_courant) {echo("selected='selected'");};
		echo (">".$jour."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='mois' id='mois'>\n\r");
	for ($i=1;$i<=12;$i++) {
		if (strlen($i) < 2){$mois="0".$i;} else {$mois=$i;};
		echo ("<option value='".$mois."'");
		if ($mois == $mois_courant) {echo("selected='selected'");};
		echo (">".$mois."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='annee' id='annee'>\n\r");
	for ($i=2005;$i<=$annee_courante;$i++) {
		echo ("<option value='".$i."'");
		if ($i == $annee_courante) {echo("selected='selected'");};
		echo (">".$i."</option>\n\r");
	}
	echo ("</select><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");
	echo "<input type='submit' name='Voir' id='Voir' value='>>Voir'></fieldset></form></p>";

	$annee_courante = substr($annee_courante,2,4);


$id_dossier=$_POST["id_dossier"];


$liste_mail=$_POST["liste_mail"];

$mail_vendeur=$_POST["mail_vendeur"];

$nom_vendeur=$_POST["nom_vendeur"];
$pays = $_POST["pays"];
$id_dossiers = $_POST["id_dossiers"];

$civilite= $_POST["civilite"];
$nom = $_POST["nom"];

//print_r($liste_mail);
//print_r($mail_vendeur);
//print_r($nom_vendeur);

$subject="voyaneo.com - Retour de voyage";



if (count($liste_mail)>0)
{
	for($i=0;$i<count($liste_mail);$i++)
	{
		$subject="voyaneo.com - Retour de voyage (Dossier n°".$id_dossiers[$i].")";
		$message = "<style>*{font-family:Verdana}</style><center><img src='http://boo.voyaneo.com/logo_voyaneo_small.jpg'></center><br/><br/>";
		$message .= "Bonjour ".$civilite[$i]." ".$nom[$i].",<br/><br/>\r\n";
		$message .= "Votre voyage ayant pris fin, je reviens vers vous en espérant que vous avez passé un bon séjour. Partagez dès à présent vos impressions, commentaires, photos sur notre page <a href='http://www.facebook.com/Agencedevoyagecom'>Facebook</a>.<br/>\r\n";
		// $message .= "N'hésitez pas à vous rendre sur le <a href='http://www.voyagemotion.com'>www.voyagemotion.com</a> afin de nous faire profiter de votre expérience au ".ucfirst(strtolower($pays[$i])).", au travers des photos ou videos que vous pourrez partager sur ce site.<br /><br/>\r\n";
		$message .= "Profitez du code promo NCBVGFHR5T  et bénéficiez  d'une réduction de 25% sur tout le site www.ouistipix.com afin de réaliser tous vos travaux photos : albums, tirages, personnalisation de souvenirs vacances... <br/>\r\n" ;
//$message .= "Vous pouvez également publier et partager vos photos et vos vidéos de vacances sur <a href='http://www.voyagemotion.com/'>VoyageMotion</a><br/><br/>\r\n";

	//	$message .= "Pour que vos vacances continuent, Monagence vous offre le d&eacute;veloppement de 25 photos (pour 50 achet&eacute;es) grace au code promo \"EB292\"  <a href='http://www.mesvacancesenphoto.com/'>Mes Vacances en photo</a><br/><br/>\r\n";
		//$message .= "Sachez aussi qu’en vous rendant sur le <a href='http://www.agencedevoyage.com/blogs/'>blog agencedevoyage.com</a>, vous pourrez vous créer et entretenir un mini site personnalisé de vos différents séjours à l’étranger.<br/><br/>\r\n";
		
		if ( substr($id_pkgs[$i], 0, 4) == "PKG-" )
		{
		//	$message .= "<center><strong>Dans le but d'améliorer nos prestations, nous vous proposons un questionnaire de satisfaction (cela ne vous prendra qu'un instant) :</strong><br/><a href='http://www.agencedevoyage.com/avis-clients/?id_dossier=".$id_dossiers[$i]."&id_pkg=".$id_pkgs[$i]."'><img src='http://www.agencedevoyage.com/images/monagence/questionnairesatisfaction.gif' border='0'/></a></center><br/><br/>\r\n";
		}
		
		$message .= "A très bientôt sur <a href='http://www.voyaneo.com/'>voyaneo.com</a> pour préparer vos prochaines vacances.<br/><br/>\r\n";
		$message .= "Cordialement,<br/><br/>\r\n";
		$message .= $nom_vendeur[$i]."<br/>\r\n";
		$message .= "Conseiller voyage<br/>\r\n";
		$message .= "http://www.voyaneo.com<br/>\r\n";
		$message .= "Tél : 01 77 69 02 57<br/>\r\n";
		$$message .= "Fax : 01 77 69 02 58 <br/>\r\n";
		$message .= $mail_vendeur[$i]."<br/>\r\n";
		
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		// Additional headers
		//$headers .= 'To: '.$liste_mail[$i]."\r\n";
		$headers .= 'From: '.$mail_vendeur[$i]. "\r\n";
		$mail=$liste_mail[$i];
		//$mail = "yveschaponic@gmail.com";

		if (mail($mail,$subject,$message,$headers, "-f".$mail_vendeur[$i]."") && mail($mail_vendeur[$i],$subject,$message,$headers))
		//if (mail($mail,$subject,$message,$headers, "-f".$mail_vendeur[$i].""))
		{
			echo("Mail bien envoyé à :".$mail." de la part de ".$mail_vendeur[$i]."<br/><br/>");
			$link = connection(MYDATABASE);
			$requete = "INSERT INTO `envoi_mail_retour` (`id_dossier` , `envoye` ) VALUES ('".$id_dossiers[$i]."', '1');";
			$result=mysql_query($requete) or die(mysql_error());			
		}
		//echo($liste_mail[$i]." - ".$subject." - ".$message." - ".$headers."<br/><br/>");
	}
}
	$mail = "julien@comptoirdesdeals.com";
	$headers .= 'To: '.$mail."\r\n";
	$headers .= 'From: '.$from. "\r\n";
	//mail($mail,$subject,$message,$headers);
	
	unset($liste_mail);
	unset($mail_vendeur);
	unset($nom_vendeur);
	unset($pays);
	unset($id_dossiers);
	unset($civilite);
	unset($nom);
	if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};
	echo("<h1>Envoi de mails de retour $jour_courant / $mois_courant / $annee_courante</h1>");
	$db = mysql_connect('localhost', 'root', 'ivescha1969');

		// on sélectionne la base
		mysql_select_db('voyagemotion',$db);
		
		mysql_query("SET NAMES 'UTF8'"); 
	$requete = "SELECT dossiers.id, dossiers.date_fin, dossiers.id_client, clients.nom, clients.mail, vendeurs.nom, vendeurs.mail, vendeurs.prenom, code_pays.lpays_fr, clients.civilite, dossiers.id_pkg  FROM `dossiers`, `clients`,`vendeurs`, `lien_dossier_vendeur`, `code_pays`  WHERE dossiers.dest_pays=code_pays.cpays AND dossiers.id=lien_dossier_vendeur.id_dossier AND lien_dossier_vendeur.id_vendeur=vendeurs.id AND clients.id=dossiers.id_client AND dossiers.etat LIKE 'Confirmé' AND SUBSTRING(dossiers.date_fin,9,2)=".$jour_courant." AND SUBSTRING(dossiers.date_fin,6,2)=".$mois_courant." AND SUBSTRING(dossiers.date_fin,1,4)=".$annee_courante." GROUP BY clients.mail";
	$result=mysql_query($requete) or die(mysql_error());

	
	$num_rows = mysql_num_rows($result);
	if ($num_rows > 0) {
		echo("<table border='1' cellpadding='2' cellspacing='0'>");
		echo("<tr>");
		echo("<td><b>ID dossier</b></td>");
		echo("<td><b>Date de retour</b></td>");
		echo("<td><b>Nom du client</b></td>");
		echo("<td><b>Accès au dossier</b></td>");
		echo("<td><b>Accès fiche client</b></td>");
		//echo("<td><b>Envoi</b></td>");
		echo("</tr>");
	}

	while($row = mysql_fetch_row($result))
	{
		
		$requete2 = "SELECT envoye FROM envoi_mail_retour WHERE id_dossier='".$row[0]."';";
		//echo $requete2;
		$result2=mysql_query($requete2) or die(mysql_error());
		$num_rows2 = mysql_num_rows($result2);	
		if($num_rows2>0){$bgcolor="#CCCCCC";} else {$bgcolor="#FFFFFF";};

		echo("<tr bgcolor='".$bgcolor."'>");
		echo("<td>".$row[0]."</td>");
		echo("<td>".$row[1]."</td>");
		echo("<td><a href='mailto:".$row[4]."'>".$row[3]."</a></td>");
		echo("<td><form  target='_blank' method='post' action='/scripts/gestion_clients/search_dossier.php?PHPSESSID=".session_id()."'><input type='hidden' value='".$row[0]."' name='id_dossier'><input type='hidden' value='modifier' name='flag'><input type='submit' class='submit' value='Dossier' style='width:60px;'></form></td>");
		echo("<td><form action='/scripts/gestion_clients/search_client.php?PHPSESSID=".session_id()."' target='_blank' name='recherche' id='recherche' method='post'><input type='hidden' name='id_client' id='id_client' value='".$row[2]."'><input type='hidden' name='flag' id='flag' value='modifier'><input type='submit' class='submit' name='Client' id='Client' value='Client' style='width:60px;'></form></td>");
		echo("</tr>");
		
		$liste_mail[] = $row[4];
		$mail_vendeur[]=$row[6];
		$nom_vendeur[]=$row[7]." ".$row[5];
		$pays[] = $row[8];
		$id_dossiers[] = $row[0];
		$nom[]=ucfirst(strtolower($row[3]));
		$civilite[]=$row[9];
		$id_pkgs[]=$row[10];
	}



	if ($num_rows > 0) {
		echo("</table>");
	}

//echo $num_rows2;


if($num_rows2==0){
echo ("<form action='".$PHP_SELF."' method='post'>");
for($i=0;$i<count($liste_mail);$i++)
{
	echo("<input type='hidden' name='liste_mail[]' value='".$liste_mail[$i]."'>\n\r");
	echo("<input type='hidden' name='nom_vendeur[]' value='".$nom_vendeur[$i]."'>\n\r");
	echo("<input type='hidden' name='mail_vendeur[]' value='".$mail_vendeur[$i]."'>\n\r");
	echo("<input type='hidden' name='pays[]' value='".$pays[$i]."'>\n\r");
	echo("<input type='hidden' name='id_dossiers[]' value='".$id_dossiers[$i]."'>\n\r");
	echo("<input type='hidden' name='civilite[]' value='".$civilite[$i]."'>\n\r");
	echo("<input type='hidden' name='nom[]' value='".$nom[$i]."'>\n\r");
}
echo("<input type='submit' value='Envoyer les mails'>");
echo("</form>");
}

function diff_date($jour , $mois , $an , $jour2 , $mois2 , $an2){  
 
$date = mktime(0, 0, 0, $mois, $jour, $an);  
$date2 = mktime(0, 0, 0, $mois2, $jour2, $an2);  
     
$diff = floor(($date - $date2) / (3600 * 24));  
return $diff;
}


	
?>
</center>
</td>
</tr>
</table>

</body></html>

