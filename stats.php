<?php
session_start();
if (!isset($_SESSION["id_vendeur"])) {
		   header("Location:index.php");
		   exit();
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Statistiques Monagence&copy;</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="client.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<table class="generale">
<tr>
<td width="120" valign="top" class="menu">
<?php
include("menu.php");
require_once("fonctions_db.php");
require_once("fonctions.php");
?>
</td>
<td width="680" style="border-left:1px solid black;padding:5px">
<?php

$mois = $_POST["mois"];
$annee = $_POST["annee"];

$mois_courant=date("m");
$annee_courante=date("y");

if (!empty($mois)) {$mois_courant = $mois;};
if (!empty($annee)) {$annee_courante = $annee;};

if (strlen($annee_courante) < 3){$annee_courante="20".$annee_courante;};

echo "<h1>Visualisation des stats pour le mois de : ".$mois_courant." / ".$annee_courante."</h1>";
echo "<p><form method='post' action='stats.php?PHPSESSID=".session_id()."'><fieldset><legend>Choix de la période</legend>";
	
	echo ("<label for='mois'>Voir une autre période : </label>\n\r");
	echo ("<select name='mois' id='mois'>\n\r");
	for ($i=1;$i<=12;$i++) {
		if (strlen($i) < 2){$mois="0".$i;} else {$mois=$i;};
		echo ("<option value='".$mois."'");
		if ($mois == $mois_courant) {echo("selected='selected'");};
		echo (">".$mois."</option>\n\r");
	}
	echo ("</select>\n\r");
	echo ("<select name='annee' id='annee'>\n\r");
	for ($i=2005;$i<=$annee_courante;$i++) {
		echo ("<option value='".$i."'");
		if ($i == $annee_courante) {echo("selected='selected'");};
		echo (">".$i."</option>\n\r");
	}
	echo ("</select><br/>\n\r");
	echo ("<br style='clear:both'/>\n\r");
echo "<input type='submit' name='Voir' id='Voir' value='Voir'></fieldset></form></p>";

$annee_courante = substr($annee_courante,2,4);
//echo $annee_courante;

function nombres_ventes_par_to($mois_courant, $annee_courante) {
		$link = connection(MYDATABASE);
		$requete = "SELECT `to`, COUNT(*) AS count FROM `dossiers` WHERE (`etat`='Confirmé' OR `etat`='Préconfirmé') AND SUBSTRING(`date_confirm`,6,7)=".$mois_courant." AND SUBSTRING(`date_confirm`,3,4)=".$annee_courante." GROUP BY `to` ORDER BY count DESC;";
		$result=mysql_query($requete) or die(mysql_error());
		while($row = mysql_fetch_row($result))
		{
			unset($pays);
				$requete2 = "SELECT `id`, lpays_fr FROM `dossiers`, code_pays WHERE dossiers.dest_pays  = code_pays.cpays AND `to`='".$row[0]."' AND (`etat`='Confirmé' OR `etat`='Préconfirmé') AND SUBSTRING(`date_confirm`,6,7)=".$mois_courant." AND SUBSTRING(`date_confirm`,3,4)=".$annee_courante.";";
				$result2=mysql_query($requete2) or die(mysql_error());
				$total_to = 0;
				while($row2 = mysql_fetch_row($result2))
				{
					$total_to += Calcul_total_dossier($row2[0]);
					$pays[] = $row2[1];
				}
				
				echo "<b>".$row[0]."</b> - ".$row[1]." dossiers - (".number_format($total_to,2,',',' ')." )<br/>";	
				$pays = array_count_values($pays);
				foreach($pays as $key=>$value)
				{
					echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".ucfirst(strtolower($key))." : ".$value."<br/>";
				}
				//print_r($pays);
				echo "<br/>";
		}
}

function nb_dossier_par_vendeur($id, $mois_courant, $annee_courante) {
	$link = connection(MYDATABASE);
	$requete = "SELECT id_dossier FROM `lien_dossier_vendeur` WHERE `id_vendeur`='".$id."';";
	//echo $requete;
	$result=mysql_query($requete) or die(mysql_error());
	$compteur_conf = 0;
	$total_vendeur = 0;
	$compteur_total = 0;
	$total_assur = 0;
	unset($assur);
	$pays=array();
	$assur=array();
	$to=array();
	
	//Date_transac : date de creation du dossier.
	
	while($row = mysql_fetch_row($result))
	{
		$link = connection(MYDATABASE);
		$requete2 = "SELECT * FROM `dossiers` WHERE `id`='".$row[0]."' AND (etat='Confirmé' OR etat='En request' OR etat='Préconfirmé') AND SUBSTRING(`date_confirm`,6,7)=".$mois_courant." AND SUBSTRING(`date_confirm`,3,4)=".$annee_courante.";";
		//echo $requete;
		$result2=mysql_query($requete2) or die(mysql_error());
		$num_rows = mysql_num_rows($result2);
		if ($num_rows > 0) 
		{
			$compteur_conf++;
			$total_vendeur += Calcul_total_dossier($row[0]);
			
			while($row2 = mysql_fetch_row($result2))
			{
				$pays[] = $row2[4];
				$to[] = $row2[47];
				$link = connection(MYDATABASE);
				//$requete4 = "SELECT `nom` FROM `assurances` WHERE `nom`='".$row2[14]."';";
				//echo $requete4;
				//$result4=mysql_query($requete4) or die(mysql_error());
				//$row4 = mysql_fetch_row($result4);
				//$assur[] = $row4[0];
			}
		$link = connection(MYDATABASE);
		$requete4 = "SELECT `assur`, id_dossier FROM `passagers` WHERE `id_dossier`='".$row[0]."';";
		//echo $requete4;
		$result4=mysql_query($requete4) or die(mysql_error());
		while($row4 = mysql_fetch_row($result4))
			{
				//print_r($row4);
				if(strlen($row4[0]) > 2)
				{
					$assur[] = $row4[0];
				}
				else
				{
					$assur[] = 'Aucune';
				}
			}
		}

		$link = connection(MYDATABASE);
		$requete3 = "SELECT * FROM `dossiers` WHERE `id`='".$row[0]."' AND SUBSTRING(`date_transac`,6,7)=".$mois_courant." AND SUBSTRING(`date_transac`,3,4)=".$annee_courante.";";
		//echo $requete3;
		$result3=mysql_query($requete3) or die(mysql_error());
		$num_rows3 = mysql_num_rows($result3);
		if ($num_rows3 > 0) 
		{
			$compteur_total++;
		}

		
	}
			$repartition_pays = array_count_values($pays);
			//echo $compteur_total;
			$total_assur = array_count_values($assur);
			$to = array_count_values($to);
			$retour=array($compteur_conf, $total_vendeur, $repartition_pays, $compteur_total, $total_assur, $to);
		return $retour;
}

	echo("<h1>Nombre de dossiers par TO</h1>");	
	nombres_ventes_par_to($mois_courant, $annee_courante);
	
	
	//echo("<hr>");
	$total_dossiers = 0;
	$compteur = 0;
	$link = connection(MYDATABASE);
	$requete = "SELECT id FROM `dossiers` WHERE  SUBSTRING(`date_confirm`,6,7)=".$mois_courant." AND SUBSTRING(`date_confirm`,3,4)=".$annee_courante."  GROUP BY dossiers.id;";
	//echo $requete;
	$result=mysql_query($requete) or die(mysql_error());
  while($row = mysql_fetch_row($result))
	{
		$total_courant = Calcul_total_dossier($row[0]);
		$total_dossiers += $total_courant;
		$compteur++;
	}
	
	echo "<hr><h1>Chiffre d'affaire total du mois :</h1> ".number_format($total_dossiers,2,',',' ')." ";
	if($compteur > 0)
	{
		echo "<hr><h1>Panier moyen ($compteur dossiers confirmés) :</h1> ".number_format($total_dossiers / $compteur,2,',',' ')." ";
	}
	
	//Stats PACK uniquement
	$total_dossiers_pack = 0;
	$compteur_pack = 0;
	$link = connection(MYDATABASE);
	$requete_pack = "SELECT id FROM `dossiers` WHERE  SUBSTRING(`date_confirm`,6,7)=".$mois_courant." AND SUBSTRING(`date_confirm`,3,4)=".$annee_courante." AND id_pkg != 'Vol Sec' AND id_pkg != 'Train' GROUP BY dossiers.id;";
	//echo $requete;
	$result_pack=mysql_query($requete_pack) or die(mysql_error());
  while($row_pack = mysql_fetch_row($result_pack))
	{
		$total_courant_pack = Calcul_total_dossier($row_pack[0]);
		$total_dossiers_pack += $total_courant_pack;
		$compteur_pack++;
	}
	
	echo "<hr><h1>Chiffre d'affaire total du mois (PACKS UNIQUEMENT) :</h1> ".number_format($total_dossiers_pack,2,',',' ')." ";
	if($compteur_pack > 0)
	{
		echo "<hr><h1>Panier moyen (PACKS UNIQUEMENT) ($compteur_pack dossiers confirmés) :</h1> ".number_format($total_dossiers_pack / $compteur_pack,2,',',' ')." ";
	}
	
	//\STATS PACK
	//Début Partie nb ventes et ca par vendeur
	echo("<hr><h1>Vendeurs</h1>");
	$link = connection(MYDATABASE);
	//$requete = "SELECT * FROM `vendeurs`;";
	$requete = "SELECT lien_dossier_vendeur.id_vendeur, COUNT(dossiers.id) AS count FROM lien_dossier_vendeur, dossiers WHERE dossiers.id=lien_dossier_vendeur.id_dossier AND (dossiers.etat='Confirmé' OR dossiers.etat='En request' OR etat='Préconfirmé') GROUP BY lien_dossier_vendeur.id_vendeur ORDER BY count DESC;";
	//echo $requete;
	$result=mysql_query($requete) or die(mysql_error());
	$rang=0;
  while($row = mysql_fetch_row($result))
	{
			//echo $row[4]." ".$row[3]." : ";
			//echo $nb_dossier_par_vendeur."<br/>";
		$retour = nb_dossier_par_vendeur($row[0], $mois_courant, $annee_courante);
		$nb_dossier_par_vendeur = $retour[0];
		$total_ca_par_vendeur = $retour[1];
		$repartition_pays = $retour[2];
		$compteur_total = $retour[3];
		$total_assur = $retour[4];
		$tour_op = $retour[5];
		
		
		$link = connection(MYDATABASE);
		$requete2 = "SELECT `nom`,`prenom` FROM `vendeurs` WHERE `id`='".$row[0]."';";
		$result2=mysql_query($requete2) or die(mysql_error());
		while($row2 = mysql_fetch_row($result2))
		{
			$vendeur=$row2[1]." ".$row2[0];
		}
		
		
		//print_r($repartition_pays);
		if($nb_dossier_par_vendeur > 0) {
			echo "<center><h2>".$vendeur." : </h2></center><br/>";
			echo $nb_dossier_par_vendeur." dossier(s) confirmé(s) <br/>(Chiffre d'affaire : ".number_format($total_ca_par_vendeur,2,',',' ')."  / Panier moyen : ".number_format($total_ca_par_vendeur / $nb_dossier_par_vendeur,2,',',' ')." )<br/>";			
			//echo("Total d'assurances vendues : ".$total_assur." <br/>");
			
			echo("<p><b>Répartition des assurances : </b></p>");
			foreach ($total_assur as $key=>$value)
			{
				echo $key." : ".$value."<br/>";
			}
			
			
			arsort($repartition_pays);
			echo("<p><b>Répartition par pays : </b></p>");
			foreach ($repartition_pays as $key=>$value)
			{
				$link = connection(MYDATABASE);
				$requete2 = "SELECT lpays_fr FROM `code_pays` WHERE cpays='".$key."';";
				//echo $requete2;
				$result2=mysql_query($requete2) or die(mysql_error());
				$row2 = mysql_fetch_row($result2);
				echo $row2[0]." : ".$value." dossier(s)<br/>";
			}
			
			arsort($tour_op);
			echo("<p><b>Répartition par TO : </b></p>");
			foreach ($tour_op as $key=>$value)
			{
				if ($key == ""){$key = "Pas de TO indiqué";};
				echo $key." : ".$value."<br/>";
			}
			
			echo("<br/><hr>");
		}

	}
//Fin Partie nb ventes et ca par vendeur


//Début - Répartition par pays et par vendeurs
	echo("<hr><h1>Répartition par pays</h1>");
	$link = connection(MYDATABASE);
	$requete = "SELECT COUNT(*) AS count, dest_pays FROM `dossiers` WHERE (`etat`='Confirmé' OR etat='Préconfirmé') AND SUBSTRING(`date_confirm`,6,7)=".$mois_courant." AND SUBSTRING(`date_confirm`,3,4)=".$annee_courante." GROUP BY dest_pays ORDER BY count DESC;";
	$result=mysql_query($requete) or die(mysql_error());
  while($row = mysql_fetch_row($result))
	{
		$link = connection(MYDATABASE);
		$requete2 = "SELECT * FROM code_pays WHERE cpays='".$row[1]."';";
		//echo $requete;
		$result2=mysql_query($requete2) or die(mysql_error());
		$row2 = mysql_fetch_row($result2);
		$num_rows = mysql_num_rows($result2);
		if($num_rows > 0)
		{
			echo "- ".$row2[2]." : ".$row[0]."<br/>";
		}
		
	}
//Fin - Répartition par pays et par vendeurs


//Début - Répartition provenances
	echo("<hr><h1>Répartition des provenances des clients</h1>");
	$link = connection(MYDATABASE);
	$requete = "SELECT `connu_par`, COUNT(id) AS count FROM `clients` WHERE YEAR(clients.date_inscription) ='20".$annee_courante."' AND MONTH(clients.date_inscription) ='".$mois_courant."' GROUP BY `connu_par` ORDER BY count DESC;";
	//echo $requete;
	$result=mysql_query($requete) or die(mysql_error());
	if (mysql_num_rows($result) > 0)
	{
	  while($row = mysql_fetch_row($result))
		{
			//print_r($row);
			$link = connection(MYDATABASE);
			$requete2 = "SELECT nom FROM referrer WHERE id='".$row[0]."'";
			//echo $requete;
			$result2=mysql_query($requete2) or die(mysql_error());
			$row2 = mysql_fetch_row($result2);
			if($row2[0] != "")
			{
				echo "- ".$row2[0]." : ".$row[1]."<br/>";
			}
		}
	}
	else
	{
		echo "Pas encore de nouveaux clients...";
	}
//Début - Répartition provenances dossiers
	echo("<hr><h1>Répartition des provenances des dossiers</h1>");
	$link = connection(MYDATABASE);
	
	$requete = "SELECT COUNT(id) AS count FROM `dossiers` WHERE YEAR(dossiers.date_transac) ='20".$annee_courante."' AND MONTH(dossiers.date_transac) ='".$mois_courant."';";
	//echo $requete;
	$result=mysql_query($requete) or die(mysql_error());
	if (mysql_num_rows($result) > 0)
	{
	  while($row = mysql_fetch_row($result))
		{
				echo "- Total de dossiers traités sur le mois : ".$row[0]."<br/>";
		}
	}

	$requete = "SELECT `provenance`, COUNT(id) AS count FROM `dossiers` WHERE YEAR(dossiers.date_transac) ='20".$annee_courante."' AND MONTH(dossiers.date_transac) ='".$mois_courant."' GROUP BY `provenance` ORDER BY count DESC;";
	//echo $requete;
	$result=mysql_query($requete) or die(mysql_error());
	if (mysql_num_rows($result) > 0)
	{
	  while($row = mysql_fetch_row($result))
		{
				echo "- ".$row[0]." : ".$row[1]."<br/>";
		}
	}
	
	echo("<hr><h1>Répartition des villes de départ</h1>");

	$requete = "
	SELECT 
	COUNT(id) AS count, 
	ville_depart 
	FROM 
	dossiers
	WHERE 
	MONTH(date_confirm) = '".$mois_courant."'
	AND
	YEAR(date_confirm) = '20".$annee_courante."'
	GROUP BY 
	ville_depart 
	ORDER BY count DESC, ville_depart ASC;
	";
	//echo $requete;
	$result=mysql_query($requete) or die(mysql_error());
	 while($row = mysql_fetch_row($result))
		{
			echo "- ".$row[0]." : ".$row[1]."<br/>";
		}

?>
</td>
</tr>
</table>

</body></html>

